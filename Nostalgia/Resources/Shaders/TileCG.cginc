float2 AnimateUV(float2 uv,float2 animation,float fps)
{
	float dummy = 0.0f;
	float count = floor( modf( floor( _Time.y*fps ) / animation.x,dummy ) * animation.x );
				
	uv.x += count * animation.y;

	return uv;
}