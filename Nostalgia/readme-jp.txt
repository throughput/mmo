﻿-----------------------------------------------------
            Nostalgia: 2D Tile Map Editor
          Copyright (c) 2014 Cait Sith Ware
          http://caitsithware.com/wordpress/
          support@caitsithware.com
-----------------------------------------------------

Nostalgiaを購入いただきありがとうございます！

【主な使用方法】

1. TileSetアセット作成。
   メニューから「Assets > Create > Nostalgia > TileSet」をクリック。
2. GameObjectにNostalgia/Mapをアタッチ。
3. MapにTileSetを設定。
4. Sceneビューをドラッグしてタイルを配置。

【サンプル】

サンプルは以下フォルダにあります。
Assets/Nostalgia/Examples/

【スクリプトリファレンス】

スクリプトリファレンスは以下にあります。
Assets/Nostalgia/Docs/

zipファイルを解凍し、index.htmlをブラウザで開いてください。

【ドキュメント】

詳しいドキュメントはこちらをご覧ください。
http://caitsithware.com/wordpress/assetstore/nostalgia/

【サポート】

メール : support@caitsithware.com
掲示板 : http://caitsithware.com/wordpress/assetstore/nostalgia/forum

【更新履歴】

Ver 1.2.0f1:
- Unity 4.6のエディタ上で実行中にNullReferenceExceptionが出力されるのを修正。

Ver 1.2.0:
- 頂点カラーのペイントツール追加。
- Sorting Layerウィンドウ追加。
- 横スクロールアクションのサンプルシーン更新。
- Map編集中にシーンビューのMapのない方向にカメラを向かせるとエラーが表示されるのを修正。

Ver 1.1.0:
- タイルにTileComponentを追加できるように対応。
- Shiftキーで配置できるパーツの状態を設定できるように対応。
- Rendererの描画順を設定するためのSortingLayerコンポーネント追加。
- HierarychyにGameObjectのアクティブ切り替えトグルを表示。
- ひとつのGameObjectに対してひとつのMapのみアタッチできるように変更。
- 横スクロールアクションのサンプルシーンを用意。
- 他のタイルを上書き配置してもColliderの設定が変更されなかったのを修正。

Ver 1.0.4:
- マテリアル色の指定対応。
- スクリーンサイズによってタイルの間に隙間が見えるのを修正。
- Sorting LayerおよびOrder in Layerを後から変更しても反映されなかったのを修正。

Ver 1.0.3:
- スクリプトリファレンス用コメント埋め込み。
- 「Create Other > Nostalgia Map」のスペルミス修正。

Ver 1.0.2:
- FIX: Textureを選択して「TileSet from selected Texture or Material」をするとエラーが出て作成できないのを修正。
- FIX: タイルを上塗りする際にColliderが正しく生成や削除されないのを修正。

Ver 1.0.1:
- FIX: エラーが出てビルドできないのを修正。
- FIX: ExampleシーンのMain Cameraが無効になっているのを修正。

Ver 1.0:
- リリース