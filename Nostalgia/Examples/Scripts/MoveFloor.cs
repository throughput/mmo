﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class MoveFloor : MonoBehaviour 
	{
		public Vector2 speed = new Vector2( 0.0f,-5.0f );
		public Bounds bounds = new Bounds( Vector3.zero,new Vector3( 50.0f,50.0f ) );

		Bounds _CheckArea;

		void Awake()
		{
			_CheckArea = new Bounds( bounds.center + transform.position,bounds.size );
		}
		
		// Update is called once per frame
		void Update () 
		{
			Vector2 pos = transform.position;
			pos += speed * Time.deltaTime;
			transform.position = pos;

			if( !_CheckArea.Contains( pos ) )
			{
				Destroy ( gameObject );
			}
		}
	}
}