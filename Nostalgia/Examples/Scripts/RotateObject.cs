﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class RotateObject : MonoBehaviour 
	{
		public float speed = 10.0f;

		// Update is called once per frame
		void Update () 
		{
			Quaternion rotate = Quaternion.AngleAxis( speed * Time.deltaTime,Vector3.forward );

			Quaternion localRotation = transform.localRotation * rotate;
			transform.localRotation = localRotation;		
		}
	}
}