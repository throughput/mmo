﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class Emitter : MonoBehaviour 
	{
		public GameObject prefab;
		public float interval = 2.0f;

		// Use this for initialization
		IEnumerator Start () 
		{
			if( prefab == null )
			{
				yield break;
			}

			while( true )
			{
				Instantiate( prefab,transform.position,Quaternion.identity );
				yield return new WaitForSeconds( interval );
			}		
		}
	}
}