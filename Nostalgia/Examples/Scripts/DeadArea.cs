﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class DeadArea : MonoBehaviour 
	{
		void OnTriggerEnter2D( Collider2D other )
		{
			if( other.tag == "Player" )
			{
				other.SendMessage( "Dead" );
			}
		}
	}
}