﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class DeadParticle : MonoBehaviour
	{
		public float waitTime = 3.0f;
		public AudioClip _Clip;

		// Use this for initialization
		IEnumerator Start () 
		{
			if( _Clip != null )
			{
				AudioSource.PlayClipAtPoint( _Clip,transform.position );
			}

			yield return new WaitForSeconds( waitTime );
			Application.LoadLevel( Application.loadedLevel );
		}
	}
}