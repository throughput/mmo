﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class CameraController : MonoBehaviour
	{
		public Transform target;
		public Map map;

		public Vector2 captureArea = new Vector2( 0.0f,0.5f );

		void LateUpdate()
		{
			if( target == null )
			{
				return;
			}

			Vector2 extents = new Vector2( GetComponent<Camera>().aspect * GetComponent<Camera>().orthographicSize,GetComponent<Camera>().orthographicSize );

			Vector2 captureExtents = Vector2.Scale(extents,captureArea);

			Vector3 pos = transform.position;

			float x = target.position.x - pos.x;

			if( x >= captureExtents.x )
			{
				pos.x += (x-captureExtents.x);
			}
			else if( x <= -captureExtents.x )
			{
				pos.x += (x+captureExtents.x);
			}

			float y = target.position.y - pos.y;

			if( y >= captureExtents.y )
			{
				pos.y += (y-captureExtents.y);
			}
			else if( y <= -captureExtents.y )
			{
				pos.y += (y+captureExtents.y );
			}

			Vector2 mapPos = map.transform.position;

			Vector2 minArea = mapPos + extents;
			Vector2 maxArea = mapPos + new Vector2( map.width,map.height ) - extents;

			pos.x = Mathf.Clamp( pos.x,minArea.x,maxArea.x );
			pos.y = Mathf.Clamp( pos.y,minArea.y,maxArea.y );

			transform.position = pos;
		}
	}
}