﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Nostalgia.Example
{
	[AddComponentMenu("Nostalgia/Example/BrokenBlockController")]
	public class BrokenBlockController : MonoBehaviour
	{
		public float lifeTime = 3.0f;
		public Vector2 force = new Vector2(250.0f, 1000.0f);
		public AudioClip clip;
		
		void Start()
		{
			if( clip != null )
			{
				AudioSource.PlayClipAtPoint( clip,transform.position );
			}

			foreach( Rigidbody2D r in GetComponentsInChildren<Rigidbody2D>() )
			{
				r.AddForce( new Vector2(Mathf.Sign(r.transform.localPosition.x) * force.x , force.y + 100.0f * r.transform.localPosition.y ) );
			}

			Destroy(gameObject, lifeTime);
		}
	}
}