﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	[AddTileMenu("Example/Breakable")]
	public class BreakableTile : TileComponent
	{
		public GameObject[] spawnObjects;
	}
}