﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class Title : MonoBehaviour 
	{
		public Player _Player;
		public GameObject _InGameUI;
		public GUIText _RecordText;

		private static readonly string _RecordTimePrefsKey = "RecordTime";

		void Start()
		{
			if( PlayerPrefs.HasKey( _RecordTimePrefsKey ) )
			{
				float recordTime = PlayerPrefs.GetFloat( _RecordTimePrefsKey );
				_RecordText.text = recordTime.ToString( "F3" );
			}
			else
			{
				_RecordText.text = "----";
			}
		}

		// Update is called once per frame
		void Update () 
		{
			if( Input.GetKeyDown( KeyCode.Space ) )
			{
				_Player.enabled = true;
				_InGameUI.SetActive( true );
				Destroy( gameObject );
			}		
		}
	}
}