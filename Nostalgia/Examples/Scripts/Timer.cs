﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class Timer : MonoBehaviour
	{
		[SerializeField] private GUIText _SecText;
		[SerializeField] private GUIText _MilliSecText;
		[SerializeField] private GameObject _NewRecort;

		private float _Timer = 0.0f;
		public float timer
		{
			get
			{
				return _Timer;
			}
		}
		private bool _IsUpdate = true;

		private static readonly string _RecordTimePrefsKey = "RecordTime";

		public void OnGoal()
		{
			_IsUpdate = false;

			if( !PlayerPrefs.HasKey( _RecordTimePrefsKey ) || PlayerPrefs.GetFloat( _RecordTimePrefsKey ) > _Timer )
			{
				PlayerPrefs.SetFloat( _RecordTimePrefsKey,_Timer );
				_NewRecort.SetActive( true );
			}
		}

		void Update () 
		{
			if( !_IsUpdate )
			{
				return;
			}

			_Timer += Time.deltaTime;

			int sec = Mathf.FloorToInt(_Timer);

			_SecText.text = sec.ToString();
			_MilliSecText.text = Mathf.FloorToInt((_Timer-sec)*1000.0f).ToString("000");
		}
	}
}