﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	[AddComponentMenu("Nostalgia/Example/Player")]
	public class Player : MonoBehaviour 
	{
		public float maxSpeed = 10.0f;
		public float jumpPower = 1000.0f;
		public GameObject deadParticle;
		public AudioClip jumpClip;
		public AudioClip goalClip;

		Animator _Animator;
		BoxCollider2D _BoxCollider;
		Rigidbody2D _Rigidbody;
		AudioSource _AudioSource;
		bool _IsGround = false;
		bool _IsGoal = false;

		Vector2 _Velocity = Vector2.zero;

		Transform _GroundObject;
		Vector2 _PreGroundPos;
		Vector2 _GroundLocalPos;

		static readonly int s_HorizontalID = Animator.StringToHash( "Horizontal" );
		static readonly int s_VerticalID = Animator.StringToHash( "Vertical" );
		static readonly int s_isGroundID = Animator.StringToHash( "isGround" );
		static readonly int s_JumpID = Animator.StringToHash( "Jump" );

		void Awake()
		{
			_Animator = GetComponent<Animator>();
			_BoxCollider = GetComponent<BoxCollider2D>();
			_Rigidbody = GetComponent<Rigidbody2D>();
			_AudioSource = GetComponent<AudioSource>();
		}

		void Update () 
		{
			if( !_IsGoal )
			{
				float x = Input.GetAxis( "Horizontal" );

				if( Mathf.Abs( x ) > 0.01f )
				{
					transform.localRotation = Quaternion.Euler( 0.0f,Mathf.Sign( x ) == 1 ? 180 : 0,0.0f );

					_Velocity = _Rigidbody.velocity;
					float speed = x * maxSpeed;
					if( !_IsGround || Mathf.Abs( _Velocity.x ) <= Mathf.Abs( speed ) )
					{
						_Velocity.x = speed;
						_Rigidbody.velocity = _Velocity;
					}
				}

				if( Input.GetButtonDown( "Jump" ) && _IsGround )
				{
					_AudioSource.clip = jumpClip;
					_AudioSource.Play();

					_Animator.SetTrigger( s_JumpID );

					_Rigidbody.AddForce( Vector2.up * jumpPower );
				}

				_Animator.SetFloat( s_HorizontalID,x );
			}
			else
			{
				_Animator.SetFloat( s_HorizontalID,0 );
			}

			_Animator.SetFloat( s_VerticalID,_Rigidbody.velocity.y );
		}

		void OnCollisionEnter2D( Collision2D collision )
		{
			Map map = collision.gameObject.GetComponent<Map>();

			if( map == null )
			{
				return;
			}

			Nostalgia.Cell cell = map.GetCellFromCollider( collision.collider );
			if( cell == null )
			{
				return;
			}

			foreach( ContactPoint2D contact in collision.contacts )
			{
				Tile tile = map.GetTile( cell );
				
				BreakableTile breakableTile = tile.GetComponent<BreakableTile>();
				if( breakableTile != null )
				{
					if( contact.normal.y < 0.0f )
					{
						Vector3 pos = map.MapPointToWorldPoint( cell.position );

						foreach( GameObject spawnObject in breakableTile.spawnObjects )
						{
							GameObject broken = Instantiate( spawnObject,pos,Quaternion.identity ) as GameObject;
							broken.transform.localScale = map.transform.lossyScale;
						}
						
						map.RemoveTile( cell.position,true );
					}
				}

				TrapTile trapTile = tile.GetComponent<TrapTile>();
				if( trapTile != null )
				{
					Dead();
					return;
				}
			}
		}

		void FixedUpdate()
		{
			Vector2 pos = transform.position;

			if( _GroundObject != null )
			{
				Vector3 nextPosition = _GroundObject.transform.TransformPoint( _GroundLocalPos );
				pos += (Vector2)nextPosition - _PreGroundPos;
				transform.position = pos;

				_PreGroundPos = transform.position;
				_GroundLocalPos = _GroundObject.transform.InverseTransformPoint( _PreGroundPos );
			}

			_IsGround = false;
			_GroundObject = null;

			Vector2 groundArea = new Vector2( _BoxCollider.size.x * 0.49f,0.1f );

			foreach( Collider2D collider in Physics2D.OverlapAreaAll( pos + groundArea,pos - groundArea ) )
			{
				if( collider.gameObject != gameObject )
				{
					_GroundObject = collider.transform;
					_IsGround = true;
					break;
				}
			}

			if( _GroundObject != null )
			{
				_PreGroundPos = transform.position;
				_GroundLocalPos = _GroundObject.transform.InverseTransformPoint( _PreGroundPos );
			}

			_Animator.SetBool( s_isGroundID,_IsGround );
		}

		void Dead()
		{
			Instantiate ( deadParticle,transform.position,Quaternion.identity );

			Destroy( gameObject );
		}

		IEnumerator NextStage()
		{
			yield return new WaitForSeconds( 10.0f );
			
			Application.LoadLevel( Application.loadedLevel );
		}

		void Goal()
		{
			Timer timer = FindObjectOfType<Timer>();
			if( timer != null )
			{
				timer.OnGoal();
			}

			GameObject bgm = GameObject.Find( "BGM" );
			AudioSource source = bgm.GetComponent<AudioSource>();
			source.clip = goalClip;
			source.loop = false;
			source.Play();

			_IsGoal = true;

			StartCoroutine( NextStage () );
		}
	}
}