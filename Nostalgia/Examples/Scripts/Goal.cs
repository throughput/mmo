﻿using UnityEngine;
using System.Collections;

namespace Nostalgia.Example
{
	public class Goal : MonoBehaviour 
	{
		void OnTriggerEnter2D( Collider2D other )
		{
			if( other.tag == "Player" )
			{
				other.SendMessage( "Goal" );

				Destroy ( gameObject );
			}
		}
	}
}