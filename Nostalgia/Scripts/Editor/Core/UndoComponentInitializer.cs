﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace NostalgiaEditor
{
	[InitializeOnLoad]
	class UndoComponentInitializer
	{
		static UndoComponentInitializer()
		{
			Nostalgia.ComponentUtility.editorAddComponent = Undo.AddComponent;
			Nostalgia.ComponentUtility.editorDestroyObjectImmediate = Undo.DestroyObjectImmediate;
		}
	}
}
