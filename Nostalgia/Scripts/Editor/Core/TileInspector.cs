﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Nostalgia;
using System.Collections.Generic;

namespace NostalgiaEditor
{
	[CustomEditor(typeof(Tile))]
	public class TileInspector : Editor
	{
		private class Styles
		{
			public static GUIStyle largeButton = (GUIStyle)"LargeButton";
			public static GUIStyle inspectroTitlebar = (GUIStyle)"IN Title";
		}

		public override bool UseDefaultMargins()
		{
			return false;
		}

		private void DrawSplitLine( float y )
		{
			if( Event.current.type == EventType.Repaint )
			{
				Texture tex = Styles.inspectroTitlebar.normal.background;

				GUI.DrawTextureWithTexCoords( new Rect( 0,y,Screen.width,1.0f),tex,new Rect( 0,1.0f,1.0f,1.0f-1.0f/(float)tex.height ) );
			}
		}

		void AddComponentButton()
		{
			EditorGUILayout.BeginHorizontal();
			
			GUIContent content = new GUIContent("Add Component");
			Rect rect = GUILayoutUtility.GetRect(content, Styles.largeButton );
			
			DrawSplitLine( rect.y - 1f );
			
			rect.y += 10f;
			rect.x += (float) (((double) rect.width - 230.0) / 2.0);
			rect.width = 230f;
			
			if( EditorTools.ButtonMouseDown( rect,content,Styles.largeButton ) )
			{
				AddTileComponentMenu.Open( rect,target as Tile );
				GUIUtility.ExitGUI();
			}
			
			EditorGUILayout.EndHorizontal();
		}

		private static TileInspector _Current;
		public static TileInspector current
		{
			get
			{
				return _Current;
			}
		}

		private TileEditorTracker _Tracker = null;
		public TileEditorTracker tracker
		{
			get
			{
				if( _Tracker == null )
				{
					_Tracker = new TileEditorTracker( target as Tile );
				}
				return _Tracker;
			}
		}

		private Dictionary<Object,bool> _ExpandedComponents = new Dictionary<Object, bool>();
		bool GetIsExpanded( Object obj )
		{
			bool expanded = true;
			if( !_ExpandedComponents.TryGetValue( obj,out expanded  ) )
			{
				expanded = UnityEditorInternal.InternalEditorUtility.GetIsInspectorExpanded( obj );
				_ExpandedComponents.Add( obj,expanded );
			}
			return expanded;
		}

		void SetIsExpanded( Object obj,bool expanded )
		{
			_ExpandedComponents[obj] = expanded;
			UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded( obj,expanded );
		}

		void DrawEditors( Editor[] editors )
		{
			foreach( Editor editor in editors )
			{
				Object target = editor.target;

				GUIUtility.GetControlID( target.GetInstanceID(), FocusType.Passive);

				bool expanded = GetIsExpanded( target );
				bool nextExpanded = EditorGUILayout.InspectorTitlebar( expanded,target );

				if( expanded != nextExpanded )
				{
					SetIsExpanded( target,nextExpanded );
				}

				if( expanded )
				{
					EditorGUILayout.BeginVertical( editor.UseDefaultMargins()? EditorStyles.inspectorDefaultMargins : GUIStyle.none );

					editor.OnInspectorGUI();

					EditorGUILayout.EndVertical();
				}
			}
		}

		void CheckDragAndDrop()
		{
			Rect dropRect = GUILayoutUtility.GetRect( 0.0f,0.0f,0.0f,float.MaxValue );

			if( !dropRect.Contains( Event.current.mousePosition ) )
			{
				return;
			}

			EventType eventType = Event.current.type;
			switch( eventType )
			{
			case EventType.DragUpdated:
			case EventType.DragPerform:
				foreach( Object obj in DragAndDrop.objectReferences )
				{
					MonoScript script = obj as MonoScript;
					if( script == null )
					{
						continue;
					}

					System.Type classType = script.GetClass();
					if( classType.IsSubclassOf( typeof(TileComponent) ) )
					{
						DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

						if( eventType == EventType.dragPerform )
						{
							Tile tile = target as Tile;

							TileComponentUtility.AddComponent( tile,classType );

							DragAndDrop.AcceptDrag();
							
							Event.current.Use();
						}
					}
				}
				break;
			}

			if( Event.current.type == EventType.MouseDown )
			{
				GUIUtility.keyboardControl = 0;
				Event.current.Use();
			}
		}

		int _IsVisible = -1;

		public override void OnInspectorGUI ()
		{
			bool expanded = false;
			if( _IsVisible == -1 )
			{
				expanded = UnityEditorInternal.InternalEditorUtility.GetIsInspectorExpanded( target );
				_IsVisible = expanded ? 1 : 0;
			}
			else
			{
				expanded = (_IsVisible == 1);
			}

			bool isExpanded = EditorGUILayout.InspectorTitlebar( expanded,target );
			if( isExpanded != expanded )
			{
				UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded( target,isExpanded );
				_IsVisible = isExpanded ? 1 : 0;
			}

			if( expanded )
			{
				SerializedProperty typeProperty = serializedObject.FindProperty( "type" );
				SerializedProperty positionProperty = serializedObject.FindProperty( "position" );
				SerializedProperty sizeProperty = serializedObject.FindProperty( "size" );
				SerializedProperty widthProperty = serializedObject.FindProperty( "width" );
				SerializedProperty heightProperty = serializedObject.FindProperty( "height" );
				SerializedProperty animationProperty = serializedObject.FindProperty( "animation" );
				SerializedProperty colliderProperty = serializedObject.FindProperty( "collider" );
				SerializedProperty physicsMaterialProperty = serializedObject.FindProperty( "physicsMaterial" );
				SerializedProperty isTriggerProperty = serializedObject.FindProperty( "isTrigger" );

				EditorGUILayout.BeginVertical( EditorStyles.inspectorDefaultMargins );

				serializedObject.Update();

				EditorGUILayout.PropertyField( typeProperty );
				EditorGUILayout.PropertyField( positionProperty );
				EditorGUILayout.PropertyField( sizeProperty );

				Tile.Type tileTile = (Tile.Type)typeProperty.enumValueIndex;
				if( tileTile==Tile.Type.Normal )
				{
					EditorTools.IntMinField( widthProperty,1 );
					EditorTools.IntMinField( heightProperty,1 );
				}
				else
				{
					if( widthProperty.intValue != 1 )
					{
						widthProperty.intValue = 1;
					}
					if( heightProperty.intValue != 1 )
					{
						heightProperty.intValue = 1;
					}
				}
				EditorTools.IntMinField( animationProperty,1 );
				EditorGUILayout.PropertyField( colliderProperty );
				EditorGUILayout.PropertyField( physicsMaterialProperty );
				EditorGUILayout.PropertyField( isTriggerProperty );

				serializedObject.ApplyModifiedProperties();

				EditorGUILayout.EndVertical();
			}

			tracker.RebuildIfNecessary();

			DrawEditors( tracker.activeEditors );

			AddComponentButton();

			CheckDragAndDrop();
		}

		void OnEnable()
		{
			Undo.undoRedoPerformed += OnUndoRedo;

			_Current = this;
		}

		void OnDisable()
		{
			_Current = null;

			Undo.undoRedoPerformed -= OnUndoRedo;
		}

		void OnUndoRedo()
		{
			Tile tile = target as Tile;
			if( tile == null )
			{
				return;
			}

			bool changed = false;

			foreach( TileComponent component in tile.GetComponents<TileComponent>() )
			{
				if( !EditorUtility.IsPersistent( component ) )
				{
					AssetDatabase.AddObjectToAsset( component,tile );
					changed = true;
				}
			}
			if( changed )
			{
				EditorUtility.SetDirty( tile );
			}
		}

		void OnDestroy()
		{
			if( _Tracker != null )
			{
				_Tracker.Dispose();
				_Tracker = null;
			}
		}
	}
}