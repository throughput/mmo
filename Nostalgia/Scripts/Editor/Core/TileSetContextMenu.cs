﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;

using Nostalgia;

namespace NostalgiaEditor
{
	public class TileSetContextMenu
	{
		static TileSet _InitializeTileSet;
		static TileSet initializeTileSet
		{
			get
			{
				if( _InitializeTileSet == null )
				{
					_InitializeTileSet = ScriptableObject.CreateInstance<TileSet>();
					_InitializeTileSet.hideFlags = HideFlags.HideAndDontSave;
				}
				return _InitializeTileSet;
			}
		}

		[MenuItem("CONTEXT/TileSet/Reset")]
		static void Reset( MenuCommand command )
		{
			TileSet tileSet = command.context as TileSet;
			string name = tileSet.name;

			Undo.RegisterCompleteObjectUndo( tileSet,"Reset" );
			foreach( Tile tile in tileSet.tiles )
			{
				Undo.RegisterCompleteObjectUndo( tile,"Reset" );

				foreach( TileComponent component in tile.GetComponents<TileComponent>() )
				{
					tile.RemoveComponent( component );
					Undo.DestroyObjectImmediate( component );
				}

				tileSet.RemoveTile( tile );
				Undo.DestroyObjectImmediate( tile );
			}

			EditorUtility.CopySerialized( initializeTileSet,tileSet );
			tileSet.name = name;

			System.Type classType = tileSet.GetType();
			MethodInfo resetMethod = classType.GetMethod ( "Reset",BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );
			if( resetMethod != null )
			{
				resetMethod.Invoke( tileSet,null );
			}
		}
	}
}