﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;

using Nostalgia;

namespace NostalgiaEditor
{
	public class TileComponentContextMenu
	{
		[MenuItem("CONTEXT/TileComponent/Reset",false,0)]
		static void Reset( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			TileComponentUtility.Reset( component );
		}

		[MenuItem("CONTEXT/TileComponent/Remove Component",false,100)]
		static void RemoveComponent( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;
			Tile tile = component.tile;

			Undo.RegisterCompleteObjectUndo( tile,"Remove Component" );
			tile.RemoveComponent( component );
			Undo.DestroyObjectImmediate( component );
		}

		[MenuItem("CONTEXT/TileComponent/Move Up",true,100)]
		static bool ValidMoveUp( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			return TileComponentUtility.IsMoveComponentUp( component );
		}
		
		[MenuItem("CONTEXT/TileComponent/Move Up",false,100)]
		static void MoveUp( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			TileComponentUtility.MoveComponentUp( component );
		}

		[MenuItem("CONTEXT/TileComponent/Move Down",true,100)]
		static bool ValidMoveDown( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			return TileComponentUtility.IsMoveComponentDown( component );
		}
		
		[MenuItem("CONTEXT/TileComponent/Move Down",false,100)]
		static void MoveDown( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			TileComponentUtility.MoveComponentDown( component );
		}

		[MenuItem("CONTEXT/TileComponent/Copy Component",false,100)]
		static void CopyComponent( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			TileComponentUtility.CopyComponent( component );
		}

		[MenuItem("CONTEXT/TileComponent/Paste Component as New",true,100)]
		static bool ValidPasteComponentAsNew( MenuCommand command )
		{
			return TileComponentUtility.IsCopiedComponent();
		}

		[MenuItem("CONTEXT/TileComponent/Paste Component as New",false,100)]
		static void PasteComponentAsNew( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;
			Tile tile = component.tile;

			TileComponentUtility.PasteComponentAsNew( tile );
		}

		[MenuItem("CONTEXT/TileComponent/Paste Component Values",true,100)]
		static bool ValidPasteComponentValues( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			return TileComponentUtility.IsPasteComponent( component );
		}
		
		[MenuItem("CONTEXT/TileComponent/Paste Component Values",false,100)]
		static void PasteComponentValues( MenuCommand command )
		{
			TileComponent component = command.context as TileComponent;

			TileComponentUtility.PasteComponentValues( component );
		}

		[MenuItem ("CONTEXT/TileComponent/Edit Script",true,200)]
		static bool ValidEditScript( MenuCommand command )
		{
			TileComponent component = (TileComponent)command.context;
			MonoScript script = MonoScript.FromScriptableObject( component );

			return script != null;
		}

		[MenuItem ("CONTEXT/TileComponent/Edit Script",false,200)]
		static void EditScript( MenuCommand command )
		{
			TileComponent component = (TileComponent)command.context;
			MonoScript script = MonoScript.FromScriptableObject( component );
			if( script != null )
			{
				AssetDatabase.OpenAsset( script );
			}
		}
	}
}