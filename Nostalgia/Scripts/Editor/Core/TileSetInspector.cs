using UnityEngine;
using UnityEditor;
using System.Collections;

using Nostalgia;

namespace NostalgiaEditor
{
	[CustomEditor(typeof(TileSet))]
	public class TileSetInspector : Editor
	{
		private int _CurrentTileID = 0;
		private Tile _NewTile;
		private Tile newTile
		{
			get
			{
				if( _NewTile == null )
				{
					_NewTile = ScriptableObject.CreateInstance<Tile>();
					_NewTile.hideFlags = HideFlags.HideAndDontSave;
				}
				return _NewTile;
			}
		}

		static bool HasVisibleChildFields(SerializedProperty property)
		{
			switch (property.propertyType)
			{
			case SerializedPropertyType.Vector2:
			case SerializedPropertyType.Vector3:
			case SerializedPropertyType.Rect:
			case SerializedPropertyType.Bounds:
				return false;
			default:
				return property.hasVisibleChildren;
			}
		}

		void OnEnable()
		{
			Undo.undoRedoPerformed += OnUndoRedo;
		}

		void OnDisable()
		{
			Undo.undoRedoPerformed -= OnUndoRedo;
		}

		void OnUndoRedo()
		{
			TileSet tileSet = target as TileSet;

			bool changed = false;
			foreach( Tile tile in tileSet.tiles )
			{
				if( !EditorUtility.IsPersistent(tile) )
				{
					AssetDatabase.AddObjectToAsset( tile,tileSet );
					changed = true;
				}
			}

			if( changed )
			{
				EditorUtility.SetDirty( tileSet );
			}
		}

		public override bool UseDefaultMargins ()
		{
			return false;
		}

		private Editor _CurrentEditor;

		public override void OnInspectorGUI()
		{
			EditorGUILayout.BeginVertical( EditorStyles.inspectorDefaultMargins );

			serializedObject.Update();

			EditorGUILayout.PropertyField( serializedObject.FindProperty( "material" ) );

			serializedObject.ApplyModifiedProperties();

			EditorGUILayout.EndVertical();

			TileSet tileSet = target as TileSet;

			if( tileSet.material != null )
			{
				if( tileSet.material.mainTexture != null )
				{
					EditorGUILayout.BeginVertical( EditorStyles.inspectorDefaultMargins );

					string texturePath = AssetDatabase.GetAssetPath( tileSet.material.mainTexture );

					TextureImporter textureImporter = AssetImporter.GetAtPath( texturePath ) as TextureImporter;

					if( textureImporter != null && textureImporter.filterMode != FilterMode.Point )
					{
						EditorGUILayout.BeginVertical( (GUIStyle)"HelpBox" );

						GUILayout.Box( new GUIContent( "FilterMode is not Point.\nAre you sure you want to change to Point?",EditorTools.helpWarnIcon ),GUIStyle.none );

						if( GUILayout.Button ( "Change to Point" ) )
						{
							textureImporter.filterMode = FilterMode.Point;

							AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
						}

						EditorGUILayout.EndVertical();
					}

					EditorGUILayout.Space();
					
					EditorGUILayout.LabelField( "Tiles",EditorStyles.boldLabel );
					
					_CurrentTileID = Mathf.Clamp( _CurrentTileID,0,tileSet.tiles.Length-1 );

					_CurrentTileID = EditorTools.SelectTileField( tileSet,_CurrentTileID );

					EditorGUILayout.BeginHorizontal();
					
					EditorGUILayout.Space();

					GUI.enabled = _CurrentTileID >= 0;

					if( GUILayout.Button ("Insert to " + _CurrentTileID,EditorStyles.miniButtonLeft,GUILayout.Width( 100.0f ) ) )
					{
						Undo.RecordObject( tileSet,"Add Tile" );
						
						Tile tile = tileSet.InsertTile( _CurrentTileID );
						
						AssetDatabase.AddObjectToAsset( tile,tileSet );

						Undo.RegisterCreatedObjectUndo( tile,"Add Tile" );

						EditorUtility.SetDirty( tileSet );
					}

					GUI.enabled = true;
					
					if( GUILayout.Button ("Add to last",EditorStyles.miniButtonRight,GUILayout.Width( 100.0f ) ) )
					{
						Undo.RecordObject( tileSet,"Add Tile" );

						Tile tile = tileSet.AddTile();

						AssetDatabase.AddObjectToAsset( tile,tileSet );

						Undo.RegisterCreatedObjectUndo( tile,"Add Tile" );

						EditorUtility.SetDirty( tileSet );

						_CurrentTileID = tileSet.tiles.Length - 1;
					}

					GUI.enabled = (0 <= _CurrentTileID && _CurrentTileID < tileSet.tiles.Length);

					Color savedColor = GUI.color;
					GUI.color = Color.red;

					if( GUILayout.Button ("Delete",EditorStyles.miniButton,GUILayout.Width( 100.0f ) ) )
					{
						Tile currentTile = tileSet.tiles[_CurrentTileID];

						Undo.RegisterCompleteObjectUndo( new Object[]{ tileSet,currentTile },"Remove Tile" );
						foreach( TileComponent component in currentTile.GetComponents<TileComponent>() )
						{
							currentTile.RemoveComponent( component );
							Undo.DestroyObjectImmediate( component );
						}
						tileSet.RemoveTile( currentTile );
						Undo.DestroyObjectImmediate( currentTile );

						_CurrentTileID = Mathf.Clamp ( _CurrentTileID,0,tileSet.tiles.Length-1 );

						EditorUtility.SetDirty( tileSet );
					}

					GUI.color = savedColor;

					GUI.enabled = true;
					
					EditorGUILayout.EndHorizontal();

					EditorGUILayout.EndVertical();

					if( 0 <= _CurrentTileID && _CurrentTileID < tileSet.tiles.Length )
					{
						Tile currentTile = tileSet.tiles[_CurrentTileID];

						if( currentTile != null )
						{
							if( _CurrentEditor==null )
							{
								_CurrentEditor = Editor.CreateEditor( currentTile );
							}
							else if( _CurrentEditor.target != currentTile )
							{
								DestroyImmediate( _CurrentEditor );
								_CurrentEditor = Editor.CreateEditor( currentTile );
							}

							if( _CurrentEditor != null )
							{
								EditorGUILayout.Space ();

								EditorGUILayout.BeginVertical( _CurrentEditor.UseDefaultMargins() ? EditorStyles.inspectorDefaultMargins : GUIStyle.none );

								_CurrentEditor.OnInspectorGUI();

								EditorGUILayout.EndVertical();
							}
						}
					}
				}
				else
				{
					EditorGUILayout.HelpBox( "Please set Texture2D to the Material.",MessageType.Warning );
				}
			}
			else
			{
				EditorGUILayout.HelpBox( "Please set Material.",MessageType.Warning );
			}
		}

		public override bool HasPreviewGUI()
		{
			TileSet tileSet = target as TileSet;

			return tileSet.material != null && tileSet.material.mainTexture != null ;
		}

		public override void OnInteractivePreviewGUI (Rect r, GUIStyle background)
		{
			EditorTools.DrawTileSetPreview( r,target as TileSet,background,true,_CurrentTileID );
		}

		public override void OnPreviewGUI( Rect r, GUIStyle background )
		{
			EditorTools.DrawTileSetPreview( r,target as TileSet,background,false,0 );
		}
	}
}