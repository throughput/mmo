﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Reflection;

using Nostalgia;

namespace NostalgiaEditor
{
	public class EditorTools
	{
		static PropertyInfo sortingLayersProperty;
		static PropertyInfo sortingLayerUniqueIDsProperty;
		static MethodInfo getSortingLayerCountMethod;
		static MethodInfo getSortingLayerUserIDMethod;
		static MethodInfo getSortingLayerNameFromUniqueIDMethod;
		static MethodInfo getSortingLayerNameMethod;
		static Object tagManager;
		static FieldInfo defaultExpandedFoldoutField;

		private static Material s_HandleWireMaterial2D;
		
		public static Material handleWireMaterial
		{
			get
			{
				if( s_HandleWireMaterial2D == null )
				{
					s_HandleWireMaterial2D = (Material) EditorGUIUtility.LoadRequired("SceneView/2DHandleLines.mat");
				}
				return s_HandleWireMaterial2D;
			}
		}
		
		private static Texture2D s_HelpWarnIcon;
		
		public static Texture2D helpWarnIcon
		{
			get
			{
				if( s_HelpWarnIcon == null )
				{
					s_HelpWarnIcon = EditorGUIUtility.FindTexture("console.warnicon");
				}
				return s_HelpWarnIcon;
			}
		}
		
		static EditorTools()
		{
			System.Type internalEditorUtilityType = typeof(InternalEditorUtility);
			
			sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
			sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty("sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);
			getSortingLayerCountMethod = internalEditorUtilityType.GetMethod( "GetSortingLayerCount",BindingFlags.Static | BindingFlags.NonPublic );
			getSortingLayerUserIDMethod = internalEditorUtilityType.GetMethod( "GetSortingLayerUserID",BindingFlags.Static | BindingFlags.NonPublic );
			getSortingLayerNameFromUniqueIDMethod = internalEditorUtilityType.GetMethod( "GetSortingLayerNameFromUniqueID",BindingFlags.Static | BindingFlags.NonPublic );
			getSortingLayerNameMethod = internalEditorUtilityType.GetMethod( "GetSortingLayerName",BindingFlags.Static | BindingFlags.NonPublic );
			
			System.Type editorApplicationType = typeof(EditorApplication);
			
			PropertyInfo tagManagerProperty = editorApplicationType.GetProperty( "tagManager",BindingFlags.Static | BindingFlags.NonPublic);
			
			tagManager = (Object)tagManagerProperty.GetValue( null,new object[0] );
			
			defaultExpandedFoldoutField = tagManager.GetType().GetField( "m_DefaultExpandedFoldout" );
		}
		
		// Get the sorting layer names
		static string[] sortingLayerNames
		{		
			get
			{
				return (string[])sortingLayersProperty.GetValue(null, new object[0]);
			}		
		}
		
		// Get the unique sorting layer IDs -- tossed this in for good measure
		static int[] sortingLayerUniqueIDs
		{
			get
			{
				return (int[])sortingLayerUniqueIDsProperty.GetValue(null, new object[0]);
			}
		}
		
		public static int GetSortingLayerCount()
		{
			return (int)getSortingLayerCountMethod.Invoke(null,null);
		}
		
		public static string GetSortingLayerName( int index )
		{
			object[] methodParams = { index };
			
			return (string)getSortingLayerNameMethod.Invoke( null,methodParams );
		}
		
		public static int GetSortingLayerUserID(int index)
		{
			object[] methodParams = { index };
			
			return (int)getSortingLayerUserIDMethod.Invoke ( null,methodParams );
		}
		
		static string GetSortingLayerNameFromUniqueID( int id )
		{
			object[] methodParams = { id };
			
			return (string)getSortingLayerNameFromUniqueIDMethod.Invoke ( null,methodParams );
		}
		
		public static int GetSortingLayerUniqueIDFromUserID( int userID )
		{
			int count = GetSortingLayerCount();
			
			for( int i=0;i<count;i++ )
			{
				if( userID == GetSortingLayerUserID( i ) )
				{
					return sortingLayerUniqueIDs[i];
				}
			}
			
			return -1;
		}
		
		public static void OpenSortingLayerSettings()
		{
			defaultExpandedFoldoutField.SetValue( tagManager,"SortingLayers" );
			Selection.activeObject = tagManager;
		}
		
		public static void LayoutSortingLayerField( SerializedProperty property,GUIContent label )
		{
			Rect rect = EditorGUILayout.GetControlRect( false, 16f );
			
			SortingLayerField(rect, property, label );
		}
		
		public static void LayoutSortingLayerField( SerializedProperty property )
		{
			LayoutSortingLayerField(property,new GUIContent(ObjectNames.NicifyVariableName(property.name)) );
		}
		
		public static void SortingLayerField( Rect position,SerializedProperty property,GUIContent label )
		{
			label = EditorGUI.BeginProperty( position,label,property );

			bool useUniqueID = property.serializedObject.targetObject is Renderer;

			int controlID = EditorGUIUtility.GetControlID( FocusType.Keyboard,position );

			int uniqueID = 0;
			if( useUniqueID )
			{
				uniqueID = property.intValue;
			}
			else
			{
				uniqueID = GetSortingLayerUniqueIDFromUserID( property.intValue );
			}
			
			string name = GetSortingLayerNameFromUniqueID( uniqueID );

			Rect contentPosition = EditorGUI.PrefixLabel(position,controlID,label);
			
			Event currentEvent = Event.current;
			
			EventType eventType = currentEvent.GetTypeForControl( controlID );
			
			GenericMenu menu = new GenericMenu();
			
			GenericMenu.MenuFunction2 function = (object id)=>{
				if( useUniqueID )
				{
					property.intValue = sortingLayerUniqueIDs[ (int)id ];
				}
				else
				{
					property.intValue = GetSortingLayerUserID( (int)id );
				}
				property.serializedObject.ApplyModifiedProperties();
			};
			
			for( int i=0;i<GetSortingLayerCount();i++ )
			{
				bool selected = false;
				if( useUniqueID )
				{
					selected = property.intValue== sortingLayerUniqueIDs[i];
				}
				else
				{
					selected = property.intValue==GetSortingLayerUserID(i);
				}
				menu.AddItem( new GUIContent(GetSortingLayerName(i)),selected,function,i );
			}
			
			menu.AddSeparator("");
			
			menu.AddItem( new GUIContent("Add Sorting Layer..."),false,OpenSortingLayerSettings );
			
			switch( eventType )
			{
			case EventType.Repaint:
				EditorStyles.popup.Draw( contentPosition,new GUIContent(name),controlID,false );
				break;
			case EventType.MouseDown:
				if( currentEvent.button == 0 && position.Contains( currentEvent.mousePosition ) )
				{
					GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
					
					if( contentPosition.Contains( currentEvent.mousePosition ) )
					{
						menu.DropDown( contentPosition );
					}
					
					currentEvent.Use();
				}
				break;
			case EventType.KeyDown:
				if( GUIUtility.keyboardControl == controlID && ( currentEvent.keyCode == KeyCode.Space || currentEvent.keyCode == KeyCode.Return ) )
				{
					menu.DropDown( contentPosition );
					
					currentEvent.Use();
				}
				break;
			}
			
			contentPosition.y += contentPosition.height;

			EditorGUI.EndProperty();
		}

		public static void DrawOutline( Rect rect, Color color )
		{
			if( Event.current.type != EventType.Repaint )
			{
				return;
			}

			Texture2D tex = EditorGUIUtility.whiteTexture;
			GUI.color = color;
			GUI.DrawTexture(new Rect(rect.xMin, rect.yMin, 1f, rect.height), tex);
			GUI.DrawTexture(new Rect(rect.xMax-1, rect.yMin, 1f, rect.height), tex);
			GUI.DrawTexture(new Rect(rect.xMin, rect.yMin, rect.width, 1f), tex);
			GUI.DrawTexture(new Rect(rect.xMin, rect.yMax-1, rect.width, 1f), tex);
			GUI.color = Color.white;
		}
		
		const int _TileWidth = 7;
		
		public static Vector2 CalcSize( TileSet tileSet )
		{
			int tileNum = tileSet.tiles.Length;
			
			int tileHeight = Mathf.CeilToInt( (float)tileNum / (float)_TileWidth );
			
			return new Vector2( _TileWidth*32+4,tileHeight*32+4);
		}
		
		public static int SelectTileField( Rect position,TileSet tileSet,int value )
		{
			if( tileSet==null )
			{
				return value;
			}
			
			Texture texture = tileSet.material.mainTexture;
			float textureWidth = (float)texture.width;
			float textureHeight = (float)texture.height;

			int tileNum = tileSet.tiles.Length;
			
			Event current = Event.current;

			GUI.BeginGroup( position );

			Rect tilePos = new Rect( 0.0f,0.0f,32,32 );
			
			for( int tileIndex=0;tileIndex<tileNum;tileIndex++ )
			{
				Tile tile = tileSet.tiles[tileIndex];

				if( tile == null )
					continue;
				
				GUI.BeginGroup( tilePos );
				
				if( GUI.Button( new Rect( 0,0,32,32), GUIContent.none,GUI.skin.box) )
				{
					value = tileIndex;
				}
				
				if( current.type == EventType.Repaint )
				{
					Rect texCoord = new Rect( (float)tile.position.x/textureWidth,1.0f-(tile.position.y+tile.size)/textureHeight,tile.size/textureWidth,tile.size/textureHeight );

					GUI.DrawTextureWithTexCoords( new Rect(0,0,32,32) ,texture,texCoord,true );
					
					if( value == tileIndex )
					{
						DrawOutline( new Rect(0,0,32,32),Color.cyan );
					}
				}

				tilePos.x += 32.0f;
				if( tilePos.x > position.width - 32.0f )
				{
					tilePos.x = 0.0f;
					tilePos.y += 32.0f;
				}
				
				GUI.EndGroup();
			}

			GUI.EndGroup();
			
			return value;
		}
		
		public static int SelectTileField( TileSet tileSet,int value )
		{
			GUILayout.BeginVertical( GUI.skin.box,GUILayout.MinHeight( 32f ) );

			if( tileSet != null && tileSet.tiles != null && tileSet.tiles.Length != 0 )
			{
				int tileWidth = (Screen.width - 20) / 32;
				int tileHeight = Mathf.CeilToInt( (float)tileSet.tiles.Length / (float)tileWidth );

				Rect aspectRect = GUILayoutUtility.GetAspectRect( (float)tileWidth / (float)tileHeight );

				value = SelectTileField( aspectRect,tileSet,value );
			}
			else
			{
				GUILayout.Label( "No tiles defined." );
			}

			GUILayout.EndVertical();

			return value;
		}

		public static int SelectPartsField( Tile tile,int value )
		{
			Rect rect = GUILayoutUtility.GetRect( 96,96,GUILayout.ExpandWidth(false),GUILayout.ExpandHeight(false) );

			GUI.Box( rect,GUIContent.none );

			bool[] sameTiles = new bool[9];
			for( int x=0;x<2;x++ )
			{
				for( int y=0;y<2;y++ )
				{
					int index = Cell.GetPartsIndex( value,x,y );

					int vx = (x%2==0)?0:2;
					int vy = (y%2==0)?0:2;

					int longitudinalIndex = vy * 3 + 1;
					int transverseIndex = 1 * 3 + vx;
					int obliqueIndex = vy * 3 + vx;

					switch( index )
					{
					case 0:
						sameTiles[longitudinalIndex] = true;
						sameTiles[transverseIndex] = true;
						sameTiles[obliqueIndex] = true;
						break;
					case 1:
						sameTiles[transverseIndex] = true;
						break;
					case 2:
						sameTiles[longitudinalIndex] = true;
						break;
					case 3:
						sameTiles[longitudinalIndex] = true;
						sameTiles[transverseIndex] = true;
						break;
					case 4:
						break;
					}
				}
			}

			EditorGUI.BeginChangeCheck();

			for( int x=0;x<3;x++ )
			{
				for( int y=0;y<3;y++ )
				{
					if( x==1 && y==1 )
					{
						continue;
					}

					Rect boxRect = new Rect( rect.x + 32 * x + 8.0f ,rect.y + 32 * y + 8.0f,16,16 );

					EditorGUI.BeginChangeCheck();
					sameTiles[ y*3+x ] = EditorGUI.Toggle( boxRect,sameTiles[ y*3+x ] );

					if( EditorGUI.EndChangeCheck() && sameTiles[ y*3+x ] )
					{
						if( x == 0 && y == 0 )
						{
							sameTiles[ 0*3+1 ] = true;
							sameTiles[ 1*3+0 ] = true;
						}
						else if( x == 2 && y == 0 )
						{
							sameTiles[ 0*3+1 ] = true;
							sameTiles[ 1*3+2 ] = true;
						}
						else if( x == 0 && y == 2 )
						{
							sameTiles[ 1*3+0 ] = true;
							sameTiles[ 2*3+1 ] = true;
						}
						else if( x == 2 && y == 2 )
						{
							sameTiles[ 1*3+2 ] = true;
							sameTiles[ 2*3+1 ] = true;
						}
					}
				}
			}

			if( EditorGUI.EndChangeCheck() )
			{
				value = 0;

				for( int x=0;x<2;x++ )
				{
					for( int y=0;y<2;y++ )
					{
						int vx = (x%2==0)?0:2;
						int vy = (y%2==0)?0:2;
						
						int longitudinalIndex = vy * 3 + 1;
						int transverseIndex = 1 * 3 + vx;
						int obliqueIndex = vy * 3 + vx;

						int index = 4;

						if( sameTiles[longitudinalIndex] && sameTiles[transverseIndex] && sameTiles[obliqueIndex] )	index = 0;
						else if( sameTiles[longitudinalIndex] && sameTiles[transverseIndex] ) index = 3;
						else if( sameTiles[longitudinalIndex] ) index = 2;
						else if( sameTiles[transverseIndex] ) index = 1;

						value = Cell.SetPartsIndex( value,x,y,index );
					}
				}
			}

			Texture texture = tile.tileSet.material.mainTexture;
			
			float textureWidth = texture.width;
			float textureHeight = texture.height;

			float uvWidth = tile.size/textureWidth*0.5f;
			float uvHeight = tile.size/textureHeight*0.5f;

			for( int x=0;x<2;x++ )
			{
				for( int y=0;y<2;y++ )
				{
					int index = Cell.GetPartsIndex( value,x,y );

					Vector2 pos = tile.IndexToPos( index,x,y,Point2.zero );

					Rect texCoord = new Rect( (float)pos.x/textureWidth,1.0f-(pos.y+tile.size*0.5f)/textureHeight,uvWidth,uvHeight );
					
					GUI.DrawTextureWithTexCoords( new Rect( rect.x + 32 + x * 16.0f,rect.y + 32 + y * 16.0f,16.0f,16.0f ),texture,texCoord,true );
				}
			}

			return value;
		}

		public static void DrawRect(Rect rect)
		{
			GL.Vertex(new Vector3(rect.xMin, rect.yMin, 0.0f));
			GL.Vertex(new Vector3(rect.xMax, rect.yMin, 0.0f));
			
			GL.Vertex(new Vector3(rect.xMax, rect.yMin, 0.0f));
			GL.Vertex(new Vector3(rect.xMax, rect.yMax, 0.0f));
			
			GL.Vertex(new Vector3(rect.xMax, rect.yMax, 0.0f));
			GL.Vertex(new Vector3(rect.xMin, rect.yMax, 0.0f));
			
			GL.Vertex(new Vector3(rect.xMin, rect.yMax, 0.0f));
			GL.Vertex(new Vector3(rect.xMin, rect.yMin, 0.0f));
		}

		public static void DrawTileSetPreview( Rect r,TileSet tileSet,GUIStyle background,bool select,int selectedIndex )
		{
			if (Event.current.type == EventType.Repaint)
			{
				background.Draw(r, false, false, false, false);
			}
			
			Texture2D texture = tileSet.material.mainTexture as Texture2D;
			
			float scale = Mathf.Min(Mathf.Min(r.width / (float) texture.width, r.height / (float) texture.height), 1f);
			
			float width = texture.width * scale;
			float height = texture.height * scale;
			
			Rect rect = new Rect(r.x + (r.width-width)*0.5f, r.y + (r.height-height)*0.5f, width,height );
			
			if( texture != null && texture.alphaIsTransparency )
				EditorGUI.DrawTextureTransparent(rect, texture);
			else
				EditorGUI.DrawPreviewTexture(rect, texture);
			
			if( tileSet.tiles != null && tileSet.tiles.Length>0 )
			{
				handleWireMaterial.SetPass(0);
				GL.PushMatrix();
				GL.MultMatrix(Handles.matrix);
				GL.Begin(1);
				
				for( int i=0;i<tileSet.tiles.Length;++i )
				{
					Tile tile = tileSet.tiles[i];

					if( tile == null )
						continue;
					
					Color color = (select && i==selectedIndex)?Color.cyan:new Color(1.0f,1.0f,1.0f,0.5f);
					
					GL.Color( color );
					
					Rect tileRect = new Rect();
					
					switch( tile.type )
					{
					case Tile.Type.Normal:
						tileRect = new Rect( tile.position.x,tile.position.y, (float)(tile.size * tile.width),(float)(tile.size * tile.height) );
						break;
					case Tile.Type.AutoFloorVX:
						tileRect = new Rect( tile.position.x,tile.position.y, (float)tile.size * 2.0f,(float)tile.size * 3.0f );
						break;
					case Tile.Type.AutoFloorWolf:
						tileRect = new Rect( tile.position.x,tile.position.y, (float)tile.size,(float)tile.size * 5.0f);
						break;
					}
					
					DrawRect( new Rect(){
						xMin = rect.xMin + (tileRect.xMin / (float) texture.width) * rect.width,
						xMax = rect.xMin + (tileRect.xMax / (float) texture.width) * rect.width,
						yMin = rect.yMin + (tileRect.yMin / (float) texture.height) * rect.height,
						yMax = rect.yMin + (tileRect.yMax / (float) texture.height) * rect.height
					} );
				}
				
				GL.End();
				GL.PopMatrix();
			}
		}

		static int s_ButtonMouseDownHash = "ButtonMouseDownHash".GetHashCode();
		
		public static bool ButtonMouseDown( Rect position, GUIContent content, GUIStyle style )
		{
			Event current = Event.current;
			
			int controlId = GUIUtility.GetControlID(s_ButtonMouseDownHash, FocusType.Passive, position);
			
			switch( current.type )
			{
			case EventType.MouseDown:
				if( position.Contains(current.mousePosition) && current.button == 0 )
				{
					Event.current.Use();
					return true;
				}
				break;
			case EventType.Repaint:
				style.Draw(position, content, controlId, false);
				break;
			}
			return false;
		}

		public static bool IntMinField( SerializedProperty property,int min )
		{
			EditorGUI.BeginChangeCheck();
			int value = EditorGUILayout.IntField( ObjectNames.NicifyVariableName(property.name),property.intValue );
			if( EditorGUI.EndChangeCheck() && value >= min )
			{
				property.intValue = value;
				return true;
			}
			return false;
		}
	}
}