﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using Nostalgia;

namespace NostalgiaEditor
{
	public class TileContextMenu
	{
		private static FieldInfo _TileSetField;
		private static FieldInfo tileSetField
		{
			get
			{
				if( _TileSetField == null )
				{
					System.Type type = typeof(Tile);
					_TileSetField = type.GetField( "_TileSet",BindingFlags.Instance | BindingFlags.NonPublic );
				}
				return _TileSetField;
			}
		}

		private static FieldInfo _ComponentsField;
		private static FieldInfo componentsField
		{
			get
			{
				if( _ComponentsField == null )
				{
					System.Type type = typeof(Tile);
					_ComponentsField = type.GetField( "_Components",BindingFlags.Instance | BindingFlags.NonPublic );
				}
				return _ComponentsField;
			}
		}

		private static Tile _InitializedTile;
		private static Tile initializedTile
		{
			get
			{
				if( _InitializedTile == null )
				{
					_InitializedTile = ScriptableObject.CreateInstance( typeof(Tile) ) as Tile;
					_InitializedTile.hideFlags = HideFlags.HideAndDontSave;
				}
				return _InitializedTile;
			}
		}

		[MenuItem ("CONTEXT/Tile/Reset",false,0)]
		static void Reset( MenuCommand command )
		{
			Tile tile = command.context as Tile;
			TileSet tileSet = tile.tileSet;
			List<TileComponent> components = new List<TileComponent>( componentsField.GetValue( tile ) as List<TileComponent> );

			Undo.RegisterCompleteObjectUndo( tile,"Reset" );

			EditorUtility.CopySerialized( initializedTile,tile );
			tileSetField.SetValue( tile,tileSet );
			componentsField.SetValue( tile,components );

			System.Type classType = tile.GetType();
			MethodInfo resetMethod = classType.GetMethod( "Reset",BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );
			if( resetMethod != null )
			{
				resetMethod.Invoke( tile,null );
			}
		}

		[MenuItem("CONTEXT/Tile/Paste Component As New",true,100)]
		static bool ValidPasteComponentAsNew( MenuCommand command )
		{
			return TileComponentUtility.IsCopiedComponent();
		}

		[MenuItem("CONTEXT/Tile/Paste Component As New",false,100)]
		static void PasteComponentAsNew( MenuCommand command )
		{
			Tile tile = command.context as Tile;

			TileComponentUtility.PasteComponentAsNew( tile );
		}

		[MenuItem ("CONTEXT/Tile/Edit Script",true,200)]
		static bool ValidEditScript( MenuCommand command )
		{
			Tile tile = command.context as Tile;
			MonoScript script = MonoScript.FromScriptableObject( tile );
			
			return script != null;
		}

		[MenuItem ("CONTEXT/Tile/Edit Script",false,200)]
		static void EditScript( MenuCommand command )
		{
			Tile tile = command.context as Tile;
			MonoScript script = MonoScript.FromScriptableObject( tile );
			if( script != null )
			{
				AssetDatabase.OpenAsset( script );
			}
		}
	}
}