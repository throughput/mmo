﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Nostalgia;

namespace NostalgiaEditor
{
	public class SortingLayerWindow : EditorWindow
	{
		private class Styles
		{
			public static GUIStyle visiblyToggle = (GUIStyle)"VisibilityToggle";
			public static GUIStyle separator = (GUIStyle)"sv_iconselector_sep";
			public static GUIStyle hiLabel = (GUIStyle) "HI Label";
			public static GUIStyle header = (GUIStyle)"OL Title";
			public static GUIStyle inspectorTitlebarText = (GUIStyle)"IN TitleText";
			public static GUIStyle rightLabel = (GUIStyle)"RightLabel";
		}

		private static Color[] s_HierarchyColors;
		private static Color[] s_DarkColors;

		static SortingLayerWindow()
		{
			s_HierarchyColors = new Color[]{
				Color.black,
				new Color(0.0f, 0.15f, 0.51f, 1f),
				new Color(0.25f, 0.05f, 0.05f, 1f),
				Color.black,
				Color.white,
				new Color(0.67f, 0.76f, 1f),
				new Color(1f, 0.71f, 0.71f, 1f),
				Color.white, 
			};

			s_DarkColors = new Color[]{
				new Color(0.705f, 0.705f, 0.705f, 1f),
				new Color(0.3f, 0.5f, 0.85f, 1f),
				new Color(0.7f, 0.4f, 0.4f, 1f),
				new Color(0.705f, 0.705f, 0.705f, 1f),
				Color.white,
				new Color(0.67f, 0.76f, 1f),
				new Color(1f, 0.71f, 0.71f, 1f),
				Color.white, 
			};
		}

		[SerializeField] private List<int> _ExpandsLayers = new List<int>();

		[MenuItem("Window/Sorting Layer")]
		static void Open()
		{
			EditorWindow.GetWindow<SortingLayerWindow>( "Sorting Layer" );
		}

		Vector2 _ScrollPos = Vector2.zero;

		interface ILayerProperty : System.IDisposable
		{
			string name{ get; }
			GameObject gameObject{ get; }
			bool active{ get;set; }
			bool enabled{ get;set; }

			int colorCode{ get; }

			int sortingLayerID{ get; }
			int sortingOrder{ get; }

			bool IsMissingComponent();

			void Move( int layerID,int order );

			void OnPreviewGUI( Rect rect,GUIStyle background );
		}

		class RendererLayerProperty : ILayerProperty
		{
			private Renderer _Renderer;
			private int _ColorCode;

			public RendererLayerProperty( Renderer renderer,int colorCode )
			{
				_Renderer = renderer;
				_ColorCode = colorCode;
			}

			public string name
			{
				get
				{
					return _Renderer.name;
				}
			}

			public GameObject gameObject
			{
				get
				{
					return _Renderer.gameObject;
				}
			}

			public bool active
			{
				get
				{
					return gameObject.activeSelf;
				}
				set
				{
					if( gameObject.activeSelf != value )
					{
						Undo.RecordObject( gameObject,"Change Active" );

						gameObject.SetActive( value );

						EditorUtility.SetDirty( gameObject );
					}
				}
			}

			public bool enabled
			{
				get
				{
					return _Renderer.enabled;
				}
				set
				{
					if( _Renderer.enabled != value )
					{
						Undo.RecordObject( _Renderer,value?"Enabled " : "Disabled " + _Renderer.GetType().Name );

						_Renderer.enabled = value;

						EditorUtility.SetDirty( _Renderer );
					}
				}
			}

			public int colorCode
			{
				get
				{
					return _ColorCode;
				}
			}

			public int sortingLayerID
			{
				get
				{
					return _Renderer.sortingLayerID;
				}
				set
				{
					if( _Renderer.sortingLayerID != value )
					{
						Undo.RecordObject( _Renderer,"Change Sorting Layer" );
						
						_Renderer.sortingLayerID = value;
						
						EditorUtility.SetDirty( _Renderer );
					}
				}
			}

			public int sortingOrder
			{
				get
				{
					return _Renderer.sortingOrder;
				}
				set
				{
					if( _Renderer.sortingOrder != value )
					{
						Undo.RecordObject( _Renderer,"Change Sorting Order" );
						
						_Renderer.sortingOrder = value;
						
						EditorUtility.SetDirty( _Renderer );
					}
				}
			}

			public bool IsMissingComponent()
			{
				return _Renderer == null;
			}

			public void Move( int layerID,int order )
			{
				Undo.RecordObject( _Renderer,"Move Sorting Layer" );
				
				_Renderer.sortingLayerID = layerID;
				_Renderer.sortingOrder = order;
				
				EditorUtility.SetDirty( _Renderer );
			}

			Editor _Editor = null;

			public void OnPreviewGUI( Rect rect,GUIStyle background )
			{
				if( _Editor == null )
				{
					_Editor = Editor.CreateEditor( _Renderer );
				}
				if( _Editor != null && _Editor.HasPreviewGUI() )
				{
					_Editor.OnPreviewGUI( rect,background );
				}
				else if( Event.current.type == EventType.Repaint )
				{
					System.Type type = (_Renderer is ParticleSystemRenderer)?typeof(ParticleSystem):_Renderer.GetType();

					GUIContent content = new GUIContent( AssetPreview.GetMiniTypeThumbnail( type ) );
					background.Draw ( rect,content,false,false,false,false );
				}
			}

			public void Dispose()
			{
				if( _Editor != null )
				{
					Object.DestroyImmediate( _Editor );
					_Editor = null;
				}
				System.GC.SuppressFinalize(this);
			}
		}

		class MapLayerProperty : ILayerProperty
		{
			private Map _Map;
			private int _ColorCode;

			public MapLayerProperty( Map map,int colorCode )
			{
				_Map = map;
				_ColorCode = colorCode;
			}

			public string name
			{
				get
				{
					return _Map.name;
				}
			}
			
			public GameObject gameObject
			{
				get
				{
					return _Map.gameObject;
				}
			}

			public bool active
			{
				get
				{
					return gameObject.activeSelf;
				}
				set
				{
					if( gameObject.activeSelf != value )
					{
						Undo.RecordObject( gameObject,"Change Active" );
						
						gameObject.SetActive( value );
						
						EditorUtility.SetDirty( gameObject );
					}
				}
			}

			public bool enabled
			{
				get
				{
					return _Map.enabled;
				}
				set
				{
					if( _Map.enabled != value )
					{
						Undo.RecordObject( _Map,value?"Enabled " : "Disabled " + "Map" );
						
						_Map.enabled = value;
						
						EditorUtility.SetDirty( _Map );
					}
				}
			}

			public int colorCode
			{
				get
				{
					return _ColorCode;
				}
			}
			
			public int sortingLayerID
			{
				get
				{
					return _Map.sortingLayerID;
				}
				set
				{
					if( _Map.sortingLayerID != value )
					{
						Undo.RecordObject( _Map,"Change Sorting Layer" );
						
						_Map.sortingLayerID = value;
						
						EditorUtility.SetDirty( _Map );
					}
				}
			}
			
			public int sortingOrder
			{
				get
				{
					return _Map.sortingOrder;
				}
				set
				{
					if( _Map.sortingOrder != value )
					{
						Undo.RecordObject( _Map,"Change Sorting Order" );
						
						_Map.sortingOrder = value;
						
						EditorUtility.SetDirty( _Map );
					}
				}
			}

			public bool IsMissingComponent()
			{
				return _Map == null;
			}

			public void Move( int layerID,int order )
			{
				Undo.RecordObject( _Map,"Move Sorting Layer" );

				_Map.sortingLayerID = layerID;
				_Map.sortingOrder = order;

				EditorUtility.SetDirty( _Map );
			}

			Editor _Editor = null;
			
			public void OnPreviewGUI( Rect rect,GUIStyle background )
			{
				if( _Editor == null )
				{
					_Editor = Editor.CreateEditor( _Map );
				}
				if( _Editor != null && _Editor.HasPreviewGUI() )
				{
					_Editor.OnPreviewGUI( rect,background );
				}
				else if( Event.current.type == EventType.Repaint )
				{
					EditorGUI.DrawTextureTransparent( rect,AssetPreview.GetMiniTypeThumbnail( _Map.GetType () ) );
				}
			}

			public void Dispose()
			{
				if( _Editor != null )
				{
					Object.DestroyImmediate( _Editor );
					_Editor = null;
				}
				System.GC.SuppressFinalize(this);
			}
		}

		List<ILayerProperty> _Properties = new List<ILayerProperty>();

		static int CompareProperty( ILayerProperty a,ILayerProperty b )
		{
			if( a.IsMissingComponent() || b.IsMissingComponent() )
			{
				return 0;
			}

			if( a.sortingLayerID != b.sortingLayerID )
			{
				return b.sortingLayerID - a.sortingLayerID;
			}
			return b.sortingOrder - a.sortingOrder;
		}

		void Rebuild()
		{
			foreach( ILayerProperty property in _Properties )
			{
				property.Dispose();
			}
			_Properties.Clear ();

			HierarchyProperty hierarychyProperty = new HierarchyProperty( HierarchyType.GameObjects );
			while( hierarychyProperty.Next( null ) )
			{
				GameObject gameObject = hierarychyProperty.pptrValue as GameObject;

				Map map = gameObject.GetComponent<Map>();
				if( map != null )
				{
					_Properties.Add ( new MapLayerProperty( map,hierarychyProperty.colorCode ) );
				}
				else
				{
					SortingLayer sortingLayer = gameObject.GetComponent<SortingLayer>();
					if( sortingLayer != null && sortingLayer.GetComponent<Renderer>() != null )
					{
						_Properties.Add ( new RendererLayerProperty( sortingLayer.GetComponent<Renderer>(),hierarychyProperty.colorCode ) );
					}
					else if( gameObject.GetComponent<Renderer>() != null && gameObject.GetComponent<Renderer>() is SpriteRenderer )
					{
						_Properties.Add ( new RendererLayerProperty( gameObject.GetComponent<Renderer>(),hierarychyProperty.colorCode ) );
					}
				}
			}

			Repaint();
		}

		void OnSelectionChange()
		{
			Repaint();
		}

		void OnEnable()
		{
			Rebuild();

			EditorApplication.hierarchyWindowChanged += Rebuild;
		}

		void OnDisable()
		{
			EditorApplication.hierarchyWindowChanged -= Rebuild;

			foreach( ILayerProperty property in _Properties )
			{
				property.Dispose();
			}
			_Properties.Clear ();
		}

		static int s_HeaderHash = "Header".GetHashCode();
		static GUIContent s_Popup = new GUIContent( EditorGUIUtility.FindTexture( "_Popup" ) );
		
		bool HeaderGUI( bool foldout,string label,params GUILayoutOption[] options )
		{
			GUIStyle style = Styles.header;
			GUIContent content = new GUIContent( label );
			Rect position = GUILayoutUtility.GetRect( content,style,options );
			
			int controlID = GUIUtility.GetControlID( s_HeaderHash,FocusType.Passive,position );
			
			Rect popupPosition = new Rect( position.xMax - style.padding.right - 2.0f - 16.0f, position.y, 16f, 16f);
			
			Event current = Event.current;
			switch( current.type )
			{
			case EventType.MouseDown:
				if( current.button == 0 )
				{
					if( popupPosition.Contains( current.mousePosition ) )
					{
						EditorTools.OpenSortingLayerSettings();
						current.Use();
					}
					else if( position.Contains( current.mousePosition ) )
					{
						GUIUtility.hotControl = controlID;
						current.Use();
					}
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID && position.Contains( current.mousePosition ) )
				{
					foldout = !foldout;
					GUI.changed = true;
					
					GUIUtility.hotControl = 0;
					current.Use();
				}
				break;
			case EventType.Repaint:
				style.Draw( position,content,controlID,foldout );
				Styles.inspectorTitlebarText.Draw ( popupPosition,s_Popup,controlID,foldout );
				break;
			}
			
			return foldout;
		}

		void PropertyGUI( ILayerProperty property )
		{
			bool focused = focusedWindow == this;

			RectOffset margin = new RectOffset( 4,4,4,4 );
			Rect rect = GUILayoutUtility.GetRect( 0.0f,0.0f,GUILayout.ExpandWidth(true),GUILayout.Height(32.0f + margin.vertical) );

			int controlID = GUIUtility.GetControlID( property.gameObject.GetInstanceID(),FocusType.Passive,rect );

			bool selected = Selection.gameObjects.Contains(property.gameObject);

			if( selected )
			{
				Color color;
				if( focused )
				{
					color = new Color32( 62,124,230,255 );
				}
				else
				{
					color = new Color32( 142,142,142,255 );
				}

				EditorGUI.DrawRect( rect,color );
			}

			rect = margin.Remove( rect );

			Rect activeRect = new Rect( rect );
			activeRect.y = rect.y+rect.height*0.5f-16.0f;
			activeRect.width = 16.0f;
			activeRect.height = 16.0f;
			
			EditorGUI.BeginChangeCheck();
			bool active = EditorGUI.Toggle( activeRect,property.active );
			if( EditorGUI.EndChangeCheck() )
			{
				property.active = active;
			}
			
			Rect visibleRect = new Rect( rect );
			visibleRect.y = rect.y+rect.height*0.5f;
			visibleRect.width = 16.0f;
			visibleRect.height = 16.0f;
			
			EditorGUI.BeginChangeCheck();
			bool enabled = EditorGUI.Toggle( visibleRect,property.enabled,Styles.visiblyToggle );
			if( EditorGUI.EndChangeCheck() )
			{
				property.enabled = enabled;
			}
			
			Rect previewRect = new Rect( rect );
			
			previewRect.x += 16.0f;
			previewRect.width = 32.0f;
			previewRect.height = 32.0f;
			
			property.OnPreviewGUI( previewRect,GUI.skin.box );

			if( Event.current.type == EventType.Repaint )
			{
				GUIContent label = new GUIContent( property.name );
				GUIContent orderContent = new GUIContent( property.sortingOrder.ToString() );

				GUIStyle labelStyle = Styles.hiLabel;
				GUIStyle orderStyle = Styles.rightLabel;
				
				Rect labelRect = new Rect( rect );

				labelRect.x += 48.0f;
				labelRect.width -= 100.0f - 48.0f;

				float labelHeight = labelStyle.CalcHeight(label,labelRect.width);
				labelRect.y += (labelRect.height-labelHeight )*0.5f;
				labelRect.height = labelHeight;

				Rect orderRect = new Rect( rect );

				orderRect.x = rect.xMax - 100.0f;
				orderRect.width = 100.0f;

				float orderHeight = orderStyle.CalcHeight(orderContent,orderRect.width);
				orderRect.y += (orderRect.height-orderHeight )*0.5f;
				orderRect.height = orderHeight;

				Color[] colorArray = !EditorGUIUtility.isProSkin ? s_HierarchyColors : s_DarkColors;

				int colorCode = property.colorCode;
				Color color = colorArray[colorCode & 3];
				Color onColor = colorArray[(colorCode & 3) + 4];
				color.a = colorCode < 4 ? (onColor.a = 1f) : (onColor.a = 0.6f);

				labelStyle.normal.textColor = color;
				labelStyle.focused.textColor = color;
				labelStyle.hover.textColor = color;
				labelStyle.active.textColor = color;
				labelStyle.onNormal.textColor = onColor;
				labelStyle.onHover.textColor = onColor;
				labelStyle.onActive.textColor = onColor;
				labelStyle.onFocused.textColor = onColor;

				labelStyle.Draw( labelRect,label,false,false,selected,focused );
				orderStyle.Draw( orderRect,orderContent,false,false,selected,focused );
			}

			Event current = Event.current;
			
			switch( current.type )
			{
			case EventType.MouseDown:
				if( current.button == 0 && rect.Contains(Event.current.mousePosition) )
				{
					if (Event.current.clickCount == 2)
					{
						if( SceneView.lastActiveSceneView != null )
						{
							SceneView.lastActiveSceneView.FrameSelected();
						}
					}
					else
					{
						GUIUtility.hotControl = controlID;
						GUIUtility.keyboardControl = 0;
					}
					
					current.Use();
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID && rect.Contains(Event.current.mousePosition) )
				{
					if( EditorGUI.actionKey )
					{
						Object[] objects = Selection.objects;
						if( objects.Contains( property.gameObject ) )
						{
							ArrayUtility.Remove( ref objects,property.gameObject );
						}
						else
						{
							ArrayUtility.Add( ref objects,property.gameObject );
						}
						Selection.objects = objects;
					}
					else
					{
						Selection.activeGameObject = property.gameObject;
					}
					
					GUIUtility.hotControl = 0;
					
					current.Use();
				}
				break;
			}
		}

		void OnGUI()
		{
			_Properties.Sort( CompareProperty );

			_ScrollPos = EditorGUILayout.BeginScrollView( _ScrollPos );

			int propertyIndex = 0;

			for( int layerIndex = EditorTools.GetSortingLayerCount()-1; layerIndex >= 0 ;layerIndex-- )
			{
				int layerUserID = EditorTools.GetSortingLayerUserID( layerIndex );

				int uniqueID = EditorTools.GetSortingLayerUniqueIDFromUserID( layerUserID );

				bool foldout = _ExpandsLayers.Contains( uniqueID );

				EditorGUI.BeginChangeCheck();
				bool nextFoldout = HeaderGUI( foldout,EditorTools.GetSortingLayerName( layerIndex ),GUILayout.ExpandWidth(true) );
				if( EditorGUI.EndChangeCheck() )
				{
					if( nextFoldout )
					{
						_ExpandsLayers.Add( uniqueID );
					}
					else
					{
						_ExpandsLayers.Remove( uniqueID );
					}
				}

				if( foldout )
				{
					GUILayout.Label( GUIContent.none,Styles.separator );
				}

				while( propertyIndex < _Properties.Count )
				{
					ILayerProperty property = _Properties[propertyIndex];

					if( property.IsMissingComponent() )
					{
						propertyIndex++;
					}
					else if( property.sortingLayerID == layerUserID )
					{
						if( foldout )
						{
							PropertyGUI( property );

							GUILayout.Label( GUIContent.none,Styles.separator );
						}

						propertyIndex++;
					}
					else
					{
						break;
					}
				}
			}

			EditorGUILayout.EndScrollView();

			int controlID = EditorGUIUtility.GetControlID( FocusType.Passive );

			switch( Event.current.type )
			{
			case EventType.MouseDown:
				if( Event.current.button == 0 )
				{
					GUIUtility.hotControl = controlID;
					Selection.activeObject = null;
					Event.current.Use();
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					Event.current.Use();
				}
				break;
			}
		}
	}
}