﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Nostalgia;

namespace NostalgiaEditor
{
	public class MapResizeWindow : EditorWindow
	{
		private class Styles
		{
			public static GUIStyle background = (GUIStyle)"sv_iconselector_back";
			public static GUIStyle separator = (GUIStyle)"sv_iconselector_sep";
		}

		public static void Show( Map map,Rect rect )
		{
			MapResizeWindow window = EditorWindow.CreateInstance<MapResizeWindow>();
			window.Init( map,rect );
		}

		Map _Map;
		int _Width = 0;
		int _Height = 0;
		Map.HorizontalPivot _HorizontalPivot = Map.HorizontalPivot.Left;
		Map.VerticalPivot _VerticalPivot = Map.VerticalPivot.Bottom;

		void Init( Map map,Rect rect )
		{
			_Map = map;
			_Width = map.width;
			_Height = map.height;

			Vector2 pos = GUIUtility.GUIToScreenPoint( new Vector2(rect.x, rect.y) );
			rect.x = pos.x;
			rect.y = pos.y;

			this.ShowAsDropDown( rect,new Vector2( 200.0f,220.0f ) );
		}

		void OnGUI()
		{
			GUI.BeginGroup(new Rect(0.0f, 0.0f, this.position.width, this.position.height), Styles.background );

			EditorGUILayout.LabelField( "Size",EditorStyles.boldLabel );

			EditorGUI.indentLevel++;

			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField( "Width",GUILayout.Width( 50.0f ) );
				_Width = Mathf.Max( EditorGUILayout.IntField( _Width ),1 );
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField( "Height",GUILayout.Width( 50.0f ) );
				_Height = Mathf.Max( EditorGUILayout.IntField( _Height ),1 );
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel--;

			GUILayout.Label( GUIContent.none , Styles.separator );

			EditorGUILayout.LabelField( "Pivot",EditorStyles.boldLabel );

			int pivot = (int)_HorizontalPivot + (int)_VerticalPivot * 3;

			EditorGUILayout.BeginHorizontal();

			GUILayout.FlexibleSpace();

			EditorGUILayout.BeginVertical();

			for( int y = 0;y<3;y++ )
			{
				EditorGUILayout.BeginHorizontal();

				for( int x = 0;x<3;x++ )
				{
					int index = x + y * 3;

					bool toggle = EditorGUILayout.Toggle( index == pivot,EditorStyles.radioButton,GUILayout.Width ( 32 ),GUILayout.Height( 32 ) );
					if( toggle )
					{
						pivot = index;
					}
				}

				EditorGUILayout.EndHorizontal();
			}

			_HorizontalPivot = (Map.HorizontalPivot)(pivot % 3);
			_VerticalPivot = (Map.VerticalPivot)(pivot / 3);

			EditorGUILayout.EndVertical();

			GUILayout.FlexibleSpace();

			EditorGUILayout.EndHorizontal();

			GUILayout.Label( GUIContent.none , Styles.separator );

			GUI.backgroundColor = new Color(1f, 1f, 1f, 0.7f);

			if( GUILayout.Button ( "Resize" ) )
			{
				bool resize = true;
				
				if( _Width < _Map.width || _Height < _Map.height )
				{
					resize = EditorUtility.DisplayDialog( "Resize","The new size is smaller than the size of the now.\nPart of the map is cut off.","OK","Cancel" );
				}
				
				if( resize )
				{
					Undo.IncrementCurrentGroup();

					List<Object> objects = new List<Object>();
					objects.Add( _Map );
					foreach( Chunk chunk in _Map.chunks )
					{
						objects.Add( chunk );
					}
					foreach( Cell cell in _Map.cells )
					{
						if( cell.collider != null )
						{
							objects.Add( cell.collider );
						}
					}					
					Undo.RegisterCompleteObjectUndo( objects.ToArray(),"Resize" );
					
					_Map.Resize( _Width,_Height,_HorizontalPivot,_VerticalPivot );
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					EditorUtility.SetDirty( _Map );

					Close();
					GUIUtility.ExitGUI();
				}
			}

			GUI.backgroundColor = Color.white;

			GUI.EndGroup();
		}
	}
}