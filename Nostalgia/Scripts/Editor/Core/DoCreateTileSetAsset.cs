﻿using UnityEngine;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;
using System.Collections;
using System.IO;

using Nostalgia;

namespace NostalgiaEditor
{
	public class DoCreateTileSetAsset : EndNameEditAction 
	{
		public override void Action (int instanceId, string pathName, string resourceFile)
		{
			TileSet tileSet = ScriptableObject.CreateInstance<TileSet>();

			string directory = Path.GetDirectoryName( pathName );
			string fileName = Path.GetFileNameWithoutExtension( pathName );

			string materialPath = directory+"/"+fileName+".mat";

			tileSet.material = AssetDatabase.LoadAssetAtPath( materialPath,typeof(Material) ) as Material;
			if( tileSet.material == null )
			{
				tileSet.material = new Material( Shader.Find( "Tiles/Default") );

				AssetDatabase.CreateAsset( tileSet.material,materialPath );
			}

			AssetDatabase.CreateAsset( tileSet,pathName );

			ProjectWindowUtil.ShowCreatedAsset( tileSet );
		}
	}
}