﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;

using Nostalgia;

namespace NostalgiaEditor
{
	public class TileComponentUtility
	{
		static FieldInfo _TileField;
		static FieldInfo tileField
		{
			get
			{
				if( _TileField == null )
				{
					System.Type type = typeof(TileComponent);
					_TileField = type.GetField( "_Tile",BindingFlags.Instance | BindingFlags.NonPublic );
				}
				return _TileField;
			}
		}

		static TileComponent _CopiedComponent = null;

		public static void AddComponent( Tile tile,System.Type classType )
		{
			Undo.RecordObject( tile,"Add Component" );
			
			TileComponent component = tile.AddComponent( classType );
			Undo.RegisterCreatedObjectUndo( component,"Add Component" );
			
			AssetDatabase.AddObjectToAsset( component,tile );
			
			EditorUtility.SetDirty( tile );
		}
		
		public static void Reset( TileComponent component )
		{
			Tile tile = component.tile;
			
			System.Type classType = component.GetType();
			Undo.RecordObject( component,"Reset " + classType.Name );
			
			TileComponent initializeComponent = ScriptableObject.CreateInstance( classType ) as TileComponent;
			EditorUtility.CopySerialized( initializeComponent,component );
			
			tileField.SetValue( component,tile );
			
			MethodInfo resetMethod = classType.GetMethod( "Reset",BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );
			if( resetMethod != null )
			{
				resetMethod.Invoke( component,null );
			}
		}

		public static bool IsMoveComponentUp( TileComponent component )
		{
			Tile tile = component.tile;
			
			TileComponent[] components = tile.GetComponents<TileComponent>();
			int index = System.Array.IndexOf( components,component );
			
			return index > 0;
		}

		public static void MoveComponentUp( TileComponent component )
		{
			Tile tile = component.tile;
			
			TileComponent[] components = tile.GetComponents<TileComponent>();
			int index = System.Array.IndexOf( components,component );
			
			Undo.RecordObject( tile,"Move Up" );
			
			tile.MoveComponent( index,index-1 );
		}

		public static bool IsMoveComponentDown( TileComponent component )
		{
			Tile tile = component.tile;
			
			TileComponent[] components = tile.GetComponents<TileComponent>();
			int index = System.Array.IndexOf( components,component );
			
			return index < components.Length - 1;
		}

		public static void MoveComponentDown( TileComponent component )
		{
			Tile tile = component.tile;
			
			TileComponent[] components = tile.GetComponents<TileComponent>();
			int index = System.Array.IndexOf( components,component );
			
			Undo.RecordObject( tile,"Move Down" );
			
			tile.MoveComponent( index,index+1 );
		}

		public static void CopyComponent( TileComponent component )
		{
			if( _CopiedComponent != null )
			{
				Object.DestroyImmediate( _CopiedComponent );
				_CopiedComponent = null;
			}
			
			System.Type classType = component.GetType();
			_CopiedComponent = ScriptableObject.CreateInstance( classType ) as TileComponent;
			_CopiedComponent.hideFlags = HideFlags.HideAndDontSave;
			
			EditorUtility.CopySerialized( component,_CopiedComponent );
		}

		public static bool IsCopiedComponent()
		{
			return _CopiedComponent != null;
		}

		public static bool IsPasteComponent( TileComponent component )
		{
			return _CopiedComponent != null && component.GetType () == _CopiedComponent.GetType();
		}

		public static void PasteComponentAsNew( Tile tile )
		{
			if( !IsCopiedComponent() )
			{
				Debug.LogError( "Not copied component." );
				return;
			}

			Undo.RecordObject( tile,"AddComponent" );
			
			System.Type classType = _CopiedComponent.GetType();
			TileComponent addComponent = tile.AddComponent( classType );
			
			AssetDatabase.AddObjectToAsset( addComponent,tile );

			EditorUtility.CopySerialized( _CopiedComponent,addComponent );
			
			tileField.SetValue( addComponent,tile );
			
			Undo.RegisterCreatedObjectUndo( addComponent,"AddComponent" );

			EditorUtility.SetDirty( tile );
		}

		public static void PasteComponentValues( TileComponent component )
		{
			if( !IsPasteComponent( component ) )
			{
				Debug.LogError( "Not copied component." );
				return;
			}

			Tile tile = component.tile;
			
			System.Type classType = _CopiedComponent.GetType();
			
			Undo.RecordObject( component,"Paste " + classType.Name + " Values" );
			
			EditorUtility.CopySerialized( _CopiedComponent,component );
			
			tileField.SetValue( component,tile );
		}
	}
}