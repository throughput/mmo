﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;

using Nostalgia;

namespace NostalgiaEditor
{
	[InitializeOnLoad]
	public class HierarchyGUI
	{
		private class Styles
		{
			public static GUIStyle selection = (GUIStyle)"IN ThumbnailShadow";
		}

		static double _RepaintTimer = 0.0;

		static HierarchyGUI()
		{
			_RepaintTimer = EditorApplication.timeSinceStartup;

			EditorApplication.update += Update;
			EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyGUI;
		}

		static void Update()
		{
			double nowTime = EditorApplication.timeSinceStartup;
			if( nowTime - _RepaintTimer >= 0.05 )
			{
				EditorApplication.RepaintHierarchyWindow();
				_RepaintTimer = nowTime;
			}
		}
		
		static void OnHierarchyGUI( int instanceID,Rect selectionRect )
		{
			GameObject gameObject = EditorUtility.InstanceIDToObject( instanceID ) as GameObject;
			if( !gameObject )
			{
				return;
			}

			int controlID = EditorGUIUtility.GetControlID( GUIContent.none,FocusType.Passive,selectionRect );

			if( selectionRect.Contains( Event.current.mousePosition ) && Event.current.type == EventType.Repaint &&
			   DragAndDrop.visualMode == DragAndDropVisualMode.None && 
			   !Selection.gameObjects.Contains(gameObject) )
			{
				Styles.selection.Draw( selectionRect,GUIContent.none,controlID,true );
			}

			Rect toggleRect = new Rect( selectionRect );
			toggleRect.x = toggleRect.x + toggleRect.width - 20.0f;
			toggleRect.width = 16.0f;

			EditorGUI.BeginChangeCheck();
			bool active = EditorGUI.Toggle( toggleRect,gameObject.activeSelf );
			if( EditorGUI.EndChangeCheck() )
			{
				Undo.RecordObject( gameObject,"Change Active" );

				gameObject.SetActive( active );

				EditorUtility.SetDirty( gameObject );
			}
		}
	}
}