﻿using UnityEngine;
using UnityEditor;
using System.IO;

using Nostalgia;

namespace NostalgiaEditor
{
	public class TileSetCreator
	{
		static readonly Texture2D _ScriptableObjectIcon = EditorGUIUtility.FindTexture("ScriptableObject Icon" );

		[MenuItem("Assets/Create/Nostalgia/TileSet")]
		public static void CreateAsset()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists( 0,ScriptableObject.CreateInstance<DoCreateTileSetAsset>(),"New TileSet.asset",_ScriptableObjectIcon,null );
		}

		[MenuItem("Assets/Create/Nostalgia/TileSet from selected Texture or Material")]
		public static void CreateAssetFromSelection()
		{
			string assetPath = AssetDatabase.GetAssetPath( Selection.activeObject );

			string directory = Path.GetDirectoryName( assetPath );
			string fileName = Path.GetFileNameWithoutExtension( assetPath );

			string pathName = AssetDatabase.GenerateUniqueAssetPath( directory + "/" + fileName + ".asset" );

			TileSet tileSet = ScriptableObject.CreateInstance<TileSet>();

			if( Selection.activeObject is Material )
			{
				tileSet.material = Selection.activeObject as Material;
			}
			else if( Selection.activeObject is Texture2D )
			{
				string materialPath = directory + "/" + fileName + ".mat";

				tileSet.material = AssetDatabase.LoadAssetAtPath( materialPath,typeof(Material) ) as Material;

				if( tileSet.material != null && tileSet.material.mainTexture != Selection.activeObject )
				{
					tileSet.material = null;

					materialPath = AssetDatabase.GenerateUniqueAssetPath( materialPath );
				}

				if( tileSet.material == null )
				{
					tileSet.material = new Material( Shader.Find( "Tiles/Default" ) );

					tileSet.material.mainTexture = Selection.activeObject as Texture2D;

					AssetDatabase.CreateAsset( tileSet.material,materialPath );
				}
			}

			AssetDatabase.CreateAsset( tileSet,pathName );
			
			ProjectWindowUtil.ShowCreatedAsset( tileSet );
		}

		[MenuItem("Assets/Create/Nostalgia/TileSet from selected Texture or Material",true)]
		public static bool ValidateCreateAssetFromSelection()
		{
			return Selection.activeObject is Texture2D || Selection.activeObject is Material;
		}
	}
}