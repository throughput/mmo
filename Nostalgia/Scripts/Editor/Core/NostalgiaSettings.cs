﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

namespace NostalgiaEditor
{
	public class NostalgiaSettings : ScriptableObject
	{
		static NostalgiaSettings _Instance;
		private static NostalgiaSettings instance
		{
			get
			{
				if( _Instance == null )
				{
					Load();
				}
				return _Instance;
			}
		}

		NostalgiaSettings()
		{
			_Instance = this;
		}

		private static readonly string _FilePath = "Library/NostalgiaSettings.asset";

		[SerializeField] private int _CursorWidth = 1;
		[SerializeField] private int _CursorHeight = 1;
		[SerializeField] private Color _BrushColor = Color.white;
		[SerializeField] private float _BrushRadius = 0.5f;

		public static int cursorWidth
		{
			get
			{
				return instance._CursorWidth;
			}
			set
			{
				if( instance._CursorWidth != value )
				{
					instance._CursorWidth = value;

					Save();
				}
			}
		}

		public static int cursorHeight
		{
			get
			{
				return instance._CursorHeight;
			}
			set
			{
				if( instance._CursorHeight != value )
				{
					instance._CursorHeight = value;

					Save();
				}
			}
		}

		public static Color brushColor
		{
			get
			{
				return instance._BrushColor;
			}
			set
			{
				if( instance._BrushColor != value )
				{
					instance._BrushColor = value;

					Save();
				}
			}
		}

		public static float brushRadius
		{
			get
			{
				return instance._BrushRadius;
			}
			set
			{
				if( instance._BrushRadius != value )
				{
					instance._BrushRadius = value;

					Save();
				}
			}
		}


		static void Load()
		{
			UnityEditorInternal.InternalEditorUtility.LoadSerializedFileAndForget( "Library/NostalgiaSettings.asset" );
			if( _Instance == null )
			{
				_Instance = ScriptableObject.CreateInstance<NostalgiaSettings>();
				_Instance.hideFlags = HideFlags.HideAndDontSave;
			}
		}

		static void Save()
		{
			string directoryName = Path.GetDirectoryName( _FilePath );
			if( !Directory.Exists( directoryName ) )
			{
				Directory.CreateDirectory( directoryName );
			}
			UnityEditorInternal.InternalEditorUtility.SaveToSerializedFileAndForget( new Object[]{_Instance},_FilePath,false );
		}
	}
}