﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Nostalgia;

namespace NostalgiaEditor
{
	[CustomEditor(typeof(SortingLayer))]
	public class SortingLayerInspector : Editor
	{
		SerializedObject _RendererObject;
		SerializedObject rendererObject
		{
			get
			{
				if( _RendererObject == null )
				{
					SortingLayer sortingLayer = serializedObject.targetObject as SortingLayer;
					Renderer renderer = sortingLayer.GetComponent<Renderer>();

					if( renderer != null )
					{
						_RendererObject = new SerializedObject( renderer );
					}
				}
				else if( _RendererObject.targetObject == null )
				{
					_RendererObject.Dispose();
					_RendererObject = null;
				}
				return _RendererObject;
			}
		}

		public override void OnInspectorGUI()
		{
			SerializedObject so = rendererObject;
			if( so != null )
			{
				so.Update();

				EditorTools.LayoutSortingLayerField( so.FindProperty( "m_SortingLayerID" ),new GUIContent("Sorting Layer") );
				EditorGUILayout.PropertyField( so.FindProperty( "m_SortingOrder" ),new GUIContent("Order in Layer") );

				so.ApplyModifiedProperties();
			}
			else
			{
				EditorGUILayout.HelpBox( "Please add a Renderer to this GameObject.",MessageType.Warning );
			}
		}
	}
}