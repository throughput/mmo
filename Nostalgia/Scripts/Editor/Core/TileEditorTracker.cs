﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Nostalgia;

namespace NostalgiaEditor
{
	public sealed class TileEditorTracker
	{
		private List<Editor> _ActiveEditors = new List<Editor>();
		private Dictionary<int,Editor> _DicEditors = new Dictionary<int, Editor>();

		public Editor[] activeEditors
		{
			get
			{
				return _ActiveEditors.ToArray();
			}
		}

		private Tile _Tile = null;

		public TileEditorTracker( Tile tile )
		{
			_Tile = tile;
			ForceRebuild();
		}

		public void RebuildIfNecessary()
		{
			if( _Tile == null )
			{
				return;
			}

			List<Editor> editors = new List<Editor>();
			Dictionary<int,Editor> dicEditors = new Dictionary<int, Editor>();

			foreach( TileComponent component in _Tile.GetComponents<TileComponent>() )
			{
				int instanceID = component.GetInstanceID();

				Editor editor;
				if( _DicEditors.TryGetValue( instanceID,out editor ) )
				{
					_ActiveEditors.Remove( editor );
				}
				else
				{
					editor = Editor.CreateEditor( component );
				}

				editors.Add( editor );
				dicEditors.Add( instanceID,editor );
			}

			foreach( Editor editor in _ActiveEditors )
			{
				Object.DestroyImmediate( editor );
			}
			_ActiveEditors.Clear();
			_DicEditors.Clear();

			_ActiveEditors = editors;
			_DicEditors = dicEditors;
		}

		public void ForceRebuild()
		{
			if( _Tile == null )
			{
				return;
			}
			
			Destroy();
			
			foreach( TileComponent component in _Tile.GetComponents<TileComponent>() )
			{
				Editor editor = Editor.CreateEditor( component );
				
				_ActiveEditors.Add( editor );
				_DicEditors.Add( component.GetInstanceID(),editor );
			}
		}

		public void Destroy()
		{
			foreach( Editor editor in _ActiveEditors )
			{
				Object.DestroyImmediate( editor );
			}
			_ActiveEditors.Clear();
			_DicEditors.Clear();
		}

		public void Dispose()
		{
			Destroy();
		}
	}
}