﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Nostalgia;

namespace NostalgiaEditor
{
	[InitializeOnLoad]
	public sealed class AddTileComponentMenu
	{
		public class Item
		{
			public System.Type classType;
			public string menuName;
		}
		static List<Item> _Items = new List<Item>();

		static AddTileComponentMenu()
		{
			foreach( MonoScript script in MonoImporter.GetAllRuntimeMonoScripts() )
			{
				if( script != null && script.hideFlags == 0 )
				{
					System.Type classType = script.GetClass();
					if( classType != null && classType.IsSubclassOf( typeof(TileComponent) ) )
					{
						string menuName = "Scripts/"+classType.Name;

						AddTileMenu[] addTileMenus = (AddTileMenu[])classType.GetCustomAttributes( typeof(AddTileMenu),false );
						if( addTileMenus != null && addTileMenus.Length > 0 )
						{
							menuName = addTileMenus[0].menuName;
						}

						if( !string.IsNullOrEmpty(menuName) )
						{
							Item item = new Item();
							item.classType = classType;
							item.menuName = menuName;

							_Items.Add( item );
						}
					}
				}
			}
		}

		static void AddComponent( object obj )
		{
			KeyValuePair<Tile,System.Type> pair = (KeyValuePair<Tile,System.Type>)obj;

			TileComponentUtility.AddComponent( pair.Key,pair.Value );
		}

		public static void Open( Rect rect,Tile tile )
		{
			GenericMenu menu = new GenericMenu();
			foreach( Item item in _Items )
			{
				menu.AddItem( new GUIContent(item.menuName),false,AddComponent,new KeyValuePair<Tile,System.Type>( tile,item.classType ) );
			}
			menu.DropDown( rect );
		}
	}
}
