﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Nostalgia;

namespace NostalgiaEditor
{
	public class MapCreator
	{
		[MenuItem("GameObject/Create Other/Nostalgia Map",false,2000)]
		public static void Create()
		{
			GameObject gameObject = new GameObject("Map", typeof(Map) );
			Undo.RegisterCreatedObjectUndo( gameObject, "Create Nostalgia Map" );
			Selection.activeObject = gameObject;
		}
	}
}