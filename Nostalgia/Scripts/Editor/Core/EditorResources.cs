﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace NostalgiaEditor
{
	public class EditorResources : ScriptableObject
	{
		private static EditorResources _Instance;
		private static EditorResources instance
		{
			get
			{
				if( _Instance == null )
				{
					_Instance = ScriptableObject.CreateInstance<EditorResources>();
					_Instance.hideFlags = HideFlags.HideAndDontSave;
				}
				return _Instance;
			}
		}

		private string _EditorDirectory;

		void OnEnable()
		{
			GUIContent content = null;
			if( !_Contents.TryGetValue( name,out content ) )
			{
				MonoScript script = MonoScript.FromScriptableObject( this );
				
				if( script != null )
				{
					string directory = AssetDatabase.GetAssetPath( script );
					
					_EditorDirectory = Path.GetDirectoryName( directory );
				}
			}
		}

		private Dictionary<string,GUIContent> _Contents = new Dictionary<string, GUIContent>();

		public static GUIContent FindTextureContent( string name )
		{
			GUIContent content = null;
			if( !instance._Contents.TryGetValue( name,out content ) )
			{
				string path = instance._EditorDirectory + "/" + name;
				
				content = new GUIContent( AssetDatabase.LoadAssetAtPath( path,typeof(Texture2D)) as Texture2D );
				
				instance._Contents.Add( name,content );
			}

			return content;
		}
	}
}