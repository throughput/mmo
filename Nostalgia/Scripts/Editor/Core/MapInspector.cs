using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Nostalgia;

namespace NostalgiaEditor
{
	[CustomEditor(typeof(Map))]
	public class MapInspector : Editor
	{
		public enum ToolType
		{
			None = -1,
			Pencil = 0,
			Rectangle,
			Bucket,
			Eraser,
			RectangleEraser,
			EyeDropper,
			ColorBrush,
			RectangleColorBrush,
		}
		
		static ToolType _ToolType = ToolType.None;

		private ToolType selectedTool
		{
			get
			{
				if( Tools.current == Tool.None )
				{
					return _ToolType;
				}
				else
				{
					return ToolType.None;
				}
			}
			set
			{
				if (value != ToolType.None)
				{
					Tools.current = Tool.None;
				}
				_ToolType = value;
			}
		}

		private GUIContent[] _ToolTypeContent;
		private GUIContent[] toolTypeContent
		{
			get
			{
				if( _ToolTypeContent == null )
				{
					_ToolTypeContent = new GUIContent[]{
						EditorResources.FindTextureContent( "Textures/EditorPencil.png" ),
						EditorResources.FindTextureContent( "Textures/EditorRectangle.png" ),
						EditorResources.FindTextureContent( "Textures/EditorBucket.png" ),
						EditorResources.FindTextureContent( "Textures/EditorEraser.png" ),
						EditorResources.FindTextureContent( "Textures/EditorRectangleEraser.png" ),
						EditorResources.FindTextureContent( "Textures/EditorEyeDropper.png" ),
						EditorResources.FindTextureContent( "Textures/EditorBrush.png" ),
						EditorResources.FindTextureContent( "Textures/EditorRectangleBrush.png" ),
					};
				}
				return _ToolTypeContent;
			}
		}

		int _CurrentTileID = 0;
		int _CurrentPartsID = 0;
		
		private Map _Map = null;

		private List<Object> records
		{
			get
			{
				List<Object> objects = new List<Object>();
				objects.Add( _Map );
				foreach( Chunk chunk in _Map.chunks )
				{
					objects.Add( chunk );
				}
				return objects;
			}
		}
		
		void OnEnable()
		{
			_Map = target as Map;
		}

		private class Styles
		{
			public static GUIStyle dropDown = (GUIStyle)"MiniPullDown";
		}

		void ResizeButton()
		{
			GUIContent label = new GUIContent( "Resize" );
			
			Rect rect = GUILayoutUtility.GetRect( label,Styles.dropDown );
			
			if( EditorTools.ButtonMouseDown( rect,label,Styles.dropDown ) )
			{
				MapResizeWindow.Show ( _Map,rect );
			}
		}

		void TilesGUI()
		{
			if( _Map.tileSet.material != null && _Map.tileSet.material.mainTexture != null && _Map.tileSet.tiles != null && _Map.tileSet.tiles.Length > 0 )
			{
				EditorGUILayout.LabelField( "Tiles",EditorStyles.boldLabel );
				
				_CurrentTileID = EditorTools.SelectTileField( _Map.tileSet,_CurrentTileID );

				Tile currentTile = _Map.tileSet.tiles[_CurrentTileID];
				if( currentTile.type != Tile.Type.Normal )
				{				
					EditorGUILayout.Space();
					
					EditorGUILayout.LabelField( "Shift Key Parts",EditorStyles.boldLabel );
					
					_CurrentPartsID = EditorTools.SelectPartsField( _Map.tileSet.tiles[_CurrentTileID],_CurrentPartsID );
				}
			}
		}

		void PencilGUI()
		{
			EditorGUILayout.LabelField( "Cursor",EditorStyles.boldLabel );

			NostalgiaSettings.cursorWidth = Mathf.Max( EditorGUILayout.IntField( "Width",NostalgiaSettings.cursorWidth ),1 );
			NostalgiaSettings.cursorHeight = Mathf.Max( EditorGUILayout.IntField( "Height",NostalgiaSettings.cursorHeight ),1 );

			TilesGUI();
		}

		void RectangleGUI()
		{
			TilesGUI();
		}

		void BucketGUI()
		{
			TilesGUI();
		}

		void EraserGUI()
		{
			EditorGUILayout.LabelField( "Cursor",EditorStyles.boldLabel );
			NostalgiaSettings.cursorWidth = Mathf.Max( EditorGUILayout.IntField( "Width",NostalgiaSettings.cursorWidth ),1 );
			NostalgiaSettings.cursorHeight = Mathf.Max( EditorGUILayout.IntField( "Height",NostalgiaSettings.cursorHeight ),1 );
		}

		void RectangleEraserGUI()
		{
		}

		void EyeDropperGUI()
		{
			TilesGUI();
		}

		void ColorBrushGUI()
		{
			NostalgiaSettings.brushColor = EditorGUILayout.ColorField( "Color",NostalgiaSettings.brushColor );
			NostalgiaSettings.brushRadius = EditorGUILayout.Slider( "Radius",NostalgiaSettings.brushRadius,0.1f,10.0f );
		}

		void RectangleColorBrushGUI()
		{
			NostalgiaSettings.brushColor = EditorGUILayout.ColorField( "Color",NostalgiaSettings.brushColor );
		}
		
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			EditorGUILayout.PropertyField( serializedObject.FindProperty( "_TileSet" ) );

			EditorGUILayout.Space ();

			EditorTools.LayoutSortingLayerField( serializedObject.FindProperty( "_SortingLayerID" ),new GUIContent("Sorting Layer") );
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "_SortingOrder" ),new GUIContent("Order in Layer") );

			EditorGUILayout.Space ();

			EditorGUILayout.PropertyField( serializedObject.FindProperty( "_Color" ) );

			EditorGUILayout.Space ();

			GUILayout.BeginHorizontal();
			{
				GUILayout.FlexibleSpace();

				ResizeButton();
			}
			GUILayout.EndHorizontal();

			if( _Map.tileSet != null )
			{
				GUILayout.BeginHorizontal();
				{
					GUILayout.FlexibleSpace();

					GUILayout.BeginVertical();
					{
						selectedTool = (ToolType)GUILayout.Toolbar( (int)selectedTool, toolTypeContent );
					}
					GUILayout.EndVertical();

					GUILayout.FlexibleSpace();
				}
				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();
				{
					GUILayout.FlexibleSpace();

					if( GUILayout.Button( "All Clear",EditorStyles.miniButton ) )
					{
						Undo.IncrementCurrentGroup();
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"All Clear" );
						
						_Map.AllClear();
						
						Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
						
						EditorUtility.SetDirty( _Map );
					}

					if( GUILayout.Button( "Refresh",EditorStyles.miniButton ) )
					{
						Undo.IncrementCurrentGroup();
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"Refresh" );
						
						_Map.Refresh();
						
						EditorUtility.SetDirty( _Map );
					}
				}
				GUILayout.EndHorizontal();

				switch( selectedTool )
				{
				case ToolType.Pencil:
					PencilGUI();
					break;
				case ToolType.Rectangle:
					RectangleGUI();
					break;
				case ToolType.Bucket:
					BucketGUI();
					break;
				case ToolType.Eraser:
					EraserGUI();
					break;
				case ToolType.RectangleEraser:
					RectangleEraserGUI();
					break;
				case ToolType.EyeDropper:
					EyeDropperGUI();
					break;
				case ToolType.ColorBrush:
					ColorBrushGUI();
					break;
				case ToolType.RectangleColorBrush:
					RectangleColorBrushGUI();
					break;
				}
			}
			else
			{
				EditorGUILayout.HelpBox( "Please set TileSet.",MessageType.Warning );
			}

			serializedObject.ApplyModifiedProperties();

			if( ( Event.current.type == EventType.ValidateCommand || Event.current.type == EventType.ExecuteCommand ) && Event.current.commandName == "UndoRedoPerformed" )
			{
				_Map.SetDirty();
			}
		}

		void DrawCursor( Vector2 pos,float width,float height )
		{
			Matrix4x4 savedMatrix = Handles.matrix;
			Handles.matrix = _Map.transform.localToWorldMatrix * Matrix4x4.TRS( pos,Quaternion.identity,Vector3.one );
			
			Vector3[] lines = new Vector3[]{ 
				Vector3.zero,
				new Vector3( width,0.0f ),
				new Vector3( width,height ),
				new Vector3( 0.0f,height ),
			};
			
			Color faceColor = Color.cyan;
			faceColor.a = 0.2f;
			
			Handles.DrawSolidRectangleWithOutline( lines,faceColor,Color.cyan );
			
			Handles.matrix = savedMatrix;
			
			HandleUtility.Repaint();
		}

		void DrawCursor( Point2 pos,int width,int height )
		{
			DrawCursor( _Map.MapPointToLocalPoint( pos ),width,height );
		}

		void DrawCircle( Vector2 pos,float radius )
		{
			Matrix4x4 savedMatrix = Handles.matrix;
			Handles.matrix = _Map.transform.localToWorldMatrix * Matrix4x4.TRS( pos,Quaternion.identity,Vector3.one );
			
			Color faceColor = Color.cyan;
			faceColor.a = 0.2f;
			
			Color savedColor = Handles.color;
			
			Handles.color = faceColor;
			
			Handles.DrawSolidDisc( Vector3.zero,Vector3.back,radius );
			
			Handles.color = Color.cyan;
			
			Handles.DrawWireDisc( Vector3.zero,Vector3.back,radius );
			
			Handles.color = savedColor;
			Handles.matrix = savedMatrix;
			
			HandleUtility.Repaint();
		}
		
		void DrawGrid()
		{
			Color oldColor = Handles.color;
			Handles.color = Color.gray;
			
			Vector3 min = new Vector3( 0.0f,0.0f,0.0f );
			Vector3 max = new Vector3( _Map.width,_Map.height,0.0f );
			
			for( int x=0;x<=_Map.width;x++ )
			{
				Vector3 startPos = _Map.transform.TransformPoint( min.x + x,min.y,0.0f );
				Vector3 endPos = _Map.transform.TransformPoint( min.x + x,max.y,0.0f );

				Handles.DrawLine( startPos,endPos );
			}
			
			for( int y=0;y<=_Map.height;y++ )
			{
				Vector3 startPos = _Map.transform.TransformPoint( min.x,min.y + y,0.0f );
				Vector3 endPos = _Map.transform.TransformPoint( max.x,min.y + y,0.0f );
				
				Handles.DrawLine( startPos,endPos );
			}
			
			Handles.color = oldColor;
		}

		void UpdatePencil()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);

			Event current = Event.current;

			EventType eventType = current.GetTypeForControl( controlID );

			if( eventType == EventType.MouseUp )
			{
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();
					
					System.GC.Collect();
				}
			}

			Point2 pos;
			
			if( !_Map.RayToPoint( HandleUtility.GUIPointToWorldRay( current.mousePosition ),out pos ) )
			{
				return;
			}

			int cursorWidth = NostalgiaSettings.cursorWidth;
			int cursorHeight = NostalgiaSettings.cursorHeight;

			Tile currentTile = _Map.tileSet.tiles[_CurrentTileID];

			if( currentTile.type == Tile.Type.Normal )
			{
				cursorWidth = Mathf.Max(cursorWidth,currentTile.width);
				cursorHeight = Mathf.Max(cursorHeight,currentTile.height);
			}
			
			Point2 minPos = new Point2( pos.x-cursorWidth/2,pos.y-cursorHeight/2 );
			
			DrawCursor( minPos,cursorWidth,cursorHeight );

			switch( eventType )
			{
			case EventType.MouseDown:
				if( current.button == 0 )
				{
					GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;

					Undo.IncrementCurrentGroup();
					Undo.RegisterCompleteObjectUndo( records.ToArray(),"Put Tile" );
					
					if( _Map.FillTile( minPos,cursorWidth,cursorHeight,_CurrentTileID,_CurrentPartsID,!current.shift ) )
					{
						EditorUtility.SetDirty( _Map );
					}
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );

					current.Use ();
				}
				break;
			case EventType.MouseDrag:
				if( GUIUtility.hotControl == controlID )
				{
					if( current.button == 0 )
					{
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"Put Tile" );
						
						if( _Map.FillTile( minPos,cursorWidth,cursorHeight,_CurrentTileID,_CurrentPartsID,!current.shift ) )
						{
							EditorUtility.SetDirty( _Map );
						}
						
						Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );

						current.Use ();
					}
				}
				break;
			}
		}

		Point2 _BeginPos;

		void UpdateRectabgle()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;

			Point2 pos;
			
			bool hit = _Map.RayToPoint( HandleUtility.GUIPointToWorldRay( current.mousePosition ),out pos );
			
			Point2 minPos = new Point2();
			int width = 1;
			int height = 1;

			if( GUIUtility.hotControl == controlID )
			{
				minPos.x = Mathf.Min( pos.x,_BeginPos.x );
				minPos.y = Mathf.Min( pos.y,_BeginPos.y );

				width = Mathf.Max( pos.x,_BeginPos.x ) - minPos.x + 1;
				height = Mathf.Max( pos.y,_BeginPos.y ) - minPos.y + 1;

				DrawCursor( minPos,width,height );
			}
			else
			{
				if( hit )
				{
					DrawCursor( pos,1,1 );
				}
			}

			switch( current.GetTypeForControl( controlID ) )
			{
			case EventType.MouseDown:
				if( hit)
				{
					if( current.button == 0 )
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;

						_BeginPos = pos;
						
						current.Use ();
					}
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;

					Undo.IncrementCurrentGroup();
					Undo.RegisterCompleteObjectUndo( records.ToArray(),"Fill Tile" );
					
					if( _Map.FillTile( minPos,width,height,_CurrentTileID,_CurrentPartsID,!current.shift ) )
					{
						EditorUtility.SetDirty( _Map );
					}
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );

					current.Use ();

					System.GC.Collect();
				}
				break;
			}
		}

		void UpdateBucket()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;
			
			Point2 pos;
			
			bool hit = _Map.RayToPoint( HandleUtility.GUIPointToWorldRay( current.mousePosition ),out pos );
			
			if( hit )
			{
				DrawCursor( pos,1,1 );
			}

			bool dirty = false;
			
			switch( current.GetTypeForControl( controlID ) )
			{
			case EventType.MouseDown:
				if( hit)
				{
					if( current.button == 0 )
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
						
						Undo.IncrementCurrentGroup();
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"Bucket Tile" );
						
						if( _Map.BucketTile( pos,_CurrentTileID,_CurrentPartsID,!current.shift ) )
						{
							dirty = true;
						}

						Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
						
						current.Use ();
					}
				}
				break;
			case EventType.MouseDrag:
				if( GUIUtility.hotControl == controlID && hit )
				{
					if( current.button == 0 )
					{
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"Bukect Tile" );

						if( _Map.BucketTile( pos,_CurrentTileID,_CurrentPartsID,!current.shift ) )
						{
							dirty = true;
						}

						Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );

						current.Use ();
					}
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();

					System.GC.Collect();
				}
				break;
			}
			
			if( dirty )
			{
				EditorUtility.SetDirty( _Map );
			}
		}

		void UpdateEraser()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;

			EventType eventType = current.GetTypeForControl( controlID );

			if( eventType == EventType.MouseUp )
			{
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();
					
					System.GC.Collect();
				}
			}
			
			Point2 pos;
			
			if( !_Map.RayToPoint( HandleUtility.GUIPointToWorldRay( current.mousePosition ),out pos ) )
			{
				return;
			}

			int cursorWidth = NostalgiaSettings.cursorWidth;
			int cursorHeight = NostalgiaSettings.cursorHeight;
			
			Point2 minPos = new Point2( pos.x-cursorWidth/2,pos.y-cursorHeight/2 );
			
			DrawCursor( minPos,cursorWidth,cursorHeight );

			bool dirty = false;
			
			switch( eventType )
			{
			case EventType.MouseDown:
				if( current.button == 0 )
				{
					GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
					
					Undo.IncrementCurrentGroup();
					Undo.RegisterCompleteObjectUndo( records.ToArray(),"Erase Tile" );
					
					for( int x=0;x<cursorWidth;x++ )
					{
						for( int y=0;y<cursorHeight;y++ )
						{
							if( _Map.RemoveTile( minPos+new Point2(x,y),!current.shift ) )
							{
								dirty = true;
							}
						}
					}
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );

					current.Use ();
				}
				break;
			case EventType.MouseDrag:
				if( GUIUtility.hotControl == controlID )
				{
					if( current.button == 0 )
					{
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"Erase Tile" );
						
						for( int x=0;x<cursorWidth;x++ )
						{
							for( int y=0;y<cursorHeight;y++ )
							{
								if( _Map.RemoveTile( minPos+new Point2(x,y),!current.shift ) )
								{
									dirty = true;
								}
							}
						}
						
						Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );

						current.Use ();
					}
				}
				break;
			}
			
			if( dirty )
			{
				EditorUtility.SetDirty( _Map );
			}
		}

		void UpdateRectabgleEraser()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;
			
			Point2 pos;
			
			bool hit = _Map.RayToPoint( HandleUtility.GUIPointToWorldRay( current.mousePosition ),out pos );
			
			Point2 minPos = new Point2();
			int width = 1;
			int height = 1;
			
			if( GUIUtility.hotControl == controlID )
			{
				minPos.x = Mathf.Min( pos.x,_BeginPos.x );
				minPos.y = Mathf.Min( pos.y,_BeginPos.y );
				
				width = Mathf.Max( pos.x,_BeginPos.x ) - minPos.x + 1;
				height = Mathf.Max( pos.y,_BeginPos.y ) - minPos.y + 1;
				
				DrawCursor( minPos,width,height );
			}
			else
			{
				if( hit )
				{
					DrawCursor( pos,1,1 );
				}
			}
			
			switch( current.GetTypeForControl( controlID ) )
			{
			case EventType.MouseDown:
				if( hit)
				{
					if( current.button == 0 )
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
						
						_BeginPos = pos;
						
						current.Use ();
					}
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.IncrementCurrentGroup();
					Undo.RegisterCompleteObjectUndo( records.ToArray(),"Erase Tile" );

					bool dirty = false;
					for( int x=0;x<width;x++ )
					{
						for( int y=0;y<height;y++ )
						{
							if( _Map.RemoveTile( minPos+new Point2(x,y),!current.shift ) )
							{
								dirty = true;
							}
						}
					}

					if( dirty )
					{
						EditorUtility.SetDirty( _Map );
					}
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();

					System.GC.Collect();
				}
				break;
			}
		}

		void UpdateEyeDropper()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;
			
			Point2 pos;
			
			bool hit = _Map.RayToPoint( HandleUtility.GUIPointToWorldRay( current.mousePosition ),out pos );
			
			if( hit )
			{
				DrawCursor( pos,1,1 );
			}

			switch( current.GetTypeForControl( controlID ) )
			{
			case EventType.MouseDown:
				if( hit)
				{
					if( current.button == 0 )
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;

						Cell cell = _Map.GetCell( pos );
						if( cell != null )
						{
							_CurrentTileID = cell.tileID;
							if( current.shift )
							{
								_CurrentPartsID = cell.partsID;
							}
							else
							{
								_CurrentPartsID = 0;
							}
							Repaint();
						}
						
						current.Use ();
					}
				}
				break;
			case EventType.MouseDrag:
				if( GUIUtility.hotControl == controlID && hit )
				{
					if( current.button == 0 )
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
						
						Cell cell = _Map.GetCell( pos );
						if( cell != null )
						{
							_CurrentTileID = cell.tileID;
							if( current.shift )
							{
								_CurrentPartsID = cell.partsID;
							}
							else
							{
								_CurrentPartsID = 0;
							}
							Repaint();
						}
						
						current.Use ();
					}
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();
				}
				break;
			}
		}

		void UpdateColorBrush()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;
			
			EventType eventType = current.GetTypeForControl( controlID );
			
			if( eventType == EventType.MouseUp )
			{
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();
					
					System.GC.Collect();
				}
			}
			
			Vector2 pos = new Vector2();
			
			if( !_Map.Raycast( HandleUtility.GUIPointToWorldRay( current.mousePosition ),ref pos ) )
			{
				return;
			}

			float brushRadius = NostalgiaSettings.brushRadius;
			Color brushColor = NostalgiaSettings.brushColor;

			DrawCircle( pos,brushRadius );

			switch( eventType )
			{
			case EventType.KeyDown:
				if( current.keyCode == KeyCode.LeftBracket )
				{
					NostalgiaSettings.brushRadius = Mathf.Clamp ( brushRadius - 0.1f,0.1f,10.0f );
				}
				else if( current.keyCode == KeyCode.RightBracket )
				{
					NostalgiaSettings.brushRadius = Mathf.Clamp ( brushRadius + 0.1f,0.1f,10.0f );
				}
				Repaint();
				break;
			case EventType.MouseDown:
				if( current.button == 0 )
				{
					GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
					
					Undo.IncrementCurrentGroup();
					Undo.RegisterCompleteObjectUndo( records.ToArray(),"Color Brush" );

					if( _Map.ColorBrush( pos,brushRadius,brushColor,current.shift ) )
					{
						EditorUtility.SetDirty( _Map );
					}

					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();
				}
				break;
			case EventType.MouseDrag:
				if( GUIUtility.hotControl == controlID )
				{
					if( current.button == 0 )
					{
						Undo.RegisterCompleteObjectUndo( records.ToArray(),"Color Brush" );

						if( _Map.ColorBrush( pos,brushRadius,brushColor,current.shift ) )
						{
							EditorUtility.SetDirty( _Map );
						}

						Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
						
						current.Use ();
					}
				}
				break;
			}
		}

		Vector2 _MouseDownPosition;

		void UpdateRectangleColorBrush()
		{
			int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			
			Event current = Event.current;
			
			Vector2 pos = new Vector2();
			
			bool hit = _Map.Raycast( HandleUtility.GUIPointToWorldRay( current.mousePosition ),ref pos );
			
			Vector2 minPos = new Vector2();
			float width = 1;
			float height = 1;
			
			if( GUIUtility.hotControl == controlID )
			{
				minPos.x = Mathf.Min( pos.x,_MouseDownPosition.x );
				minPos.y = Mathf.Min( pos.y,_MouseDownPosition.y );
				
				width = Mathf.Max( pos.x,_MouseDownPosition.x ) - minPos.x;
				height = Mathf.Max( pos.y,_MouseDownPosition.y ) - minPos.y;
				
				DrawCursor( minPos,width,height );
			}

			Color brushColor = NostalgiaSettings.brushColor;

			switch( current.GetTypeForControl( controlID ) )
			{
			case EventType.MouseDown:
				if( hit)
				{
					if( current.button == 0 )
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;
						
						_MouseDownPosition = pos;
						
						current.Use ();
					}
				}
				break;
			case EventType.MouseUp:
				if( GUIUtility.hotControl == controlID )
				{
					GUIUtility.hotControl = 0;
					GUIUtility.keyboardControl = 0;
					
					Undo.IncrementCurrentGroup();
					Undo.RegisterCompleteObjectUndo( records.ToArray(),"Color Brush" );

					if( _Map.ColorBrushRectangle( minPos,width,height,brushColor,current.shift ) )
					{
						EditorUtility.SetDirty( _Map );
					}
					
					Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
					
					current.Use ();
					
					System.GC.Collect();
				}
				break;
			}
		}
		
		public void OnSceneGUI()
		{
			if( !_Map.gameObject.activeInHierarchy || !_Map.enabled )
			{
				return;
			}

			DrawGrid();

			switch( selectedTool )
			{
			case ToolType.Pencil:
				UpdatePencil();
				break;
			case ToolType.Rectangle:
				UpdateRectabgle();
				break;
			case ToolType.Bucket:
				UpdateBucket();
				break;
			case ToolType.Eraser:
				UpdateEraser();
				break;
			case ToolType.RectangleEraser:
				UpdateRectabgleEraser();
				break;
			case ToolType.EyeDropper:
				UpdateEyeDropper();
				break;
			case ToolType.ColorBrush:
				UpdateColorBrush();
				break;
			case ToolType.RectangleColorBrush:
				UpdateRectangleColorBrush();
				break;
			}
		}

		private PreviewRenderUtility _PreviewUtility;
		
		void InitPreview()
		{
			if( _PreviewUtility != null )
			{
				return;
			}
			
			_PreviewUtility = new PreviewRenderUtility();
			_PreviewUtility.m_CameraFieldOfView = 30f;
		}

		public override bool HasPreviewGUI ()
		{
			return true;
		}

		public override void OnPreviewSettings ()
		{
			if( !ShaderUtil.hardwareSupportsRectRenderTexture )
			{
				return;
			}
			GUI.enabled = true;
			InitPreview();
		}

		void DoRenderPreview()
		{
			Map map = target as Map;

			bool fog = RenderSettings.fog;
			Unsupported.SetRenderSettingsUseFogNoDirty( false );

			Vector3 size = new Vector3( map.width,map.height,0.0f );
			Bounds bounds = new Bounds( size*0.5f,size );

			float magnitude = bounds.extents.magnitude;
			float num = magnitude * 2;

			Camera camera = _PreviewUtility.m_Camera;
			camera.transform.position = -Vector3.forward * num + bounds.center;
			camera.transform.rotation = Quaternion.identity;
			camera.nearClipPlane = num - magnitude * 1.1f;
			camera.farClipPlane = num + magnitude * 1.1f;
			camera.clearFlags = CameraClearFlags.Color;
			camera.backgroundColor = Color.clear;

			foreach( Chunk chunk in map.chunks )
			{
				_PreviewUtility.DrawMesh( chunk.cachedMesh,chunk.transform.localPosition,Quaternion.identity,map.material,0 );
			}
			camera.Render ();

			Unsupported.SetRenderSettingsUseFogNoDirty( fog );
		}

		public override void OnPreviewGUI (Rect r, GUIStyle background)
		{
			if( !ShaderUtil.hardwareSupportsRectRenderTexture )
			{
				if( Event.current.type == EventType.Repaint )
				{
					EditorGUI.DropShadowLabel( new Rect(r.x,r.y,r.width,40f),"Map preview requires\nrender texture support" );
				}
			}
			else
			{
				InitPreview();
				if( Event.current.type == EventType.Repaint )
				{
					_PreviewUtility.BeginPreview( r,background );
					DoRenderPreview();
					Texture image = _PreviewUtility.EndPreview();
					EditorGUI.DrawTextureTransparent( r,image,ScaleMode.ScaleToFit );
				}
			}
		}

		public void OnDestroy()
		{
			if( _PreviewUtility != null )
			{
				_PreviewUtility.Cleanup();
				_PreviewUtility = null;
			}
			if( !target )
			{
				Map map = (Map)target;
				map.DestroySubComponents();
			}
		}
	}
}