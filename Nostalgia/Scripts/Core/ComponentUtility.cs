﻿/// @cond
using UnityEngine;
using System.Collections;

namespace Nostalgia
{
	public static class ComponentUtility
	{
		public delegate Component EditorAddComponent( GameObject gameObject,System.Type type );
		public delegate void EditorDestroyObjectImmediate( Object obj );

		public static EditorAddComponent editorAddComponent;
		public static EditorDestroyObjectImmediate editorDestroyObjectImmediate;

		public static GameObject CreateGameObject( string name )
		{
			GameObject gameObject = new GameObject( name );
#if UNITY_EDITOR
			UnityEditor.Undo.RegisterCreatedObjectUndo( gameObject,"Created " + name );
#endif

			return gameObject;
		}

		public static Component AddComponent( GameObject gameObject,System.Type type )
		{
			if( Application.isEditor && !Application.isPlaying && editorAddComponent != null )
			{
				return editorAddComponent( gameObject,type );
			}
			return gameObject.AddComponent( type );
		}
		
		public static Type AddComponent<Type>( GameObject gameObject ) where Type : Component
		{
			if( Application.isEditor && !Application.isPlaying && editorAddComponent != null )
			{
				return editorAddComponent( gameObject,typeof(Type) ) as Type;
			}
			return gameObject.AddComponent<Type>();
		}
		
		public static void Destroy( Object obj )
		{
			if( Application.isEditor && !Application.isPlaying && editorDestroyObjectImmediate != null )
			{
				editorDestroyObjectImmediate( obj );
				return;
			}
			Object.Destroy( obj );
		}

		public static void DestroyImmediate( Object obj )
		{
			if( Application.isEditor && !Application.isPlaying )
			{
				Object.DestroyImmediate( obj );
				return;
			}
			Object.Destroy( obj );
		}
	}
}
/// @endcond