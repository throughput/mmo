﻿using UnityEngine;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// 整数型の２次元座標を扱うクラス。
	/// @else
	/// Classes for handling two-dimensional integer coordinates.
	/// @endif
	/// </summary>
	[System.Serializable]
	public sealed class Point2
	{
		/// <summary>
		/// @if JAPANESE
		/// (0,0)のPoint2を取得。
		/// @else
		/// Get Point2 of (0,0).
		/// @endif
		/// </summary>
		public static Point2 zero
		{
			get
			{
				return new Point2(0,0);
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// X座標
		/// @else
		/// The x coordinate.
		/// @endif
		/// </summary>
		public int x = 0;

		/// <summary>
		/// @if JAPANESE
		/// Y座標
		/// @else
		/// The y coordinate.
		/// @endif
		/// </summary>
		public int y = 0;

		/// <summary>
		/// @if JAPANESE
		/// <see cref="Nostalgia.Point2"/>クラスの新しいインスタンスを初期化。
		/// @else
		/// Initializes a new instance of the <see cref="Nostalgia.Point2"/> class.
		/// @endif
		/// </summary>
		public Point2()
		{
			this.x = 0;
			this.y = 0;
		}

		/// <summary>
		/// @if JAPANESE
		/// <see cref="Nostalgia.Point2"/>クラスの新しいインスタンスを初期化。
		/// @else
		/// Initializes a new instance of the <see cref="Nostalgia.Point2"/> class.
		/// @endif
		/// </summary>
		/// <param name="x">
		/// @if JAPANESE
		/// X座標
		/// @else
		/// The x coordinate.
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// Y座標
		/// @else
		/// The y coordinate.
		/// @endif
		/// </param>
		public Point2( int x,int y )
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>
		/// @if JAPANESE
		/// <see cref="Nostalgia.Point2"/>クラスの新しいインスタンスを初期化。
		/// @else
		/// Initializes a new instance of the <see cref="Nostalgia.Point2"/> class.
		/// @endif
		/// </summary>
		/// <param name="p">
		/// Point2
		/// </param>
		public Point2( Point2 p )
		{
			this.x = p.x;
			this.y = p.y;
		}

		/// <summary>
		/// @if JAPANESE
		/// このPoint2の長さの２乗を返す。
		/// @else
		/// Returns the squared length of this Point2.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// 長さの２乗。
		/// @else
		/// the squared length.
		/// @endif
		/// </value>
		public float sqrMagnitude
		{
			get
			{
				return (float)(x*x+y*y);
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// このPoint2の長さを返す。
		/// @else
		/// Returns the length of this Point2.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// 長さ。
		/// @else
		/// The magnitude.
		/// @endif
		/// </value>
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt( sqrMagnitude );
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// Point2同士の加算。
		/// @else
		/// Addition of Point2 each other.
		/// @endif
		/// </summary>
		public static Point2 operator+(Point2 a,Point2 b)
		{
			return new Point2( a.x+b.x,a.y+b.y );
		}

		/// <summary>
		/// @if JAPANESE
		/// Point2同士の減算。
		/// @if JAPANESE
		/// Subtraction of Point2 each other.
		/// @endif
		/// </summary>
		public static Point2 operator-(Point2 a,Point2 b)
		{
			return new Point2( a.x-b.x,a.y-b.y );
		}

		/// <summary>
		/// @if JAPANESE
		/// 符号の反転。
		/// @else
		/// Inversion of the sign.
		/// @else
		/// </summary>
		public static Point2 operator-(Point2 a)
		{
			return new Point2( -a.x,-a.y );
		}

		/// <summary>
		/// @if JAPANESE
		/// Point2の乗算。
		/// @else
		/// Multiplication of Point2.
		/// @endif
		/// </summary>
		public static Point2 operator*(Point2 a,int b)
		{
			return new Point2( a.x*b,a.y*b );
		}

		/// <summary>
		/// @if JAPANESE
		/// Point2の乗算。
		/// @else
		/// Multiplication of Point2.
		/// @endif
		/// </summary>
		public static Point2 operator*(int a,Point2 b)
		{
			return new Point2( a*b.x,a*b.y );
		}

		/// <summary>
		/// @if JAPANESE
		/// Point2の除算。
		/// @else
		/// Division of Point2.
		/// @endif
		/// </summary>
		public static Point2 operator/(Point2 a,int b)
		{
			return new Point2( a.x/b,a.y/b );
		}

		/// <summary>
		/// @if JAPANESE
		/// Point2が等しければtrueを返す。
		/// @else
		/// Returns true if the Point2s are equal.
		/// @endif
		/// </summary>
		public static bool operator==(Point2 a,Point2 b)
		{
			// If both are null, or both are same instance, return true.
			if (System.Object.ReferenceEquals(a, b))
			{
				return true;
			}
			
			// If one is null, but not both, return false.
			if (((object)a == null) || ((object)b == null))
			{
				return false;
			}
			
			return a.x == b.x && a.y == b.y;
		}

		/// <summary>
		/// @if JAPANESE
		/// 等しくなければtrueを返す。
		/// @else
		/// Returns true if Point2s different.
		/// @endif
		/// </summary>
		public static bool operator!=(Point2 a,Point2 b)
		{
			return !(a == b);
		}

		public static implicit operator Point2(Vector2 v)
		{
			return new Point2( (int)v.x,(int)v.y );
		}

		public static implicit operator Vector2(Point2 p)
		{
			return new Vector2( p.x,p.y );
		}

		public override bool Equals(System.Object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}
			
			// If parameter cannot be cast to Point return false.
			Point2 p = (Point2)obj;
			if ((System.Object)p == null)
			{
				return false;
			}
			
			// Return true if the fields match:
			return x == p.x && y == p.y;
		}
		
		public override int GetHashCode()
		{
			return x ^ y;
		}
		
		public override string ToString ()
		{
			return "(" + x + ", " + y + ")";
		}
	}
}