using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// タイルのデータ。
	/// @else
	/// Data of the tile.
	/// @endif
	/// </summary>
	[System.Serializable]
	public sealed class Tile : ScriptableObject
	{
		[SerializeField] private TileSet _TileSet;

		/// <summary>
		/// @if JAPANESE
		/// タイルセット
		/// @else
		/// The tile set.
		/// @endif
		/// </summary>
		public TileSet tileSet
		{
			get
			{
				return _TileSet;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルのタイプ
		/// @else
		/// Type of tile.
		/// @endif
		/// </summary>
		public enum Type
		{
			/// <summary>
			/// @if JAPANESE
			/// オートタイルではない通常タイル。
			/// @else
			/// The tiles usually not auto tile.
			/// @endif
			/// </summary>
			Normal,

			/// <summary>
			/// @if JAPANESE
			/// RPGツクールVXの床タイル互換のオートタイル。
			/// @else
			/// Auto tile compatible RPG Maker VX floor.
			/// @endif
			/// </summary>
			AutoFloorVX,

			/// <summary>
			/// @if JAPANESE
			/// WOLF RPG エディター互換のオートタイル。
			/// @else
			/// Auto tile compatible WOLF RPG Editor.
			/// @endif
			/// </summary>
			AutoFloorWolf,
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルのタイプ
		/// @else
		/// Type of tile.
		/// @endif
		/// </summary>
		public Type type;

		/// <summary>
		/// @if JAPANESE
		/// テクスチャ上の左上座標(ピクセル)
		/// @else
		/// Upper left coordinates on the texture (pixels).
		/// @endif
		/// </summary>
		public Vector2 position;

		/// <summary>
		/// @if JAPANESE
		/// １タイルの幅(ピクセル)
		/// @else
		/// Width of one tile (pixels).
		/// @endif
		/// </summary>
		public int size;

		/// <summary>
		/// @if JAPANESE
		/// 通常タイルの横幅
		/// @else
		/// width
		/// @endif
		/// </summary>
		public int width = 1;
		
		/// <summary>
		/// @if JAPANESE
		/// 通常タイルの縦幅
		/// @else
		/// height
		/// @endif
		/// </summary>
		public int height = 1;

		/// <summary>
		/// @if JAPANESE
		/// コマアニメするフレーム数。
		/// アニメの画像は、<see cref="Nostalgia.Tile.position"/>で指定した座標から右に並べておく必要があります。
		/// @else
		/// Number of frames to the frame animation. 
		/// The image of the animation, you must be arranged to the right from the coordinates that you specify in the <see cref = "Nostalgia.Tile.position" />.
		/// @endif
		/// </summary>
		public int animation = 1;

		/// <summary>
		/// @if JAPANESE
		/// Colliderの有無。
		/// @else
		/// Presence or absence of the Collider.
		/// @endif
		/// </summary>
		public bool collider = false;

		/// <summary>
		/// @if JAPANESE
		/// Colliderに設定するPhysicsMaterial2D。
		/// @else
		/// PhysicsMaterial2D be set to Collider.
		/// @endif
		/// </summary>
		public PhysicsMaterial2D physicsMaterial;

		/// <summary>
		/// @if JAPANESE
		/// Colliderに設定するIs Trigger。
		/// @else
		/// Is Trigger be set to Collider.
		/// @endif
		/// </summary>
		public bool isTrigger = false;

		[SerializeField] private List<TileComponent> _Components = new List<TileComponent>();

		private readonly Vector2[,] normalPartsPoint = new Vector2[,]
		{
			{ new Vector2( 0.0f,0.0f ),new Vector2( 0.5f,0.0f ) },
			{ new Vector2( 0.0f,0.5f ),new Vector2( 0.5f,0.5f ) },
		};
		
		private readonly Vector2[,,] vxFloorPartsPoint = new Vector2[,,]
		{
			{
				{ new Vector2( 1.0f,2.0f ),new Vector2( 0.5f,2.0f ) },
				{ new Vector2( 1.0f,1.5f ),new Vector2( 0.5f,1.5f ) },
			},
			{
				{ new Vector2( 1.0f,1.0f ),new Vector2( 0.5f,1.0f ) },
				{ new Vector2( 1.0f,2.5f ),new Vector2( 0.5f,2.5f ) },
			},
			{
				{ new Vector2( 0.0f,2.0f ),new Vector2( 1.5f,2.0f ) },
				{ new Vector2( 0.0f,1.5f ),new Vector2( 1.5f,1.5f ) },
			},
			{
				{ new Vector2( 1.0f,0.0f ),new Vector2( 1.5f,0.0f ) },
				{ new Vector2( 1.0f,0.5f ),new Vector2( 1.5f,0.5f ) },
			},
			{
				{ new Vector2( 0,1.0f ),new Vector2( 1.5f,1.0f ) },
				{ new Vector2( 0,2.5f ),new Vector2( 1.5f,2.5f ) },
			},
		};
		
		private readonly Vector2[,,] wolfFloorPartsPoint = new Vector2[,,]
		{
			{
				{ new Vector2( 0.0f,4.0f ),new Vector2( 0.5f,4.0f ) },
				{ new Vector2( 0.0f,4.5f ),new Vector2( 0.5f,4.5f ) },
			},
			
			{
				{ new Vector2( 0.0f,2.0f ),new Vector2( 0.5f,2.0f ) },
				{ new Vector2( 0.0f,2.5f ),new Vector2( 0.5f,2.5f ) },
			},
			{
				{ new Vector2( 0.0f,1.0f ),new Vector2( 0.5f,1.0f ) },
				{ new Vector2( 0.0f,1.5f ),new Vector2( 0.5f,1.5f ) },
			},
			{
				{ new Vector2( 0.0f,3.0f ),new Vector2( 0.5f,3.0f ) },
				{ new Vector2( 0.0f,3.5f ),new Vector2( 0.5f,3.5f ) },
			},
			{
				{ new Vector2( 0.0f,0.0f ),new Vector2( 0.5f,0.0f ) },
				{ new Vector2( 0.0f,0.5f ),new Vector2( 0.5f,0.5f ) },
			},
		};

		/// <summary>
		/// @if JAPANESE
		/// パーツのインデックスからテクスチャ座標を取得。
		/// @else
		/// Get the Texture position from the index of the part.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// UV座標
		/// @else
		/// The texture position.
		/// @endif
		/// </returns>
		/// <param name="index">
		/// @if JAPANESE
		/// パーツのインデックス。
		/// @else
		/// Index.
		/// @endif
		/// </param>
		/// <param name="x">
		/// @if JAPANESE
		/// X座標(0～1)。
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// Y座標(0～1)。
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		public Vector2 IndexToPos( int index,int x,int y,Point2 tilePos )
		{
			Vector2 pos = position;

			switch( type )
			{
			case Type.Normal:
				pos += (normalPartsPoint[y%2,x%2]+(Vector2)tilePos) * size;
				break;
			case Type.AutoFloorVX:
				pos += vxFloorPartsPoint[index,y % 2,x % 2] * size;
				break;
			case Type.AutoFloorWolf:
				pos += wolfFloorPartsPoint[index,y % 2,x % 2] * size;
				break;
			}
			
			return pos;
		}

		/// <summary>
		/// @if JAPANESE
		/// パーツの横幅を取得。
		/// @else
		/// Gets the width of the parts.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// パーツの横幅
		/// @else
		/// The parts width.
		/// @endif
		/// </returns>
		public float GetPartsWidth()
		{
			float width = size;
			switch( type )
			{
			case Type.Normal:
				width *= this.width;
				break;
			case Type.AutoFloorVX:
				width *= 2.0f;
				break;
			}
			
			return width;
		}

		static FieldInfo _TileField;
		static FieldInfo tileField
		{
			get
			{
				if( _TileField == null )
				{
					System.Type type = typeof(TileComponent);
					_TileField = type.GetField( "_Tile",BindingFlags.Instance | BindingFlags.NonPublic );
				}
				return _TileField;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentを追加する。
		/// @else
		/// Adds the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 追加されたTileComponent。
		/// @else
		/// TileComponent that have been added.
		/// @endif
		/// </returns>
		/// <param name="type">
		/// @if JAPANESE
		/// 追加するクラスのType。
		/// @else
		/// Type of the class you want to add.
		/// @endif
		/// </param>
		public TileComponent AddComponent( System.Type type )
		{
			if( !type.IsSubclassOf( typeof(TileComponent) ) )
			{
				Debug.LogError( type.Name + " isn't subclass of TileComponent." );
				return null;
			}

			TileComponent component = ScriptableObject.CreateInstance( type ) as TileComponent;
			component.hideFlags = HideFlags.HideInHierarchy;

			tileField.SetValue( component,this );

			_Components.Add( component );

			return component;
		}

		/// <summary>
		/// @if JAPANESE
		/// TileにTileComponentを追加する。
		/// @else
		/// Adds the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 追加されたTileComponent。
		/// @else
		/// TTileComponent that have been added.
		/// @endif
		/// </returns>
		/// <typeparam name="T">
		/// @if JAPANESE
		/// 追加するTileComponentのクラス。
		/// @else
		/// Class of TileComponent to add.
		/// @endif
		/// </typeparam>
		public T AddComponent<T>() where T : TileComponent
		{
			return AddComponent( typeof(T) ) as T;
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentを取得する。
		/// @else
		/// Gets the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponent。<br>
		/// 無かった場合はnullを返す。
		/// @else
		/// TileComponent.<br>
		/// The return value may be null if it was not.
		/// @endif
		/// </returns>
		/// <param name="type">
		/// @if JAPANESE
		/// 取得したいクラスのType。
		/// @else
		/// Type of the class you want to get.
		/// @endif
		/// </param>
		public TileComponent GetComponent( System.Type type )
		{
			foreach( TileComponent component in _Components )
			{
				System.Type classType = component.GetType();
				if( classType == type || classType.IsSubclassOf( type ) )
				{
					return component;
				}
			}

			return null;
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentを取得する。
		/// @else
		/// Gets the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponent。<br>
		/// 無かった場合はnullを返す。
		/// @else
		/// TileComponent.<br>
		/// The return value may be null if it was not.
		/// @endif
		/// </returns>
		/// <typeparam name="T">
		/// @if JAPANESE
		/// 取得したいTileComponentのクラス。
		/// @else
		/// Class of TileComponent you want to get.
		/// @endif
		/// </typeparam>
		public T GetComponent<T>() where T : TileComponent
		{
			return GetComponent ( typeof(T) ) as T;
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentの配列を取得する。
		/// @else
		/// Gets the TileComponents.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponentの配列。
		/// @else
		/// Array of TileComponent.
		/// @endif
		/// </returns>
		/// <param name="type">
		/// @if JAPANESE
		/// 取得したいクラスのType。
		/// @else
		/// Type of the class you want to get.
		/// @endif
		/// </param>
		public TileComponent[] GetComponents( System.Type type )
		{
			List<TileComponent> components = new List<TileComponent>();

			foreach( TileComponent component in _Components )
			{
				System.Type classType = component.GetType();
				if( classType == type || classType.IsSubclassOf( type ) )
				{
					components.Add( component );
				}
			}

			return components.ToArray();
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentの配列を取得する。
		/// @else
		/// Gets the TileComponents.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponentの配列。
		/// @else
		/// Array of TileComponent.
		/// @endif
		/// </returns>
		/// <typeparam name="T">
		/// @if JAPANESE
		/// 取得したいTileComponentのクラス。
		/// @else
		/// Class of TileComponent you want to get.
		/// @endif
		/// </typeparam>
		public T[] GetComponents<T>() where T : TileComponent
		{
			return (T[])GetComponents( typeof(T) );
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentの順番を移動する。
		/// @else
		/// Move the order of TileComponent.
		/// @endif
		/// </summary>
		/// <param name="from">
		/// @if JAPANESE
		/// 移動元のインデックス。
		/// @else
		/// Index of the moving source.
		/// @endif
		/// </param>
		/// <param name="to">
		/// @if JAPANESE
		/// 移動先のインデックス
		/// @else
		/// Index of the destination.
		/// @endif
		/// </param>
		public void MoveComponent( int from,int to )
		{
			TileComponent component = _Components[from];

			_Components.RemoveAt( from );
			_Components.Insert( to,component );
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentを削除する。
		/// @else
		/// Remove the TileComponent.
		/// @endif
		/// </summary>
		/// <param name="component">
		/// @if JAPANESE
		/// 削除するTileComponent。
		/// @else
		/// TileComponent you want to remove.
		/// @endif
		/// </param>
		public void RemoveComponent( TileComponent component )
		{
			_Components.Remove( component );
		}
	}
}