﻿using UnityEngine;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// グリッド上に配置してあるタイル(１マス)のデータ。
	/// @else
	/// Tiles are placed on the grid data (one square).
	/// </summary>
	[System.Serializable]
	public sealed class Cell
	{
		/// <summary>
		/// @if JAPANESE
		/// 座標
		/// @else
		/// The position.
		/// @endif
		/// </summary>
		public Point2 position;

		/// <summary>
		/// @if JAPANESE
		/// タイルID。
		/// <see cref="Nostalgia.TileSet.tiles"/>のインデックス。
		/// @else
		/// The tile ID.
		/// Index of <see cref="Nostalgia.TileSet.tiles"/>.
		/// @endif
		/// </summary>
		public int tileID;

		/// <summary>
		/// @if JAPANESE
		/// タイル内の位置
		/// @else
		/// Position within the tile.
		/// @endif
		/// </summary>
		public Point2 tilePos = Point2.zero;

		/// <summary>
		/// @if JAPANESE
		/// パーツID。
		/// オートタイルのパーツ状態。
		/// @else
		/// The parts ID.
		/// Parts of the state auto tile.
		/// @endif
		/// </summary>
		public int partsID;

		/// <summary>
		/// @if JAPANESE
		/// Collider
		/// @else
		/// The collider.
		/// @endif
		/// </summary>
		public BoxCollider2D collider;

		[SerializeField]
		private Color[] _VertexColors;

		/// <summary>
		/// @if JAPANESE
		/// <see cref="Nostalgia.Cell"/>クラスの新しいインスタンスを初期化。
		/// @else
		/// Initializes a new instance of the <see cref="Nostalgia.Cell"/> class.
		/// @endif
		/// </summary>
		/// <param name="position">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		/// <param name="tileID">
		/// @if JAPANESE
		/// タイルID
		/// @else
		/// Tile ID.
		/// @endif
		/// </param>
		public Cell( Point2 position,int tileID )
		{
			this.position = new Point2(position);
			this.tileID = tileID;
		}

		/// <summary>
		/// @if JAPANESE
		/// パーツインデックスを設定する。
		/// @else
		/// Sets the index of the parts.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// パーツインデックス。
		/// @else
		/// The parts index.
		/// @endif
		/// </returns>
		/// <param name="partsID">
		/// @if JAPANESE
		/// 現在のパーツID。
		/// @else
		/// Current parts ID.
		/// @endif
		/// </param>
		/// <param name="x">
		/// @if JAPANESE
		/// x座標(0～1)
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// y座標(0～1)
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="index">
		/// @if JAPANESE
		/// インデックス
		/// @else
		/// Index.
		/// @endif
		/// </param>
		public static int SetPartsIndex( int partsID,int x,int y,int index )
		{
			int partsIndex = x + y*2;
			partsID |= (index << (partsIndex*3));
			return partsID;
		}

		/// <summary>
		/// @if JAPANESE
		/// パーツのインデックスを取得。
		/// @else
		/// Gets the index of the parts.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// パーツのインデックス。
		/// @else
		/// The parts index.
		/// @endif
		/// </returns>
		/// <param name="partsID">
		/// @if JAPANESE
		/// 現在のパーツID。
		/// @else
		/// Current parts ID.
		/// @endif
		/// </param>
		/// <param name="x">
		/// @if JAPANESE
		/// x座標(0～1)
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// y座標(0～1)
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		public static int GetPartsIndex( int partsID,int x,int y )
		{
			int partsIndex = x + y*2;

			return (partsID >> (partsIndex*3)) & 0x7;
		}

		/// <summary>
		/// @if JAPANESE
		/// パーツのインデックスを設定。
		/// @else
		/// Sets the index of the parts.
		/// @endif
		/// </summary>
		/// <param name="x">
		/// @if JAPANESE
		/// x座標(0～1)
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// y座標(0～1)
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="index">
		/// @if JAPANESE
		/// インデックス
		/// @else
		/// Index.
		/// @endif
		/// </param>
		public void SetPartsIndex( int x,int y,int index )
		{
			this.partsID = SetPartsIndex( this.partsID,x,y,index );
		}

		/// <summary>
		/// @if JAPANESE
		/// パーツのインデックスを取得。
		/// @else
		/// Gets the index of the parts.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// パーツのインデックス。
		/// @else
		/// The parts index.
		/// @endif
		/// </returns>
		/// <param name="x">
		/// @if JAPANESE
		/// X座標(0～1)。
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// Y座標(0～1)。
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		public int GetPartsIndex( int x,int y )
		{
			return GetPartsIndex ( this.partsID,x,y );
		}

		/// <summary>
		/// @if JAPANESE
		/// 頂点カラーを設定する
		/// @else
		/// Sets the color of the vertex.
		/// @endif
		/// </summary>
		/// <param name="x">
		/// @if JAPANESE
		/// x座標(0～1)
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// y座標(0～1)
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="index">
		/// @if JAPANESE
		/// 頂点インデックス
		/// @else
		/// Vertex Index.
		/// @endif
		/// </param>
		/// <param name="color">
		/// @if JAPANESE
		/// 頂点カラー
		/// @else
		/// Vertex Color.
		/// @endif
		/// </param>
		public void SetVertexColor( int x,int y,int index,Color color )
		{
			int vertexIndex = index + (x + y*2) * 4;

			if( _VertexColors == null || _VertexColors.Length == 0 )
			{
				_VertexColors = new Color[16]{ 
					Color.white,Color.white,Color.white,Color.white,
					Color.white,Color.white,Color.white,Color.white,
					Color.white,Color.white,Color.white,Color.white,
					Color.white,Color.white,Color.white,Color.white
				};
			}

			_VertexColors[vertexIndex] = color;
		}

		/// <summary>
		/// @if JAPANESE
		/// 頂点カラーを取得する。
		/// @else
		/// Gets the color of the vertex.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 頂点カラー
		/// @else
		/// The vertex color.
		/// @endif
		/// </returns>
		/// <param name="x">
		/// @if JAPANESE
		/// x座標(0～1)
		/// @else
		/// The x coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="y">
		/// @if JAPANESE
		/// y座標(0～1)
		/// @else
		/// The y coordinate(between 0 and 1).
		/// @endif
		/// </param>
		/// <param name="index">
		/// @if JAPANESE
		/// 頂点インデックス
		/// @else
		/// Vertex Index.
		/// @endif
		/// </param>
		public Color GetVertexColor( int x,int y,int index )
		{
			int vertexIndex = index + (x + y*2) * 4;

			if( _VertexColors == null || _VertexColors.Length == 0 )
			{
				return Color.white;
			}
			return _VertexColors[vertexIndex];
		}
	}
}