﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// Mapのレンダリング用チャンク。
	/// 基本的にはチャンクを操作する必要はありません。
	/// @else
	/// Rendering chunk of Map.
	/// You do not need to manipulate the chunk basically.
	/// @endif
	/// </summary>
	[AddComponentMenu("")]
	[ExecuteInEditMode]
	public sealed class Chunk : MonoBehaviour
	{
		/// <summary>
		/// @if JAPANESE
		/// 属しているマップ。
		/// @else
		/// Map to which they belong.
		/// @endif
		/// </summary>
		public Map map;

		/// <summary>
		/// @if JAPANESE
		/// 位置。
		/// @else
		/// The position.
		/// @endif
		/// </summary>
		public Point2 position;

		/// <summary>
		/// @if JAPANESE
		/// 変更されているかどうか.
		/// @else
		/// The dirty.
		/// @endif
		/// </summary>
		[System.NonSerialized] public bool dirty = false;

		[HideInInspector][SerializeField] private MeshFilter _MeshFilter;
		[HideInInspector][SerializeField] private MeshRenderer _MeshRenderer;

		/// <summary>
		/// @if JAPANESE
		/// sortingLayerIDの取得もしくは設定。
		/// @else
		/// Gets or sets the sorting layer ID.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// Sorting Layer ID
		/// @else
		/// The sorting layer ID.
		/// @endif
		/// </value>
		public int sortingLayerID
		{
			get
			{
				if( _MeshRenderer == null )
				{
					return 0;
				}
				return _MeshRenderer.sortingLayerID;
			}
			set
			{
				if( _MeshRenderer != null )
				{
					_MeshRenderer.sortingLayerID = value;
				}
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// SortingOrderの取得もしくは設定。
		/// @else
		/// Gets or sets the sorting order.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// Sorting Order
		/// @else
		/// The sorting order.
		/// @endif
		/// </value>
		public int sortingOrder
		{
			get
			{
				if( _MeshRenderer == null )
				{
					return 0;
				}
				return _MeshRenderer.sortingOrder;
			}
			set
			{
				if( _MeshRenderer != null )
				{
					_MeshRenderer.sortingOrder = value;
				}
			}
		}

		private Mesh _CachedMesh;
		public Mesh cachedMesh
		{
			get
			{
				if( _CachedMesh == null )
				{
					_CachedMesh = new Mesh();
					_CachedMesh.hideFlags = HideFlags.HideAndDontSave;
					_CachedMesh.MarkDynamic();
				}
				
				return _CachedMesh;
			}
		}

		void Awake()
		{
			Initialize();
		}

		void OnValidate()
		{
			if( _MeshFilter != null )
			{
				_MeshFilter.sharedMesh = cachedMesh;
			}
		}

		void Initialize()
		{
			dirty = true;

			if( _MeshFilter == null )
			{
				_MeshFilter = ComponentUtility.AddComponent<MeshFilter>( gameObject );
			}
			_MeshFilter.sharedMesh = cachedMesh;
			
			if( _MeshRenderer == null )
			{
				_MeshRenderer = ComponentUtility.AddComponent<MeshRenderer>( gameObject );
			}
		}

		private List<Vector3> _Vertices = new List<Vector3>();
		private List<Color> _Colors = new List<Color>();
		private List<Vector2> _UVs = new List<Vector2>();
		private List<Vector2> _Animations = new List<Vector2>();
		private List<int> _Indices = new List<int>();

		void UpdateTile( Cell cell )
		{
			if( map == null || map.tileSet== null || map.tileSet.tiles == null || map.tileSet.tiles.Length == 0 || map.tileSet.tiles.Length <= cell.tileID )
			{
				return;
			}

			TileSet tileSet = map.tileSet;
			
			Tile tile = tileSet.tiles[cell.tileID];
			
			float halfSize = 0.5f;
			
			Vector3 localPos = map.MapPointToLocalPoint( cell.position );
			
			Texture texture = tileSet.material.mainTexture;
			
			float textureWidth = texture.width;
			float textureHeight = texture.height;

			float epsilon = 1e-4f;

			float tileSize = tile.size-epsilon;

			float uvWidth = tileSize/textureWidth*0.5f;
			float uvHeight = tileSize/textureHeight*0.5f;
			
			int animationFrame = tile.animation;
			if( animationFrame == 0 )
			{
				animationFrame = 1;
			}
			
			Vector2 animationUV = new Vector2( animationFrame,tile.GetPartsWidth()/textureHeight );
			
			for( int y=0;y<2;y++ )
			{
				for( int x=0;x<2;x++ )
				{
					int index = cell.GetPartsIndex( x,y );
					
					int vertCount = _Vertices.Count;
					
					Vector3 vertex = localPos + new Vector3( x*halfSize,(1-y)*halfSize,0.0f );
					_Vertices.Add ( vertex + new Vector3(     0.0f, halfSize,0.0f));
					_Vertices.Add ( vertex + new Vector3( halfSize, halfSize,0.0f) );
					_Vertices.Add ( vertex + new Vector3(     0.0f, 0.0f,0.0f) );
					_Vertices.Add ( vertex + new Vector3( halfSize, 0.0f,0.0f) );
					
					Vector2 pos = tile.IndexToPos( index,x,y,cell.tilePos ) + new Vector2( epsilon,epsilon );
					
					Vector2 uv = new Vector2( pos.x/textureWidth,(textureHeight-pos.y)/textureHeight );
					_UVs.Add ( uv );
					_UVs.Add ( uv + new Vector2( uvWidth,0.0f ) );
					_UVs.Add ( uv + new Vector2( 0.0f,-uvHeight ) );
					_UVs.Add ( uv + new Vector2( uvWidth,-uvHeight ) );
					
					_Animations.Add( animationUV );
					_Animations.Add( animationUV );
					_Animations.Add( animationUV );
					_Animations.Add( animationUV );
					
					_Colors.Add( cell.GetVertexColor( x,y,0 ) );
					_Colors.Add( cell.GetVertexColor( x,y,1 ) );
					_Colors.Add( cell.GetVertexColor( x,y,2 ) );
					_Colors.Add( cell.GetVertexColor( x,y,3 ) );
					
					_Indices.Add( vertCount+0 );
					_Indices.Add( vertCount+1 );
					_Indices.Add( vertCount+2 );
					_Indices.Add( vertCount+2 );
					_Indices.Add( vertCount+1 );
					_Indices.Add( vertCount+3 );
				}
			}
		}

		Point2 GetBasePos()
		{
			return new Point2( position.x*32,position.y*32 );
		}

		/// <summary>
		/// @if JAPANESE
		/// <see cref="Nostalgia.Cell"/>を保持しているか。
		/// @else
		/// Hads the <see cref="Nostalgia.Cell"/>.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// 保持している。
		/// @else
		/// if cells was haded,
		/// @endif
		/// <c>false</c>
		/// @if JAPANESE
		/// それ以外
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		public bool HadCells()
		{
			Point2 basePos = GetBasePos();

			int xMax = Mathf.Min( basePos.x+32,map.width );
			int yMax = Mathf.Min( basePos.y+32,map.height );
			
			Point2 pos = new Point2();
			for( pos.x = basePos.x;pos.x<xMax;pos.x++ )
			{
				for( pos.y = basePos.y;pos.y<yMax;pos.y++ )
				{
					Cell cell = map.GetCell( pos );
					if( cell!= null )
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// @if JAPANESE
		/// メッシュを更新
		/// @else
		/// Updates the mesh.
		/// @endif
		/// </summary>
		public void UpdateMesh()
		{
			Mesh mesh = cachedMesh;
			mesh.Clear();

			if( map == null || map.tileSet == null || map.tileSet.material == null )
			{
				return;
			}

			_MeshRenderer.sharedMaterial = map.material;

			Point2 basePos = GetBasePos();

			int xMax = Mathf.Min( basePos.x+32,map.width );
			int yMax = Mathf.Min( basePos.y+32,map.height );

			Point2 pos = new Point2();
			for( pos.x = basePos.x;pos.x<xMax;pos.x++ )
			{
				for( pos.y = basePos.y;pos.y<yMax;pos.y++ )
				{
					Cell cell = map.GetCell( pos );
					if( cell!= null )
					{
						UpdateTile( cell );
					}
				}
			}

			mesh.vertices = _Vertices.ToArray();
			mesh.colors = _Colors.ToArray();
			mesh.uv = _UVs.ToArray();
			mesh.uv2 = _Animations.ToArray();
			mesh.triangles = _Indices.ToArray();
			
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();

			_Vertices.Clear();
			_Colors.Clear ();
			_UVs.Clear ();
			_Animations.Clear();
			_Indices.Clear();
		}

		void OnDestroy()
		{
			if( _CachedMesh != null )
			{
				ComponentUtility.DestroyImmediate( _CachedMesh );
				_CachedMesh = null;
			}
		}
	}
}