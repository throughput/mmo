using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// 2Dタイルマップのコンポーネント。
	/// GameObjectにアタッチして使用する。
	/// @else
	/// Components of 2D tile map.
	/// And it may be attached to a GameObject.
	/// @endif
	/// </summary>
	[AddComponentMenu("Nostalgia/Map")]
	[ExecuteInEditMode]
	public class Map : MonoBehaviour 
	{
		[SerializeField] private int _Width = 16;
		[SerializeField] private int _Height = 16;
		[SerializeField] private TileSet _TileSet;
		[SerializeField] private List<Cell> _Cells = new List<Cell>();
		[SerializeField] private List<Chunk> _Chunks = new List<Chunk>();

		[SerializeField] private int _SortingLayerID;
		[SerializeField] private int _SortingOrder;

		[SerializeField] private Color _Color = Color.white;

		/// <summary>
		/// @if JAPANESE
		/// 横方向のピボットを指定。
		/// Resize()メソッドで使用する。
		/// @else
		/// Specifies the pivot in the horizontal direction. 
		/// Use a Resize.
		/// @endif
		/// </summary>
		public enum HorizontalPivot
		{
			/// <summary>
			/// @if JAPANESE
			/// 左側
			/// @else
			/// The left.
			/// @endif
			/// </summary>
			Left,

			/// <summary>
			/// @if JAPANESE
			/// 中央
			/// @else
			/// The center.
			/// @endif
			/// </summary>
			Center,

			/// <summary>
			/// @if JAPANESE
			/// 右側
			/// @else
			/// The right.
			/// @endif
			/// </summary>
			Right,
		}

		/// <summary>
		/// @if JAPANESE
		/// 縦方向のピボットを指定。
		/// Resize()メソッドで使用する。
		/// @else
		/// Specifies the pivot in the vertical direction. 
		/// Use a Resize.
		/// @endif
		/// </summary>
		public enum VerticalPivot
		{
			/// <summary>
			/// @if JAPANESE
			/// 上側
			/// @else
			/// The top.
			/// @endif
			/// </summary>
			Top,

			/// <summary>
			/// @if JAPANESE
			/// 中央
			/// @else
			/// The center.
			/// @endif
			/// </summary>
			Center,

			/// <summary>
			/// @if JAPANESE
			/// 下側
			/// @else
			/// The bottom.
			/// @endif
			/// </summary>
			Bottom,
		}

		/// <summary>
		/// @if JAPANESE
		/// 横幅を取得。
		/// @else
		/// Gets the width.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// 横幅。
		/// @else
		/// The width.
		/// @endif
		/// </value>
		public int width
		{
			get
			{
				return _Width;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// 縦幅を取得。
		/// @else
		/// Gets the height.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// 縦幅。
		/// @else
		/// The height.
		/// @endif
		/// </value>
		public int height
		{
			get
			{
				return _Height;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルセットを取得。
		/// @else
		/// Gets the tile set.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// タイルセット。
		/// @else
		/// The tile set.
		/// @endif
		/// </value>
		public TileSet tileSet
		{
			get
			{
				return _TileSet;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// セルの配列を取得。
		/// @else
		/// Gets the cells.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// セルの配列。
		/// @else
		/// The cells.
		/// @endif
		/// </value>
		public Cell[] cells
		{
			get
			{
				return _Cells.ToArray();
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// チャンクの配列を取得。
		/// @else
		/// Gets the chunks.
		/// @endif
		/// </summary>
		/// <value>
		/// @if JAPANESE
		/// チャンクの配列。
		/// @else
		/// The chunks.
		/// @endif
		/// </value>
		public Chunk[] chunks
		{
			get
			{
				return _Chunks.ToArray();
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// マテリアルカラーの取得/設定。
		/// @else
		/// Gets or sets the material color.
		/// @endif
		/// </summary>
		public Color color
		{
			get
			{
				return _Color;
			}
			set
			{
				_Color = value;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// SortingLayerIDの取得/設定。
		/// @else
		/// Gets or sets the sortingLayerID.
		/// @endif
		/// </summary>
		public int sortingLayerID
		{
			get
			{
				return _SortingLayerID;
			}
			set
			{
				if( _SortingLayerID != value )
				{
					_SortingLayerID = value;

					foreach( Chunk chunk in _Chunks )
					{
						if( chunk != null )
						{
							chunk.sortingLayerID = _SortingLayerID;
						}
					}
				}
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// sortingOrderの取得/設定。
		/// @else
		/// Gets or sets the sortingOrder.
		/// @endif
		/// </summary>
		public int sortingOrder
		{
			get
			{
				return _SortingOrder;
			}
			set
			{
				if( _SortingOrder != value )
				{
					_SortingOrder = value;

					foreach( Chunk chunk in _Chunks )
					{
						if( chunk != null )
						{
							chunk.sortingOrder = _SortingOrder;
						}
					}
				}
			}
		}

		private bool _ChangedCellDic = false;

		private Dictionary<Point2,Cell> _CellDic = null;
		private Dictionary<Point2,Cell> cellDic
		{
			get
			{
				if( _CellDic == null || _ChangedCellDic )
				{
					_CellDic = new Dictionary<Point2, Cell>();
					
					foreach( Cell cell in _Cells )
					{
						_CellDic.Add( cell.position,cell );
					}

					_ChangedCellDic = false;
				}
				
				return _CellDic;
			}
		}

		private Dictionary<Collider2D,Cell> _ColliderCellDic = null;
		private Dictionary<Collider2D,Cell> colliderCellDic
		{
			get
			{
				if( _ColliderCellDic == null )
				{
					_ColliderCellDic = new Dictionary<Collider2D,Cell>();
					
					foreach( Cell cell in _Cells )
					{
						if( cell.collider != null )
						{
							_ColliderCellDic.Add( cell.collider,cell );
						}
					}
				}
				
				return _ColliderCellDic;
			}
		}

		private Dictionary<Point2,Chunk> _ChunkDic = null;
		private Dictionary<Point2,Chunk> chunkDic
		{
			get
			{
				if( _ChunkDic == null )
				{
					_ChunkDic = new Dictionary<Point2, Nostalgia.Chunk>();

					foreach( Chunk chunk in _Chunks )
					{
						_ChunkDic.Add( chunk.position,chunk );
					}
				}

				return _ChunkDic;
			}
		}

		static List<Map> _Maps = new List<Map>();

		[SerializeField,HideInInspector] private MeshRenderer _DummyRenderer = null;

		void Awake()
		{
			if( GetComponent<Renderer>() != null && ( GetComponent<Renderer>() is SpriteRenderer || GetComponent<Renderer>() is MeshRenderer ) && GetComponent<Renderer>() != _DummyRenderer || gameObject.GetComponent<SortingLayer>() != null )
			{
				Debug.LogError( "Can't add the Component for Renderer already exists!",gameObject );

#if UNITY_EDITOR
				DestroyImmediate( this );
#else
				Destroy( this );
#endif
				return;
			}

			_Maps.Add( this );

			if( _DummyRenderer == null )
			{
				_DummyRenderer = gameObject.AddComponent<MeshRenderer>();
				_DummyRenderer.hideFlags = HideFlags.HideInInspector;
				_DummyRenderer.enabled = false;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// Map.FindCell()の結果が格納されるクラス。
		/// @else
		/// Class that will contain the result of Map.FindCell ().
		/// @endif
		/// </summary>
		public class CellResult
		{
			/// <summary>
			/// @if JAPANESE
			/// 該当Cellが属しているMap。
			/// @else
			/// Map the corresponding Cell belongs.
			/// @endif
			/// </summary>
			public Map map;

			/// <summary>
			/// @if JAPANESE
			/// 見つけたCell。
			/// @else
			/// The found Cell.
			/// @endif
			/// </summary>
			public Cell cell;
		}

		/// <summary>
		/// @if JAPANESE
		/// ワールド座標からCellを探す。
		/// @else
		/// Finds the Cell from the world position.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 見つけた場合にCellResultが返る。
		/// ない場合はnull。
		/// @else
		/// CellResult is returned if you find. 
		/// Null is returned if it does not exist.
		/// </returns>
		/// <param name="worldPos">
		/// @if JAPANESE
		/// ワールド座標。
		/// @else
		/// World position.
		/// @endif
		/// </param>
		public static CellResult FindCell( Vector3 worldPos )
		{
			foreach( Map map in _Maps )
			{
				if( !map.gameObject.activeInHierarchy || !map.enabled )
				{
					continue;
				}

				Cell cell = map.GetCellFromWorldPos( worldPos );
				if( cell != null )
				{
					CellResult cellResult = new CellResult();
					cellResult.map = map;
					cellResult.cell = cell;

					return cellResult;
				}
			}

			return null;
		}

		/// <summary>
		/// @if JAPANESE
		/// 指定したワールド座標上にある全てのCellを探す。
		/// @else
		/// Find Cell everything in the world coordinates to the specified.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 見つかったCellのCellResultの配列。
		/// @else
		/// Array of CellResult of Cell found.
		/// @endif
		/// </returns>
		/// <param name="worldPos">
		/// @if JAPANESE
		/// ワールド座標。
		/// @else
		/// World position.
		/// @endif
		/// </param>
		public static CellResult[] FindCells( Vector3 worldPos )
		{
			List<CellResult> cellResults = new List<CellResult>();
			foreach( Map map in _Maps )
			{
				if( !map.gameObject.activeInHierarchy || !map.enabled )
				{
					continue;
				}

				Cell cell = map.GetCellFromWorldPos( worldPos );
				if( cell != null )
				{
					CellResult cellResult = new CellResult();
					cellResult.map = map;
					cellResult.cell = cell;

					cellResults.Add( cellResult );
				}
			}
			return cellResults.ToArray();
		}

		bool IsCellContains( Point2 pos )
		{
			return 0<=pos.x && pos.x < _Width &&
				0<=pos.y && pos.y < _Height;
		}

		/// <summary>
		/// @if JAPANESE
		/// セルを取得。
		/// @else
		/// Gets the cell.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// セル。
		/// ない場合はnullを返す。
		/// @else
		/// The cell.
		/// The return value may be null if it does not exist.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		public Cell GetCell( Point2 pos )
		{
			Cell cell = null;
			if( cellDic.TryGetValue( pos,out cell ) )
			{
				return cell;
			}
			
			return null;
		}

		/// <summary>
		/// @if JAPANESE
		/// ワールド座標からCellを取得。
		/// @else
		/// Gets the cell from world position.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 見つかったCell。
		/// ない場合はnullを返す。
		/// @else
		/// It is the Cell found. 
		/// I return null if it does not.
		/// </returns>
		/// <param name="worldPos">
		/// @if JAPANESE
		/// ワールド座標。
		/// @else
		/// World position.
		/// @endif
		/// </param>
		public Cell GetCellFromWorldPos( Vector3 worldPos )
		{
			return GetCell( WorldPointToMapPoint( worldPos ) );
		}

		/// <summary>
		/// @if JAPANESE
		/// Colliderからセルを取得する。
		/// @else
		/// Gets the cell from collider.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// Colliderを保持するセル。
		/// ない場合はnullを返す。
		/// @else
		/// The cell from collider.
		/// The return value may be null if it does not exist.
		/// @endif
		/// </returns>
		/// <param name="collider">
		/// @if JAPANESE
		/// Collider.
		/// @else
		/// Collider.
		/// @endif
		/// </param>
		public Cell GetCellFromCollider( Collider2D collider )
		{
			Cell cell = null;
			if( colliderCellDic.TryGetValue( collider,out cell ) )
			{
				return cell;
			}
			
			return null;
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルIDを取得。
		/// @else
		/// Gets the tileID.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// タイルID
		/// ない場合は-1を返す。
		/// @else
		/// The tileID.
		/// The return value may be -1 if it does not exist.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		public int GetTileID( Point2 pos )
		{
			Cell cell = GetCell( pos );
			if( cell==null )
			{
				return -1;
			}
			
			return cell.tileID;
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルを取得。
		/// @else
		/// Gets the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// タイル。
		/// ない場合はnullを返す。
		/// @else
		/// The tile.
		/// The return value may be null if it does not exist.
		/// @endif
		/// </returns>
		/// <param name="cell">
		/// @if JAPANESE
		/// セル。
		/// @else
		/// Cell.
		/// </param>
		public Tile GetTile( Cell cell )
		{
			if( cell == null || _TileSet== null || _TileSet.tiles == null || _TileSet.tiles.Length == 0 || _TileSet.tiles.Length <= cell.tileID )
			{
				return null;
			}
			
			return _TileSet.tiles[cell.tileID];
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルを取得。
		/// @else
		/// Gets the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// タイル。
		/// ない場合はnullを返す。
		/// @else
		/// The tile.
		/// The return value may be null if it does not exist.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		public Tile GetTile( Point2 pos )
		{
			return GetTile( GetCell( pos ) );
		}

		Chunk AddChunk( Point2 pos )
		{
			GameObject obj = ComponentUtility.CreateGameObject( "Chunk");
			obj.hideFlags = HideFlags.HideInHierarchy;

			obj.transform.parent = transform;
			obj.transform.localPosition = Vector3.zero;
			obj.transform.localRotation = Quaternion.identity;
			obj.transform.localScale = Vector3.one;

			Chunk chunk = obj.AddComponent<Chunk>();
			chunk.map = this;
			chunk.position = pos;
			chunk.sortingLayerID = _SortingLayerID;
			chunk.sortingOrder = _SortingOrder;

			chunkDic.Add( pos,chunk );

			_Chunks.Add ( chunk );

			return chunk;
		}

		Chunk GetChunk( Point2 pos )
		{
			Chunk chunk = null;
			if( chunkDic.TryGetValue( pos,out chunk ) )
			{
				return chunk;
			}
			
			return null;
		}

		/// <summary>
		/// @if JAPANESE
		/// マップ座標からローカル座標に変換。
		/// @else
		/// Maps the point to local point.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANSESE
		/// ローカル座標
		/// @else
		/// The point to local point.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		public Vector3 MapPointToLocalPoint( Point2 pos )
		{
			return new Vector3( pos.x , pos.y,0.0f );
		}

		/// <summary>
		/// @if JAPANESE
		/// マップ座標からワールド座標に変換。
		/// @else
		/// Maps the point to world point.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// ワールド座標
		/// @else
		/// The point to world point.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		public Vector3 MapPointToWorldPoint( Point2 pos )
		{
			return transform.TransformPoint( MapPointToLocalPoint( pos ) );
		}

		/// <summary>
		/// @if JAPANESE
		/// ローカル座標からマップ座標に変換。
		/// @else
		/// Locals the point to map point.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// マップ座標
		/// @else
		/// The point to map point.
		/// @endif
		/// </returns>
		/// <param name="localPos">
		/// @if JAPANESE
		/// ローカル座標
		/// @else
		/// Local position.
		/// @endif
		/// </param>
		public Point2 LocalPointToMapPoint( Vector3 localPos )
		{
			return new Point2( Mathf.FloorToInt( localPos.x ),Mathf.FloorToInt( localPos.y ) );
		}

		/// <summary>
		/// @if JAPANESE
		/// ワールド座標からマップ座標に変換。
		/// @else
		/// Worlds the point to map point.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// マップ座標
		/// @else
		/// The point to map point.
		/// @endif
		/// </returns>
		/// <param name="worldPos">
		/// @if JAPANESE
		/// ワールド座標
		/// @else
		/// World position.
		/// @endif
		/// </param>
		public Point2 WorldPointToMapPoint( Vector3 worldPos )
		{
			return LocalPointToMapPoint( transform.InverseTransformPoint( worldPos ) );
		}

		/// <summary>
		/// @if JAPANESE
		/// レイからマップ座標に変換。
		/// @else
		/// Raies to point.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// レイ上にマップがある。
		/// @else
		/// if to point was rayed,
		/// @endif
		/// <c>false</c> 
		/// @if JAPANESE
		/// それ以外。
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="ray">
		/// @if JAPANESE
		/// レイ
		/// @else
		/// Ray.
		/// @endif
		/// </param>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		public bool RayToPoint( Ray ray,out Point2 pos )
		{
			Plane plane = new Plane( transform.forward,transform.position );
			
			float enter = 0.0f;
			
			if( plane.Raycast( ray,out enter ) )
			{
				Vector3 hitPos = ray.GetPoint( enter );
				
				pos = WorldPointToMapPoint( hitPos );
				
				return IsCellContains( pos );
			}
			
			pos = null;
			
			return false;
		}

		/// <summary>
		/// @if JAPANESE
		/// レイからローカル座標を取得。
		/// @else
		/// Raies to local point.
		/// @endif
		/// </summary>
		/// <param name="ray">
		/// @if JAPANESE
		/// レイ
		/// @else
		/// Ray.
		/// @endif
		/// </param>
		/// <param name="localPos">
		/// @if JAPANESE
		/// ローカル座標
		/// @else
		/// Local position.
		/// @endif
		/// </param>
		public bool Raycast( Ray ray,ref Vector2 localPos )
		{
			Plane plane = new Plane( transform.forward,transform.position );
			
			float enter = 0.0f;
			
			if( plane.Raycast( ray,out enter ) )
			{
				Vector3 hitPos = ray.GetPoint( enter );

				localPos = (Vector2)transform.InverseTransformPoint( hitPos );
				
				Point2 pos = LocalPointToMapPoint( localPos );
				
				return IsCellContains( pos );
			}
			
			return false;
		}

		/// <summary>
		/// @if JAPANESE
		/// サイズ変更。
		/// @else
		/// Resize.
		/// @endif
		/// </summary>
		/// <param name="width">
		/// @if JAPANESE
		/// 横幅
		/// @else
		/// Width.
		/// @endif
		/// </param>
		/// <param name="height">
		/// @if JAPANESE
		/// 縦幅
		/// @else
		/// Height.
		/// @endif
		/// </param>
		/// <param name="horizontalPivot">
		/// @if JAPANESE
		/// 横方向のピボット
		/// @else
		/// Horizontal pivot.
		/// @endif
		/// </param>
		/// <param name="verticalPivot">
		/// @if JAPANESE
		/// 縦方向のピボット
		/// @else
		/// Vertical pivot.
		/// @endif
		/// </param>
		public void Resize( int width,int height,HorizontalPivot horizontalPivot,VerticalPivot verticalPivot )
		{
			if( _Width == width && _Height == height )
			{
				return;
			}
			
			Point2 offset = new Point2(0,0);
			
			switch( horizontalPivot )
			{
			case HorizontalPivot.Left:
				offset.x = 0;
				break;
			case HorizontalPivot.Center:
				offset.x = (width-_Width)/2;
				break;
			case HorizontalPivot.Right:
				offset.x = width-_Width;
				break;
			}
			
			switch( verticalPivot )
			{
			case VerticalPivot.Top:
				offset.y = height-_Height;
				break;
			case VerticalPivot.Center:
				offset.y = (height-_Height)/2;
				break;
			case VerticalPivot.Bottom:
				offset.y = 0;
				break;
			}
			
			_Width = width;
			_Height = height;
			
			List<Point2> removeCells = new List<Point2>();
			
			foreach( Cell cell in _Cells )
			{
				if( offset.x != 0 || offset.y != 0 )
				{
					Point2 preChunkPos = cell.position/32;
					Chunk preChunk = GetChunk( preChunkPos );
					preChunk.dirty = true;

					cell.position += offset;

					if( cell.collider != null )
					{
						float halfSize = 0.5f;
						
						Vector3 localPos = MapPointToLocalPoint( cell.position );
						
						cell.collider.offset = new Vector2( localPos.x+halfSize,localPos.y+halfSize );
					}

					Point2 nextChunkPos = cell.position/32;
					Chunk nextChunk = GetChunk( nextChunkPos );
					if( nextChunk == null )
					{
						nextChunk = AddChunk( nextChunkPos );
					}
					nextChunk.dirty = true;
				}
				
				if( !IsCellContains( cell.position ) )
				{
					removeCells.Add( cell.position );
				}
			}

			_ChangedCellDic = offset.x != 0 || offset.y != 0;

			foreach( Point2 pos in removeCells )
			{
				RemoveTile( pos,false );
			}
		}

		int Mod( int a,int b )
		{
			int r = a % b;
			if( r < 0 )
			{
				return b+r;
			}
			return r;
		}

		/// <summary>
		/// @if JAPANESE
		/// タイル配置
		/// @else
		/// Puts the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// 配置できた。
		/// @else
		/// if tile was put,
		/// @endif
		/// <c>false</c>
		/// @if JAPANESE
		/// それ以外。
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		/// <param name="tileID">
		/// @if JAPANESE
		/// タイルID
		/// @else
		/// Tile ID.
		/// @endif
		/// </param>
		/// <param name="partsID">
		/// @if JAPANESE
		/// パーツID
		/// @else
		/// Parts ID.
		/// @endif
		/// </param>
		/// <param name="autoTiling">
		/// @if JAPANESE
		/// <c>true</c>を設定した場合オートタイル。
		/// @else
		/// If set to <c>true</c> auto tiling.
		/// @endif
		/// </param>
		/// <param name="changeParts">
		/// @if JAPANESE
		/// <c>true</c>を設定した場合パーツ変更。
		/// @else
		/// If set to <c>true</c> change parts.
		/// @endif
		/// </param>
		public bool PutTile( Point2 pos,int tileID,int partsID,Point2 tilePos,bool autoTiling,bool changeParts )
		{
			bool changed = false;

			if( !IsCellContains( pos ) )
			{
				return false;
			}

			bool add = false;

			Point2 chunkPos = pos/32;
			Chunk chunk = GetChunk( chunkPos );
			if( chunk == null )
			{
				chunk = AddChunk( chunkPos );

				changed = true;
			}

			Cell cell = GetCell( pos );
			if( cell == null )
			{
				cell = new Cell( pos,tileID );

				add = true;

				changed = true;
			}

			Tile tile = _TileSet.tiles[tileID];
			if( tile.collider )
			{
				if( cell.collider == null )
				{
					cell.collider = ComponentUtility.AddComponent<BoxCollider2D>( gameObject );
					
					cell.collider.hideFlags = HideFlags.HideInInspector;
				}

				cell.collider.sharedMaterial = tile.physicsMaterial;
				
				cell.collider.isTrigger = tile.isTrigger;

				float halfSize = 0.5f;
				
				Vector3 localPos = MapPointToLocalPoint( pos );
				
				cell.collider.offset = new Vector2( localPos.x+halfSize,localPos.y+halfSize );
				cell.collider.size = new Vector2( 1.0f,1.0f );
				
				if( _ColliderCellDic != null && !_ColliderCellDic.ContainsKey( cell.collider ) )
				{
					_ColliderCellDic.Add( cell.collider,cell );
				}
			}
			else
			{
				if( cell.collider != null )
				{
					if( _ColliderCellDic != null && _ColliderCellDic.ContainsKey( cell.collider ) )
					{
						_ColliderCellDic.Remove( cell.collider );
					}
					
					ComponentUtility.Destroy( cell.collider );
					cell.collider = null;
				}
			}

			Point2 tilePosTmp = ( tile.type == Tile.Type.Normal )?new Point2( Mod(tilePos.x,tile.width),Mod(tilePos.y,tile.height) ) : Point2.zero;

			if( cell.tilePos != tilePosTmp )
			{
				cell.tilePos = tilePosTmp;
				changed = true;
			}

			if( add )
			{
				_Cells.Add( cell );
				if( _CellDic != null )
				{
					_CellDic.Add( cell.position,cell );
				}
			}
			else
			{
				if( cell.tileID != tileID )
				{
					cell.tileID = tileID;
					
					changed = true;
				}
			}

			if( changeParts )
			{
				if( autoTiling )
				{
					int cellPartsID = cell.partsID;
					GenerateIndices( cell );
					if( cell.partsID != cellPartsID )
					{
						changed = true;
					}
					
					for( int x=-1;x<=1;x++ )
					{
						for( int y=-1;y<=1;y++ )
						{
							if( x==0 && y==0 )
							{
								continue;
							}
							
							Cell aroundCell = GetCell( pos + new Point2( x,y ) );
							if( aroundCell != null )
							{
								if( GenerateIndices( aroundCell ) )
								{
									changed = true;
								}
							}
						}
					}
				}
				else
				{
					if( cell.partsID != partsID )
					{
						cell.partsID = partsID;
						changed = true;
					}
				}
			}

			if( changed )
			{
				chunk.dirty = true;
			}
			
			return changed;
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルを塗る
		/// @else
		/// Fills the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// 塗ることができた。
		/// @else
		/// if tile was filled, 
		/// @endif
		/// <c>false</c>
		/// @if JAPANESE
		/// それ以外
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		/// <param name="width">
		/// @if JAPANESE
		/// 横幅
		/// @else
		/// Width.
		/// @endif
		/// </param>
		/// <param name="height">
		/// @if JAPANESE
		/// 縦幅
		/// @else
		/// Height.
		/// @endif
		/// </param>
		/// <param name="tileID">
		/// @if JAPANESE
		/// タイルID
		/// @else
		/// Tile ID.
		/// @endif
		/// </param>
		/// <param name="partsID">
		/// @if JAPANESE
		/// パーツID
		/// @else
		/// Parts ID.
		/// @endif
		/// </param>
		/// <param name="autoTiling">
		/// @if JAPANESE
		/// <c>true</c>を設定した場合オートタイル。
		/// @else
		/// If set to <c>true</c> auto tiling.
		/// @endif
		/// </param>
		public bool FillTile( Point2 pos,int width,int height,int tileID,int partsID,bool autoTiling )
		{
			bool changed = false;

			if( _FilledCells != null )
			{
				_FilledCells.Clear();
			}
			else
			{
				_FilledCells = new HashSet<Point2>();
			}

			for( int x=0;x<width;x++ )
			{
				for( int y=0;y<height;y++ )
				{
					Point2 tilePos = new Point2(x,height-y-1);
					Point2 cellPos = pos+new Point2(x,y);
					if( IsCellContains( cellPos ) )
					{
						if( PutTile( cellPos,tileID,partsID,tilePos,false,!autoTiling ) || autoTiling )
						{
							_FilledCells.Add( cellPos );

							changed = true;
						}
					}
				}
			}

			if( autoTiling )
			{
				HashSet<Point2> aroundCells = new HashSet<Point2>();
				
				foreach( Point2 currentPos in _FilledCells )
				{
					Cell cell = GetCell( currentPos );
					
					if( GenerateIndices( cell ) )
					{
						changed = true;
					}
					
					for( int x=-1;x<=1;x++ )
					{
						for( int y=-1;y<=1;y++ )
						{
							if( x==0 && y==0 )
							{
								continue;
							}
							
							Point2 aroundPos = cell.position + new Point2( x,y );
							
							if( IsCellContains( aroundPos ) && !_FilledCells.Contains( aroundPos ) )
							{
								aroundCells.Add( aroundPos );
							}
						}
					}
				}
				
				foreach( Point2 aroundPos in aroundCells )
				{
					Cell cell = GetCell( aroundPos );
					
					if( cell != null )
					{				
						if( GenerateIndices( cell ) )
						{
							changed = true;
						}
					}
				}
			}

			return changed;
		}

		private int _FillFirstTileID = -1;
		private HashSet<Point2> _FilledCells = null;
		
		void ScanLine( int leftX, int rightX, int y,Stack<Point2> stack )
		{
			Point2 leftPos = new Point2(leftX, y);
			while( leftPos.x <= rightX )
			{
				for (; leftPos.x <= rightX; leftPos.x++)
				{
					if( !_FilledCells.Contains( leftPos ) && GetTileID( leftPos ) == _FillFirstTileID )
					{
						break;
					}
				}
				
				if( rightX < leftPos.x ) break;
				
				for (; leftPos.x <= rightX; leftPos.x++)
				{
					if( GetTileID( leftPos ) != _FillFirstTileID)
					{
						break;
					}
				}
				stack.Push( new Point2(leftPos.x - 1, leftPos.y) );
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルを塗りつぶし
		/// @else
		/// Buckets the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// 塗りつぶしできた。
		/// @else
		/// if tile was bucketed, 
		/// @endif
		/// <c>false</c>
		/// @if JAPANESE
		/// それ以外。
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		/// <param name="tileID">
		/// @if JAPANESE
		/// タイルID
		/// @else
		/// Tile ID.
		/// @endif
		/// </param>
		/// <param name="partsID">
		/// @if JAPANESE
		/// パーツID
		/// @else
		/// Parts ID.
		/// @endif
		/// </param>
		/// <param name="autoTiling">
		/// @if JAPANESE
		/// <c>true</c>を設定した場合オートタイル。
		/// @else
		/// If set to <c>true</c> auto tiling.
		/// @endif
		/// </param>
		public bool BucketTile( Point2 pos,int tileID,int partsID,bool autoTiling )
		{
			bool changed = false;

			_FillFirstTileID = GetTileID( pos );
			if( _FilledCells != null )
			{
				_FilledCells.Clear();
			}
			else
			{
				_FilledCells = new HashSet<Point2>();
			}
			
			Stack<Point2> stack = new Stack<Point2>();
			stack.Push ( pos );
			
			Point2 leftPos = new Point2();
			Point2 rightPos = new Point2();
			
			while( stack.Count > 0 )
			{
				Point2 currentPos = stack.Pop();
				
				if( _FilledCells.Contains( currentPos ) )
				{
					continue;
				}
				
				leftPos.x = currentPos.x;
				leftPos.y = currentPos.y;
				while( 1 <= leftPos.x )
				{
					leftPos.x--;
					if( GetTileID( leftPos ) != _FillFirstTileID )
					{
						leftPos.x++;
						break;
					}
				}
				
				rightPos.x = currentPos.x;
				rightPos.y = currentPos.y;
				while( rightPos.x < _Width -1 )
				{
					rightPos.x++;
					if( GetTileID( rightPos ) != _FillFirstTileID )
					{
						rightPos.x--;
						break;
					}
				}
				
				for( int fillX = leftPos.x ; fillX<=rightPos.x ; fillX++ )
				{
					Point2 fillPos = new Point2( fillX,currentPos.y );
					Point2 tilePos = fillPos-pos;
					tilePos.y = -tilePos.y;
					
					if( PutTile( fillPos,tileID,partsID,tilePos,false,!autoTiling ) || autoTiling )
					{					
						_FilledCells.Add( fillPos );
						changed = true;
					}
				}
				
				if(currentPos.y + 1 < _Height)
				{
					ScanLine(leftPos.x, rightPos.x, currentPos.y + 1, stack);
				}
				if( 0 <= currentPos.y - 1)
				{
					ScanLine(leftPos.x, rightPos.x, currentPos.y - 1, stack);
				}
			}
			
			if( autoTiling )
			{
				HashSet<Point2> aroundCells = new HashSet<Point2>();
				
				foreach( Point2 currentPos in _FilledCells )
				{
					Cell cell = GetCell( currentPos );
					
					if( GenerateIndices( cell ) )
					{
						changed = true;
					}

					for( int x=-1;x<=1;x++ )
					{
						for( int y=-1;y<=1;y++ )
						{
							if( x==0 && y==0 )
							{
								continue;
							}
							
							Point2 aroundPos = cell.position + new Point2( x,y );
							
							if( IsCellContains( aroundPos ) && !_FilledCells.Contains( aroundPos ) )
							{
								aroundCells.Add( aroundPos );
							}
						}
					}
				}
				
				foreach( Point2 aroundPos in aroundCells )
				{
					Cell cell = GetCell( aroundPos );
					
					if( cell != null )
					{				
						if( GenerateIndices( cell ) )
						{
							changed = true;
						}
					}
				}
			}

			return changed;
		}

		/// <summary>
		/// @if JAPANESE
		/// タイル削除。
		/// @else
		/// Removes the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// タイル削除できた。
		/// @else
		/// if tile was removed,
		/// @endif
		/// <c>false</c>
		/// @if JAPANESE
		/// それ以外。
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		/// <param name="autoTiling">
		/// @if JAPANESE
		/// <c>true</c>を設定した場合オートタイル。
		/// @else
		/// If set to <c>true</c> auto tiling.
		/// @endif
		/// </param>
		public bool RemoveTile( Point2 pos,bool autoTiling )
		{
			Cell cell = GetCell( pos );
			if( cell==null )
			{
				return false;
			}

			if( cell.collider != null )
			{
				if( _ColliderCellDic != null && _ColliderCellDic.ContainsKey( cell.collider ) )
				{
					_ColliderCellDic.Remove( cell.collider );
				}
				
				ComponentUtility.Destroy( cell.collider );
				cell.collider = null;
			}

			_Cells.Remove( cell );

			if( _CellDic!=null )
			{
				_CellDic.Remove( pos );
			}

			Point2 chunkPos = cell.position/32;
			Chunk chunk = GetChunk( chunkPos );
			if( chunk != null )
			{
				if( !chunk.HadCells() )
				{
					ComponentUtility.Destroy( chunk.gameObject );

					_Chunks.Remove( chunk );
					if( _ChunkDic!=null )
					{
						_ChunkDic.Remove ( chunkPos );
					}
				}
				else
				{
					chunk.dirty = true;
				}
			}

			if( autoTiling )
			{
				for( int x=-1;x<=1;x++ )
				{
					for( int y=-1;y<=1;y++ )
					{
						if( x==0 && y==0 )
						{
							continue;
						}
						
						Point2 aroundPos = pos + new Point2( x,y );
						
						Cell aroundCell = GetCell( aroundPos );
						if( aroundCell != null )
						{
							GenerateIndices( aroundCell );
						}
					}
				}
			}

			return true;
		}

		/// <summary>
		/// @if JAPANESE
		/// カラーブラシで頂点を塗る。
		/// @else
		/// Colors the brush.
		/// @endif
		/// </summary>
		/// <returns>
		/// <c>true</c>,
		/// @if JAPANESE
		/// 変更した
		/// @else
		/// Changed.
		/// @endif
		/// <c>false</c>
		/// @if JAPANESE
		/// それ以外
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="center">
		/// @if JAPANESE
		/// 中心座標
		/// @else
		/// Center.
		/// @endif
		/// </param>
		/// <param name="radius">
		/// @if JAPANESE
		/// 半径
		/// @else
		/// Radius.
		/// @endif
		/// </param>
		/// <param name="color">
		/// @if JAPANESE
		/// 色
		/// @else
		/// Color.
		/// @endif
		/// </param>
		/// <param name="halftile">
		/// @if JAPANESE
		/// 半タイルごとに塗りつぶす。
		/// @else
		/// Fill half per tile.
		/// @endif
		/// </param>
		public bool ColorBrush( Vector2 center,float radius,Color color,bool halftile )
		{
			Vector2 extents = new Vector2( radius,radius );

			Point2 mapMinPos = LocalPointToMapPoint( center-extents );
			Point2 mapMaxPos = LocalPointToMapPoint( center+extents );

			float halfSize = 0.5f;

			bool dirty = false;

			Point2 mapPos = new Point2();
			for( mapPos.x = mapMinPos.x;mapPos.x<=mapMaxPos.x;++mapPos.x )
			{
				for( mapPos.y = mapMinPos.y;mapPos.y<=mapMaxPos.y;++mapPos.y )
				{
					Cell cell = GetCell( mapPos );

					if( cell == null )
					{
						continue;
					}

					Vector2 cellPos = MapPointToLocalPoint( cell.position );

					for( int y=0;y<2;y++ )
					{
						for( int x=0;x<2;x++ )
						{
							Vector2 vertex = cellPos + new Vector2( x*halfSize,(1-y)*halfSize );

							Vector2[] vertices = new Vector2[]{
								vertex + new Vector2(     0.0f, halfSize),
								vertex + new Vector2( halfSize, halfSize),
								vertex + new Vector2(     0.0f, 0.0f),
								vertex + new Vector2( halfSize, 0.0f),
							};

							bool check = true;

							for( int index=0;index<4;index++ )
							{
								if( halftile )
								{
									if( (vertices[index]-center).magnitude > radius )
									{
										check = false;
									}
								}
								else
								{
									float t = 1.0f-Mathf.Clamp01( (vertices[index]-center).magnitude / radius );

									Color oldColor = cell.GetVertexColor(x,y,index);
									Color nextColor = Color.Lerp( oldColor,color,t );

									cell.SetVertexColor( x,y,index,nextColor );

									if( t != 0.0f )
									{
										dirty = true;
									}
								}
							}

							if( halftile && check )
							{
								for( int index=0;index<4;index++ )
								{
									cell.SetVertexColor( x,y,index,color );
								}

								dirty = true;
							}
						}
					}

					Point2 chunkPos = cell.position/32;
					Chunk chunk = GetChunk( chunkPos );
					if( chunk != null )
					{
						chunk.dirty = true;
					}
				}
			}

			return dirty;
		}

		/// <summary>
		/// @if JAPANESE
		/// 矩形指定で頂点カラーを塗る。
		/// @else
		/// Paint a vertex color rectangle specified.
		/// @endif
		/// </summary>
		/// <returns><c>true</c>, 
		/// @if JAPANESE
		/// 変更した。
		/// @else
		/// Changed.
		/// @endif
		/// <c>false</c> 
		/// @if JAPANESE
		/// それ以外
		/// @else
		/// otherwise.
		/// @endif
		/// </returns>
		/// <param name="pos">
		/// @if JAPANESE
		/// 座標
		/// @else
		/// Position.
		/// @endif
		/// </param>
		/// <param name="width">
		/// @if JAPANESE
		/// 横幅
		/// @else
		/// Width.
		/// @endif
		/// </param>
		/// <param name="height">
		/// @if JAPANESE
		/// 縦幅
		/// @else
		/// Height.
		/// @endif
		/// </param>
		/// <param name="color">
		/// @if JAPANESE
		/// 色
		/// @else
		/// Color.
		/// @endif
		/// </param>
		/// <param name="halftile">
		/// @if JAPANESE
		/// 半タイルごとに塗りつぶす。
		/// @else
		/// Fill half per tile.
		/// @endif
		/// </param>
		public bool ColorBrushRectangle( Vector2 pos,float width,float height,Color color,bool halftile )
		{
			Rect rect = new Rect( pos.x,pos.y,width,height );

			Point2 mapMinPos = LocalPointToMapPoint( pos );
			Point2 mapMaxPos = LocalPointToMapPoint( pos + new Vector2( width,height ) );
			
			float halfSize = 0.5f;
			
			bool dirty = false;
			
			Point2 mapPos = new Point2();
			for( mapPos.x = mapMinPos.x;mapPos.x<=mapMaxPos.x;++mapPos.x )
			{
				for( mapPos.y = mapMinPos.y;mapPos.y<=mapMaxPos.y;++mapPos.y )
				{
					Cell cell = GetCell( mapPos );
					
					if( cell == null )
					{
						continue;
					}
					
					Vector2 cellPos = MapPointToLocalPoint( cell.position );
					
					for( int y=0;y<2;y++ )
					{
						for( int x=0;x<2;x++ )
						{
							Vector2 vertex = cellPos + new Vector2( x*halfSize,(1-y)*halfSize );
							
							Vector2[] vertices = new Vector2[]{
								vertex + new Vector2(     0.0f, halfSize),
								vertex + new Vector2( halfSize, halfSize),
								vertex + new Vector2(     0.0f, 0.0f),
								vertex + new Vector2( halfSize, 0.0f),
							};

							bool check = true;
							
							for( int index=0;index<4;index++ )
							{
								if( halftile )
								{
									if( !rect.Contains( vertices[index] ) )
									{
										check = false;
									}
								}
								else
								{
									if( rect.Contains( vertices[index] ) )
									{
										cell.SetVertexColor( x,y,index,color );
										dirty = true;
									}
								}
							}

							if( halftile && check )
							{
								for( int index=0;index<4;index++ )
								{
									cell.SetVertexColor( x,y,index,color );
								}
								dirty = true;
							}
						}
					}
					
					Point2 chunkPos = cell.position/32;
					Chunk chunk = GetChunk( chunkPos );
					if( chunk != null )
					{
						chunk.dirty = true;
					}
				}
			}
			
			return dirty;
		}

		/// <summary>
		/// @if JAPANESE
		/// 全てクリア
		/// @else
		/// Alls the clear.
		/// @endif
		/// </summary>
		public void AllClear()
		{
			Cell[] cells = _Cells.ToArray();
			_Cells.Clear();
			if( _CellDic!=null )
			{
				_CellDic.Clear();
			}
			if( _ColliderCellDic != null )
			{
				_ColliderCellDic.Clear();
			}

			Chunk[] chunks = _Chunks.ToArray();

			_Chunks.Clear();
			if( _ChunkDic != null )
			{
				_ChunkDic.Clear();
			}

			foreach( Cell cell in cells )
			{
				if( cell.collider != null )
				{
					ComponentUtility.Destroy( cell.collider );
				}
			}

			foreach( Chunk chunk in chunks )
			{
				ComponentUtility.Destroy( chunk.gameObject );
			}
		}

		bool GenerateIndices( Cell cell )
		{
			int oldPartsID = cell.partsID;

			cell.partsID = 0;
			
			Point2 aroundPos = new Point2();

			int id = cell.tileID;

			for( int y=0;y<2;y++ )
			{
				for( int x=0;x<2;x++ )
				{
					int vx = (x%2==0)?-1:1;
					int vy = (y%2==0)?1:-1;
					
					aroundPos.x = cell.position.x + 0;
					aroundPos.y = cell.position.y + vy;
					int longitudinalID = id;
					if( IsCellContains( aroundPos ) )
					{
						longitudinalID = GetTileID( aroundPos );
					}

					aroundPos.x = cell.position.x + vx;
					aroundPos.y = cell.position.y + 0;
					int transverseID = id;
					if( IsCellContains( aroundPos ) )
					{
						transverseID = GetTileID( aroundPos );
					}

					aroundPos.x = cell.position.x + vx;
					aroundPos.y = cell.position.y + vy;
					int obliqueID = id;
					if( IsCellContains( aroundPos ) )
					{
						obliqueID = GetTileID( aroundPos );
					}

					int index = 4;

					if (id == transverseID && id == longitudinalID && id == obliqueID) index = 0;
					else if (id == transverseID && id == longitudinalID) index = 3;
					else if (id == longitudinalID) index = 2;
					else if (id == transverseID) index = 1;

					cell.SetPartsIndex( x,y,index );
				}
			}

			if( cell.partsID != oldPartsID )
			{
				Chunk chunk = GetChunk ( cell.position/32 );
				if( chunk != null )
				{
					chunk.dirty = true;
				}

				return true;
			}

			return false;
		}

#if UNITY_EDITOR
		void OnValidate()
		{
			bool moved = false;

			foreach( Chunk chunk in _Chunks )
			{
				if( chunk != null && chunk.map != null && chunk.map != this )
				{
					moved = true;

					Debug.Log ( "Moved chunk",gameObject );

					break;
				}
			}

			if( !moved )
			{
				foreach( Cell cell in _Cells )
				{
					if( cell.collider != null && cell.collider.gameObject != gameObject )
					{
						moved = true;

						Debug.Log ( "Moved collider",gameObject );

						break;
					}
				}
			}

			if( moved )
			{
				Debug.Log ( "Moved",gameObject );

				Chunk[] oldChunks = _Chunks.ToArray();
				_Chunks.Clear ();

				foreach( Chunk oldChunk in oldChunks )
				{
					Chunk chunk = AddChunk( oldChunk.position );
					chunk.dirty = true;
				}

				foreach( Cell cell in _Cells )
				{
					Tile tile = _TileSet.tiles[cell.tileID];
					
					if( tile.collider )
					{
						cell.collider = ComponentUtility.AddComponent<BoxCollider2D>( gameObject );
						
						cell.collider.hideFlags = HideFlags.HideInInspector;
						
						cell.collider.sharedMaterial = tile.physicsMaterial;

						cell.collider.isTrigger = tile.isTrigger;
												
						float halfSize = 0.5f;
						
						Vector3 localPos = MapPointToLocalPoint( cell.position );
						
						cell.collider.offset = new Vector2( localPos.x+halfSize,localPos.y+halfSize );
						cell.collider.size = new Vector2( 1.0f,1.0f );
						
						if( _ColliderCellDic != null && !_ColliderCellDic.ContainsKey( cell.collider ) )
						{
							_ColliderCellDic.Add( cell.collider,cell );
						}
					}
				}
			}

			UnityEditor.PrefabType prefabType = UnityEditor.PrefabUtility.GetPrefabType( this );

			foreach( Chunk chunk in _Chunks )
			{
				if( chunk != null )
				{
					chunk.gameObject.hideFlags = HideFlags.HideInHierarchy;

					chunk.sortingLayerID = _SortingLayerID;
					chunk.sortingOrder = _SortingOrder;

					if( prefabType == UnityEditor.PrefabType.PrefabInstance )
					{
						chunk.dirty = true;
					}
				}
			}

			foreach( Cell cell in _Cells )
			{
				if( cell.collider != null )
				{
					if( prefabType == UnityEditor.PrefabType.Prefab )
					{
						cell.collider.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
					}
					else
					{
						cell.collider.hideFlags = HideFlags.HideInInspector;
					}
				}
			}

			if( _DummyRenderer != null )
			{
				if( prefabType == UnityEditor.PrefabType.Prefab )
				{
					_DummyRenderer.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
				}
				else
				{
					_DummyRenderer.hideFlags = HideFlags.HideInInspector;
				}
			}
			
			_ChunkDic = null;
			_ChangedCellDic = true;
		}

		void OnDrawGizmos ()
		{
			if( gameObject.activeInHierarchy && enabled )
			{
				Vector3 center = new Vector3( _Width * 0.5f,_Height*0.5f,0.0f );
				Vector3 size = new Vector3( _Width,_Height,0.01f );
				
				Gizmos.matrix = transform.localToWorldMatrix;
				Gizmos.color = ( UnityEditor.Selection.activeGameObject == gameObject ) ? Color.white : new Color(1.0f,1.0f,1.0f,0.2f );
				Gizmos.DrawWireCube(center, size);
				
				Gizmos.color = Color.clear;
				Gizmos.DrawCube(center, size);
			}
		}
#endif

		private bool _Changed = false;

		/// <summary>
		/// @if JAPANESE
		/// セルを再構築します。
		/// TileSetを更新した場合などに呼び出してください。
		/// @else
		/// Refresh this instance.
		/// Please call if you have updated the TileSet.
		/// @endif
		/// </summary>
		public bool Refresh()
		{
			bool changed = false;

			foreach( Cell cell in _Cells )
			{
				Tile tile = GetTile( cell );

				if( tile.collider )
				{
					if( cell.collider == null )
					{
						cell.collider = ComponentUtility.AddComponent<BoxCollider2D>( gameObject );
						
						cell.collider.hideFlags = HideFlags.HideInInspector;
						
						float halfSize = 0.5f;
						
						Vector3 localPos = MapPointToLocalPoint( cell.position );
						
						cell.collider.offset = new Vector2( localPos.x+halfSize,localPos.y+halfSize );
						cell.collider.size = new Vector2( 1.0f,1.0f );
						
						if( _ColliderCellDic != null && !_ColliderCellDic.ContainsKey( cell.collider ) )
						{
							_ColliderCellDic.Add( cell.collider,cell );
						}

						changed = true;
					}

					if( cell.collider.sharedMaterial != tile.physicsMaterial )
					{
						cell.collider.sharedMaterial = tile.physicsMaterial;

						changed = true;
					}

					if( cell.collider.isTrigger != tile.isTrigger )
					{
						cell.collider.isTrigger = tile.isTrigger;

						changed = true;
					}
				}
				else if( cell.collider != null )
				{
					ComponentUtility.Destroy( cell.collider );
					cell.collider = null;

					changed = true;
				}
			}

			SetDirty();

			return changed;
		}

		/// <summary>
		/// @if JAPANESE
		/// メッシュを再構築するように設定します。
		/// Cellを直接変更した際に呼び出してください。
		/// @else
		/// You set up to rebuild the mesh. 
		/// Please call when you directly modify the Cell.
		/// @endif
		/// </summary>
		public void SetDirty()
		{
			_ChunkDic = null;
			_Changed = true;
			_ChangedCellDic = true;
		}

		private TileSet _CachedTileSet = null;
		private Material _CachedMaterial = null;

		private Material _Material = null;

		/// <summary>
		/// @if JAPANESE
		/// マテリアルを取得
		/// @else
		/// Gets the material.
		/// @endif
		/// </summary>
		public Material material
		{
			get
			{
				return _Material;
			}
		}

		private Texture _CachedTexture = null;

		void LateUpdate()
		{
			if( _TileSet != _CachedTileSet )
			{
				_Changed = true;
			}

			if( _TileSet != null )
			{
				if( _TileSet.material != _CachedMaterial )
				{
					_Changed = true;
				}
				else if( _TileSet.material != null && _TileSet.material.mainTexture != _CachedTexture )
				{
					_Changed = true;
				}
			}

			_CachedTileSet = _TileSet;

			if( _TileSet != null )
			{
				if( _TileSet.material != _CachedMaterial )
				{
					_CachedMaterial = _TileSet.material;

					if( _Material != null )
					{
						Object.Destroy( _Material );
						_Material = null;
					}

					if( _CachedMaterial != null )
					{
						_Material = new Material( _CachedMaterial );
						_Material.hideFlags = HideFlags.HideAndDontSave;
					}
				}

				if( _TileSet.material != null )
				{
					_CachedTexture = _TileSet.material.mainTexture;
					_Material.mainTexture = _TileSet.material.mainTexture;
				}
				else
				{
					_CachedTexture = null;
				}
			}
			else
			{
				_CachedMaterial = null;
				if( _Material != null )
				{
					Object.Destroy( _Material );
					_Material = null;
				}
				_CachedTexture = null;
			}

			if( _Material != null && _Material.color != _Color )
			{
				_Material.color = _Color;
			}

			foreach( Chunk chunk in _Chunks )
			{
				if( _Changed || chunk != null && chunk.dirty )
				{
					chunk.UpdateMesh();

					chunk.dirty = false;
				}
			}

			_Changed = false;
		}

		void OnEnable()
		{
			foreach( Chunk chunk in _Chunks )
			{
				if( chunk != null )
				{
					chunk.gameObject.SetActive( true );
				}
			}

			foreach( Cell cell in _Cells )
			{
				if( cell.collider != null )
				{
					cell.collider.enabled = true;
				}
			}
		}

		void OnDisable()
		{
			foreach( Chunk chunk in _Chunks )
			{
				if( chunk != null )
				{
					chunk.gameObject.SetActive( false );
				}
			}

			foreach( Cell cell in _Cells )
			{
				if( cell.collider != null )
				{
					cell.collider.enabled = false;
				}
			}
		}

		void OnDestroy()
		{
			if( Application.isPlaying )
			{
				DestroySubComponents();
			}

			_Maps.Remove( this );
		}

		/// <summary>
		/// @if JAPANESE
		/// サブコンポーネントを削除します。
		/// 通常、呼び出す必要はありません。
		/// @else
		/// Destroies the sub components.
		/// Usually, you do not need to call.
		/// @endif
		/// </summary>
		public void DestroySubComponents()
		{
			foreach( Chunk chunk in _Chunks )
			{
				if( chunk != null )
				{
					ComponentUtility.Destroy( chunk.gameObject );
				}
			}

			foreach( Cell cell in _Cells )
			{
				if( cell.collider != null )
				{
					ComponentUtility.Destroy( cell.collider );
					cell.collider = null;
				}
			}

			if( _DummyRenderer != null )
			{
				ComponentUtility.Destroy( _DummyRenderer );
				_DummyRenderer = null;
			}
		}
	}
}