﻿using UnityEngine;
using System.Collections;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// Tileに付加できるComponentの基本クラス。<br>
	/// このクラスを継承して必要なデータを定義する。
	/// @else
	/// Base class of the Component can be added to the Tile. 
	/// To define the required data by inheriting this class.
	/// @endif
	/// </summary>
	public class TileComponent : ScriptableObject
	{
		[SerializeField,HideInInspector] private Tile _Tile;

		/// <summary>
		/// @if JAPANESE
		/// Tileを取得
		/// @else
		/// Gets the tile.
		/// @endif
		/// </summary>
		public Tile tile
		{
			get
			{
				return _Tile;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// TileにTileComponentを追加する。
		/// @else
		/// Adds the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 追加されたTileComponent。
		/// @else
		/// TileComponent that have been added.
		/// @endif
		/// </returns>
		/// <param name="type">
		/// @if JAPANESE
		/// 追加するクラスのType。
		/// @else
		/// Type of the class you want to add.
		/// @endif
		/// </param>
		public TileComponent AddComponent( System.Type type )
		{
			if( _Tile == null )
			{
				return null;
			}
			return _Tile.AddComponent( type );
		}

		/// <summary>
		/// @if JAPANESE
		/// TileにTileComponentを追加する。
		/// @else
		/// Adds the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 追加されたTileComponent。
		/// @else
		/// TTileComponent that have been added.
		/// @endif
		/// </returns>
		/// <typeparam name="T">
		/// @if JAPANESE
		/// 追加するTileComponentのクラス。
		/// @else
		/// Class of TileComponent to add.
		/// @endif
		/// </typeparam>
		public T AddComponent<T>() where T : TileComponent
		{
			if( _Tile == null )
			{
				return null;
			}
			return _Tile.AddComponent<T>();
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentを取得する。
		/// @else
		/// Gets the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponent。<br>
		/// 無かった場合はnullを返す。
		/// @else
		/// TileComponent.<br>
		/// The return value may be null if it was not.
		/// @endif
		/// </returns>
		/// <param name="type">
		/// @if JAPANESE
		/// 取得したいクラスのType。
		/// @else
		/// Type of the class you want to get.
		/// @endif
		/// </param>
		public TileComponent GetComponent( System.Type type )
		{
			if( _Tile == null )
			{
				return null;
			}
			return _Tile.GetComponent( type );
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentを取得する。
		/// @else
		/// Gets the TileComponent.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponent。<br>
		/// 無かった場合はnullを返す。
		/// @else
		/// TileComponent.<br>
		/// The return value may be null if it was not.
		/// @endif
		/// </returns>
		/// <typeparam name="T">
		/// @if JAPANESE
		/// 取得したいTileComponentのクラス。
		/// @else
		/// Class of TileComponent you want to get.
		/// @endif
		/// </typeparam>
		public T GetComponent<T>() where T : TileComponent
		{
			if( _Tile == null )
			{
				return null;
			}
			return _Tile.GetComponent<T>();
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentの配列を取得する。
		/// @else
		/// Gets the TileComponents.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponentの配列。
		/// @else
		/// Array of TileComponent.
		/// @endif
		/// </returns>
		/// <param name="type">
		/// @if JAPANESE
		/// 取得したいクラスのType。
		/// @else
		/// Type of the class you want to get.
		/// @endif
		/// </param>
		public TileComponent[] GetComponents( System.Type type )
		{
			if( _Tile == null )
			{
				return null;
			}
			return _Tile.GetComponents( type );
		}

		/// <summary>
		/// @if JAPANESE
		/// TileComponentの配列を取得する。
		/// @else
		/// Gets the TileComponents.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// TileComponentの配列。
		/// @else
		/// Array of TileComponent.
		/// @endif
		/// </returns>
		/// <typeparam name="T">
		/// @if JAPANESE
		/// 取得したいTileComponentのクラス。
		/// @else
		/// Class of TileComponent you want to get.
		/// @endif
		/// </typeparam>
		public T[] GetComponets<T>() where T : TileComponent
		{
			if( _Tile == null )
			{
				return null;
			}
			return _Tile.GetComponents<T>();
		}
	}
}