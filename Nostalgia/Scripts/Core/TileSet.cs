﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// タイルセットのデータ。
	/// @else
	/// Data of the tile set.
	/// @endif
	/// </summary>
	public sealed class TileSet : ScriptableObject
	{
		/// <summary>
		/// @if JAPANESE
		/// マテリアル。
		/// @else
		/// The material.
		/// @endif
		/// </summary>
		public Material material;

		[SerializeField] private List<Tile> _Tiles = new List<Tile>();

		/// <summary>
		/// @if JAPANESE
		/// タイルの配列。
		/// @else
		/// The tiles.
		/// @endif
		/// </summary>
		public Tile[] tiles
		{
			get
			{
				return _Tiles.ToArray();
			}
		}

		static FieldInfo _TileSetField;
		static FieldInfo tileSetField
		{
			get
			{
				if( _TileSetField == null )
				{
					System.Type type = typeof(Tile);
					_TileSetField = type.GetField( "_TileSet",BindingFlags.Instance | BindingFlags.NonPublic );
				}
				return _TileSetField;
			}
		}

		/// <summary>
		/// @if JAPANESE
		/// Tileを追加する。
		/// @else
		/// Adds the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 追加されたTile。
		/// @else
		/// The added tile.
		/// @endif
		/// </returns>
		public Tile AddTile()
		{
			Tile tile = ScriptableObject.CreateInstance<Tile>();
			tile.hideFlags = HideFlags.HideInHierarchy;

			tileSetField.SetValue( tile,this );

			_Tiles.Add( tile );

			return tile;
		}

		/// <summary>
		/// @if JAPANESE
		/// Tileを挿入する。
		/// @else
		/// Inserts the tile.
		/// @endif
		/// </summary>
		/// <returns>
		/// @if JAPANESE
		/// 挿入されたTile。
		/// @else
		/// The inserted tile.
		/// @endif
		/// </returns>
		/// <param name="index">
		/// @if JAPANESE
		/// 挿入先のインデックス
		/// @else
		/// Index of the insert destination.
		/// @endif
		/// </param>
		public Tile InsertTile( int index )
		{
			Tile tile = ScriptableObject.CreateInstance<Tile>();
			tile.hideFlags = HideFlags.HideInHierarchy;
			
			tileSetField.SetValue( tile,this );

			_Tiles.Insert( index,tile );

			return tile;
		}

		/// <summary>
		/// @if JAPANESE
		/// タイルを削除する。
		/// @else
		/// Removes the tile.
		/// @endif
		/// </summary>
		/// <param name="tile">
		/// @if JAPANESE
		/// 削除するTile。
		/// @else
		/// Tile you want to remove.
		/// @endif
		/// </param>
		public void RemoveTile( Tile tile )
		{
			_Tiles.Remove( tile );
		}
	}
}