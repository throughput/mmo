﻿using System;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// タイルのAdd Componentボタンに表示するメニュー。<br>
	/// 指定しない場合は"Scripts/"以下に追加される。<br>
	/// menuNameをnullに指定した場合表示しない。<br>
	/// @else
	/// Menu to be displayed in the Add Component button on the tile. <br>
	/// It is added to the following "Scripts /" If you do not specify. <br>
	///	Do not show if you specify to null menuName.<br>
	/// @endif
	/// </summary>
	public sealed class AddTileMenu : System.Attribute
	{
		private string _MenuName;
		public string menuName
		{
			get
			{
				return _MenuName;
			}
		}

		public AddTileMenu( string menuName )
		{
			_MenuName = menuName;
		}
	}
}