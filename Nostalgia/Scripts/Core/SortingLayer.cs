﻿using UnityEngine;
using System.Collections;

namespace Nostalgia
{
	/// <summary>
	/// @if JAPANESE
	/// GameObjectにアタッチされているRendererのSorting Layerを変更する。
	/// @else
	/// Change the Sorting Layer of Renderer that is attached to the GameObject.
	/// @endif
	/// </summary>
	[ExecuteInEditMode,AddComponentMenu("Nostalgia/SortingLayer")]
	public class SortingLayer : MonoBehaviour 
	{
		void Awake()
		{
			foreach( Component component in GetComponents<Component>() )
			{
				if( component is SpriteRenderer || component is Map || (component is SortingLayer && component != this ) )
				{
					Debug.LogError( "Can't add the Component for SortingLayer Component already exists!",gameObject );
					
#if UNITY_EDITOR
					DestroyImmediate( this );
#else
					Destroy( this );
#endif
					return;
				}
			}
		}

		void Reset()
		{
			if( GetComponent<Renderer>() != null )
			{
				GetComponent<Renderer>().sortingLayerID = 0;
				GetComponent<Renderer>().sortingOrder = 0;
			}
		}
	}
}