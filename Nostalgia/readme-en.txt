﻿-----------------------------------------------------
            Nostalgia: 2D Tile Map Editor
          Copyright (c) 2014 Cait Sith Ware
          http://caitsithware.com/wordpress/
          support@caitsithware.com
-----------------------------------------------------

Thank you for purchasing Nostalgia!

[Main flow]

1. Create TileSet asset.
   Click on the "Assets> Create> Nostalgia> TileSet" from the menu.
2. Attaching the Nostalgia / Map to GameObject.
3. Set TileSet to Map.
4. Place a tile by dragging the Scene view.

[Sample]

Here are the sample.
Assets/Nostalgia/Examples/

[ScriptReference]

Here are the script reference. 
Assets/Nostalgia/Docs/ 

Unzip the zip file, please open it in your browser index.html.

[Document] 

Click here for detailed document. 
http://caitsithware.com/wordpress/assetstore/nostalgia/

(Please select from the sidebar if you want to English)

[Support]

Mail : support@caitsithware.com
BBS  : http://caitsithware.com/wordpress/assetstore/nostalgia/forum

[Update History] 

Ver 1.2.0f1:
- Fixed NullReferenceException during execution is output on the editor of Unity 4.6.

Ver 1.2.0:
- Vertex color paint tool added.
- SortingLayer window added.
- Sample scene update of Platformer.
- Fixed error when was oriented in the opposite direction of the scene view camera to the Map editing.

Ver 1.1:
- Corresponding to be able to add to TileComponent tile.
- Corresponding to be able to set the state of the parts that can be placed in the Shift key.
- SortingLayer additional components for setting the drawing order of the Renderer.
- View active switching toggle the GameObject to Hierarychy.
- Change to be able to attach only one Map for GameObject one.
- To prepare a sample scene of Platformer.
- Fixed setting of Collider code does not change when you overwrite place the other tiles.

Ver 1.0.4:
- Designated corresponding material color. 
- Fixed gap is visible between the tiles by the screen size.
- Fixed a was not reflected even later change the Order in Layer and Sorting Layer.

Ver 1.0.3:
- Embedded document for comment.
- Fix misspelling of "Create Other> Nostalgia Map".

Ver 1.0.2
- FIX: The Fixed a can not create error out when the "TileSet from selected Texture or Material" by selecting the Texture.
- FIX: Fixed Collider is not deleted or generated correctly when you overcoated with a tile.

Ver 1.0.1
- FIX: Fixed to the can not build error out.
- FIX: Fixed Main Camera of Example scene of the disabled.

Ver 1.0
- Released.