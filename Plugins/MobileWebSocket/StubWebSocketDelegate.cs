using UnityEngine;
using System.Collections;

public class StubWebSocketDelegate : MobileWebSocketDelegate {
	
	private readonly object[] EMPTY_MESSAGES = {};
	
	public StubWebSocketDelegate(MobileWebSocket mobileWebSocket)
		: base(mobileWebSocket) {
	}
	
	protected internal override void Awake() {
		Debug.Log ("Using Stub WebSocket");
	}

	public override void Connect() {}
	public override void Disconnect() {}

	public override object[] PeekMessages(object[] reuseableMessageArray) {
		return EMPTY_MESSAGES;
	}
	
	public override void SendBinaryMessage(byte[] payload) {}
	public override void SendTextMessage(string payload) {}
	
	protected override void SetUri(string uri) {}
	protected override void SetGameObjectName(string name) {}
	protected override void ClearMessages() {}

}
