#if UNITY_IPHONE

using UnityEngine;
using System;
using System.Runtime.InteropServices;

public class IPhoneWebSocketDelegate : MobileWebSocketDelegate {
	
	[DllImport ("__Internal")]
	private static extern IntPtr IPhoneWebSocket_newWebSocket(
		[MarshalAs(UnmanagedType.LPStr)] string url,
        int maxBytesHeld,
        int maxMessagesHeld,
        [MarshalAs(UnmanagedType.LPStr)] string discardPolicy,
		IntPtr pinnedSslCertificates
	);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_destroyWebSocket(IntPtr nativeWebSocket);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_connect(IntPtr nativeWebSocket);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_disconnect(IntPtr nativeWebSocket);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_sendBinaryMessage(IntPtr nativeWebSocket, byte[] payload, uint length);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_sendTextMessage(IntPtr nativeWebSocket, [MarshalAs(UnmanagedType.LPStr)] string payload);
	[DllImport ("__Internal")]
	private static extern IntPtr IPhoneWebSocket_getMessages(IntPtr nativeWebSocket);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_updateMessages(IntPtr nativeWebSocket);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_clearMessages(IntPtr nativeWebSocket);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_setGameObjectName(IntPtr nativeWebSocket, [MarshalAs(UnmanagedType.LPStr)] string name);
	[DllImport ("__Internal")]
	private static extern IntPtr IPhoneWebSocket_newCertificateArray(IntPtr cCertStrings);
	[DllImport ("__Internal")]
	private static extern void IPhoneWebSocket_destroyCertificateArray(IntPtr certs);

	
	private IntPtr NativeWebSocket;
	// each element of this array is pointer to {PointerToBinary, PointerToText}
	private IntPtr[] NativeMessagesArray;
	private IntPtr NativePinnedSSLCerts = IntPtr.Zero;
	
	private IPhoneWebSocketOptions Options;
	
	//====================
	// Constructor
	//====================
	
	public IPhoneWebSocketDelegate(MobileWebSocket mobileWebSocket)
		: base(mobileWebSocket) {
	}
	
	//====================
	// Connection API
	//====================

	public override void Connect() {
		IPhoneWebSocket_connect(NativeWebSocket);
	}
	public override void Disconnect() {
		IPhoneWebSocket_disconnect(NativeWebSocket);
	}
	
	//===============
	// Read API
	//===============

	public override object[] PeekMessages(object[] reuseableMessageArray) {
		IPhoneWebSocket_updateMessages(NativeWebSocket);
		
		IntPtr[] message2Types = new IntPtr[2];
		byte[] lenLittleEndian = new byte[4];
		bool foundNull = false;
		for (int i = 0; i < MaxMessagesHeld && !foundNull; ++i) {
			// 2-element array, exactly one non-null: { binaryMessage, textMessage }
			Marshal.Copy(NativeMessagesArray[i], message2Types, 0, 2);
			IntPtr binaryMessage  = message2Types[0];
			IntPtr textMessage    = message2Types[1];
			
			if (binaryMessage != IntPtr.Zero) {
				Marshal.Copy(binaryMessage, lenLittleEndian, 0, 4);
				int len =
					(lenLittleEndian[0]) +
					(lenLittleEndian[1] << 8) +
					(lenLittleEndian[2] << 16) +
					(lenLittleEndian[3] << 24);
				
				byte[] dest = new byte[len];
				reuseableMessageArray[i] = dest;
				
				// http://social.msdn.microsoft.com/Forums/vstudio/en-US/eac3bbbd-e5d6-48e1-86b6-fee6476eba5e/read-data-with-marshalcopy
				IntPtr binaryMessageData = new IntPtr(binaryMessage.ToInt64() + 4);
				Marshal.Copy (binaryMessageData, dest, 0, (int)len);
			}
			else if (textMessage != IntPtr.Zero) {
				Marshal.Copy(textMessage, lenLittleEndian, 0, 4);
				int len =
					(lenLittleEndian[0]) +
					(lenLittleEndian[1] << 8) +
					(lenLittleEndian[2] << 16) +
					(lenLittleEndian[3] << 24);
				
				byte[] stringBytes = new byte[len];
				
				IntPtr textMessageData = new IntPtr(textMessage.ToInt64() + 4);
				Marshal.Copy (textMessageData, stringBytes, 0, (int)len);
				// http://stackoverflow.com/questions/1003275/byte-to-string-in-c-sharp
				reuseableMessageArray[i] = System.Text.Encoding.UTF8.GetString(stringBytes);
			}
			else {
				// { null, null }: no more messages
				foundNull = true;
			}
		}
		return reuseableMessageArray;
	}
	
	//=================
	// Write API
	//=================
	
	public override void SendBinaryMessage(byte[] payload) {
		IPhoneWebSocket_sendBinaryMessage(NativeWebSocket, payload, (uint)payload.Length);
	}
	public override void SendTextMessage(string payload) {
		IPhoneWebSocket_sendTextMessage(NativeWebSocket, payload);
	}
	
	// ====================================
	// Unity Lifecycle
	// ====================================
	
	protected internal override void Awake() {
		base.Awake();
		
		Options = m_MobileWebSocket.GetComponent<IPhoneWebSocketOptions>();

		if (Options.PinnedSSLCertificatesPEM != null && Options.PinnedSSLCertificatesPEM.Length > 0) {
			// first, copy certs to unmanaged memory
			
			int numCerts = Options.PinnedSSLCertificatesPEM.Length;
			IntPtr[] nativeStrings = new IntPtr[numCerts + 1];
			for (int i = 0; i < numCerts; i++) {
				string cert = Options.PinnedSSLCertificatesPEM[i];
				nativeStrings[i] = Marshal.StringToHGlobalAuto(cert);
			}
			nativeStrings[numCerts] = IntPtr.Zero;  // NULL-terminated
			
			int ptrSize = Marshal.SizeOf(typeof(IntPtr));
			IntPtr cNativeStrings = Marshal.AllocHGlobal((numCerts + 1) * ptrSize);
			Marshal.Copy (nativeStrings, 0, cNativeStrings, numCerts + 1);
			
			// create in iOS
			NativePinnedSSLCerts = IPhoneWebSocket_newCertificateArray(cNativeStrings);
			
			// free memory
			Marshal.FreeHGlobal(cNativeStrings);
			for (int i = 0; i < numCerts; i++) {
				Marshal.FreeHGlobal(nativeStrings[i]);
			}
		}

		NativeWebSocket = IPhoneWebSocket_newWebSocket(
			WebSocketUri, MaxBytesHeld, MaxMessagesHeld, MessageDiscardPolicy.ToString(), NativePinnedSSLCerts
		);
	}
	
	protected internal override void OnEnable() {
		base.OnEnable();
		InitMessagesArray();
	}
	
	protected internal override void Start() {
		base.Start();
	}
	
	protected internal override void OnDisable() {
		NativeMessagesArray = null;
		base.OnDisable();
	}
	
	protected internal override void OnDestroy() {
		IPhoneWebSocket_destroyWebSocket(NativeWebSocket);
		if (NativePinnedSSLCerts != IntPtr.Zero)
			IPhoneWebSocket_destroyCertificateArray(NativePinnedSSLCerts);
		base.OnDestroy();
	}
	
	protected override void SetUri(string uri) {
		// no-op. URI required in SRWebSocket constructor
	}
	
	protected override void SetGameObjectName(string name) {
		IPhoneWebSocket_setGameObjectName(NativeWebSocket, name);
	}
	
	protected override void ClearMessages() {
		IPhoneWebSocket_clearMessages(NativeWebSocket);
	}
	
	private void InitMessagesArray() {
		NativeMessagesArray = new IntPtr[MaxMessagesHeld];
		IntPtr array0 = IPhoneWebSocket_getMessages(NativeWebSocket);
		Marshal.Copy(array0, NativeMessagesArray, 0, MaxMessagesHeld);
	}
}

#endif
