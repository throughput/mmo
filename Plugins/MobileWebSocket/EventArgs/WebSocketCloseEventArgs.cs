using UnityEngine;
using System;

public class WebSocketCloseEventArgs : EventArgs
{
	public int Code;
	public string Reason;
}

