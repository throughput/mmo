using UnityEngine;
using System;

public class WebSocketErrorEventArgs : EventArgs
{
	public int Code;
	public string Description;
}

