using UnityEngine;
using System;

public class WebSocketIncomingMessagesEventArgs : EventArgs {
	
	public bool HasIncomingMessages;
	
}
