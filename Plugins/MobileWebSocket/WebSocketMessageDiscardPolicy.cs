public enum WebSocketMessageDiscardPolicy {
	KeepOldest,
	KeepNewest
}
