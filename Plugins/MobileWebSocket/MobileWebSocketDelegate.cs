using UnityEngine;
using System;

public abstract class MobileWebSocketDelegate {
	
	protected MobileWebSocket m_MobileWebSocket;
	
	protected string WebSocketUri {
		get {
			return m_MobileWebSocket.WebSocketUri;
		}
	}
	protected bool ConnectOnStart {
		get {
			return m_MobileWebSocket.ConnectOnStart;
		}
	}
	protected int MaxMessagesHeld {
		get {
			return m_MobileWebSocket.MaxMessagesHeld;
		}
	}
	protected int MaxBytesHeld {
		get {
			return m_MobileWebSocket.MaxBytesHeld;
		}
	}
	protected WebSocketMessageDiscardPolicy MessageDiscardPolicy {
		get {
			return m_MobileWebSocket.MessageDiscardPolicy;
		}
	}
	
	protected internal bool hasIncomingMessages;
	public bool HasIncomingMessages {
		get {
			return hasIncomingMessages;
		}
	}
	
	private string name {
		get {
			return m_MobileWebSocket.name;
		}
	}
	
	//===================================
	// Constructor
	//===================================
	
	public MobileWebSocketDelegate(MobileWebSocket mobileWebSocket) {
		m_MobileWebSocket = mobileWebSocket;
	}
	
	//=====================================
	// Connect API
	//=====================================

	public abstract void Connect();
	public abstract void Disconnect();
	
	//=====================================
	// Read API
	//=====================================
	
	public object[] TakeMessages(object[] reuseableMessageArray) {
		object[] result = PeekMessages(reuseableMessageArray);
		
		ClearMessages();
		hasIncomingMessages = false;
		
		return result;
	}
	
	public abstract object[] PeekMessages(object[] reuseableMessageArray);
	
	//=================================
	// Write API
	//=================================

	public abstract void SendBinaryMessage(byte[] payload);
	public abstract void SendTextMessage(string payload);
	
	// ====================================
	// Unity Lifecycle
	// http://answers.unity3d.com/questions/217941/onenable-awake-start-order.html
	// ====================================
	
	protected internal virtual void Awake() {}
	
	protected internal virtual void OnEnable() {
		SetUri(this.WebSocketUri);
		SetGameObjectName(this.name);
	}
	
	protected internal virtual void Start() {
		if (ConnectOnStart) {
			Connect();
		}
	}
	
	protected internal virtual void OnDisable() {
		ClearMessages();
		Disconnect();
	}
	
	protected internal virtual void OnDestroy() {}

	protected abstract void SetUri(string uri);
	protected abstract void SetGameObjectName(string name);
	protected abstract void ClearMessages();

}
