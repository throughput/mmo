using UnityEngine;
using System;

[RequireComponent (typeof (AndroidWebSocketOptions))]
[RequireComponent (typeof (IPhoneWebSocketOptions))]
public class MobileWebSocket : MonoBehaviour {
	
	private MobileWebSocketDelegate Delegate;
	
	public string WebSocketUri = "wss://echo.websocket.org/";
	
	public bool ConnectOnStart = true;
	
	public int MaxMessagesHeld = 120;
	public int MaxBytesHeld = 524288;
	
	public WebSocketMessageDiscardPolicy MessageDiscardPolicy = WebSocketMessageDiscardPolicy.KeepNewest;
	
	public bool HasIncomingMessages {
		get {
			return Delegate.HasIncomingMessages;
		}
	}
	
	private bool m_IsOpen;
	public bool IsOpen {
		get {
			return m_IsOpen;
		}
	}
	
	//==================================
	// Constructor
	//==================================
	
	public MobileWebSocket() : base() {
#if UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android) {
			Delegate = new AndroidWebSocketDelegate(this);
		}
#elif UNITY_IPHONE
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			Delegate = new IPhoneWebSocketDelegate(this);
		}
#endif
		if (Delegate == null) {
			Delegate = new StubWebSocketDelegate(this);
		}
	}
	
	//==================================
	// Event Handlers
	//==================================
	
	private EventHandler m_OnOpen;
    public event EventHandler OnOpen
    {
        add { m_OnOpen += value; }
        remove { m_OnOpen -= value; }
    }
	
	private EventHandler<WebSocketCloseEventArgs> m_OnClose;
    public event EventHandler<WebSocketCloseEventArgs> OnClose
    {
        add { m_OnClose += value; }
        remove { m_OnClose -= value; }
    }
	
	private EventHandler<WebSocketErrorEventArgs> m_OnError;
    public event EventHandler<WebSocketErrorEventArgs> OnError
    {
        add { m_OnError += value; }
        remove { m_OnError -= value; }
    }
	
	private EventHandler<WebSocketIncomingMessagesEventArgs> m_OnIncomingMessages;
    public event EventHandler<WebSocketIncomingMessagesEventArgs> OnIncomingMessages
    {
        add { m_OnIncomingMessages += value; }
        remove { m_OnIncomingMessages -= value; }
    }
	
	//=====================================
	// Read API
	//=====================================
	
	public object[] TakeMessages() {
		return Delegate.TakeMessages(NewReuseableMessageArray());
	}
	public object[] TakeMessages(object[] reuseableMessageArray) {
		return Delegate.TakeMessages(reuseableMessageArray);
	}
	public object[] PeekMessages() {
		return Delegate.PeekMessages(NewReuseableMessageArray());
	}
	public object[] PeekMessages(object[] reuseableMessageArray) {
		return Delegate.PeekMessages(reuseableMessageArray);
	}
	
	public object[] NewReuseableMessageArray() {
		return new object[MaxMessagesHeld];
	}
	
	//=================================
	// Write API
	//=================================

	public void SendBinaryMessage(byte[] payload) {
		Delegate.SendBinaryMessage(payload);
	}
	public void SendTextMessage(string payload) {
		Delegate.SendTextMessage(payload);
	}
	
	// ====================================
	// Callbacks from native
	// ====================================
	
	// Callback
	public void OnWebSocketOpen(string voidParams) {
		m_IsOpen = true;
		
		if (m_OnOpen != null) {
			m_OnOpen(this, EventArgs.Empty);
		}
	}
	
	// Callback
	public void OnWebSocketClose(string codeAndReasonParams) {
		m_IsOpen = false;
		
		if (m_OnClose != null) {
			int separatorIndex = codeAndReasonParams.IndexOf(' ');
			
			string codeString = codeAndReasonParams.Substring(0, separatorIndex);
			int code = int.Parse(codeString);
			string reason = codeAndReasonParams.Substring(separatorIndex+1);
			
			WebSocketCloseEventArgs eventArgs = new WebSocketCloseEventArgs();
			eventArgs.Code = code;
			eventArgs.Reason = reason;
			m_OnClose(this, eventArgs);
		}
	}
	
	// Callback
	public void OnWebSocketError(string codeAndDescriptionParams) {
		if (m_OnError != null) {
			int separatorIndex = codeAndDescriptionParams.IndexOf(' ');
			
			string codeString = codeAndDescriptionParams.Substring(0, separatorIndex);
			int code = int.Parse(codeString);
			string description = codeAndDescriptionParams.Substring(separatorIndex+1);
			
			WebSocketErrorEventArgs eventArgs = new WebSocketErrorEventArgs();
			eventArgs.Code = code;
			eventArgs.Description = description;
			m_OnError(this, eventArgs);
		}
	}
	
	// Callback
	public void SetHasIncomingMessages(string booleanString) {
		Delegate.hasIncomingMessages = "true".Equals(booleanString);
		
		if (m_OnIncomingMessages != null) {
			WebSocketIncomingMessagesEventArgs eventArgs = new WebSocketIncomingMessagesEventArgs();
			eventArgs.HasIncomingMessages = Delegate.HasIncomingMessages;
			m_OnIncomingMessages(this, eventArgs);
		}
	}
	
	// ====================================
	// Unity Lifecycle
	// http://answers.unity3d.com/questions/217941/onenable-awake-start-order.html
	// ====================================
	
	void Awake() {
		Delegate.Awake();
	}
	
	void OnEnable() {
		Delegate.OnEnable();
	}
	
	void Start() {
		Delegate.Start();
	}
	
	void OnDisable() {
		Delegate.OnDisable();
	}
	
	void OnDestroy() {
		Delegate.OnDestroy();
	}

}
