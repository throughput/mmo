#if UNITY_ANDROID

using UnityEngine;
using System;

public class AndroidWebSocketDelegate : MobileWebSocketDelegate {
	
	private enum JavaDiscardPolicy {
		KEEP_OLDEST,
		KEEP_NEWEST
	}
	
	private AndroidWebSocketOptions Options;
	
	//================================
	// JNI references
	//================================
	
	// global object refs
	
	private IntPtr   JavaClass;
	private IntPtr   JavaMessagesArray;
	private IntPtr[] JavaMessagesArrayElements;
	private IntPtr   JavaWebSocketOptions;
	
	// methods
	
	private IntPtr   JavaConnect;
	private IntPtr   JavaDisconnect;
	
	private IntPtr   JavaSetGameObjectName;
	private IntPtr   JavaSetUri;
	
	private IntPtr   JavaSendBinaryMessage;
	private IntPtr   JavaSendTextMessage;
	
	private IntPtr   JavaGetMessages;
	private IntPtr   JavaUpdateMessages;
	private IntPtr   JavaClearMessages;
	
	private static readonly jvalue[] EMPTY_ARGS = {};
	
	//=====================================
	// Constructor
	//=====================================
	
	public AndroidWebSocketDelegate(MobileWebSocket mobileWebSocket)
		: base(mobileWebSocket) {
	}
	
	//=====================================
	// Connect API
	//=====================================
	
	public override void Connect() {
		AndroidJNI.CallVoidMethod(JavaClass, JavaConnect, EMPTY_ARGS);
	}

	public override void Disconnect() {
		AndroidJNI.CallVoidMethod(JavaClass, JavaDisconnect, EMPTY_ARGS);
	}
	
	//=====================================
	// Read API
	//=====================================
	
	public override object[] PeekMessages(object[] reuseableMessageArray) {
		AndroidJNI.CallVoidMethod(JavaClass, JavaUpdateMessages, EMPTY_ARGS);
		bool foundNull = false;
		for (int i = 0; i < MaxMessagesHeld && !foundNull; ++i) {
			IntPtr messageTuple = JavaMessagesArrayElements[i];
			
			// 3-element array, exactly one non-null: { binaryMessage:byte[], rawTextMessage:byte[], textMessage:java.lang.String }
			IntPtr binaryMessage  = AndroidJNI.GetObjectArrayElement(messageTuple, 0);
			IntPtr rawTextMessage = AndroidJNI.GetObjectArrayElement(messageTuple, 1);
			IntPtr textMessage    = AndroidJNI.GetObjectArrayElement(messageTuple, 2);
			
			if (binaryMessage != IntPtr.Zero) {
				reuseableMessageArray[i] = AndroidJNI.FromByteArray(binaryMessage);
				AndroidJNI.DeleteLocalRef(binaryMessage);
			}
			else if (rawTextMessage != IntPtr.Zero) {
				reuseableMessageArray[i] = AndroidJNI.FromByteArray(rawTextMessage);
				AndroidJNI.DeleteLocalRef(rawTextMessage);
			}
			else if (textMessage != IntPtr.Zero) {
				reuseableMessageArray[i] = AndroidJNI.GetStringUTFChars(textMessage);
				AndroidJNI.DeleteLocalRef(textMessage);
			}
			else {
				// { null, null, null }: no more messages
				foundNull = true;
			}
		}
		return reuseableMessageArray;
	}
	
	//=================================
	// Write API
	//=================================
	
	public override void SendBinaryMessage(byte[] payload) {
		object[] csArgs  = new object[]{ payload };
		jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(csArgs);
		AndroidJNI.CallVoidMethod(JavaClass, JavaSendBinaryMessage, jniArgs);
		//AndroidJNIHelper.DeleteJNIArgArray(csArgs, jniArgs);
	}
	
	public override void SendTextMessage(string payload) {
		object[] csArgs  = new object[]{ payload };
		jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(csArgs);
		AndroidJNI.CallVoidMethod(JavaClass, JavaSendTextMessage, jniArgs);
		//AndroidJNIHelper.DeleteJNIArgArray(csArgs, jniArgs);
	}
	
	// ====================================
	// Unity Lifecycle
	// http://answers.unity3d.com/questions/217941/onenable-awake-start-order.html
	// ====================================
	
	protected internal override void Awake() {
		//Debug.Log ("AndroidWebSocket.Awake");
		base.Awake();
		
		Options = m_MobileWebSocket.GetComponent<AndroidWebSocketOptions>();
		CreateJavaWebSocketOptions();
		
        // create a JavaClass object...
        IntPtr cls_JavaClass    = AndroidJNI.FindClass("com/talklittle/unity3d/androidwebsocket/AndroidWebSocketManager");
		IntPtr mid_JavaClass    = AndroidJNI.GetMethodID(cls_JavaClass, "<init>", "(IILjava/lang/String;Lde/tavendo/autobahn/WebSocketOptions;)V");
		string discardPolicyArg = ((JavaDiscardPolicy) MessageDiscardPolicy).ToString();
		jvalue[] jniArgs        = new jvalue[4];
		jniArgs[0].i            = MaxBytesHeld;
		jniArgs[1].i            = MaxMessagesHeld;
		jniArgs[2].l            = AndroidJNI.NewStringUTF(discardPolicyArg);
		jniArgs[3].l            = JavaWebSocketOptions;
		IntPtr obj_JavaClass    = AndroidJNI.NewObject(cls_JavaClass, mid_JavaClass, jniArgs);
        JavaClass               = AndroidJNI.NewGlobalRef(obj_JavaClass);
		AndroidJNI.DeleteLocalRef(jniArgs[2].l);
		
		// API methods
		JavaConnect                 = AndroidJNI.GetMethodID(cls_JavaClass, "connect", "()V");
		JavaDisconnect              = AndroidJNI.GetMethodID(cls_JavaClass, "disconnect", "()V");
		JavaSetGameObjectName       = AndroidJNI.GetMethodID(cls_JavaClass, "setGameObjectName", "(Ljava/lang/String;)V");
		JavaSetUri                  = AndroidJNI.GetMethodID(cls_JavaClass, "setUri", "(Ljava/lang/String;)V");
		JavaSendBinaryMessage       = AndroidJNI.GetMethodID(cls_JavaClass, "sendBinaryMessage", "([B)V");
		JavaSendTextMessage         = AndroidJNI.GetMethodID(cls_JavaClass, "sendTextMessage", "(Ljava/lang/String;)V");
		JavaGetMessages             = AndroidJNI.GetMethodID(cls_JavaClass, "getMessages", "()[[Ljava/lang/Object;");
		JavaUpdateMessages          = AndroidJNI.GetMethodID(cls_JavaClass, "updateMessages", "()V");
		JavaClearMessages           = AndroidJNI.GetMethodID(cls_JavaClass, "clearMessages", "()V");
	}
	
	protected internal override void OnEnable() {
		//Debug.Log ("AndroidWebSocket.OnEnable");
		base.OnEnable();
		InitMessagesArray();
	}
	
	protected internal override void Start() {
		//Debug.Log ("AndroidWebSocket.Start");
		base.Start();
	}
	
	protected internal override void OnDisable() {
		//Debug.Log ("AndroidWebSocket.OnDisable");
		DestroyMessagesArray();
		base.OnDisable();
	}
	
	protected internal override void OnDestroy() {
		//Debug.Log ("AndroidWebSocket.OnDestroy");
		AndroidJNI.DeleteGlobalRef(JavaClass);
		AndroidJNI.DeleteGlobalRef(JavaWebSocketOptions);
		base.OnDestroy();
	}
	
	//======================================
	// End of Lifecycle methods
	//======================================
	
	private void CreateJavaWebSocketOptions() {
        IntPtr cls_JavaWebSocketOptions          = AndroidJNI.FindClass("de/tavendo/autobahn/WebSocketOptions");
		IntPtr mid_JavaWebSocketOptions          = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "<init>", "()V");
        IntPtr obj_JavaWebSocketOptions          = AndroidJNI.NewObject(cls_JavaWebSocketOptions, mid_JavaWebSocketOptions, EMPTY_ARGS);
        
		JavaWebSocketOptions                     = AndroidJNI.NewGlobalRef(obj_JavaWebSocketOptions);
		
		IntPtr mid_setReceiveTextMessagesRaw     = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setReceiveTextMessagesRaw", "(Z)V");
		IntPtr mid_setMaxFramePayloadSize        = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setMaxFramePayloadSize", "(I)V");
		IntPtr mid_setMaxMessagePayloadSize      = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setMaxMessagePayloadSize", "(I)V");
		IntPtr mid_setTcpNoDelay                 = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setTcpNoDelay", "(Z)V");
		IntPtr mid_setSocketReceiveTimeout       = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setSocketReceiveTimeout", "(I)V");
		IntPtr mid_setSocketConnectTimeout       = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setSocketConnectTimeout", "(I)V");
		IntPtr mid_setValidateIncomingUtf8       = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setValidateIncomingUtf8", "(Z)V");
		IntPtr mid_setMaskClientFrames           = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setMaskClientFrames", "(Z)V");
		IntPtr mid_setReconnectInterval          = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setReconnectInterval", "(I)V");
		IntPtr mid_setVerifyCertificateAuthority = AndroidJNI.GetMethodID(cls_JavaWebSocketOptions, "setVerifyCertificateAuthority", "(Z)V");
		
		SetWebSocketOption(JavaWebSocketOptions, mid_setReceiveTextMessagesRaw, Options.ReceiveTextMessagesRaw);
		SetWebSocketOption(JavaWebSocketOptions, mid_setMaxFramePayloadSize, Options.MaxFramePayloadSize);
		SetWebSocketOption(JavaWebSocketOptions, mid_setMaxMessagePayloadSize, Options.MaxMessagePayloadSize);
		SetWebSocketOption(JavaWebSocketOptions, mid_setTcpNoDelay, Options.TcpNoDelay);
		SetWebSocketOption(JavaWebSocketOptions, mid_setSocketReceiveTimeout, Options.SocketReceiveTimeout);
		SetWebSocketOption(JavaWebSocketOptions, mid_setSocketConnectTimeout, Options.SocketConnectTimeout);
		SetWebSocketOption(JavaWebSocketOptions, mid_setValidateIncomingUtf8, Options.ValidateIncomingUtf8);
		SetWebSocketOption(JavaWebSocketOptions, mid_setMaskClientFrames, Options.MaskClientFrames);
		SetWebSocketOption(JavaWebSocketOptions, mid_setReconnectInterval, Options.ReconnectInterval);
		SetWebSocketOption(JavaWebSocketOptions, mid_setVerifyCertificateAuthority, Options.VerifyCertificateAuthority);
	}
			
	private void SetWebSocketOption(IntPtr objectId, IntPtr methodId, bool argument) {
		jvalue[] jniArgs = new jvalue[1];
		jniArgs[0].z = argument;
		AndroidJNI.CallVoidMethod(objectId, methodId, jniArgs);
	}
	private void SetWebSocketOption(IntPtr objectId, IntPtr methodId, int argument) {
		jvalue[] jniArgs = new jvalue[1];
		jniArgs[0].i = argument;
		AndroidJNI.CallVoidMethod(objectId, methodId, jniArgs);
	}
	
	protected override void SetGameObjectName(string name) {
		object[] csArgs  = { name };
		jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(csArgs);
		AndroidJNI.CallVoidMethod(JavaClass, JavaSetGameObjectName, jniArgs);
		//AndroidJNIHelper.DeleteJNIArgArray(csArgs, jniArgs);
	}
	
	protected override void SetUri(string uri) {
		object[] csArgs  = { uri };
		jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(csArgs);
		AndroidJNI.CallVoidMethod(JavaClass, JavaSetUri, jniArgs);
		//AndroidJNIHelper.DeleteJNIArgArray(csArgs, jniArgs);
	}
	
	protected override void ClearMessages() {
		AndroidJNI.CallVoidMethod(JavaClass, JavaClearMessages, EMPTY_ARGS);
	}
	
	private void InitMessagesArray() {
		IntPtr localJavaMessagesArray = AndroidJNI.CallObjectMethod(JavaClass, JavaGetMessages, EMPTY_ARGS);
		JavaMessagesArray = AndroidJNI.NewGlobalRef(localJavaMessagesArray);
		
		JavaMessagesArrayElements = new IntPtr[MaxMessagesHeld];
		for (int i = 0; i < MaxMessagesHeld; ++i) {
			IntPtr messageTuple = AndroidJNI.GetObjectArrayElement(JavaMessagesArray, i);
			JavaMessagesArrayElements[i] = AndroidJNI.NewGlobalRef(messageTuple);
		}
	}
	
	private void DestroyMessagesArray() {
		foreach (IntPtr messageTuple in JavaMessagesArrayElements) {
			AndroidJNI.DeleteGlobalRef(messageTuple);
		}
		AndroidJNI.DeleteGlobalRef(JavaMessagesArray);
	}
	
	private IntPtr GetCurrentActivity() {
		// http://timshaya.wordpress.com/2012/10/01/1294/
        IntPtr cls_Activity     = AndroidJNI.FindClass("com/unity3d/player/UnityPlayer");
        IntPtr fid_Activity     = AndroidJNI.GetStaticFieldID(cls_Activity, "currentActivity", "Landroid/app/Activity;");
        IntPtr obj_Activity     = AndroidJNI.GetStaticObjectField(cls_Activity, fid_Activity);
        Debug.Log("obj_Activity = " + obj_Activity);
		return obj_Activity;
	}
	
}

#endif
