using UnityEngine;
using System.Collections;

public class AndroidWebSocketOptions : MonoBehaviour {

	public bool ReceiveTextMessagesRaw     = false;
	public int  MaxFramePayloadSize        = 128 * 1024;
	public int  MaxMessagePayloadSize      = 128 * 1024;
	public bool TcpNoDelay                 = true;
	public int  SocketReceiveTimeout       = 200;
	public int  SocketConnectTimeout       = 6000;
	public bool ValidateIncomingUtf8       = true;
	public bool MaskClientFrames           = true;
	public int  ReconnectInterval          = 0;
	public bool VerifyCertificateAuthority = true;

}
