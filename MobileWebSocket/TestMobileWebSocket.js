#pragma strict

public var testGUIText:UnityEngine.GUIText;

private var webSocket:MobileWebSocket;
private var numClicks = 0;


function Start() {
	webSocket = gameObject.GetComponent(MobileWebSocket);
}

function OnGUI() {
	if (GUI.Button (Rect (10,10,150,100), "Send Hello")) {
		if (webSocket.IsOpen) {
			webSocket.SendTextMessage("Hello WebSocket " + (++numClicks));
		}
	}
}

function Update() {
	if (webSocket.HasIncomingMessages) {
		var messages:Array = webSocket.TakeMessages();
		var length:int = messages.length;
		for (var i:int = 0; i < length; i++) {
			var message = messages[i];
			if (message instanceof byte[]) {
				var binary:byte[] = message as byte[];
				Debug.Log("New binary message of length " + binary.Length);
			}
			else if (message instanceof String) {
				var text:String = message as String;
				testGUIText.text = text + "\n" + testGUIText.text;
			}
		}
	}
}
