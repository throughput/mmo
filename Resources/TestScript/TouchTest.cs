﻿using UnityEngine;
using System.Collections;

public class TouchTest : MonoBehaviour {

    GameObject TouchEffect;
    GameObject TouchLayer;

	// Use this for initialization
	void Start () {

        GameObject player = GameObject.Find("PlayerObject");

        TouchLayer = (GameObject)Instantiate(
            Resources.Load("TestScript/TestTouchLayer"),
            Vector3.zero,
            Quaternion.Euler(90,0,0));

        TouchEffect = (GameObject)Instantiate(
            Resources.Load("TestScript/TestTouchEffect"),
            Vector3.zero,
            Quaternion.Euler(90,0,0));

        TouchLayer.transform.parent = player.transform;
        TouchLayer.transform.position = new Vector3(0,-0.5f,0);
	}
	
	// Update is called once per frame
    void Update () {

        if (Input.GetMouseButtonDown (0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100.0f)) {
                TouchEffect.transform.position = new Vector3 (hit.point.x,0f,hit.point.z);
            }
        }

	}
}
