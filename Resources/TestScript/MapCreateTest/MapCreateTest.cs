﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapCreateTest : MonoBehaviour {

    string dir = "Graphics/Objects/Dungeon/Cave/1/";

    public GameObject parent;
    public Text createTime1;
    public Text createTime2;
    public Text deleteTime;
    public Text objects;
    
    int createObjs;
    GameObject obj;
    Object[] objs;

    void Awake(){
        objs = Resources.LoadAll(dir,typeof(GameObject));
    }

    public void Load (){

        Reset ();

        createObjs = int.Parse(objects.text);

        var startTime = Time.realtimeSinceStartup;
        for(int i = 0;i < createObjs;i++){
            obj = Instantiate(Resources.Load(dir + "Pin"),new Vector3(0,0,0),Quaternion.identity)as GameObject;
            obj.transform.parent = parent.transform;
        }
        
        var result = Time.realtimeSinceStartup - startTime;
        createTime1.text = "CreateTime:" + result;

    }
    
    public void Copy (){

        Reset ();
        
        createObjs = int.Parse(objects.text);
        
        var startTime = Time.realtimeSinceStartup;

        for(int i = 0;i < createObjs;i++){
            obj = Object.Instantiate(objs[14]) as GameObject;
            obj.transform.position = new Vector3(0,0,0);
            obj.transform.eulerAngles = new Vector3(0,0,0);
            obj.transform.parent = parent.transform;
        }

        var result = Time.realtimeSinceStartup - startTime;
        createTime2.text = "CreateTime:" + result;

    }
    
    public void Reset (){
        var startTime = Time.realtimeSinceStartup;

        foreach ( Transform n in parent.transform ) GameObject.Destroy(n.gameObject);

        var result = Time.realtimeSinceStartup - startTime;
        deleteTime.text = "DeleteTime:" + result;
    }
}
