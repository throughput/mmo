info face="Good Times Rg" size=-32 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=2 padding=1,1,1,1 spacing=0,0 outline=0
common lineHeight=39 base=32 scaleW=256 scaleH=256 pages=1 packed=0 alphaChnl=0 redChnl=4 greenChnl=4 blueChnl=4
page id=0 file="GoodTimes_0.png"
chars count=103
char id=32   x=251   y=0     width=4     height=3     xoffset=-1    yoffset=37    xadvance=9     page=0  chnl=15
char id=33   x=245   y=157   width=7     height=26    xoffset=1     yoffset=7     xadvance=9     page=0  chnl=15
char id=34   x=119   y=215   width=13    height=11    xoffset=1     yoffset=7     xadvance=14    page=0  chnl=15
char id=35   x=199   y=157   width=23    height=26    xoffset=0     yoffset=7     xadvance=22    page=0  chnl=15
char id=36   x=222   y=157   width=23    height=26    xoffset=0     yoffset=7     xadvance=24    page=0  chnl=15
char id=39   x=139   y=215   width=7     height=11    xoffset=0     yoffset=7     xadvance=8     page=0  chnl=15
char id=40   x=0     y=0     width=10    height=32    xoffset=1     yoffset=4     xadvance=11    page=0  chnl=15
char id=41   x=10    y=0     width=10    height=32    xoffset=0     yoffset=4     xadvance=11    page=0  chnl=15
char id=42   x=93    y=215   width=14    height=13    xoffset=0     yoffset=7     xadvance=15    page=0  chnl=15
char id=43   x=186   y=183   width=21    height=21    xoffset=0     yoffset=9     xadvance=22    page=0  chnl=15
char id=44   x=132   y=215   width=7     height=11    xoffset=1     yoffset=26    xadvance=8     page=0  chnl=15
char id=45   x=114   y=105   width=11    height=6     xoffset=-1    yoffset=17    xadvance=9     page=0  chnl=15
char id=46   x=140   y=131   width=7     height=6     xoffset=0     yoffset=27    xadvance=8     page=0  chnl=15
char id=47   x=138   y=189   width=17    height=26    xoffset=-1    yoffset=7     xadvance=14    page=0  chnl=15
char id=48   x=135   y=137   width=27    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=49   x=240   y=131   width=12    height=26    xoffset=-1    yoffset=7     xadvance=12    page=0  chnl=15
char id=50   x=52    y=163   width=26    height=26    xoffset=0     yoffset=7     xadvance=26    page=0  chnl=15
char id=51   x=27    y=137   width=27    height=26    xoffset=0     yoffset=7     xadvance=26    page=0  chnl=15
char id=52   x=140   y=105   width=28    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=53   x=168   y=105   width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=54   x=196   y=105   width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=55   x=78    y=163   width=25    height=26    xoffset=0     yoffset=7     xadvance=24    page=0  chnl=15
char id=56   x=58    y=85    width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=57   x=86    y=85    width=28    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=58   x=17    y=215   width=7     height=17    xoffset=0     yoffset=16    xadvance=8     page=0  chnl=15
char id=59   x=207   y=183   width=7     height=21    xoffset=0     yoffset=16    xadvance=8     page=0  chnl=15
char id=60   x=235   y=183   width=21    height=20    xoffset=0     yoffset=10    xadvance=22    page=0  chnl=15
char id=61   x=24    y=215   width=21    height=14    xoffset=0     yoffset=13    xadvance=22    page=0  chnl=15
char id=62   x=214   y=183   width=21    height=20    xoffset=0     yoffset=10    xadvance=22    page=0  chnl=15
char id=63   x=230   y=27    width=26    height=26    xoffset=0     yoffset=7     xadvance=25    page=0  chnl=15
char id=64   x=214   y=131   width=26    height=26    xoffset=0     yoffset=7     xadvance=26    page=0  chnl=15
char id=65   x=96    y=27    width=34    height=26    xoffset=-1    yoffset=7     xadvance=31    page=0  chnl=15
char id=66   x=114   y=79    width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=67   x=54    y=137   width=27    height=26    xoffset=0     yoffset=7     xadvance=26    page=0  chnl=15
char id=68   x=142   y=79    width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=69   x=127   y=163   width=24    height=26    xoffset=0     yoffset=7     xadvance=24    page=0  chnl=15
char id=70   x=23    y=189   width=23    height=26    xoffset=0     yoffset=7     xadvance=22    page=0  chnl=15
char id=71   x=170   y=79    width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=72   x=188   y=131   width=26    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=73   x=172   y=189   width=7     height=26    xoffset=1     yoffset=7     xadvance=8     page=0  chnl=15
char id=74   x=0     y=189   width=23    height=26    xoffset=-1    yoffset=7     xadvance=22    page=0  chnl=15
char id=75   x=29    y=85    width=29    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=76   x=92    y=189   width=23    height=26    xoffset=0     yoffset=7     xadvance=22    page=0  chnl=15
char id=77   x=186   y=0     width=37    height=27    xoffset=-1    yoffset=7     xadvance=35    page=0  chnl=15
char id=78   x=108   y=137   width=27    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=79   x=190   y=53    width=31    height=26    xoffset=0     yoffset=7     xadvance=31    page=0  chnl=15
char id=80   x=198   y=79    width=28    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=81   x=0     y=59    width=32    height=26    xoffset=0     yoffset=7     xadvance=31    page=0  chnl=15
char id=82   x=159   y=53    width=31    height=26    xoffset=0     yoffset=7     xadvance=29    page=0  chnl=15
char id=83   x=226   y=79    width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=84   x=151   y=163   width=24    height=26    xoffset=-1    yoffset=7     xadvance=23    page=0  chnl=15
char id=85   x=223   y=0     width=28    height=27    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=86   x=0     y=32    width=34    height=27    xoffset=-1    yoffset=7     xadvance=31    page=0  chnl=15
char id=87   x=63    y=0     width=43    height=27    xoffset=-1    yoffset=7     xadvance=40    page=0  chnl=15
char id=88   x=164   y=27    width=33    height=26    xoffset=-1    yoffset=7     xadvance=32    page=0  chnl=15
char id=89   x=32    y=59    width=32    height=26    xoffset=-1    yoffset=7     xadvance=29    page=0  chnl=15
char id=90   x=162   y=131   width=26    height=26    xoffset=0     yoffset=7     xadvance=25    page=0  chnl=15
char id=91   x=49    y=0     width=8     height=32    xoffset=2     yoffset=4     xadvance=10    page=0  chnl=15
char id=92   x=155   y=189   width=17    height=26    xoffset=-1    yoffset=7     xadvance=14    page=0  chnl=15
char id=93   x=40    y=0     width=9     height=32    xoffset=0     yoffset=4     xadvance=10    page=0  chnl=15
char id=94   x=0     y=215   width=17    height=17    xoffset=0     yoffset=7     xadvance=18    page=0  chnl=15
char id=95   x=68    y=54    width=25    height=4     xoffset=-1    yoffset=34    xadvance=21    page=0  chnl=15
char id=96   x=174   y=215   width=11    height=7     xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=97   x=130   y=27    width=34    height=26    xoffset=-1    yoffset=7     xadvance=31    page=0  chnl=15
char id=98   x=28    y=111   width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=99   x=81    y=137   width=27    height=26    xoffset=0     yoffset=7     xadvance=26    page=0  chnl=15
char id=100  x=56    y=111   width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=101  x=175   y=157   width=24    height=26    xoffset=0     yoffset=7     xadvance=24    page=0  chnl=15
char id=102  x=69    y=189   width=23    height=26    xoffset=0     yoffset=7     xadvance=22    page=0  chnl=15
char id=103  x=84    y=111   width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=104  x=0     y=163   width=26    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=105  x=179   y=183   width=7     height=26    xoffset=1     yoffset=7     xadvance=8     page=0  chnl=15
char id=106  x=115   y=189   width=23    height=26    xoffset=-1    yoffset=7     xadvance=22    page=0  chnl=15
char id=107  x=0     y=85    width=29    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=108  x=46    y=189   width=23    height=26    xoffset=0     yoffset=7     xadvance=22    page=0  chnl=15
char id=109  x=149   y=0     width=37    height=27    xoffset=-1    yoffset=7     xadvance=35    page=0  chnl=15
char id=110  x=224   y=105   width=27    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=111  x=128   y=53    width=31    height=26    xoffset=0     yoffset=7     xadvance=31    page=0  chnl=15
char id=112  x=0     y=111   width=28    height=26    xoffset=0     yoffset=7     xadvance=27    page=0  chnl=15
char id=113  x=96    y=53    width=32    height=26    xoffset=0     yoffset=7     xadvance=31    page=0  chnl=15
char id=114  x=221   y=53    width=31    height=26    xoffset=0     yoffset=7     xadvance=29    page=0  chnl=15
char id=115  x=112   y=111   width=28    height=26    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=116  x=103   y=163   width=24    height=26    xoffset=-1    yoffset=7     xadvance=23    page=0  chnl=15
char id=117  x=68    y=27    width=28    height=27    xoffset=0     yoffset=7     xadvance=28    page=0  chnl=15
char id=118  x=34    y=32    width=34    height=27    xoffset=-1    yoffset=7     xadvance=31    page=0  chnl=15
char id=119  x=106   y=0     width=43    height=27    xoffset=-1    yoffset=7     xadvance=40    page=0  chnl=15
char id=120  x=197   y=27    width=33    height=26    xoffset=-1    yoffset=7     xadvance=32    page=0  chnl=15
char id=121  x=64    y=59    width=32    height=26    xoffset=-1    yoffset=7     xadvance=29    page=0  chnl=15
char id=122  x=26    y=163   width=26    height=26    xoffset=0     yoffset=7     xadvance=25    page=0  chnl=15
char id=123  x=30    y=0     width=10    height=32    xoffset=0     yoffset=4     xadvance=11    page=0  chnl=15
char id=124  x=57    y=0     width=6     height=32    xoffset=2     yoffset=4     xadvance=9     page=0  chnl=15
char id=125  x=20    y=0     width=10    height=32    xoffset=0     yoffset=4     xadvance=11    page=0  chnl=15
char id=126  x=146   y=215   width=21    height=8     xoffset=0     yoffset=16    xadvance=22    page=0  chnl=15
char id=165  x=0     y=137   width=27    height=26    xoffset=-1    yoffset=7     xadvance=25    page=0  chnl=15
char id=168  x=96    y=79    width=12    height=5     xoffset=0     yoffset=2     xadvance=12    page=0  chnl=15
char id=171  x=45    y=215   width=20    height=14    xoffset=0     yoffset=13    xadvance=20    page=0  chnl=15
char id=175  x=125   y=105   width=11    height=4     xoffset=0     yoffset=3     xadvance=11    page=0  chnl=15
char id=176  x=107   y=215   width=12    height=12    xoffset=0     yoffset=7     xadvance=11    page=0  chnl=15
char id=180  x=185   y=209   width=11    height=7     xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=183  x=196   y=204   width=7     height=7     xoffset=0     yoffset=17    xadvance=8     page=0  chnl=15
char id=184  x=167   y=215   width=7     height=8     xoffset=0     yoffset=32    xadvance=7     page=0  chnl=15
char id=185  x=85    y=215   width=8     height=14    xoffset=-1    yoffset=7     xadvance=7     page=0  chnl=15
char id=187  x=65    y=215   width=20    height=14    xoffset=0     yoffset=13    xadvance=20    page=0  chnl=15
kernings count=437
kerning first=34  second=65  amount=-3  
kerning first=34  second=74  amount=-4  
kerning first=34  second=97  amount=-3  
kerning first=34  second=106 amount=-4  
kerning first=187 second=121 amount=-4  
kerning first=187 second=120 amount=-3  
kerning first=187 second=106 amount=-5  
kerning first=187 second=89  amount=-4  
kerning first=187 second=88  amount=-3  
kerning first=187 second=74  amount=-5  
kerning first=183 second=121 amount=-6  
kerning first=183 second=119 amount=-4  
kerning first=183 second=118 amount=-4  
kerning first=183 second=116 amount=-3  
kerning first=183 second=89  amount=-6  
kerning first=183 second=87  amount=-4  
kerning first=39  second=65  amount=-3  
kerning first=39  second=74  amount=-4  
kerning first=39  second=97  amount=-3  
kerning first=39  second=106 amount=-4  
kerning first=183 second=86  amount=-4  
kerning first=183 second=84  amount=-3  
kerning first=171 second=121 amount=-4  
kerning first=171 second=120 amount=-3  
kerning first=171 second=106 amount=-5  
kerning first=171 second=89  amount=-4  
kerning first=171 second=88  amount=-3  
kerning first=171 second=74  amount=-5  
kerning first=121 second=187 amount=-4  
kerning first=121 second=171 amount=-4  
kerning first=121 second=109 amount=-4  
kerning first=121 second=106 amount=-5  
kerning first=44  second=34  amount=-3  
kerning first=44  second=39  amount=-3  
kerning first=44  second=84  amount=-3  
kerning first=44  second=86  amount=-4  
kerning first=44  second=87  amount=-4  
kerning first=44  second=89  amount=-6  
kerning first=44  second=116 amount=-3  
kerning first=44  second=118 amount=-4  
kerning first=44  second=119 amount=-4  
kerning first=44  second=121 amount=-6  
kerning first=121 second=103 amount=-3  
kerning first=121 second=99  amount=-3  
kerning first=121 second=97  amount=-7  
kerning first=121 second=77  amount=-4  
kerning first=121 second=74  amount=-5  
kerning first=121 second=71  amount=-3  
kerning first=45  second=74  amount=-5  
kerning first=45  second=88  amount=-3  
kerning first=45  second=89  amount=-4  
kerning first=45  second=106 amount=-5  
kerning first=45  second=120 amount=-3  
kerning first=45  second=121 amount=-4  
kerning first=121 second=67  amount=-3  
kerning first=121 second=65  amount=-7  
kerning first=46  second=34  amount=-3  
kerning first=46  second=39  amount=-3  
kerning first=46  second=84  amount=-3  
kerning first=46  second=86  amount=-4  
kerning first=46  second=87  amount=-4  
kerning first=46  second=89  amount=-6  
kerning first=46  second=116 amount=-3  
kerning first=46  second=118 amount=-4  
kerning first=46  second=119 amount=-4  
kerning first=46  second=121 amount=-6  
kerning first=121 second=47  amount=-5  
kerning first=121 second=46  amount=-6  
kerning first=121 second=45  amount=-4  
kerning first=121 second=44  amount=-6  
kerning first=120 second=187 amount=-3  
kerning first=120 second=171 amount=-3  
kerning first=47  second=65  amount=-6  
kerning first=47  second=74  amount=-4  
kerning first=47  second=77  amount=-3  
kerning first=47  second=97  amount=-6  
kerning first=47  second=106 amount=-4  
kerning first=47  second=109 amount=-3  
kerning first=120 second=113 amount=-3  
kerning first=120 second=111 amount=-3  
kerning first=120 second=103 amount=-3  
kerning first=120 second=99  amount=-3  
kerning first=120 second=81  amount=-3  
kerning first=120 second=79  amount=-3  
kerning first=120 second=71  amount=-3  
kerning first=120 second=67  amount=-3  
kerning first=120 second=57  amount=-3  
kerning first=120 second=56  amount=-3  
kerning first=120 second=54  amount=-3  
kerning first=120 second=48  amount=-3  
kerning first=48  second=65  amount=-3  
kerning first=48  second=86  amount=-3  
kerning first=48  second=87  amount=-3  
kerning first=48  second=88  amount=-3  
kerning first=48  second=89  amount=-3  
kerning first=48  second=97  amount=-3  
kerning first=48  second=118 amount=-3  
kerning first=48  second=119 amount=-3  
kerning first=48  second=120 amount=-3  
kerning first=48  second=121 amount=-3  
kerning first=120 second=45  amount=-3  
kerning first=119 second=113 amount=-3  
kerning first=119 second=111 amount=-3  
kerning first=119 second=109 amount=-3  
kerning first=119 second=106 amount=-4  
kerning first=119 second=103 amount=-3  
kerning first=119 second=99  amount=-3  
kerning first=119 second=97  amount=-7  
kerning first=119 second=81  amount=-3  
kerning first=119 second=79  amount=-3  
kerning first=119 second=77  amount=-3  
kerning first=119 second=74  amount=-4  
kerning first=119 second=71  amount=-3  
kerning first=119 second=67  amount=-3  
kerning first=54  second=65  amount=-3  
kerning first=55  second=44  amount=-5  
kerning first=55  second=46  amount=-5  
kerning first=119 second=65  amount=-7  
kerning first=119 second=57  amount=-3  
kerning first=119 second=56  amount=-3  
kerning first=57  second=65  amount=-3  
kerning first=57  second=86  amount=-3  
kerning first=57  second=87  amount=-3  
kerning first=57  second=88  amount=-3  
kerning first=57  second=89  amount=-3  
kerning first=57  second=97  amount=-3  
kerning first=57  second=118 amount=-3  
kerning first=57  second=119 amount=-3  
kerning first=57  second=120 amount=-3  
kerning first=57  second=121 amount=-3  
kerning first=119 second=54  amount=-3  
kerning first=119 second=48  amount=-3  
kerning first=119 second=47  amount=-5  
kerning first=119 second=46  amount=-4  
kerning first=119 second=44  amount=-4  
kerning first=118 second=113 amount=-3  
kerning first=118 second=111 amount=-3  
kerning first=118 second=109 amount=-3  
kerning first=118 second=106 amount=-4  
kerning first=118 second=103 amount=-3  
kerning first=118 second=99  amount=-3  
kerning first=118 second=97  amount=-7  
kerning first=118 second=81  amount=-3  
kerning first=118 second=79  amount=-3  
kerning first=118 second=77  amount=-3  
kerning first=118 second=74  amount=-4  
kerning first=65  second=34  amount=-3  
kerning first=65  second=39  amount=-3  
kerning first=65  second=48  amount=-3  
kerning first=65  second=54  amount=-3  
kerning first=65  second=56  amount=-3  
kerning first=65  second=57  amount=-3  
kerning first=65  second=67  amount=-3  
kerning first=65  second=71  amount=-3  
kerning first=65  second=79  amount=-3  
kerning first=65  second=81  amount=-3  
kerning first=65  second=84  amount=-4  
kerning first=65  second=86  amount=-7  
kerning first=65  second=87  amount=-7  
kerning first=65  second=89  amount=-6  
kerning first=65  second=99  amount=-3  
kerning first=65  second=103 amount=-3  
kerning first=65  second=111 amount=-3  
kerning first=65  second=113 amount=-3  
kerning first=65  second=116 amount=-4  
kerning first=65  second=118 amount=-7  
kerning first=65  second=119 amount=-7  
kerning first=65  second=121 amount=-6  
kerning first=118 second=71  amount=-3  
kerning first=118 second=67  amount=-3  
kerning first=118 second=65  amount=-7  
kerning first=118 second=57  amount=-3  
kerning first=118 second=56  amount=-3  
kerning first=118 second=54  amount=-3  
kerning first=118 second=48  amount=-3  
kerning first=118 second=47  amount=-5  
kerning first=118 second=46  amount=-4  
kerning first=118 second=44  amount=-4  
kerning first=116 second=106 amount=-3  
kerning first=116 second=97  amount=-4  
kerning first=116 second=74  amount=-3  
kerning first=116 second=65  amount=-4  
kerning first=116 second=47  amount=-4  
kerning first=116 second=46  amount=-3  
kerning first=116 second=44  amount=-3  
kerning first=113 second=121 amount=-3  
kerning first=113 second=120 amount=-3  
kerning first=113 second=119 amount=-3  
kerning first=113 second=118 amount=-3  
kerning first=113 second=97  amount=-3  
kerning first=68  second=65  amount=-3  
kerning first=68  second=86  amount=-3  
kerning first=68  second=87  amount=-3  
kerning first=68  second=88  amount=-3  
kerning first=68  second=89  amount=-3  
kerning first=68  second=97  amount=-3  
kerning first=68  second=118 amount=-3  
kerning first=68  second=119 amount=-3  
kerning first=68  second=120 amount=-3  
kerning first=68  second=121 amount=-3  
kerning first=113 second=89  amount=-3  
kerning first=113 second=88  amount=-3  
kerning first=113 second=87  amount=-3  
kerning first=113 second=86  amount=-3  
kerning first=113 second=65  amount=-3  
kerning first=112 second=106 amount=-3  
kerning first=112 second=97  amount=-3  
kerning first=112 second=74  amount=-3  
kerning first=112 second=65  amount=-3  
kerning first=112 second=46  amount=-3  
kerning first=112 second=44  amount=-3  
kerning first=111 second=121 amount=-3  
kerning first=111 second=120 amount=-3  
kerning first=111 second=119 amount=-3  
kerning first=111 second=118 amount=-3  
kerning first=70  second=44  amount=-3  
kerning first=70  second=46  amount=-3  
kerning first=70  second=65  amount=-3  
kerning first=70  second=97  amount=-3  
kerning first=111 second=97  amount=-3  
kerning first=111 second=89  amount=-3  
kerning first=111 second=88  amount=-3  
kerning first=111 second=87  amount=-3  
kerning first=111 second=86  amount=-3  
kerning first=111 second=65  amount=-3  
kerning first=109 second=121 amount=-4  
kerning first=109 second=119 amount=-3  
kerning first=109 second=118 amount=-3  
kerning first=109 second=89  amount=-4  
kerning first=109 second=87  amount=-3  
kerning first=109 second=86  amount=-3  
kerning first=108 second=187 amount=-4  
kerning first=108 second=171 amount=-4  
kerning first=108 second=121 amount=-5  
kerning first=108 second=119 amount=-5  
kerning first=108 second=118 amount=-5  
kerning first=75  second=45  amount=-4  
kerning first=75  second=48  amount=-3  
kerning first=75  second=54  amount=-3  
kerning first=75  second=56  amount=-3  
kerning first=75  second=57  amount=-3  
kerning first=75  second=67  amount=-3  
kerning first=75  second=71  amount=-3  
kerning first=75  second=79  amount=-3  
kerning first=75  second=81  amount=-3  
kerning first=75  second=99  amount=-3  
kerning first=75  second=103 amount=-3  
kerning first=75  second=111 amount=-3  
kerning first=75  second=113 amount=-3  
kerning first=75  second=171 amount=-4  
kerning first=75  second=187 amount=-4  
kerning first=108 second=116 amount=-3  
kerning first=108 second=89  amount=-5  
kerning first=108 second=87  amount=-5  
kerning first=108 second=86  amount=-5  
kerning first=108 second=84  amount=-3  
kerning first=108 second=45  amount=-4  
kerning first=108 second=39  amount=-4  
kerning first=108 second=34  amount=-4  
kerning first=107 second=187 amount=-4  
kerning first=107 second=171 amount=-4  
kerning first=107 second=113 amount=-3  
kerning first=107 second=111 amount=-3  
kerning first=107 second=103 amount=-3  
kerning first=107 second=99  amount=-3  
kerning first=107 second=81  amount=-3  
kerning first=107 second=79  amount=-3  
kerning first=107 second=71  amount=-3  
kerning first=107 second=67  amount=-3  
kerning first=76  second=34  amount=-4  
kerning first=76  second=39  amount=-4  
kerning first=76  second=45  amount=-4  
kerning first=76  second=84  amount=-3  
kerning first=76  second=86  amount=-5  
kerning first=76  second=87  amount=-5  
kerning first=76  second=89  amount=-5  
kerning first=76  second=116 amount=-3  
kerning first=76  second=118 amount=-5  
kerning first=76  second=119 amount=-5  
kerning first=76  second=121 amount=-5  
kerning first=76  second=171 amount=-4  
kerning first=76  second=187 amount=-4  
kerning first=107 second=57  amount=-3  
kerning first=107 second=56  amount=-3  
kerning first=107 second=54  amount=-3  
kerning first=107 second=48  amount=-3  
kerning first=107 second=45  amount=-4  
kerning first=102 second=97  amount=-3  
kerning first=102 second=65  amount=-3  
kerning first=102 second=46  amount=-3  
kerning first=77  second=86  amount=-3  
kerning first=77  second=87  amount=-3  
kerning first=77  second=89  amount=-4  
kerning first=77  second=118 amount=-3  
kerning first=77  second=119 amount=-3  
kerning first=77  second=121 amount=-4  
kerning first=102 second=44  amount=-3  
kerning first=100 second=121 amount=-3  
kerning first=79  second=118 amount=-3  
kerning first=79  second=119 amount=-3  
kerning first=79  second=120 amount=-3  
kerning first=79  second=121 amount=-3  
kerning first=100 second=120 amount=-3  
kerning first=100 second=119 amount=-3  
kerning first=100 second=118 amount=-3  
kerning first=100 second=97  amount=-3  
kerning first=100 second=89  amount=-3  
kerning first=100 second=88  amount=-3  
kerning first=100 second=87  amount=-3  
kerning first=100 second=86  amount=-3  
kerning first=100 second=65  amount=-3  
kerning first=97  second=121 amount=-6  
kerning first=97  second=119 amount=-7  
kerning first=97  second=118 amount=-7  
kerning first=97  second=116 amount=-4  
kerning first=97  second=113 amount=-3  
kerning first=97  second=111 amount=-3  
kerning first=97  second=103 amount=-3  
kerning first=80  second=44  amount=-3  
kerning first=80  second=46  amount=-3  
kerning first=80  second=65  amount=-3  
kerning first=80  second=74  amount=-3  
kerning first=80  second=97  amount=-3  
kerning first=80  second=106 amount=-3  
kerning first=97  second=99  amount=-3  
kerning first=97  second=89  amount=-6  
kerning first=97  second=87  amount=-7  
kerning first=97  second=86  amount=-7  
kerning first=97  second=84  amount=-4  
kerning first=97  second=81  amount=-3  
kerning first=97  second=79  amount=-3  
kerning first=97  second=71  amount=-3  
kerning first=97  second=67  amount=-3  
kerning first=97  second=57  amount=-3  
kerning first=97  second=56  amount=-3  
kerning first=97  second=54  amount=-3  
kerning first=97  second=48  amount=-3  
kerning first=97  second=39  amount=-3  
kerning first=97  second=34  amount=-3  
kerning first=89  second=187 amount=-4  
kerning first=89  second=171 amount=-4  
kerning first=81  second=65  amount=-3  
kerning first=81  second=86  amount=-3  
kerning first=81  second=87  amount=-3  
kerning first=81  second=88  amount=-3  
kerning first=81  second=89  amount=-3  
kerning first=81  second=97  amount=-3  
kerning first=81  second=118 amount=-3  
kerning first=81  second=119 amount=-3  
kerning first=81  second=120 amount=-3  
kerning first=81  second=121 amount=-3  
kerning first=89  second=113 amount=-3  
kerning first=89  second=111 amount=-3  
kerning first=89  second=109 amount=-4  
kerning first=89  second=106 amount=-5  
kerning first=89  second=103 amount=-3  
kerning first=89  second=99  amount=-3  
kerning first=89  second=97  amount=-7  
kerning first=89  second=81  amount=-3  
kerning first=89  second=79  amount=-3  
kerning first=89  second=77  amount=-4  
kerning first=89  second=74  amount=-5  
kerning first=89  second=71  amount=-3  
kerning first=89  second=67  amount=-3  
kerning first=89  second=65  amount=-7  
kerning first=89  second=57  amount=-3  
kerning first=89  second=56  amount=-3  
kerning first=84  second=44  amount=-3  
kerning first=84  second=46  amount=-3  
kerning first=84  second=47  amount=-4  
kerning first=84  second=65  amount=-4  
kerning first=84  second=74  amount=-3  
kerning first=84  second=97  amount=-4  
kerning first=84  second=106 amount=-3  
kerning first=89  second=54  amount=-3  
kerning first=89  second=48  amount=-3  
kerning first=89  second=47  amount=-5  
kerning first=89  second=46  amount=-6  
kerning first=89  second=45  amount=-4  
kerning first=89  second=44  amount=-6  
kerning first=88  second=187 amount=-3  
kerning first=88  second=171 amount=-3  
kerning first=88  second=113 amount=-3  
kerning first=88  second=111 amount=-3  
kerning first=88  second=103 amount=-3  
kerning first=88  second=99  amount=-3  
kerning first=88  second=81  amount=-3  
kerning first=88  second=79  amount=-3  
kerning first=88  second=71  amount=-3  
kerning first=88  second=67  amount=-3  
kerning first=88  second=57  amount=-3  
kerning first=86  second=44  amount=-4  
kerning first=86  second=46  amount=-4  
kerning first=86  second=47  amount=-5  
kerning first=86  second=48  amount=-3  
kerning first=86  second=54  amount=-3  
kerning first=86  second=56  amount=-3  
kerning first=86  second=57  amount=-3  
kerning first=86  second=65  amount=-7  
kerning first=86  second=67  amount=-3  
kerning first=86  second=71  amount=-3  
kerning first=86  second=74  amount=-4  
kerning first=86  second=77  amount=-3  
kerning first=86  second=79  amount=-3  
kerning first=86  second=81  amount=-3  
kerning first=86  second=97  amount=-7  
kerning first=86  second=99  amount=-3  
kerning first=86  second=103 amount=-3  
kerning first=86  second=106 amount=-4  
kerning first=86  second=109 amount=-3  
kerning first=86  second=111 amount=-3  
kerning first=86  second=113 amount=-3  
kerning first=88  second=56  amount=-3  
kerning first=88  second=54  amount=-3  
kerning first=88  second=48  amount=-3  
kerning first=88  second=45  amount=-3  
kerning first=87  second=113 amount=-3  
kerning first=87  second=111 amount=-3  
kerning first=87  second=109 amount=-3  
kerning first=87  second=106 amount=-4  
kerning first=87  second=103 amount=-3  
kerning first=87  second=99  amount=-3  
kerning first=87  second=97  amount=-7  
kerning first=87  second=81  amount=-3  
kerning first=87  second=79  amount=-3  
kerning first=87  second=77  amount=-3  
kerning first=87  second=74  amount=-4  
kerning first=87  second=71  amount=-3  
kerning first=87  second=67  amount=-3  
kerning first=87  second=65  amount=-7  
kerning first=87  second=57  amount=-3  
kerning first=87  second=56  amount=-3  
kerning first=87  second=54  amount=-3  
kerning first=87  second=48  amount=-3  
kerning first=87  second=47  amount=-5  
kerning first=87  second=46  amount=-4  
kerning first=87  second=44  amount=-4  
