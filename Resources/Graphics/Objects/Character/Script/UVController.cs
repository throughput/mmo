﻿using UnityEngine;
using System.Collections;

public class UVController : MonoBehaviour {

    public Vector2[] newUV = new Vector2[20];
    MeshFilter meshFilter;

    void Start()
    {
        Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;

        for(int i = 0; i < mesh.uv.Length; i++)
        {
            newUV[i] = mesh.uv[i];
        }

        meshFilter = gameObject.GetComponent<MeshFilter>();
        
    }
}
