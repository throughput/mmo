﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ParticleCheck : MonoBehaviour {

    public GameObject[] particleList;
    public int length;
    public int nowNo = 0;

    public GameObject playerPrefab;

    int CharaNum = 0;
    Vector2 vec;

    Text call;

	// Use this for initialization
    void Awake () {
        
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = 60;

        particleList = GameObject.FindGameObjectsWithTag("ScreenEffect");
        call = GameObject.Find("Canvas/Call").GetComponent<Text>();

        length = particleList.Length;
        Reset();
    }

    public void SelectPrev (){
        Reset();
        nowNo--;
        if(nowNo < 0){
            nowNo = length - 1;
        }
        
        particleList[nowNo].SetActive(true);
    }
    
    public void SelectNext (){
        Reset();
        nowNo++;
        if(nowNo >= length){
            nowNo = 0;
        }

        particleList[nowNo].SetActive(true);
    }
    
    public void Restart(){
        Reset();
        particleList[nowNo].SetActive(true);
    }
    
    public void Plus(){

        float x = Random.Range(-4f,4f);
        float z = Random.Range(-6f,6f);
        GameObject prefab = Instantiate (playerPrefab, new Vector3(x,0.3f,z), Quaternion.identity) as GameObject;
        
        print (CharaNum+1+"体目");
        call.text = (CharaNum+1).ToString();

        float move = 0.0625f;

        if(CharaNum <= 31){
            print ("1段目");
            float posX = CharaNum * move;
            float posY = 0 * move;
            vec = new Vector2(posX,posY);
            CharaNum++;
        }
        else if(CharaNum <= 63){
            print ("2段目");
            float posX = CharaNum * move;
            float posY = 1 * move;
            vec = new Vector2(posX,posY);
            CharaNum++;
        }
        else if(CharaNum <= 95){
            print ("3段目");
            float posX = CharaNum * move;
            float posY = 2 * move;
            vec = new Vector2(posX,posY);
            CharaNum++;
        }
        else if(CharaNum <= 127){
            print ("4段目");
            float posX = CharaNum * move;
            float posY = 3 * move;
            vec = new Vector2(posX,posY);
            CharaNum++;
        }
        else if(CharaNum <= 159){
            print ("4段目");
            float posX = CharaNum * move;
            float posY = 4 * move;
            vec = new Vector2(posX,posY);
            CharaNum++;
        }

        prefab.transform.FindChild("Character").GetComponent<MeshRenderer>().material.SetTextureOffset("_MainTex", vec);

    }

    void Reset(){
        for(int i=0; i < length; ++i){
            particleList[i].SetActive(false);
        }
    }
}
