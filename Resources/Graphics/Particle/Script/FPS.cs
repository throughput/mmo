﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPS : MonoBehaviour {

    int frameCount;
    float nextTime;

    Text fps;
    
    void Awake () {
        fps = GameObject.Find("Canvas/FPS").GetComponent<Text>();
    }

    void Update () {
        frameCount++;
        
        if ( Time.time >= nextTime ) {
            fps.text = frameCount.ToString();
            frameCount = 0;
            nextTime += 1;
        }
    }
}
