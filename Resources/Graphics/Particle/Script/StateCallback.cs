﻿using UnityEngine;
using System.Collections;

public class StateCallback: StateMachineBehaviour
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.speed = 0.0f;
    }
}