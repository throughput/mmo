﻿using UnityEngine;
using System.Collections;

public class AnimationNum : MonoBehaviour {

    public int animationNumber;

	void Awake () {
        Animator Anim = GetComponent<Animator>();
        Anim.SetInteger("AnimNum",animationNumber);
	}
}
