﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public enum ItemWindowKind{
            ITEM_WINDOW_USE,
            ITEM_WINDOW_EQUIP,
            ITEM_WINDOW_SLOT_SET,
            ITEM_WINDOW_ATTACH,
        };
		public class ItemController : ThpmmoMonoBehavier {
            public const int MAX_SLOT_COUNT = 8;
			public ItemInfo itemInfo;
            public GameObject growupEquipWindow;
            Dictionary<string,GameObject> popupEquipSlot = new Dictionary<string,GameObject>();

            public GameObject button1;
            public GameObject button2;
            public GameObject button3;
            public ItemWindowKind itewWindowKind = ItemWindowKind.ITEM_WINDOW_USE;

            // 装備品UV設定情報
            const float uvBaseParam = 0.03124f;
            const int equipListVMax = 32;
            const float armsListHStart   = 0.03124f;
            const float shieldsListHStart = 0.03124f;
            const float armorsListHStart  = 0.03124f;

            // Use this for initialization
            void Awake () {
        	}
            void Start(){
            }
        	// Update is called once per frame
        	void Update () {
        	}
            void OnEnable() {
                switch (itewWindowKind) {
                case ItemWindowKind.ITEM_WINDOW_EQUIP:
                    button1.SetActive(true);
                    button2.SetActive(true);
                    button3.SetActive(true);
                    button1.name = "B_Stone";
                    button2.name = "B_Equip";
                    button3.name = "B_Close";
                    break;
                case ItemWindowKind.ITEM_WINDOW_ATTACH:
                    button1.SetActive (true);
                    button2.SetActive (false);
                    button3.SetActive (true);
                    button1.name = "B_Attach";
                    button3.name = "B_Close";
                    break;
                case ItemWindowKind.ITEM_WINDOW_USE:
                    button1.SetActive(true);
                    button2.SetActive(false);
                    button3.SetActive(true);
                    button1.name = "B_Use";
                    button3.name = "B_Close";
                    break;
                case ItemWindowKind.ITEM_WINDOW_SLOT_SET:
                    button1.SetActive(true);
                    button2.SetActive(false);
                    button3.SetActive(true);
                    button1.name = "B_Set";
                    button3.name = "B_Close";
                    break;
                }
                Debug.Log (GameTextTable.Uitext () [button1.name]);
                if(button1.activeSelf) button1.GetComponentInChildren<UILabel> ().text = GameTextTable.Uitext()[button1.name];
                if(button2.activeSelf) button2.GetComponentInChildren<UILabel> ().text = GameTextTable.Uitext()[button2.name];
                if(button3.activeSelf) button3.GetComponentInChildren<UILabel> ().text = GameTextTable.Uitext()[button3.name];



            }
            public void MsgItemStatusButton (string word) {
                switch (word) {
                case "B_Stone":
                    {
                        MenuController.Instance.CloseMenu ();
                        GameObject obj = MenuController.Instance.GetMenuWindow ("M_Rare");
                        SlotSetController slotSetController= obj.GetComponent<SlotSetController> ();
                        slotSetController.itemInfo = itemInfo;
                        MenuController.Instance.openMenu ("M_Rare");
                        gameObject.SetActive (false);
                    }
                    break;
                case "B_PowerUp":
                    {
                        GrowupEquipController growupEquipController = growupEquipWindow.GetComponent<GrowupEquipController> ();
                        growupEquipController.itemInfo = itemInfo;
                        growupEquipWindow.SetActive (true);
                        gameObject.SetActive (false);
                    }
                    break;
                case "B_Equip":
                    {
                        StartCoroutine(HttpController.Instance.changeEquip (equipment,itemInfo.itemBoxNo));
                    }
                    break;
                case "B_Close":
                    {
                        MenuController.Instance.CloseMsg (gameObject);
                    }
                    break;
                case "B_Attach":
                    {
                        MenuController.Instance.sendMailAttachItem (itemInfo);
                    }
                    break;
                // 魔石セット
                case "B_Set":
                    {
                        MenuController.Instance.setSlot (itemInfo);
                        gameObject.SetActive (false);
                    }
                    break;
                default:
                    {
                    }
                    break;
                }
            }
            /// <summary>
            /// 装備結果
            /// </summary>
            /// <param name="result">Result.</param>
            /// <param name="itemBoxNo">Item box no.</param>
            private void equipment(EquipError result,int itemBoxNo){
                if (result == EquipError.SUCCESS) {
                    MenuController.Instance.equipment (itemInfo.itemBoxNo);
                    ItemController.characterEquip (PlayerInfo.Instance.characterInfo, DungeonPlayer.Instance.gameObject);
                } else {
                    MenuController.Instance.OpenDialog (DialogForm.DIALOG_OK,GameTextTable.GameDialogtext()["equipmentError"]);
                }
                MenuController.Instance.CloseMsg (gameObject);
            }
            /// <summary>
            /// キャラクター装備
            /// </summary>
            /// <param name="mapData">Map data.</param>
            static public void characterEquip(CharacterInfo charachterInfo,GameObject characterObject){
                // UV変更
                MeshFilter[] meshFilters = characterObject.GetComponentsInChildren<MeshFilter> ();
                foreach (MeshFilter meshFilter in meshFilters) {
                    Vector2[] newUV = new Vector2[4];
                    float startHpos = armsListHStart;
                    int equipID = 0;
                    Debug.Log (meshFilter.name);
                    switch (meshFilter.name){
                    case "Weapon":
                        equipID = charachterInfo.swordID;
                        startHpos = armsListHStart;
                        break;
                    case "Shield":
                        equipID = charachterInfo.shieldID;
                        startHpos = shieldsListHStart;
                        break;
                    case "Character":
                        equipID = charachterInfo.swordID;
                        startHpos = armorsListHStart;
                        break;
                    }
                    if (meshFilter.name == "Weapon" || meshFilter.name == "Shield") {
                        float x = (equipID % equipListVMax) * uvBaseParam;
                        float y = startHpos + Mathf.Floor (equipID / equipListVMax) * uvBaseParam;
                        newUV [0] = new Vector2 (0.0f + x, 0.96876f - y);
                        newUV [1] = new Vector2 (0.03124f + x, 1.0f - y);
                        newUV [2] = new Vector2 (0.03124f + x, 0.96876f - y);
                        newUV [3] = new Vector2 (0.0f + x, 1.0f - y);
                    //                        meshFilter.mesh.uv = newUV;
                    }
                }
            }
        }
    }
}
