﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
		public enum EffectKind{
            EFFECT_GROWUP_SUCCESS       = 0,
            EFFECT_GROWUP_GREAT_SUCCESS = 1,
            EFFECT_GROWUP_FAIL          = 2,
            EFFECT_GACHA_NORMAL         = 100,
            EFFECT_GACHA_RARE           = 101,
            EFFECT_GACHA_GEKIRARE       = 102,
        };
        public enum CutinKind{
            CUTIN_GROWUP_FAIL,
            CUTIN_GROWUP_NORMAL,
            CUTIN_GROWUP_HOT,
            CUTIN_GROWUP_VERY_HOT,
            CUTIN_GROWUP_PERFECT,
        }
		public class EffectManager : SingletonMonoBehaviour<EffectManager> {
            private System.Action<Animator> EndCallback;
            public Dictionary<EffectKind,string>animatorNameDic = new Dictionary<EffectKind,string>(){
                {EffectKind.EFFECT_GROWUP_SUCCESS,"S_Seikou"},
                {EffectKind.EFFECT_GROWUP_GREAT_SUCCESS,"S_Daiseikou"},
                {EffectKind.EFFECT_GROWUP_FAIL,"S_Shippai"},
                {EffectKind.EFFECT_GACHA_NORMAL,"G_Normal"},
                {EffectKind.EFFECT_GACHA_RARE,"G_Rare"},
                {EffectKind.EFFECT_GACHA_GEKIRARE,"G_GekiRare"}
            };
            public Dictionary<CutinKind,string>cutinNameDic = new Dictionary<CutinKind,string>(){
                {CutinKind.CUTIN_GROWUP_FAIL,"C_Blue"},
                {CutinKind.CUTIN_GROWUP_NORMAL,"C_Yellow"},
                {CutinKind.CUTIN_GROWUP_HOT,"C_Green"},
                {CutinKind.CUTIN_GROWUP_VERY_HOT,"C_Red"},
                {CutinKind.CUTIN_GROWUP_PERFECT,"C_Gold"},
            };
            Dictionary<string,Animator> effectAnimators = new Dictionary<string,Animator>();
            public Dictionary<string,GameObject> cutins = new Dictionary<string,GameObject>();
            public List<GameObject> cutinParent = new List<GameObject>();
            public GameObject backGround;

            // Use this for initialization
            void Awake () {
                foreach (GameObject obj in cutinParent) {
                    cutins.Add (obj.name, obj);
                    obj.SetActive (false);
                }
                foreach (Animator animator in gameObject.GetComponentsInChildren<Animator>()) {
                    effectAnimators.Add (animator.name, animator);
                    UIButton closeButton = animator.gameObject.GetComponentInChildren<UIButton> ();
                    if (closeButton!=null) {
                        EventDelegate callback = new EventDelegate (this, "closeEffect");
                        if (animatorNameDic.ContainsValue (animator.name)) {
                            callback.parameters [0].value = animator.name;
                            closeButton.onClick.Add (callback);
                        }
                    }
                    animator.gameObject.SetActive (false);
                    backGround.SetActive (false);
                }
        	}
            void Start(){
            }
        	// Update is called once per frame
        	void Update () {
        	}
            public void playCutin (CutinKind cutinKind) {
                backGround.SetActive(true);
                cutins [cutinNameDic [cutinKind]].SetActive(true);
            }
            public void stopCutin (CutinKind cutinKind) {
                cutins [cutinNameDic [cutinKind]].SetActive(false);
            }
            public void resetCutin() {
                foreach (GameObject obj in cutins.Values) {
                    obj.SetActive (false);
                }
            }
            public void playEffect (EffectKind effectKind,System.Action<Animator> rCallback) {
                effectAnimators [animatorNameDic [effectKind]].gameObject.SetActive (true);
                Animator animator = effectAnimators[animatorNameDic [effectKind]];
                animator.SetInteger ("AnimNum", (int)effectKind);
                EndCallback = rCallback;
                EffectAnimationCallbackStateMachineBehaviour animationStateMachineBehaviour = animator.GetBehaviour<EffectAnimationCallbackStateMachineBehaviour>();
//                animationStateMachineBehaviour.SetEndCallBack (playEffectCallBack);
            }
            public void closeEffect(string objectName){
                effectAnimators [objectName].gameObject.SetActive (false);
                backGround.SetActive(false);
                // クローズするときにカットインをリセットする
                resetCutin ();
            }
            public void playEffectCallBack (Animator animator) {
//                animator.gameObject.SetActive (false);
//                effectObjects ["Background"].SetActive(false);
                EndCallback (animator);
            }
        }
    }
}
