﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
	namespace ThpMmo{
		public class DungeonCharacter : ThpmmoMonoBehavier {

			private float TimedeltaTime;
			
			GameObject Directional;

			public CharacterInfo charaInfo;

			public GameObject playerObj;
			public GameObject playerObj2;
			public GameObject gameCamera;
            private GameObject nameObject;
			float playerSpeed=0.1f;

			Vector2 prePosition=new Vector2(0.0f,0.0f);
			Vector2 movePosition=new Vector2(0.0f,0.0f);
			Vector2 nowPostion=new Vector2(0.0f,0.0f);
			Vector3 camPos;
			enum PROC{
				INIT,
				WAIT,
			}
            private PROC proc=PROC.INIT;
			// Use this for initialization
			void Awake(){
				charaInfo = new CharacterInfo ();
			}
			void Start () {
				Directional = GameObject.Find("Directional");
				camPos = gameCamera.transform.position;
				proc=PROC.INIT;
				float roomSize = DungeonPlayer.ROOM_SIZE;
				float roomCameraSize = DungeonPlayer.ROOM_CAMERA_SIZE;
				float x = (float)(charaInfo.x);
				float y = (float)(charaInfo.y);
				transform.position = new Vector3 ((float)charaInfo.x, 0, (float)charaInfo.y);
				nowPostion = new Vector2 (x, y);
			}
			
			// Update is called once per frame
			void Update () {
				switch(proc){
					case PROC.INIT:
						InitChara();
						proc=PROC.WAIT;
						break;
					case PROC.WAIT:
						MoveChara ();
						break;
				}
			}
			private void InitChara(){
				Debug.Log("InitChara");
				float roomSize = DungeonPlayer.ROOM_SIZE;
				float roomCameraSize = DungeonPlayer.ROOM_CAMERA_SIZE;
				float x = (float)(charaInfo.x);
				float y = (float)(charaInfo.y);
				Debug.Log(string.Format ("startPos:{0},{1}",charaInfo.x, charaInfo.y));
				transform.position = new Vector3 ((float)charaInfo.x, 0, (float)charaInfo.y);
				nowPostion = new Vector2 (x, y);
			}
            /// <summary>
            /// UI表示
            /// </summary>
            private void viewUI(){
                nameObject = CharacterUtility.viewCharaName (gameObject, nameObject, charaInfo, DungeonCharacterManager.Instance.nameParent.transform);
            }
            public void beforeDestroy(){
                Destroy (nameObject);
                nameObject = null;
            }
            /// <summary>
            /// レベルアップ
            /// </summary>
            public void startLevelup(){
                CharacterUtility.startLevelup (gameObject, DungeonCharacterManager.Instance.nameParent.transform);
            }
            /// <summary>
            /// ダメージを受けるアクションを再生
            /// </summary>
            /// <param name="damageInfo">Damage info.</param>
            public void playDamageAction(DamageInfo damageInfo){
                GameObject damageTextObject;
                damageTextObject = (GameObject)Instantiate (Resources.Load ("Prefab/DamageNum"));
                damageTextObject.name = "damageLabel";
                UILabel label = damageTextObject.GetComponent<UILabel> ();
                label.fontSize = DungeonPlayer.NAME_FONT_SIZE;
                damageTextObject.transform.parent = DungeonCharacterManager.Instance.nameParent.transform;
                label.gameObject.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
                label.text = "[ffffff]"+damageInfo.damage.ToString()+"[ffffff]";
                var cameraPos = Camera.main.WorldToScreenPoint(transform.localPosition);
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + DungeonPlayer.DAMAGE_POSISION, -10.0f);
                // ダメージ演出スクリプト追加
                damageTextObject.AddComponent<DamageLabel> ();

                // ダメージパーティクル表示
                DamageManager.playDamageParticle (damageInfo.magicKind, damageInfo.magicLevel,gameObject);

                charaInfo.Hp = damageInfo.characterInfo.Hp;

                // 表示位置
                damageTextObject.transform.localPosition = pointPos;
            }
            /// <summary>
            /// Moves the chara.
            /// </summary>
			private void MoveChara () {
                viewUI ();
				TimedeltaTime = 55 * Time.deltaTime;
				Vector2 movePos = new Vector2(charaInfo.x, charaInfo.y);
                Vector3 beforePos = transform.position;
//				Debug.Log(string.Format ("movePos:{0},{1}",charaInfo.x, charaInfo.y));
//				Debug.Log(string.Format ("charaPos:{0},{1}",transform.position.x, transform.position.z));
                if(Mathf.FloorToInt(transform.position.z) < Mathf.FloorToInt(movePos.y) ) {
					transform.position += new Vector3 (0, 0, playerSpeed * TimedeltaTime);
//					playerObj.transform.localRotation = Quaternion.Euler(50,0,0);
//					playerObj2.GetComponent<Animator>().SetInteger("L_Direction",3);
//					playerObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.z - Mathf.Floor (movePos.y)) < 0.1f) {
                        transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Floor (movePos.y));
                    }
				}
                else if(Mathf.CeilToInt( transform.position.z ) > Mathf.FloorToInt(movePos.y) ){
					transform.position -= new Vector3 (0, 0, playerSpeed * TimedeltaTime);
//					playerObj.transform.localRotation = Quaternion.Euler(-50,180,0);
//					playerObj2.GetComponent<Animator>().SetInteger("L_Direction",1);
//					playerObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.z - Mathf.Floor (movePos.y)) < 0.1f) {
                        transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Floor (movePos.y));
                    }
				}
                if(Mathf.FloorToInt(transform.position.x) < Mathf.FloorToInt(movePos.x) ) {
					transform.position += new Vector3 (playerSpeed * TimedeltaTime, 0, 0);
//					playerObj.transform.localRotation = Quaternion.Euler(0,90,50);
//					playerObj2.GetComponent<Animator>().SetInteger("L_Direction",2);
//					playerObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.x - Mathf.Floor (movePos.x)) < 0.1f) {
                        transform.position = new Vector3 (Mathf.Floor (movePos.x), transform.position.y, transform.position.z);
                    }
				}
                else if( Mathf.CeilToInt(transform.position.x ) > Mathf.FloorToInt(movePos.x) ){
					transform.position -= new Vector3 (playerSpeed * TimedeltaTime, 0, 0);
//					playerObj.transform.localRotation = Quaternion.Euler(0,270,-50);
//					playerObj2.GetComponent<Animator>().SetInteger("L_Direction",4);
//					playerObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.x - Mathf.Floor (movePos.x)) < 0.1f) {
                        transform.position = new Vector3 (Mathf.Floor (movePos.x), transform.position.y, transform.position.z);
                    }
				}
                // コリジョンチェック
                COLLISION_RESULT collisionResult = DungeonMaker.Instance.checkCollision (transform.position.x, transform.position.z);
                if (collisionResult == COLLISION_RESULT.COLLISION_RESULT_WALL) {
                    transform.position = beforePos;
//                    playerObj2.GetComponent<Animator>().speed = 0.0f;
                }

			}
		}
}}
