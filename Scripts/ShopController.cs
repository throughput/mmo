﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
    namespace ThpMmo{
        public class ShopController : SingletonMonoBehaviour<ShopController> {
            public GameObject shopList;
        	// Use this for initialization
        	void Start () {
        	
        	}
        	
        	// Update is called once per frame
        	void Update () {
        	
        	}
            // Update is called once per frame
            public void Open () {
                shopList.SetActive (true);
                UIGrid grid = shopList.GetComponentInChildren<UIGrid> ();
                MenuController.DestroyList (grid.gameObject);
                foreach (string itemId in MainController.Instance.itemTableDic.Keys) {
                    ItemTableInfo itemTable = MainController.Instance.itemTableDic[itemId];
                    GameObject listObj = (GameObject)Instantiate(Resources.Load ("Prefab/List/L_Shop"));
                    listObj.transform.parent = grid.gameObject.transform;
                    foreach(UILabel label in listObj.GetComponentsInChildren<UILabel> ()){
                        switch (label.name) {
                        case "Name":
                            label.text = itemTable.name;
                            break;
                        }
                    }
                    listObj.transform.parent = grid.gameObject.transform;
                    foreach (UIButton button in listObj.GetComponentsInChildren<UIButton> ()) {
                        EventDelegate callback= null;
                        switch(button.name){
                        case "B_Shop":
                            callback = new EventDelegate (this, "selectTarget");
                            callback.parameters [0].value = itemId;
                            break;
                        }
                        if (callback!=null) {
                            button.onClick.Add (callback);
                            button.enabled = true;
                        }
                    }
                    listObj.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                    listObj.transform.localScale = Vector3.one;
                }
                UIScrollView scrollView = shopList.GetComponentInChildren<UIScrollView> ();
                grid.Reposition ();
                scrollView.ResetPosition();
            }

            void selectTarget(string itemID){
            }
        }
    }}
