﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Throughputjp{
    namespace ThpMmo{
        public class GameTextTable{
            static public Dictionary<string,string> GameDialogtext(){
                return TextTable.Instance.Table ["table_gameDialogtext"];
            }
            static public Dictionary<string,string> CommonDialogtext(){
                return TextTable.Instance.Table ["table_commonDialogtext"];
            }
            static public Dictionary<string,string> Uitext(){
                return TextTable.Instance.Table ["table_uiText"];
            }
            static public Dictionary<string,string> PageTitle(){
                return TextTable.Instance.Table ["table_pageTitle"];
            }
            static public Dictionary<string,string> SystemText(){
                return TextTable.Instance.Table ["table_systemText"];
            }
        }
    }
}