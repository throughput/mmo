﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
	namespace ThpMmo{
		public class DungeonMonster : ThpmmoMonoBehavier {

			private float TimedeltaTime;
            private GameObject nameObject;
            private GameObject targetObject;
			GameObject Directional;

            public MonsterInfo monsterInfo;
            //			public GameObject monsterObj;
            //			public GameObject monsterObj2;
			public GameObject gameCamera;
			float playerSpeed=0.1f;
            public const float TARGET_POSISION = 40.0f;

			Vector2 prePosition=new Vector2(0.0f,0.0f);
			Vector2 movePosition=new Vector2(0.0f,0.0f);
			Vector2 nowPostion=new Vector2(0.0f,0.0f);
			Vector3 camPos;
            const float DEAD_ANIMATION_TIME=3.0f;
            const float FLASH_TIMEOUT = 0.5f;
            float flashTimer = 0.0f;
            bool colorFilterFlag = false;
			enum PROC{
				INIT,
				WAIT,
			}
			private PROC proc=PROC.INIT;
			// Use this for initialization
			void Awake(){
                monsterInfo = new MonsterInfo ();
			}

            void Start () {
				Directional = GameObject.Find("Directional");
				camPos = gameCamera.transform.position;
				proc=PROC.INIT;
				float roomSize = DungeonPlayer.ROOM_SIZE;
				float roomCameraSize = DungeonPlayer.ROOM_CAMERA_SIZE;
				Vector2 roomPosition = new Vector2 (
					monsterInfo.areaCodex, 
					monsterInfo.areaCodey);
				float x = (float)(monsterInfo.x);
				float y = (float)(monsterInfo.y);
//				transform.position = new Vector3 ((float)monsterInfo.x, 0, (float)monsterInfo.y);
				nowPostion = new Vector2 (x, y);
			}
			
			// Update is called once per frame
			void Update () {
				switch(proc){
					case PROC.INIT:
						InitChara();
						proc=PROC.WAIT;
						break;
					case PROC.WAIT:
						MoveChara ();
						break;
				}
			}
            /// <summary>
            /// 色フィルタ変更
            /// </summary>
            /// <param name="color">Color.</param>
            private void colorFilterChange(Color color,float timer){
                Mesh mesh = gameObject.GetComponentInChildren<MeshFilter> ().mesh;
                Color[] colors = mesh.colors;

                for ( int i = 0 ; i < mesh.colors.Length ; ++i ) {
                    colors[i] = color;
                }
                flashTimer = timer;
                if (flashTimer > 0.0f) {
                    colorFilterFlag = true;
                }
                mesh.colors = colors;
            }
            /// <summary>
            /// 攻撃演出
            /// </summary>
            /// <param name="damageInfo">Damage info.</param>
            public void playAttackAction(DamageInfo damageInfo){
                Color color = new Color(1, 1, 0, 1);
                colorFilterChange (color,FLASH_TIMEOUT);
                if (damageInfo.magicKind == 0) {
                    SoundManager.Instance.PlaySE ((int)SeList.ATTACK_SWORD);
                } else {
                    switch (damageInfo.magicKind) {
                    case MagicKind.MAGIC_FIRE:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_WIND:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_WATER:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_ICE:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_EARTH:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_THUNDER:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_LIGHT:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    case MagicKind.MAGIC_DARK:
                        SoundManager.Instance.PlaySE ((int)SeList.MAGIC_WIND_1);
                        break;
                    }
                }
            }
            private void colorFilterReset(){
                Color color = new Color(1, 1, 1, 1);
                colorFilterChange (color,0.0f);
            }
            /// <summary>
            /// Inits the chara.
            /// </summary>
			private void InitChara(){
				Debug.Log("InitChara");
				float roomSize = DungeonPlayer.ROOM_SIZE;
				float roomCameraSize = DungeonPlayer.ROOM_CAMERA_SIZE;
				Vector2 roomPosition = new Vector2 (
                    monsterInfo.areaCodex, 
                    monsterInfo.areaCodey);
				float x = (float)(monsterInfo.x);
				float y = (float)(monsterInfo.y);
				Debug.Log(string.Format ("startPos:{0},{1}",monsterInfo.x, monsterInfo.y));
                transform.position = new Vector3 ((float)monsterInfo.x, 0.6f, (float)monsterInfo.y);
				nowPostion = new Vector2 (x, y);
			}
            /// <summary>
            /// Befores the destroy.
            /// </summary>
            public void dead(){
                GameObject deadObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Dead"));
                deadObject.transform.parent = gameObject.transform.parent;
                deadObject.transform.localPosition = gameObject.transform.localPosition;
                deadObject.transform.localScale = new Vector3 (0.0f, 0.0f, 0.0f);
                Destroy (deadObject, DEAD_ANIMATION_TIME);
            }

            /// <summary>
            /// Befores the destroy.
            /// </summary>
            public void beforeDestroy(){
                Destroy (nameObject);
                Destroy (targetObject);
                nameObject = null;
            }
            /// <summary>
            /// キャラクタ名表示
            /// </summary>
            private void viewCharaName(){
                if (nameObject == null) {
                    nameObject = (GameObject)Instantiate (Resources.Load ("Prefab/NameLabel"));
                }
                UILabel label = nameObject.GetComponent<UILabel> ();
                label.fontSize = DungeonPlayer.NAME_FONT_SIZE;
                nameObject.transform.parent = DungeonCharacterManager.Instance.nameParent.transform;
                label.gameObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
                if (monsterInfo != null) {
                    if(MainController.Instance.monsterTableInfoDic.ContainsKey(monsterInfo.monsterID.ToString())){
                        label.text = MainController.Instance.monsterTableInfoDic[monsterInfo.monsterID.ToString()].name;
                    }else{
                        label.text = monsterInfo.monsterID.ToString();
                    }
                }
                var cameraPos = Camera.main.WorldToScreenPoint(transform.localPosition);
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + DungeonPlayer.NAME_POSISION, -10.0f);

                // 表示位置
                nameObject.transform.localPosition = pointPos;
            }
            /// <summary>
            /// モンスターにターゲットをつける
            /// </summary>
            private void monsterTarget(){
                if (targetObject == null) {
                    targetObject = (GameObject)Instantiate (Resources.Load ("Prefab/MonsterTarget"));
                }
                targetObject.name = "monsterTarget";
                targetObject.transform.parent = DungeonCharacterManager.Instance.nameParent.transform;
                targetObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
                var cameraPos = Camera.main.WorldToScreenPoint(transform.localPosition);
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + TARGET_POSISION, -10.0f);

                // 表示位置
                targetObject.transform.localPosition = pointPos;
            }
            /// <summary>
            /// ダメージを受けるアクションを再生
            /// </summary>
            /// <param name="attackInfo">Attack info.</param>
            public void playDamageAction(AttackInfo attackInfo){
                GameObject damageTextObject;
                damageTextObject = (GameObject)Instantiate (Resources.Load ("Prefab/DamageNum"));
                damageTextObject.name = "damageLabel";
                UILabel label = damageTextObject.GetComponent<UILabel> ();
                label.fontSize = DungeonPlayer.NAME_FONT_SIZE;
                damageTextObject.transform.parent = DungeonCharacterManager.Instance.nameParent.transform;
                label.gameObject.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
                label.text = "[ffffff]"+attackInfo.damage.ToString()+"[ffffff]";
                var cameraPos = Camera.main.WorldToScreenPoint(transform.localPosition);
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + DungeonPlayer.DAMAGE_POSISION, -10.0f);

                Color color = new Color(1, 0, 0, 1);
                colorFilterChange (color,FLASH_TIMEOUT);

                // ダメージ数演出スクリプト追加
                damageTextObject.AddComponent<DamageLabel> ();

                // ダメージパーティクル表示
                DamageManager.playDamageParticle (attackInfo.magicKind, attackInfo.magicLevel,gameObject);

                // HP反映
                monsterInfo.Hp = attackInfo.monsterInfo.Hp;

                // 表示位置
                damageTextObject.transform.localPosition = pointPos;
            }
            /// <summary>
            /// Moves the chara.
            /// </summary>
			private void MoveChara () {
                viewCharaName ();
                if (monsterInfo.target) {
                    monsterTarget ();
                } else {
                    Destroy (targetObject);
                    targetObject = null;
                }
                // 色フィルタリセット
                if (flashTimer < 0.0f) {
                    colorFilterReset ();
                } else {
                    flashTimer -= Time.deltaTime;
                }
				TimedeltaTime = 55 * Time.deltaTime;
				Vector2 movePos = new Vector2(monsterInfo.x, monsterInfo.y); 
//                Debug.Log(string.Format ("movePos:{0},{1}",monsterInfo.x, monsterInfo.y));
//                Debug.Log(string.Format ("charaPos:{0},{1}",transform.position.x, transform.position.z));

                Vector3 beforePos = transform.position;
				if(Mathf.FloorToInt(transform.position.z)< Mathf.FloorToInt(movePos.y) ) {
					transform.position += new Vector3 (0, 0, playerSpeed * TimedeltaTime);
//					monsterObj.transform.localRotation = Quaternion.Euler(50,0,0);
//					monsterObj2.GetComponent<Animator>().SetInteger("L_Direction",3);
//					monsterObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.z - Mathf.Floor (movePos.y)) < 0.1f) {
                        transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Floor (movePos.y));
                    }
				}
                else if(Mathf.CeilToInt( transform.position.z ) > Mathf.FloorToInt(movePos.y) ){
					transform.position -= new Vector3 (0, 0, playerSpeed * TimedeltaTime);
//					monsterObj.transform.localRotation = Quaternion.Euler(-50,180,0);
//					monsterObj2.GetComponent<Animator>().SetInteger("L_Direction",1);
//					monsterObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.z - Mathf.Floor (movePos.y)) < 0.1f) {
                        transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Floor (movePos.y));
                    }
				}
				if(Mathf.FloorToInt(transform.position.x)< Mathf.FloorToInt(movePos.x) ) {
					transform.position += new Vector3 (playerSpeed * TimedeltaTime, 0, 0);
//					monsterObj.transform.localRotation = Quaternion.Euler(0,90,50);
//					monsterObj2.GetComponent<Animator>().SetInteger("L_Direction",2);
//					monsterObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.x - Mathf.Floor (movePos.x)) < 0.1f) {
                        transform.position = new Vector3 (Mathf.Floor (movePos.x), transform.position.y, transform.position.z);
                    }
				}
                else if( Mathf.CeilToInt(transform.position.x ) > Mathf.FloorToInt(movePos.x) ){
					transform.position -= new Vector3 (playerSpeed * TimedeltaTime, 0, 0);
//					monsterObj.transform.localRotation = Quaternion.Euler(0,270,-50);
//					monsterObj2.GetComponent<Animator>().SetInteger("L_Direction",4);
//					monsterObj2.GetComponent<Animator>().speed = 1.0f;
                    if (Mathf.Abs (transform.position.x - Mathf.Floor (movePos.x)) < 0.1f) {
                        transform.position = new Vector3 (Mathf.Floor (movePos.x), transform.position.y, transform.position.z);
                    }
				}
                // コリジョンチェック
                COLLISION_RESULT collisionResult = DungeonMaker.Instance.checkCollision (transform.position.x, transform.position.z);
                if (collisionResult == COLLISION_RESULT.COLLISION_RESULT_WALL) {
                    transform.position = beforePos;
                    // playerObj2.GetComponent<Animator>().speed = 0.0f;
                }
			}
		}
}}
