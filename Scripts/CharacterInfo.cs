﻿using System.Collections.Generic;

namespace Throughputjp{
	namespace ThpMmo{
		public class CharacterInfo{
            public int characterID=0;
            public string friendCode = "";
			public int mapCode=0;
            public int x=0;
            public int y=0;
            public int swordID=0;
            public int armorID=0;
            public int shieldID=0;
            public int headID=0;
            public string name="";
            public int growupPoint = 0;
            public int ticket = 0;
            public int medal = 0;
            public int treasureLevel = 0;
            public int battleLevel = 0;
            public int attack=0;
            public int defence=0;
            public int attackSpeed=0;
            public int maxHp=0;
            public int maxSp=0;
            public int Hp=0;
            public int Sp=0;
		}
	}
}