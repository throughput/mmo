﻿using UnityEngine;
using System.Collections;

public class UIButtons : MonoBehaviour {

	GameObject target;
	
	Transform[] Anc_T_Lists;
	Transform[] Anc_C_Lists;
	Transform[] Anc_B_Lists;

	void Awake () {

		GameObject UITop = GameObject.Find("AnchorTop");
		GameObject UICenter = GameObject.Find("AnchorCenter");
		GameObject UIBottom = GameObject.Find("AnchorBottom");
		Anc_T_Lists = UITop.transform.GetComponentsInChildren<Transform>();
		Anc_C_Lists = UICenter.transform.GetComponentsInChildren<Transform>();
		Anc_B_Lists = UIBottom.transform.GetComponentsInChildren<Transform>();

		foreach (Transform List in Anc_T_Lists) {
			if(List.name == "A_Header"){
				List.gameObject.SetActive(true);
			}
			if(List.name == "A_Menu"){
				List.gameObject.SetActive(true);
			}
		}
	}

	void Update () {
	
	}


}
