﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
    namespace ThpMmo{
        public enum TreasureKind{
            TREASURE_GOLD   = 1,
            TREASURE_SILVER = 2,
            TREASURE_BRONZE = 3,
        }
        public class TreasureController : MonoBehaviour {
            const int GOLD_INSENTIE_ID = 50101;
            const int SILVER_INSENTIE_ID = 50102;
            const int BRONZE_INSENTIE_ID = 50103;
            public TreasureKind treasureKind = TreasureKind.TREASURE_BRONZE;
            public bool openFlag = false;
            public bool gachaFlag = false;
            /// <summary>
            /// 宝箱のトリガー
            /// </summary>
            /// <param name="other">Other.</param>
            private void OnTriggerEnter(Collider other)
            {
                if (other.name == "PlayerObject") {
                    openFlag = true;
                    string gachaInsentiveName = GameTextTable.SystemText () ["commonChargeInsentive"];
                    string ticketInsentiveName = GameTextTable.SystemText () ["commonChargeInsentive"];
                    int useInsentiveCount = 0;
                    int treasureTicketID = BRONZE_INSENTIE_ID;
                    switch (treasureKind) {
                    case TreasureKind.TREASURE_GOLD:
                        ticketInsentiveName = GameTextTable.SystemText () ["goldInsentive"];
                        treasureTicketID = GOLD_INSENTIE_ID;
                        useInsentiveCount = GameInfo.Instance.chargeGachaIncentiveExpensive;
                        gachaInsentiveName = GameTextTable.SystemText () ["commonChargeInsentive"];
                        break;
                    case TreasureKind.TREASURE_SILVER:
                        ticketInsentiveName = GameTextTable.SystemText () ["silverInsentive"];
                        treasureTicketID = SILVER_INSENTIE_ID;
                        useInsentiveCount = GameInfo.Instance.chargeGachaIncentive;
                        gachaInsentiveName = GameTextTable.SystemText () ["commonChargeInsentive"];
                        break;
                    case TreasureKind.TREASURE_BRONZE:
                        ticketInsentiveName = GameTextTable.SystemText () ["bronzeInsentive"];
                        treasureTicketID = BRONZE_INSENTIE_ID;
                        useInsentiveCount = GameInfo.Instance.freeGachaIncentive;
                        gachaInsentiveName = GameTextTable.SystemText () ["commonFreeInsentive"];
                        break;
                    }
                    bool ticketFlag=false;
                    // チケットを持ってるか確認
                    foreach (ItemInfo itemInfo in PlayerInfo.Instance.myItemList.Values) {
                        if (itemInfo.id == treasureTicketID) {
                        // 念のためアイテムの数も確認
                            if (itemInfo.count > 0) {
                                gachaInsentiveName = ticketInsentiveName;
                                useInsentiveCount = 1;
                            }
                        }
                    }
                    if (gachaFlag) {
                        MenuController.Instance.OpenDialog (DialogForm.DIALOG_YES_NO,
                            gachaInsentiveName + useInsentiveCount.ToString() + GameTextTable.GameDialogtext () ["questionOpenGacha"],openGacha);
                    } else {
                        Animator anime = GetComponent<Animator> ();
                        anime.Play ("Chest_Open");
                    }

                }
            }
            /// <summary>
            /// ガチャひらく
            /// </summary>
            /// <param name="result">Result.</param>
            private void openGacha(DialogResult result){
                if (result == DialogResult.RESULT_YES) {
                    Animator anime = GetComponent<Animator> ();
                    GachaController.GachaKind gachaKind = GachaController.GachaKind.GACHA_BRONZE;
                    bool isTicket=false;
                    switch (treasureKind) {
                    case TreasureKind.TREASURE_GOLD:
                        gachaKind = GachaController.GachaKind.GACHA_GOLD;
                        isTicket = true;
                        break;
                    case TreasureKind.TREASURE_SILVER:
                        gachaKind = GachaController.GachaKind.GACHA_SILVER;
                        isTicket = true;
                        break;
                    case TreasureKind.TREASURE_BRONZE:
                        gachaKind = GachaController.GachaKind.GACHA_BRONZE;
                        isTicket = false;
                        break;
                    }
                    GachaController.Instance.openGacha (gachaKind,isTicket);
                    anime.Play ("Chest_Open");
                } else {
                }
            }
            /// <summary>
            /// 箱と重なってないときの動作
            /// </summary>
            /// <param name="other">Other.</param>
            private void OnTriggerExit(Collider other)
            {
                if (other.name == "PlayerObject") {
                    openFlag = false;
                    Animator anime = GetComponent<Animator> ();
                    anime.Play ("Chest_Close");
                }
            }   

        }
    }
}