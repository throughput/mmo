﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Throughputjp{
	namespace ThpMmo{
		public class DungeonMonsterManager : SingletonMonoBehaviour<DungeonMonsterManager> {
			List<GameObject> dungeonMonsterArray;
			public GameObject gameCamera;
            const float uvBaseParam = 0.03124f;
            const float uvBigBaseParam = 0.03124f*2.0f;
            const int monsterListVMax = 32;
			// Use this for initialization
			void Start () {
				dungeonMonsterArray = new List<GameObject> ();
			}
			// Update is called once per frame
			void Update () {
                if(SocketNetwork.Instance.monsterInfoArray != null){
                    if(SocketNetwork.Instance.monsterInfoArray.Count>0){
                        foreach(MonsterInfo netWorkMonsterInfo in SocketNetwork.Instance.monsterInfoArray){
							bool newCharaFlag=true;
							foreach(GameObject dungeonMonsterObject in dungeonMonsterArray){
								DungeonMonster dungeonMonster=dungeonMonsterObject.GetComponent<DungeonMonster>();
                                MonsterInfo localMonsterInfo=dungeonMonster.monsterInfo;
                                if(localMonsterInfo.monsterID==netWorkMonsterInfo.monsterID && localMonsterInfo.monsterNum == netWorkMonsterInfo.monsterNum){
                                    localMonsterInfo.x = netWorkMonsterInfo.x;
                                    localMonsterInfo.y = netWorkMonsterInfo.y;
                                    localMonsterInfo.mapCode = netWorkMonsterInfo.mapCode;
                                    localMonsterInfo.areaCodex = netWorkMonsterInfo.areaCodex;
                                    localMonsterInfo.areaCodey = netWorkMonsterInfo.areaCodey;
                                    localMonsterInfo.monsterID = netWorkMonsterInfo.monsterID;
                                    localMonsterInfo.monsterNum = netWorkMonsterInfo.monsterNum;
                                    localMonsterInfo.MaxHp = netWorkMonsterInfo.MaxHp;
                                    localMonsterInfo.Hp = netWorkMonsterInfo.Hp;
                                    dungeonMonster.monsterInfo = localMonsterInfo;
                                    newCharaFlag = false;
								}
							}
							if(newCharaFlag)
							{
                                if (netWorkMonsterInfo.Hp <= 0) {
                                    continue;
                                }
                                MonsterTableInfo monsterTableInfo = MainController.Instance.monsterTableInfoDic [netWorkMonsterInfo.monsterID.ToString ()];
                                GameObject newMonster;
                                Debug.Log (monsterTableInfo.isBoss);
                                if (monsterTableInfo.isBoss) {
                                    newMonster= (GameObject)Instantiate (Resources.Load ("Graphics/Objects/Monster/BigMonster"));
                                } else {
                                    newMonster = (GameObject)Instantiate (Resources.Load ("Graphics/Objects/Monster/SmallMonster"));
                                }
                                DungeonMonster dungeonMonster=newMonster.AddComponent<DungeonMonster>();
								MonsterInfo localMonsterInfo=new MonsterInfo();
                                localMonsterInfo.x=netWorkMonsterInfo.x;
                                localMonsterInfo.y=netWorkMonsterInfo.y;
                                localMonsterInfo.mapCode=netWorkMonsterInfo.mapCode;
                                localMonsterInfo.areaCodex=netWorkMonsterInfo.areaCodex;
                                localMonsterInfo.areaCodey=netWorkMonsterInfo.areaCodey;
                                localMonsterInfo.monsterID=netWorkMonsterInfo.monsterID;
                                localMonsterInfo.monsterNum=netWorkMonsterInfo.monsterNum;
                                localMonsterInfo.MaxHp = netWorkMonsterInfo.MaxHp;
                                localMonsterInfo.Hp = netWorkMonsterInfo.Hp;
                                changeMonsterUV(newMonster,monsterTableInfo.imageID);
                                dungeonMonster.monsterInfo=localMonsterInfo;
								dungeonMonster.gameCamera=gameCamera;
								dungeonMonsterArray.Add(newMonster);
								newMonster.transform.parent=gameObject.transform;
                                newMonster.transform.localPosition = new Vector3 ((float)netWorkMonsterInfo.x, 0.0f, (float)netWorkMonsterInfo.y);
					        }
						}
                        SocketNetwork.Instance.monsterInfoArray.Clear ();
					}
				}
                if(SocketNetwork.Instance.deleteMonsterIDArray != null){
                    if(SocketNetwork.Instance.deleteMonsterIDArray.Count>0){
                        foreach(int deleteCharaID in SocketNetwork.Instance.deleteMonsterIDArray){
                            for(int i=0;i<dungeonMonsterArray.Count;++i){
                                GameObject dungeonMonsterObject=dungeonMonsterArray[i];
                                if(deleteCharaID==dungeonMonsterObject.GetComponent<DungeonMonster>().monsterInfo.monsterID){
                                    dungeonMonsterArray.Remove(dungeonMonsterObject);
                                    dungeonMonsterObject.GetComponent<DungeonMonster> ().beforeDestroy ();
                                    Destroy(dungeonMonsterObject);
                                }
                            }
                        }
                    }
                }
                // 死亡確認
                List<MonsterInfo> deleteMosterInfoList = new List<MonsterInfo>();
                for (int i = 0; i < dungeonMonsterArray.Count; ++i) {
                    GameObject dungeonMonsterObject = dungeonMonsterArray [i];
                    if (dungeonMonsterObject.GetComponent<DungeonMonster> ().monsterInfo.Hp <= 0) {
                        deleteMosterInfoList.Add (dungeonMonsterObject.GetComponent<DungeonMonster> ().monsterInfo);
                    }
                }
                // 死亡処理
                foreach(MonsterInfo deleteMonsterInfo in deleteMosterInfoList){
                    for (int i = 0; i < dungeonMonsterArray.Count; ++i) {
                        GameObject dungeonMonsterObject = dungeonMonsterArray [i];
                        DungeonMonster dungeonMosnter = dungeonMonsterObject.GetComponent<DungeonMonster> ();
                        if (dungeonMosnter.monsterInfo.monsterID == deleteMonsterInfo.monsterID &&
                            dungeonMosnter.monsterInfo.monsterNum == deleteMonsterInfo.monsterNum) {
                            dungeonMonsterArray.Remove (dungeonMonsterObject);
                            dungeonMosnter.dead ();
                            dungeonMosnter.beforeDestroy ();
                            Destroy (dungeonMonsterObject);
                            break;
                        }
                    }
                }
			}
            /// <summary>
            /// 攻撃アクションを再生
            /// </summary>
            /// <param name="damageInfo">Damage info.</param>
            public void playAttackAction(DamageInfo damageInfo){
                foreach (GameObject dungeonMonsterObject in dungeonMonsterArray) {
                    DungeonMonster dungeonMonster = dungeonMonsterObject.GetComponent<DungeonMonster> ();
                    if (dungeonMonster.monsterInfo.monsterID == damageInfo.monsterInfo.monsterID) {
                        if (dungeonMonster.monsterInfo.monsterNum == damageInfo.monsterInfo.monsterNum) {
                            dungeonMonster.playAttackAction (damageInfo);
                        }
                    }
                }
            }

            /// <summary>
            /// ダメージを受けるアクションを再生
            /// </summary>
            /// <param name="damageInfo">Damage info.</param>
            public void playDamageAction(AttackInfo attackInfo){
                foreach (GameObject dungeonMonsterObject in dungeonMonsterArray) {
                    DungeonMonster dungeonMonster = dungeonMonsterObject.GetComponent<DungeonMonster> ();
                    if (dungeonMonster.monsterInfo.monsterID == attackInfo.monsterInfo.monsterID) {
                        if (dungeonMonster.monsterInfo.monsterNum == attackInfo.monsterInfo.monsterNum) {
                            dungeonMonster.playDamageAction (attackInfo);
                        }
                    }
                }
            }
            /// <summary>
            /// Changes the monster UV
            /// </summary>
            /// <param name="monsterObject">Monster object.</param>
            /// <param name="monsterImageID">Monster image I.</param>
            /// <param name="isBig">If set to <c>true</c> is big.</param>
            void changeMonsterUV(GameObject monsterObject,int monsterImageID){
                // UV変更
                MeshFilter meshFilter = monsterObject.GetComponentInChildren<MeshFilter> ();
                Vector2[] newUV = new Vector2[4];
                float x=(monsterImageID%monsterListVMax) * uvBaseParam;
                float y=Mathf.Floor(monsterImageID / monsterListVMax) * uvBaseParam;
                newUV [0] = new Vector2 (0.0f + x, 0.96876f - y);
                newUV [1] = new Vector2 (0.03124f + x, 1.0f - y);
                newUV [2] = new Vector2 (0.03124f + x, 0.96876f - y);
                newUV [3] = new Vector2 (0.0f + x, 1.0f - y);
                meshFilter.mesh.uv = newUV;
            }
            /// <summary>
            /// 指定の位置あたりに敵がいるかチェック
            /// </summary>
            /// <returns>The position monster I.</returns>
            /// <param name="x">The x coordinate.</param>
            /// <param name="y">The y coordinate.</param>
            public MonsterInfo getPositionMonsterID(int x,int y){
                // ターゲットクリア
                for (int i = 0; i < dungeonMonsterArray.Count; ++i) {
                    GameObject dungeonMonsterObject=dungeonMonsterArray[i];
                    MonsterInfo monsterInfo = dungeonMonsterObject.GetComponent<DungeonMonster> ().monsterInfo;
                    monsterInfo.target = false;
                }
                for(int i=0;i<dungeonMonsterArray.Count;++i){
                    GameObject dungeonMonsterObject=dungeonMonsterArray[i];
                    MonsterInfo monsterInfo = dungeonMonsterObject.GetComponent<DungeonMonster> ().monsterInfo;
                    if((monsterInfo.x>=x-1 && monsterInfo.x<=x+1)  && (monsterInfo.y>=y-1 && monsterInfo.y<=y+1) ){
                        monsterInfo.target = true;
                        return monsterInfo;
                    }
                }
                return null;
            }
            /// <summary>
            /// Destroies all monster.
            /// </summary>
			public void DestroyAllMonster(){
                if(dungeonMonsterArray!=null){
					if(dungeonMonsterArray.Count>0){
                        for(int i=0;i<dungeonMonsterArray.Count;++i){
                            GameObject dungeonMonsterObject=dungeonMonsterArray[i];
                            dungeonMonsterObject.GetComponent<DungeonMonster> ().beforeDestroy ();
							Destroy(dungeonMonsterObject);
						}
                        dungeonMonsterArray.Clear();
                        SocketNetwork.Instance.monsterInfoArray.Clear ();
					}
				}
			}
		}
	}
}