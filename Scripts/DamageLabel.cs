﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Throughputjp{
    namespace ThpMmo{
        public enum NumberColor {
            NUMBER_COLOR_BLUE,
            NUMBER_COLOR_RED,
        };
        public class DamageLabel : ThpmmoMonoBehavier {
            float damagePos = 0.0f;
            const float EFFECT_HIGH = 1.0f;
            const float EFFECT_SPEED = 0.5f;
            int frame =0;
            const int VIEW_FRAME=30;
            // Use this for initialization
            void Start () {
//                myTexture = (Texture)Resources.Load("Graphics/UI/GUIAtlas");
            }
        	// Update is called once per frame
        	void Update () {
                gameObject.gameObject.transform.localPosition += Vector3.up*damagePos;
                if (damagePos < EFFECT_HIGH) {
                    damagePos += EFFECT_SPEED;
                }
                if (frame > VIEW_FRAME) {
                    Destroy (gameObject);
                }
                ++frame;
        	}
        }
    }
}