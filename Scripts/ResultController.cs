﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
    namespace ThpMmo{
        public class ResultController : SingletonMonoBehaviour<ResultController> {
            public GameObject resultList;
            public int growupPoint = 0;
        	// Use this for initialization
        	void Start () {
                gameObject.SetActive (false);
                resultList.SetActive (false);
        	}
        	
        	// Update is called once per frame
        	void Update () {
        	
        	}
            public void OpenResult(){
                gameObject.SetActive (true);
                resultList.SetActive (true);
                growupPoint = 0;
                UIGrid grid = resultList.GetComponentInChildren<UIGrid> ();
                MenuController.DestroyList (grid.gameObject);
                // 入手アイテム一覧
                foreach (TreasureInfo tresure in PlayerInfo.Instance.resultInfo.treasures) {
                    GameObject listObj = Instantiate ((GameObject)Resources.Load ("Prefab/List/L_ResultItem"));
                    listObj.transform.parent = grid.gameObject.transform;
                    listObj.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                    listObj.transform.localScale = Vector3.one;
                }

                UIScrollView scrollView = resultList.GetComponentInChildren<UIScrollView> ();
                grid.Reposition ();
                scrollView.ResetPosition();
            }
            public void Close(){
                for(int i=0; i < PlayerInfo.Instance.resultInfo.treasures.Count; ++i){
                    // アイテムBOXの空きを探す
                    int itemBoxNo = ItemUtility.getSpaceItemBoxNo (PlayerInfo.Instance.myItemList);
                    if (itemBoxNo == ItemUtility.itemBoxNone) {
                        // Todo:事前にチェックして、アイテム入らないエラーだす
                    } else {
                        PlayerInfo.Instance.resultInfo.treasures [i].itemInfo.itemBoxNo = itemBoxNo;
                        PlayerInfo.Instance.setItem (PlayerInfo.Instance.resultInfo.treasures [i].itemInfo);
                    }
                }
                gameObject.SetActive (false);
                resultList.SetActive (false);
                MainController.Instance.ResultSend ();
            }
        }
    }
}