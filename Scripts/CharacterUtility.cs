﻿using UnityEngine;
using System.Collections;
namespace Throughputjp{
    namespace ThpMmo{

        public class CharacterUtility {
            // レベルアップ演出
            private const float LEVELUP_EFFECT_TIME = 1.0f;

            public static GameObject viewCharaName(GameObject charaObject,GameObject nameObject,CharacterInfo charaInfo,Transform uiTransform){
                if (nameObject == null) {
                    nameObject = (GameObject)GameObject.Instantiate (Resources.Load ("Prefab/NameLabel"));
                }
                UILabel label = nameObject.GetComponent<UILabel> ();
                label.fontSize = DungeonPlayer.NAME_FONT_SIZE;
                nameObject.transform.parent = uiTransform;
                label.gameObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
                if (charaInfo != null) {
                    label.text = charaInfo.name;
                }
                var cameraPos = Camera.main.WorldToScreenPoint(charaObject.transform.localPosition);
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + DungeonPlayer.NAME_POSISION, -10.0f);

                // 表示位置
                nameObject.transform.localPosition = pointPos;
                return nameObject;
            }
            public static void startLevelup(GameObject charaObject,Transform uiTransform){
                GameObject levelupEffect = (GameObject)GameObject.Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/LevelUp"));
                SoundManager.Instance.PlaySE ((int)SeList.LEVEL_UP);
                levelupEffect.transform.parent = uiTransform;
                levelupEffect.gameObject.transform.localScale = new Vector3 (0.25f, 0.25f, 1.0f);
                var cameraPos = Camera.main.WorldToScreenPoint(charaObject.transform.localPosition);
                // キャラクターの分上にあげる
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2, -10.0f);
                // 表示位置
                levelupEffect.transform.localPosition = pointPos;
                GameObject.Destroy (levelupEffect, LEVELUP_EFFECT_TIME);
            }
        }
    }
}