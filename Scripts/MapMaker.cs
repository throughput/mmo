﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
public class MapMaker {
    public enum DataParam{
        WALL = 0,
        FLOOR = 1,
        START = 2,
        GOAL =3,
        TREASURE_RANK1 =4,
        TREASURE_RANK2 =5,
        TREASURE_RANK3 =6,
        MONSTER_SET =7,
        WAY = 8,
    }
    List<List<DataParam>> mapData;
    public string mapStr = "";
    public int mapSizeX = 0;
    public int mapSizeY = 1;
    int randSeed;
    Random rnd;
    bool setStartFlag = false;
    bool setGoalFlag = false;

    // ダンジョン情報リスト
    List<Rect> rect_list = new List<Rect>();
    List<Room> room_list = new List<Room>();
    List<Couple> couple_list = new List<Couple> ();

    // ダンジョンルーム
    class Room {
        public int lx, ly, hx, hy;
        public bool startFlag;
        public bool goalFlag;
    };
    // ダンジョン区画
    class Rect {
        public Boolean done_split_v = false;
        public Boolean done_split_h = false;
        public int lx, ly, hx, hy;
        public Room room;
    };
    // 通路
    class Couple {
        public int v_or_h;
        public Rect rect0, rect1; 
    };

    // サイズランダムでダンジョン作成
    public void makeRandomSizeDungeon(int maxMapSizeX,int mapMapSizeY){
        mapSizeX = rnd.Next (20, maxMapSizeX);
        mapSizeY = rnd.Next (20, mapMapSizeY);
        makeDungeon (mapSizeX, mapSizeY);
    }
    // 初期化
    private void init(){
        mapStr = "";
        rect_list.Clear();
        room_list.Clear();
        couple_list.Clear();
        setStartFlag = false;
        setGoalFlag = false;
    }
    public MapMaker(){
        // 乱数初期化
        randSeed = (int)DateTime.Now.Ticks;
        rnd = new Random(randSeed);
    }
    // ダンジョン作成
    public void makeDungeon(int sizeX, int sizeY){
        // 初期化
        init ();

        makeDungeonBase (sizeX, sizeY);
        rect_split(rect_add(0, 0, sizeX - 1, sizeY - 1));
        room_make ();
        couple_more (sizeX,sizeY);
        int i,j;
        // 区切り部分をtrueに（確認用)
//                foreach (Rect rect in rect_list) {
//            //                    break;
//                for (i = rect.lx, j = rect.ly; i <= rect.hx; i++) mapData[j][i] = DataParam.FLOOR;
//                for (i = rect.lx, j = rect.hy; i <= rect.hx; i++) mapData[j][i] = DataParam.FLOOR;
//                for (i = rect.lx, j = rect.ly; j <= rect.hy; j++) mapData[j][i] = DataParam.FLOOR;
//                for (i = rect.hx, j = rect.ly; j <= rect.hy; j++) mapData[j][i] = DataParam.FLOOR;
//                }
        // 部屋部分をtrueに
        while (!(setStartFlag && setGoalFlag)) {
            for (int room_count =0; room_count < room_list.Count; ++room_count) {
                Room room = room_list [room_count];
                bool localSetStartFlag = false;
                // ランダムで配置場所決めておく
                i = rnd.Next (room.lx, room.hx);
                j = rnd.Next (room.ly, room.hy);
                // 10%の確率でスタートに
                if (!setStartFlag && rnd.Next (0, 100) < 10) {
                    // ゴール位置にスタートを作らない
                    if (room_list [room_count].goalFlag) {
                        break;
                    }
                    if (mapData [j] [i] == DataParam.WALL) {
                        mapData [j] [i] = DataParam.START;
                        setStartFlag = true;
                        room_list [room_count].startFlag = true;
                    }
                    // 10%の確率でゴールに
                } else if (!setGoalFlag && rnd.Next (0, 100) < 10) {
                    // スタート位置にゴールを作らない
                    if (room_list [room_count].startFlag) {
                        break;
                    }
                    if (mapData [j] [i] == DataParam.WALL) {
                        mapData [j] [i] = DataParam.GOAL;
                        setGoalFlag = true;
                        room_list [room_count].goalFlag = true;
                    }
                }
            }
        }
        // 宝箱セット
        foreach (Room room in room_list) {
            i = rnd.Next (room.lx, room.hx);
            j = rnd.Next (room.ly, room.hy);
            if (mapData [j] [i] == DataParam.WALL) {
                if (rnd.Next (0, 100) < 10) {
                    mapData [j] [i] = DataParam.TREASURE_RANK1;
                } else if (rnd.Next (0, 100) < 10) {
                    mapData [j] [i] = DataParam.TREASURE_RANK2;
                } else if (rnd.Next (0, 100) < 10) {
                    mapData [j] [i] = DataParam.TREASURE_RANK3;
                }
            }
        }
        // モンスターセット
        foreach (Room room in room_list) {
            i = rnd.Next (room.lx, room.hx);
            j = rnd.Next (room.ly, room.hy);
            if (mapData [j] [i] == DataParam.WALL) {
                if (rnd.Next (0, 100) < 50) {
                    mapData [j] [i] = DataParam.MONSTER_SET;
                }
            }
        }
        foreach (Room room in room_list) {
            for (i = room.lx; i <= room.hx; i++) {
                for (j = room.ly; j <= room.hy; j++) {
                    // スタートやゴールが配置済みなら配置しない
                    if (mapData [j] [i] == DataParam.WALL) {
                        mapData [j] [i] = DataParam.FLOOR;
                    }
                }
            }
        }
        // 道の作成
        int c0x, c0y, c1x, c1y;
        for (int coupleNo = 0; coupleNo<couple_list.Count; ++coupleNo) {
            Couple couple = couple_list [coupleNo];
            switch (couple.v_or_h) {
            case COUPLE_HORIZONAL:
                Debug.Assert(couple.rect0.hx == couple.rect1.lx);
                c0x = couple.rect0.hx;
				c0y = couple.rect0.room.ly;
                c0y = rnd.Next(couple.rect0.room.ly + 1, couple.rect0.room.hy);
                c1x = couple.rect1.lx;
				c1y = couple.rect1.room.hy;
				c1y = rnd.Next(couple.rect1.room.ly + 1, couple.rect1.room.hy);
                line(c0x, c0y, c1x, c1y,sizeX,sizeY);
                line(couple.rect0.room.hx, c0y, c0x, c0y,sizeX,sizeY);
                line(couple.rect1.room.lx, c1y, c1x, c1y,sizeX,sizeY);
                break;
            case COUPLE_VERTICAL:
                Debug.Assert(couple.rect0.hy == couple.rect1.ly);
				c0x = couple.rect0.room.hx;
                c0x = rnd.Next(couple.rect0.room.lx + 1, couple.rect0.room.hx);
                c0y = couple.rect0.hy;
				c1x = couple.rect1.room.hx;
                c1x = rnd.Next(couple.rect1.room.lx + 1, couple.rect1.room.hx);
                c1y = couple.rect1.ly;
                line(c0x, c0y, c1x, c1y,sizeX,sizeY);
                line(c0x, couple.rect0.room.hy, c0x, c0y,sizeX,sizeY);
                line(c1x, couple.rect1.room.ly, c1x, c1y,sizeX,sizeY);
                break;
            };
        };
    }

    // ダンジョンのベース作成
    public void makeDungeonBase(int sizeX, int sizeY){
        mapData = new List<List<DataParam>>();
        for (int j = 0; j < sizeY; j++) {
            List<DataParam> mapLine = new List<DataParam>();
            for (int i = 0; i < sizeX; i++) {
                mapLine.Add (DataParam.WALL);
            };
            mapData.Add (mapLine);
        };
    }
    // ダンジョンテキスト表示
    public void printDungeon(){
        foreach (List<DataParam>mapLine in mapData) { 
            foreach (int mapInt in mapLine) {
                mapStr += mapInt.ToString();
            };
            mapStr += "\r";
        };
    }

    // ダンジョン条件指定
    const int MINIMUM_ROOM_SIZE = 4;
    const int MARGIN_BETWEEN_RECT_ROOM = 2;
    const int MINIMUM_RECT_SIZE = MINIMUM_ROOM_SIZE + (MARGIN_BETWEEN_RECT_ROOM * 2);
    const int COUPLE_VERTICAL = 0;
    const int COUPLE_HORIZONAL = 1;


    // ダンジョン区画作成
    Rect rect_add(int lx, int ly, int hx, int hy)
    {
        Rect rect;
        rect = new Rect();
        rect.lx = lx;
        rect.ly = ly;
        rect.hx = hx;
        rect.hy = hy;
        rect_list.Add(rect);
        return(rect);
    }
    Couple couple_add(int v_or_h, Rect rect0, Rect rect1)
    {
        Couple couple;
        couple = new Couple();
        couple.v_or_h = v_or_h;
        couple.rect0 = rect0;
        couple.rect1 = rect1;
        couple_list.Add(couple);
        return(couple);
    }

    void rect_split(Rect rect_parent)
    {
        Rect rect_child;
        if (rect_parent.hy - rect_parent.ly <= MINIMUM_RECT_SIZE * 2) {
            rect_parent.done_split_v = true;
        };
        if (rect_parent.hx - rect_parent.lx <= MINIMUM_RECT_SIZE * 2) {
            rect_parent.done_split_h = true;
        };
        if ((rect_parent.done_split_v) &&
            (rect_parent.done_split_h)) {
            return;
        };


//        if ((rect_parent.hy - rect_parent.ly <= MINIMUM_RECT_SIZE * 2) ||
//            (rect_parent.hx - rect_parent.lx <= MINIMUM_RECT_SIZE * 2)) {
//            return;
//        };
        rect_child = rect_add(rect_parent.lx, rect_parent.ly,
                              rect_parent.hx, rect_parent.hy);
        if (rect_parent.done_split_v == false && rnd.Next(0,100)<50) {
            int split_coord_y;
            split_coord_y = rnd.Next (rect_parent.ly + MINIMUM_RECT_SIZE, rect_parent.hy - MINIMUM_RECT_SIZE);
            rect_parent.hy = split_coord_y;
            rect_child.ly = split_coord_y;
//            rect_parent.done_split_v = true;
//            rect_child.done_split_v = true;
            couple_add (COUPLE_VERTICAL, rect_parent, rect_child);
            rect_split (rect_parent);
            rect_split (rect_child);
            return;
        }
        if(rect_parent.done_split_h == false){
            int split_coord_x;
            split_coord_x = rnd.Next(rect_parent.lx + MINIMUM_RECT_SIZE, rect_parent.hx - MINIMUM_RECT_SIZE);
            rect_parent.hx = split_coord_x;
            rect_child.lx = split_coord_x;
//            rect_parent.done_split_h = true;
//            rect_child.done_split_h = true;
            couple_add(COUPLE_HORIZONAL, rect_parent, rect_child);
            rect_split(rect_parent);
            rect_split(rect_child);
            return;
        };
    }

    // ダンジョン部屋作成
    Room room_add(int lx, int ly, int hx, int hy)
    {
        Room room;
        room = new Room();
        room.lx = lx;
        room.ly = ly;
        room.hx = hx;
        room.hy = hy;
        room_list.Add(room);
        return(room);
    }
    void room_make()
    {
        int x, y, w, h;
        for(int rectNo = 0; rectNo<rect_list.Count; ++rectNo) {
            Rect rect = rect_list [rectNo];
//            w = rnd.Next(MINIMUM_ROOM_SIZE, rect.hx - rect.lx - (MARGIN_BETWEEN_RECT_ROOM * 2) + 1);
//            h = rnd.Next(MINIMUM_ROOM_SIZE, rect.hy - rect.ly - (MARGIN_BETWEEN_RECT_ROOM * 2) + 1);
//            x = rnd.Next(rect.lx + MARGIN_BETWEEN_RECT_ROOM, rect.hx - MARGIN_BETWEEN_RECT_ROOM - w + 1);
//            y = rnd.Next(rect.ly + MARGIN_BETWEEN_RECT_ROOM, rect.hy - MARGIN_BETWEEN_RECT_ROOM - h + 1);
            w = rect.hx - rect.lx - (MARGIN_BETWEEN_RECT_ROOM * 2) + 1;
            h = rect.hy - rect.ly - (MARGIN_BETWEEN_RECT_ROOM * 2) + 1;
            x = rect.hx - MARGIN_BETWEEN_RECT_ROOM - w + 1;
            y = rect.hy - MARGIN_BETWEEN_RECT_ROOM - h + 1;
            rect_list [rectNo].room = room_add(x, y, x + w, y + h);
        };
    }

    void line(int x0, int y0, int x1, int y1,int sizeX,int sizeY)
    {
        int min_x, max_x, min_y, max_y, i, j;
        min_x = Math.Min(x0, x1);
        max_x = Math.Max(x0, x1);
        min_y = Math.Min(y0, y1);
        max_y = Math.Max(y0, y1);
        Debug.Assert((min_x >= 0) && (max_x < sizeX) && (min_y >= 0) && (max_y < sizeY));
        if ((x0 <= x1) && (y0 >= y1)) {
            for (i = min_x; i <= max_x; i++) set_line_floor(max_y,i);
            for (j = min_y; j <= max_y; j++) set_line_floor(j,max_x);
            return;
        };
        if ((x0 > x1) && (y0 > y1)) {
            for (i = min_x; i <= max_x; i++) set_line_floor(min_y,i);
            for (j = min_y; j <= max_y; j++) set_line_floor(j,max_x);
            return;
        };
        if ((x0 > x1) && (y0 <= y1)) {
            for (i = min_x; i <= max_x; i++) set_line_floor(min_y,i);
            for (j = min_y; j <= max_y; j++) set_line_floor(j,min_x);
            return;
        };
        if ((x0 <= x1) && (y0 < y1)) {
            for (i = min_x; i <= max_x; i++) set_line_floor(max_y,i);
            for (j = min_y; j <= max_y; j++) set_line_floor(j,min_x);
            return;
        };
    }
    void set_line_floor(int j,int i){
        if (mapData [j] [i] == DataParam.WALL) {
			mapData[j][i] = DataParam.FLOOR;
//            mapData[j][i] = DataParam.WAY;
        }
    }
    void couple_more(int sizeX,int sizeY)
    {
        List<List<Rect>> rectmap = new List<List<Rect>>();
        int i=0, j=0;
        // 枠を作る
        for (j = 0; j < sizeX; j++) {
            List<Rect> rectLine = new List<Rect> ();
            for (i = 0; i < sizeY; i++) {
                rectLine.Add (new Rect());
            }
            rectmap.Add (rectLine);
        }

        // データを作る
        foreach (Rect rect in rect_list) {
            for (j = rect.lx; j < rect.hx; j++) {
                List<Rect> rectLine = rectmap[j];
                for (i = rect.ly; i < rect.hy; i++) {
                    rectLine[i] = rect;
                }
                rectmap[j] = rectLine ;
            }
        }
        for (i = 0; i < sizeY - 2; i++) {
            for (j = 0; j < sizeX - 2; j++) {
                Debug.Assert(rectmap[j][i] != null);
                Debug.Assert(rectmap[j + 1][i] != null);
                Debug.Assert(rectmap[j][i + 1] != null);
                if (rectmap[j][i] != rectmap[j][i + 1]) {
                    if (rnd.Next(0, 64) == 0) {
                        couple_add(COUPLE_VERTICAL, rectmap[j][i], rectmap[j][i + 1]);
                    }
                }
                if (rectmap[j][i] != rectmap[j + 1][i]) {
                    if (rnd.Next(0, 64) == 0) {
                        couple_add(COUPLE_HORIZONAL, rectmap[j][i], rectmap[j + 1][i]);
                    }
                }
            }
        }
    }
}
