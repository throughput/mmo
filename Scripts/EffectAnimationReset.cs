﻿using UnityEngine;
using System.Collections;

public class EffectAnimationReset : StateMachineBehaviour{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.speed = 1f;
    }

}