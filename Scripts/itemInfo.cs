﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	namespace ThpMmo{
        public class ItemInfo{
			public int id = 0;
			public int rank = 0;
			public int count = 0;
            public int slotCount = 0;
            public int itemBoxNo = 0;
            public Dictionary<int,int> slot = new Dictionary<int,int>(ItemController.MAX_SLOT_COUNT);
		}
	}
}