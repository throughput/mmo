﻿using UnityEngine;
using System.Collections;

public class UVScroll : MonoBehaviour {

	public float uvSpeedX;
	public float uvSpeedY;
	public int frame;
	int i;

	void Update () {
		if(frame >= i){
			i++;
		}
		else{
			float deltaTime = Time.deltaTime;
			GetComponent<Renderer>().material.mainTextureOffset += new Vector2(uvSpeedX * deltaTime,uvSpeedY * deltaTime);
			i = 0;
		}
	}

}
