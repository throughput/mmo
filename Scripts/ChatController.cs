﻿using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// Very simple example of how to use a TextList with a UIInput for chat.
/// </summary>
namespace Throughputjp{
	namespace ThpMmo{
        public enum ChatMode{
            CHAT_MODE_AREA,
            CHAT_MODE_TARGET,
            CHAT_MODE_ALL,
        }
		[RequireComponent(typeof(UIInput))]
//		[AddComponentMenu("NGUI/Examples/Chat Input")]
		public class ChatController : ThpmmoMonoBehavier
		{
            public ChatMode chatMode;
			public UITextList textList;
			public bool fillWithDummyData = false;
            public int whisperTargetID;
            UIInput mInput;
			/// <summary>
			/// Add some dummy text to the text list.
			/// </summary>
			
			void Start ()
			{
                chatMode = ChatMode.CHAT_MODE_AREA;
				mInput = GetComponent<UIInput>();
				mInput.label.maxLineCount = 1;
				if (fillWithDummyData && textList != null)
				{
					for (int i = 0; i < 30; ++i)
					{
						textList.Add(((i % 2 == 0) ? "[FFFFFF]" : "[AAAAAA]") +
						             "This is an example paragraph for the text list, testing line " + i + "[-]");
					}
				}
			}
			
			/// <summary>
			/// Submit notification is sent by UIInput when 'enter' is pressed or iOS/Android keyboard finalizes input.
			/// </summary>
			
			public void OnSubmit ()
			{
				if (textList != null)
				{
					// It's a good idea to strip out all symbols as we don't want user input to alter colors, add new lines, etc
					string text = NGUIText.StripSymbols(mInput.value);
					if (!string.IsNullOrEmpty(text))
					{
                        switch(chatMode){
                        case ChatMode.CHAT_MODE_TARGET:
                            SocketNetwork.Instance.sendWhisperChatData (PlayerInfo.Instance.characterInfo, text, whisperTargetID);
                            break;
                        case ChatMode.CHAT_MODE_AREA:
                            SocketNetwork.Instance.sendChatData (PlayerInfo.Instance.characterInfo, text);
                            break;
                        case ChatMode.CHAT_MODE_ALL:
                            SocketNetwork.Instance.sendChatData (PlayerInfo.Instance.characterInfo, text,true);
                            break;
                        }
						mInput.value = "";
						mInput.isSelected = false;
					}
				}
			}
			void Update(){
				if (SocketNetwork.Instance.chatInfoArray != null) {
					if (SocketNetwork.Instance.chatInfoArray.Count > 0) {
						Debug.Log ("chat");
                        string chatColor = "[ffffff]";
						foreach (ChatInfo netWorkChatInfo in SocketNetwork.Instance.chatInfoArray) {
                            switch(netWorkChatInfo.chatMode){
                            case ChatMode.CHAT_MODE_TARGET:
                                chatColor = "[ffed00]";
                                break;
                            case ChatMode.CHAT_MODE_AREA:
                                chatColor = "[ffffff]";
                                break;
                            case ChatMode.CHAT_MODE_ALL:
                                chatColor = "[83ffff]";
                                break;
                            }
                            textList.Add (chatColor+netWorkChatInfo.chatText+"[ffffff]");
						}
						SocketNetwork.Instance.chatInfoArray.Clear();
					}
				}
			}
		}
	}
}