﻿using UnityEngine;
using System.Collections;

enum BgmList{
    TOWN,
    FIRST_DUNGEON,
    CAVE_01_10,
    CAVE_11_20,
    CAVE_21_30,
    CAVE_31_40,
    CAVE_41_50,
    LIMITS_DUNGEON,
    FREE_FIELD,
    GACHA_ROOM,
    BGM_NUM
};
enum SeList{
    GACHA_WIN,
    USE_MONEY,
    OPEN_TRESURE,
    DUNGEON_CLEAR,
    LEVEL_UP,
    GAME_OVER,
    DECIDE,
    CANCEL,
    ATTACK_HATCHET,
    ATTACK_SWORD,
    ATTACK_KNIFE,
    ATTACK_AX,
    ATTACK_SPIER,
    CRITICAL_ATTACK,
    MAGIC_FIRE_1,
    MAGIC_FIRE_2,
    MAGIC_FIRE_3,
    MAGIC_FIRE_4,
    MAGIC_FIRE_5,
    MAGIC_FIRE_6,
    MAGIC_FIRE_7,
    MAGIC_FIRE_8,
    MAGIC_WIND_1,
    MAGIC_WIND_2,
    MAGIC_WIND_3,
    MAGIC_WIND_4,
    MAGIC_WIND_5,
    MAGIC_WIND_6,
    MAGIC_WIND_7,
    MAGIC_WIND_8,
    MAGIC_WATER_1,
    MAGIC_WATER_2,
    MAGIC_WATER_3,
    MAGIC_WATER_4,
    MAGIC_WATER_5,
    MAGIC_WATER_6,
    MAGIC_WATER_7,
    MAGIC_WATER_8,
    MAGIC_ICE_1,
    MAGIC_ICE_2,
    MAGIC_ICE_3,
    MAGIC_ICE_4,
    MAGIC_ICE_5,
    MAGIC_ICE_6,
    MAGIC_ICE_7,
    MAGIC_ICE_8,
    MAGIC_EARTH_1,
    MAGIC_EARTH_2,
    MAGIC_EARTH_3,
    MAGIC_EARTH_4,
    MAGIC_EARTH_5,
    MAGIC_EARTH_6,
    MAGIC_EARTH_7,
    MAGIC_EARTH_8,
    MAGIC_THUNDER_1,
    MAGIC_THUNDER_2,
    MAGIC_THUNDER_3,
    MAGIC_THUNDER_4,
    MAGIC_THUNDER_5,
    MAGIC_THUNDER_6,
    MAGIC_THUNDER_7,
    MAGIC_THUNDER_8,
    MAGIC_LIGHT_1,
    MAGIC_LIGHT_2,
    MAGIC_LIGHT_3,
    MAGIC_LIGHT_4,
    MAGIC_LIGHT_5,
    MAGIC_LIGHT_6,
    MAGIC_LIGHT_7,
    MAGIC_LIGHT_8,
    MAGIC_DARK_1,
    MAGIC_DARK_2,
    MAGIC_DARK_3,
    MAGIC_DARK_4,
    MAGIC_DARK_5,
    MAGIC_DARK_6,
    MAGIC_DARK_7,
    MAGIC_DARK_8,
    SE_NUM
};
namespace Throughputjp{
    namespace ThpMmo{
        public class SoundList : ThpmmoMonoBehavier{
            void Awake(){
                const string soundFolder = "sound/";
                const string bgmFolder = soundFolder+"bgm/";
                const string seFolder = soundFolder+"se/";
                string soundListStr = ((TextAsset)Resources.Load ("soundList")).text;
                Hashtable soundList = (Hashtable)JsonController.JsonDecode (soundListStr);
                ArrayList bgmList = (ArrayList)soundList ["bgmlist"];
                ArrayList seList = (ArrayList)soundList ["selist"];
                SoundManager.Instance.BGM = new AudioClip[(int)BgmList.BGM_NUM];
                SoundManager.Instance.SE = new AudioClip[(int)SeList.SE_NUM];
                for(int i = 0;i < (int)BgmList.BGM_NUM; ++i){
                    Debug.Log (bgmFolder + (string)bgmList [i]);
                    if ((string)bgmList [i] != string.Empty) {
                        SoundManager.Instance.BGM [i] = (AudioClip)Resources.Load (bgmFolder + (string)bgmList [i]);
                    }
                }
                for(int i = 0;i < (int)SeList.SE_NUM;++i){
                    if ((string)seList [i] != string.Empty) {
                        SoundManager.Instance.SE [i] = (AudioClip)Resources.Load (seFolder + (string)seList [i]);
                    }
                }

            }
        }
    }
}