﻿// #define LOCAL_MAP_MAKE

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Procurios.Public;
using System.IO;
using System;
namespace Throughputjp{
    namespace ThpMmo{
        public enum GrowupResult{
            GROW_UP_ULTRA_SUCCESS = 3,
            GROW_UP_BIG_SUCCESS = 2,
            GROW_UP_SUCCESS = 1,
            GROW_UP_FAIL = 0,
            GROW_UP_ERROR = -1
        }
        public enum GrowupCritical{
            GROW_UP_CRITICAL_BLUE = 0,
            GROW_UP_CRITICAL_YELLOW = 1,
            GROW_UP_CRITICAL_GREEN = 2,
            GROW_UP_CRITICAL_RED = 3,
            GROW_UP_CRITICAL_GOLD = 4
        }
        public enum MailError{
            SUCCESS = 0,
            SEND_ERROR = -1,
            MAIL_COUNT_ERROR = 2,
            MAIL_LIST_ERROR = 3,
        }
        public enum FriendError{
            SUCCESS = 0,
            LIST_ERROR = -1,
            ERROR = 2,
            REGIST_ERROR = 3,
        }
        public enum EquipError{
            SUCCESS = 0,
            FAIL = 1,
            ERROR = -1,
            NOT_EQUIP_ERROR = 2,
        }
        public enum SetSlotResult{
            SUCCESS = 0,
            ERROR = -1,
            FAIL = 1,
        }
        public enum GachaResult{
            SUCCESS = 0,
            ERROR = -1,
            NOT_ENOUGH = 1,
        }
        public enum ChangeItemResult{
            SUCCESS = 0,
            ERROR = -1,
            FAIL = 1,
        }
        public class HttpController : SingletonMonoBehaviour<HttpController> {
            private const string hostDomain="dwsrv.com/mmo";   //Main
            public string dungeonData = "";
            private bool setDungeonData;
            private bool isNetWorkError = false;
            private const float NETWORK_TIMEOUT_COUNT=5.0f;
            private bool sessionTimeOutFlag=false;
            private bool versionCheck = true;
            private const string userIdFilename="userid.dat";
            public int gachaRank;
            private enum MailCommand
            {
                GET_MAIL_COUNT = 1,
                SEND_MAIL = 2,
                RECV_MAIL = 3,
                MAIL_OPEN = 4,
            };
            private enum FriendCommand
            {
                REGIST = 0,
                DELETE = 1,
                LIST = 2,
            };

            public Stack<EquipInfo> treasureResult = new Stack<EquipInfo>();
            public Queue<ItemInfo> myItemInfo = new Queue<ItemInfo>();
            public Queue<MailFormat> mailBox = new Queue<MailFormat> ();
            public Queue<CharacterInfo> friendList = new Queue<CharacterInfo>();
            public string userName = "";
            public int swordID = 0;
            public int armorID = 0;
            public int shieldID = 0;
            public int growupPoint = 0;
            public int ticket = 0;
            public int medal = 0;
            public string friendCode;
            public int chargeGachaIncentiveExpensive=0;
            public int chargeGachaIncentive=0;
            public int freeGachaIncentive=0;
            public int firstChargeGachaIncentiveExpensive=0;
            public int firstChargeGachaIncentive=0;
            public int firstFreeGachaIncentive=0;
            public List<int> magicalSlot = new List<int> (BattleMenuController.SLOT_NUM);
            public bool IsSessionTimeOut{
                get{return sessionTimeOutFlag;}
                set{sessionTimeOutFlag=value;}
            }
            public bool IsNetworkError{
                get{return isNetWorkError;}
            }
            public int mailCount = 0;
            void Awake(){
                for (int i = 0; i < BattleMenuController.SLOT_NUM; ++i) {
                    magicalSlot.Add (0);
                }
            }
            /// <summary>
            /// ダンジョン取得
            /// </summary>
            /// <returns>The load dungeon.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="floorNo">Floor no.</param>
            public IEnumerator DownLoadDungeon (Action callback,int floorNo) {    
                setDungeonData = false;

                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"f",floorNo.ToString()},
                };
#if LOCAL_MAP_MAKE
                DungeonMaker.Instance.localMapMake ();
                dungeonData = DungeonMaker.Instance.mapMaker.mapStr;
                yield return true;
#else
                IEnumerator wwwReturn=LoginAndAccess("/makedungeon.aspx?d="+System.DateTime.Now.Ticks.ToString(),formParams,callback);
                yield return StartCoroutine(wwwReturn);
                try{
                    dungeonData=(String)wwwReturn.Current;
                }catch{
                    yield break;
                }
                yield return true;
#endif
                if(callback!=null) callback();
            }
            /// <summary>
            /// メール数取得
            /// </summary>
            /// <returns>The mail count.</returns>
            /// <param name="callback">Callback.</param>
            public IEnumerator GetMailCount (Action<MailError> callback) {
                IEnumerator wwwReturn = _RunMailCommand (MailCommand.GET_MAIL_COUNT,null);
                yield return StartCoroutine (wwwReturn);
                MailError mailError = MailError.SUCCESS;
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    mailCount = int.Parse((string)result["r"]);
                    if(mailCount == -1){
                        mailError = MailError.MAIL_COUNT_ERROR;
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(mailError);
            }
            /// <summary>
            /// メール送信
            /// </summary>
            /// <returns>The mail.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="friendCode">Friend code.</param>
            /// <param name="subject">Subject.</param>
            /// <param name="body">Body.</param>
            /// <param name="itemBoxNo">Item box no.</param>
            public IEnumerator SendMail (Action<MailError> callback,string friendCode,string subject,string body,int itemBoxNo = 0) {
                IEnumerator wwwReturn = _RunMailCommand (MailCommand.SEND_MAIL,null,itemBoxNo,-1,friendCode,subject,body);
                yield return StartCoroutine (wwwReturn);
                MailError mailError = MailError.SUCCESS;
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    int resultInt = int.Parse((string)result["r"]);
                    if(resultInt == -1){
                        mailError = MailError.SEND_ERROR;
                        yield break;
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(mailError);
            }
            /// <summary>
            /// メール受信
            /// </summary>
            /// <returns>The mail.</returns>
            /// <param name="callback">Callback.</param>
            public IEnumerator RecvMail(Action<MailError> callback){
                IEnumerator wwwReturn = _RunMailCommand (MailCommand.RECV_MAIL,null);
                yield return StartCoroutine (wwwReturn);
                MailError mailError = MailError.SUCCESS;
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    int resultInt = int.Parse((string)result["r"]);
                    if(resultInt == -1){
                        mailError = MailError.SEND_ERROR;
                        yield break;
                    }
                    if (result.ContainsKey ("mail")) {
                        foreach (string key in ((Hashtable)result["mail"]).Keys) {
                            Hashtable mailFormatHash =(Hashtable) ((Hashtable)result ["mail"]) [key];
                            MailFormat mailFormat = new MailFormat ();
                            mailFormat.mailNo = int.Parse(key);
                            mailFormat.friendCode = (string)mailFormatHash ["fc"];
                            mailFormat.from = (string)mailFormatHash ["subject"];
                            mailFormat.body = WWW.UnEscapeURL((string)mailFormatHash ["body"]);
                            mailFormat.itemInfo = getItemInfo(mailFormatHash,"0");
                            mailBox.Enqueue (mailFormat);
                        }
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(mailError);
            }
            /// <summary>
            /// アイテム装備変更
            /// </summary>
            /// <returns>The equip.</returns>
            /// <param name="itemBoxNo">Item box no.</param>
            /// <param name="equipKind">Equip kind.</param>
            public IEnumerator changeEquip(Action<EquipError,int> callback,int itemBoxNo){
                ItemInfo targetItemInfo = null;
                foreach(ItemInfo itemInfo in PlayerInfo.Instance.myItemList.Values){
                    if (itemBoxNo == itemInfo.itemBoxNo) {
                        targetItemInfo = itemInfo;
                    }
                }
                ItemTableInfo itemTableInfo = MainController.Instance.itemTableDic[targetItemInfo.id.ToString()];
                EquipKind equipKind = itemTableInfo.kind;

                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"ic",itemBoxNo.ToString()}, // アイテムボックス番号
                    {"key", ((int)equipKind).ToString()},                            // 装備
                };
                EquipError equipError = EquipError.SUCCESS;
                if (equipKind >= EquipKind.EQUIP_MAX) {
                    if(callback!=null) callback(EquipError.NOT_EQUIP_ERROR,itemBoxNo);
                    yield break;
                }
                IEnumerator wwwReturn=LoginAndAccess("/changesoubi.aspx?d="+System.DateTime.Now.Ticks.ToString(),formParams,null);
                yield return StartCoroutine (wwwReturn);
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    int resultInt = int.Parse((string)result["r"]);
                    if(resultInt == -1){
                        equipError = EquipError.ERROR;
                        yield break;
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(equipError,itemBoxNo);
            }

            /// <summary>
            /// 汎用アイテム情報取得
            /// </summary>
            /// <returns>The item info.</returns>
            /// <param name="itemInfoHash">Item info hash.</param>
            /// <param name="key">Key.</param>
            private ItemInfo getItemInfo(Hashtable itemInfoHash,string key){
                ItemInfo itemInfo = new ItemInfo ();
                itemInfo.id = int.Parse ((string)itemInfoHash ["i"]);
                itemInfo.count = int.Parse ((string)itemInfoHash ["ic"]);
                itemInfo.rank = int.Parse ((string)itemInfoHash ["ir"]);
                itemInfo.slotCount = int.Parse ((string)itemInfoHash ["isc"]);
                itemInfo.itemBoxNo = int.Parse(key);
                if (itemInfoHash.ContainsKey ("is")) {
                    Hashtable itemSlot = (Hashtable)itemInfoHash ["is"];
                    foreach (string hashkey in itemSlot.Keys) {
                        string slotStr = (string)itemSlot [hashkey];
                        int listNo = int.Parse (hashkey);
                        itemInfo.slot[listNo]=int.Parse(slotStr);
                    }
                }

                return itemInfo;
            }
            /// <summary>
            /// メール既読セット
            /// </summary>
            /// <returns>The mail.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="mailNo">Mail no.</param>
            /// <param name="itemBoxNo">Item box no.</param>
            public IEnumerator OpenMail (Action<MailError> callback,int mailNo,int itemBoxNo = 0) {
                IEnumerator wwwReturn = _RunMailCommand (MailCommand.MAIL_OPEN,null,itemBoxNo, mailNo);
                yield return StartCoroutine (wwwReturn);
                MailError mailError = MailError.SUCCESS;
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    int returnResult = int.Parse((string)result["r"]);
                    if(returnResult == -1){
                        mailError = MailError.MAIL_COUNT_ERROR;
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(mailError);
            }
            /// <summary>
            /// フレンド登録
            /// </summary>
            /// <returns>The friend.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="friendCode">Friend code.</param>
            public IEnumerator RegistFriend(Action<FriendError> callback,string friendCode){
                IEnumerator wwwReturn = _RunFriendCommand (FriendCommand.REGIST,null,friendCode);
                yield return StartCoroutine (wwwReturn);
                FriendError friendError = FriendError.SUCCESS;
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    int errorResult = int.Parse((string)result["r"]);
                    if(errorResult == 1){
                        friendError = FriendError.ERROR;
                    }
                    if(errorResult == 2){
                        friendError = FriendError.REGIST_ERROR;
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(friendError);
            }
            /// <summary>
            /// フレンド削除
            /// </summary>
            /// <returns>The friend.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="friendCode">Friend code.</param>
            public IEnumerator DeleteFriend(Action<FriendError> callback,string friendCode){
                Debug.Log (friendCode);
                IEnumerator wwwReturn = _RunFriendCommand (FriendCommand.DELETE,null,friendCode);
                yield return StartCoroutine (wwwReturn);
                FriendError friendError = FriendError.SUCCESS;
                try{
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    int errorResult = int.Parse((string)result["r"]);
                    if(errorResult == 1){
                        friendError = FriendError.ERROR;
                    }
                    if(errorResult == 2){
                        friendError = FriendError.REGIST_ERROR;
                    }
                }catch{
                    yield break;
                }
                yield return true;
                if(callback!=null) callback(friendError);
            }
            /// <summary>
            /// フレンド一覧取得
            /// </summary>
            /// <returns>The friend list.</returns>
            /// <param name="callback">Callback.</param>
            public IEnumerator GetFriendList(Action callback){
                IEnumerator wwwReturn = _RunFriendCommand (FriendCommand.LIST,callback);
                yield return StartCoroutine (wwwReturn);
                try{
                }catch{
                    yield break;
                }
                Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                FriendError friendError = FriendError.SUCCESS;
                if (result.ContainsKey ("friend")) {
                    foreach (Hashtable data in ((ArrayList)result["friend"])) {
                        Hashtable friendFormatHash =(Hashtable) ((Hashtable)data);
                        CharacterInfo charaInfo = new CharacterInfo ();
                        charaInfo.name = (string)friendFormatHash ["n"];
                        charaInfo.friendCode = (string)friendFormatHash ["cd"];
                        friendList.Enqueue (charaInfo);
                    }
                }
                yield return true;
                if(callback!=null) callback();
            }
            /// <summary>
            /// 共通メール命令
            /// </summary>
            /// <returns>The run mail command.</returns>
            /// <param name="eMailCommnad">E mail commnad.</param>
            /// <param name="callback">Callback.</param>
            /// <param name="itemBoxNo">Item box no.</param>
            /// <param name="mailNo">Mail no.</param>
            /// <param name="friendCode">Friend code.</param>
            /// <param name="subject">Subject.</param>
            /// <param name="body">Body.</param>
            private IEnumerator _RunMailCommand(MailCommand eMailCommnad,Action callback,int itemBoxNo = 0,int mailNo = -1,string friendCode = "", string subject = "",string body = ""){
                string itemBoxNoStr = "";
                string mailNoStr = "";
                if (itemBoxNo > 0) {
                    itemBoxNoStr = itemBoxNo.ToString ();
                }
                if (mailNo >= 0) {
                    mailNoStr = mailNo.ToString ();
                }
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"com",((int)eMailCommnad).ToString()},
                };
                if(friendCode != String.Empty) formParams.Add ("fc", friendCode);
                if(subject != String.Empty) formParams.Add ("subject", WWW.EscapeURL(subject));
                if(body != String.Empty) formParams.Add ("body", WWW.EscapeURL(body));
                if(itemBoxNoStr != String.Empty) formParams.Add ("in", itemBoxNoStr);
                if(mailNoStr != String.Empty) formParams.Add ("mn", mailNoStr);
                IEnumerator wwwReturn=LoginAndAccess("/mail.aspx?d="+System.DateTime.Now.Ticks.ToString(),formParams,callback);
                return wwwReturn;
            }
            /// <summary>
            /// 共通フレンド命令
            /// </summary>
            /// <returns>The run friend command.</returns>
            /// <param name="eFriendCommnad">E friend commnad.</param>
            /// <param name="callback">Callback.</param>
            /// <param name="friendCode">Friend code.</param>
            private IEnumerator _RunFriendCommand(FriendCommand eFriendCommnad,Action callback,string friendCode = ""){
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"f",((int)eFriendCommnad).ToString()},
                };
                if(friendCode != String.Empty) formParams.Add ("fc", friendCode);
                IEnumerator wwwReturn=LoginAndAccess("/friend.aspx?d="+System.DateTime.Now.Ticks.ToString(),formParams,callback);
                return wwwReturn;
            }

            /// <summary>
            /// ガチャオープン
            /// </summary>
            /// <returns>The gacha.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="gachaKind">Gacha kind.</param>
            /// <param name="itemBoxNo">Item box no.</param>
            public IEnumerator openGacha (Action<GachaController.GachaKind,GachaResult> callback,GachaController.GachaKind gachaKind,int itemBoxNo,bool isTicket) {    
                Debug.Log ("HttpController:openGacha");
                int type = 1;
                switch (gachaKind) {
                case GachaController.GachaKind.GACHA_BRONZE:
                    type = 3;
                    break;
                case GachaController.GachaKind.GACHA_SILVER:
                    type = 2;
                    break;
                case GachaController.GachaKind.GACHA_GOLD:
                    type = 1;
                    break;
                }
                int ticketFlag = -2;
                if (isTicket) {
                    ticketFlag = -1;
                }
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"itemno",itemBoxNo.ToString()}, // アイテムボックス番号
                    {"type", type.ToString()},                            // ガチャタイプ
                    {"mt",ticketFlag.ToString()},                                    // 支払い方法(-1:チケット、-2:ゲームマネー)
                };

                IEnumerator wwwReturn=LoginAndAccess("/gacha.aspx?d="+System.DateTime.Now.Ticks.ToString(),formParams,null);
                yield return StartCoroutine(wwwReturn);
                GachaResult gachaResult = GachaResult.SUCCESS;
                try{
                    Debug.Log ("GetTime:"+DateTime.Now.ToString());
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    gachaResult = (GachaResult)int.Parse((string)result["r"]);
                    if(gachaResult == GachaResult.SUCCESS){
                        EquipInfo equipInfo = new EquipInfo();
                        equipInfo.id = int.Parse((string)result["i"]);
                        equipInfo.socket = int.Parse((string)result["s"]);
                        equipInfo.rank = int.Parse((string)result["t"]);
                        treasureResult.Push(equipInfo);
                        gachaRank = int.Parse((string)result["t"]);
                        // アイテムに追加
                        ItemInfo itemInfo = new ItemInfo();
                        itemInfo.id = equipInfo.id;
                        itemInfo.rank = equipInfo.rank;
                        itemInfo.slotCount = equipInfo.socket;
                        itemInfo.itemBoxNo = itemBoxNo;
                        itemInfo.count = 1; // 基本ガチャからは⒈つゲットなので1に。
                        myItemInfo.Enqueue(itemInfo);
                    }
                }catch{
                    Debug.Log ("Error");
                    yield break;
                }
                yield return true;
                if (callback != null) {
                    callback (gachaKind,gachaResult);
                }
            }
            /// <summary>
            /// アイテム強化
            /// </summary>
            /// <returns>The item.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="itemBoxNo">Item box no.</param>
            /// <param name="growupPoint">Growup point.</param>
            public IEnumerator growupItem (Action<GrowupResult,GrowupCritical> callback, int itemBoxNo, int growupPoint) {    
                Debug.Log ("HttpController:growupItem:"+itemBoxNo.ToString()+":"+growupPoint.ToString());
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"box",itemBoxNo.ToString()},
                    {"gp", growupPoint.ToString()},
                };

                IEnumerator wwwReturn=LoginAndAccess("/rare.aspx?d=" + System.DateTime.Now.Ticks.ToString(), formParams, null);
                yield return StartCoroutine(wwwReturn);
                GrowupResult growupResult = GrowupResult.GROW_UP_ERROR;
                GrowupCritical critical = 0;
                try{
                    Debug.Log ("GetTime:"+DateTime.Now.ToString());
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    growupResult = (GrowupResult)int.Parse((string)result["r"]);
                    critical=(GrowupCritical)int.Parse((string)result["c"]);
                }catch{
                    Debug.Log ("Error");
                    yield break;
                }
                yield return true;
                if (callback != null) {
                    callback (growupResult,critical);
                }
            }
            /// <summary>
            /// 武器スロットセット
            /// </summary>
            /// <returns>The slot.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="weaponitemBoxNo">Weaponitem box no.</param>
            /// <param name="slotNo">Slot no.</param>
            /// <param name="jewelItemBoxId">Jewel item box identifier.</param>
            public IEnumerator setSlot (Action<SetSlotResult> callback, int weaponitemBoxNo, int slotNo,int jewelItemBoxId) {    
                Debug.Log ("HttpController:growupItem:"+weaponitemBoxNo.ToString()+":"+slotNo.ToString()+":"+jewelItemBoxId.ToString());
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"ic",weaponitemBoxNo.ToString()},
                    {"sc", (slotNo+1).ToString()},
                    {"masekino", jewelItemBoxId.ToString()},
                };

                IEnumerator wwwReturn=LoginAndAccess("/masekiup.aspx?d=" + System.DateTime.Now.Ticks.ToString(), formParams, null);
                yield return StartCoroutine(wwwReturn);
                SetSlotResult setSlotResult = SetSlotResult.ERROR;
                try{
                    Debug.Log ("GetTime:"+DateTime.Now.ToString());
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    setSlotResult = (SetSlotResult)int.Parse((string)result["r"]);
                }catch{
                    Debug.Log ("Error");
                    yield break;
                }
                yield return true;
                if (callback != null) {
                    callback (setSlotResult);
                }
            }
            /// <summary>
            /// 魔法スロットセット
            /// </summary>
            /// <returns>The magical.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="magicals">Magicals.</param>
            public IEnumerator setMagical (Action<SetSlotResult> callback, int weaponID, List<int> magicals) {    
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"s1",magicals[0].ToString()},
                    {"s2",magicals[1].ToString()},
                    {"s3",magicals[2].ToString()},
                    {"s4",magicals[3].ToString()},
                    {"s5",magicals[4].ToString()},
                    {"s6",magicals[5].ToString()},
                    {"s7",magicals[6].ToString()},
                    {"s8",magicals[7].ToString()},
                    {"sid",weaponID.ToString()},
                };

                IEnumerator wwwReturn=LoginAndAccess("/maseki.aspx?d=" + System.DateTime.Now.Ticks.ToString(), formParams, null);
                yield return StartCoroutine(wwwReturn);
                SetSlotResult setSlotResult = SetSlotResult.ERROR;
                try{
                    Debug.Log ("GetTime:"+DateTime.Now.ToString());
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    setSlotResult = (SetSlotResult)int.Parse((string)result["r"]);
                }catch{
                    Debug.Log ("Error");
                    yield break;
                }
                yield return true;
                if (callback != null) {
                    callback (setSlotResult);
                }
            }
            /// <summary>
            /// アイテムの使用結果送信
            /// </summary>
            /// <returns>The item change.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="changeItemInfo">Change item info.</param>
            public IEnumerator ResultItemChange(Action<ChangeItemResult> callback, Dictionary<int,ItemInfo> changeItemInfo){
                Hashtable changeItemInfoHash = new Hashtable ();
                foreach(int key in changeItemInfo.Keys){
                    ItemInfo item = changeItemInfo [key];
                    ArrayList itemInfoArray = new ArrayList (){item.id.ToString(),item.count.ToString() };
                    changeItemInfoHash.Add (key, itemInfoArray);
                }
                string changeItemInfoStr = JSON.JsonEncode (changeItemInfoHash);
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"i",changeItemInfoStr},
                };
                IEnumerator wwwReturn=LoginAndAccess("/changeitem.aspx?d=" + System.DateTime.Now.Ticks.ToString(), formParams, null);
                yield return StartCoroutine(wwwReturn);
                ChangeItemResult changeItemResult = ChangeItemResult.ERROR;
                try{
                    Debug.Log ("GetTime:"+DateTime.Now.ToString());
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    changeItemResult = (ChangeItemResult)int.Parse((string)result["r"]);
                }catch{
                    Debug.Log ("Error");
                    yield break;
                }
                yield return true;
                if (callback != null) {
                    callback (changeItemResult);
                }
            }

            /// <summary>
            /// 名前変更
            /// </summary>
            /// <returns>The name.</returns>
            /// <param name="callback">Callback.</param>
            /// <param name="newName">New name.</param>
            public IEnumerator changeName (Action callback, string newName) {    
                Debug.Log ("HttpController:changeName");
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
                    {"n",newName.ToString()},
                };

                IEnumerator wwwReturn=LoginAndAccess("/changename.aspx?d=" + System.DateTime.Now.Ticks.ToString(), formParams, null);
                yield return StartCoroutine(wwwReturn);
                GrowupResult growupResult = GrowupResult.GROW_UP_ERROR;
                try{
                    Debug.Log ("GetTime:"+DateTime.Now.ToString());
                    Hashtable result=(Hashtable)JSON.JsonDecode((String)wwwReturn.Current);
                    growupResult = (GrowupResult)int.Parse((string)result["r"]);
                }catch{
                    Debug.Log ("Error");
                    yield break;
                }
                yield return true;
                if (callback != null) {
                    callback ();
                }
            }
            /// <summary>
            /// ゲーム開始
            /// </summary>
            /// <returns>The start.</returns>
            /// <param name="callback">Callback.</param>
            public IEnumerator GameStart(Action callback){
                yield return StartCoroutine(Login(callback));
                try{
                    if(!String.IsNullOrEmpty((string)AccessController.Instance.SessionCookie["Cookie"])){
                        Debug.Log ("session:"+AccessController.Instance.SessionCookie["Cookie"]);
                        Debug.Log ("login ok");
                    }else{
                        // 
                        Debug.Log ("login error");
                        throw new Exception("login error");
                    }
                }catch{
                    //                        NowLoadingScreen.SetActive(false);
                    yield break;
                }
                if(callback!=null) callback();
            }
            /// <summary>
            /// ログイン
            /// </summary>
            /// <param name="callback">Callback.</param>
            private IEnumerator Login(Action callback){
                Debug.Log ("login");
                string guid=FileController.Instance.read (userIdFilename);
                if(guid==string.Empty){
                    guid=Guid.NewGuid().ToString();
                    FileController.Instance.write(userIdFilename,guid);
                }
                Debug.Log ("guid:"+guid);
                Dictionary<string,string> formParams=new Dictionary<string, string>()
                {
//                    #if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
//                    {"pc","1"},
//                    #endif 
                    {"gid",guid},

//                    {"ver",ver},
//                    #if UNITY_IPHONE
//                    {"dv","1"},
//                    #elif UNITY_ANDROID
//                    {"dv","2"},
//                    #else 
//                    {"dv","3"},
//                    #endif
                };
                WWW www= AccessController.Instance.Access("http://"+hostDomain+"/login.aspx",formParams,hostDomain);
                yield return StartCoroutine(ResponceCheckForTimeOutWWW(www,NETWORK_TIMEOUT_COUNT));
                try{
                    if (!String.IsNullOrEmpty(www.error)){
                        Debug.Log(www.error);
                        throw new Exception("WWW.Error");
                    }
                    if(!String.IsNullOrEmpty(www.error)||String.IsNullOrEmpty(www.text)){
                        Debug.Log("network error");
                        return false;
                    }
                }catch{
                    isNetWorkError=true;
                    Debug.Log ("networkErrorDialog");
//                   DialogBox.Instance.viewMessage(DialogForm.DIALOG_OK,MenuResources.Instance.dialogComment["networkErrorDialog"].ToString(),RetryAccess);
                    yield break;
                }
                Debug.Log (www.text);
                Hashtable result=(Hashtable)JSON.JsonDecode(www.text);
//                versionCheck = true;
//                if (result.ContainsKey ("vc")) {
//                    if ((string)result ["vc"] != "1"){
//                        versionCheck = false;
//                    }
//                }
//                if(!versionCheck) yield break;
                //      string errmsg = "Network communication error."; // Not Use
                if (result.ContainsKey ("username")) {
                    userName = (string)result["username"];
                }
                if (result.ContainsKey ("fc")) {
                    friendCode = ((string)result["fc"]);
                }
                // 装備品取得
                if (result.ContainsKey ("b")) {
                    swordID = int.Parse(((string)result["b"]));
                }
                if (result.ContainsKey ("y")) {
                    armorID = int.Parse(((string)result["y"]));
                }
                if (result.ContainsKey ("tate")) {
                    shieldID = int.Parse((string)result["tate"]);
                }
                if (result.ContainsKey ("gp")) {
                    growupPoint = int.Parse((string)result["gp"]);
                }
                if (result.ContainsKey ("t")) {
                    ticket = int.Parse((string)result["t"]);
                }
                if (result.ContainsKey ("m")) {
                    medal = int.Parse((string)result["m"]);
                }
                if (result.ContainsKey ("itembox")) {
                    foreach (string key in ((Hashtable)result["itembox"]).Keys) {
                        Hashtable itemInfoHash =(Hashtable) ((Hashtable)result ["itembox"]) [key];
                        myItemInfo.Enqueue (getItemInfo(itemInfoHash,key));
                    }
                }
                if (result.ContainsKey ("kin")) {
                    chargeGachaIncentiveExpensive = int.Parse((string)result["kin"]);
                }
                if (result.ContainsKey ("gin")) {
                    chargeGachaIncentive = int.Parse((string)result["gin"]);
                }
                if (result.ContainsKey ("dou")) {
                    freeGachaIncentive = int.Parse((string)result["dou"]);
                }
                if (result.ContainsKey ("kp1")) {
                    magicalSlot[0] = int.Parse((string)result["kp1"]);
                }
                if (result.ContainsKey ("kp2")) {
                    magicalSlot[1] = int.Parse((string)result["kp2"]);
                }
                if (result.ContainsKey ("kp3")) {
                    magicalSlot[2] = int.Parse((string)result["kp3"]);
                }
                if (result.ContainsKey ("kp4")) {
                    magicalSlot[3] = int.Parse((string)result["kp4"]);
                }
                if (result.ContainsKey ("kp5")) {
                    magicalSlot[4] = int.Parse((string)result["kp5"]);
                }
                if (result.ContainsKey ("kp6")) {
                    magicalSlot[5] = int.Parse((string)result["kp6"]);
                }
                if (result.ContainsKey ("kp7")) {
                    magicalSlot[6] = int.Parse((string)result["kp7"]);
                }
                if (result.ContainsKey ("kp8")) {
                    magicalSlot[7] = int.Parse((string)result["kp8"]);
                }
                // アイテム取得
                if (www.text != string.Empty)
                {
                    try
                    {
                        // extract the public name of player
                        AccessController.Instance.ClearSessionCookie();

                        // check if session cookie was send, if not, well, no use to continue then
                        Debug.Log (www.responseHeaders.ContainsKey("SET-COOKIE"));
                        foreach(KeyValuePair<string,string> a in www.responseHeaders){
                            Debug.Log ("Key:"+a.Key+"/Value:"+a.Value);
                        }
                        if (www.responseHeaders.ContainsKey("SET-COOKIE"))
                        {
                            // extract the session identifier cookie and save it
                            // the cookie will be named, "auth" (this could be something else in your case)
                            char[] splitter = { ';' };
                            string[] v = www.responseHeaders["SET-COOKIE"].Split(splitter);
                            foreach (string s in v)
                            {
                                Debug.Log (s);

                                if (string.IsNullOrEmpty(s)) continue;
                                if (s.Substring(0, 17).ToLower().Equals("asp.net_sessionid"))
                                {   // found it
                                    AccessController.Instance.SetSessionCookie(s);
                                    Debug.Log ("Set Session");
                                    break;
                                }
                            }
                        }

                        // Check Login Bonus Check
//                        LoginBonusCheck (www.text);
                    }
                    catch {
                        // this should only possibly happen during development
                        //                return www;
                    }
                }
                yield return www;
            }
            /// <summary>
            /// ログインとアクセス
            /// </summary>
            /// <returns>The and access.</returns>
            /// <param name="url">URL.</param>
            /// <param name="formParams">Form parameters.</param>
            /// <param name="callback">Callback.</param>
            /// <param name="loadingScreen">If set to <c>true</c> loading screen.</param>
            private IEnumerator LoginAndAccess(string url, Dictionary<string,string> formParams,Action callback,bool loadingScreen=true){
//                if(loadingScreen){
//                    NowLoadingScreen.SetActive(true);
//                }
                if((!AccessController.Instance.SessionCookie.ContainsKey("Cookie")) && LoginCheck()){
                    yield return StartCoroutine(Login(callback));
                    try{
                        if(!String.IsNullOrEmpty((string)AccessController.Instance.SessionCookie["Cookie"])){
                            Debug.Log ("session:"+AccessController.Instance.SessionCookie["Cookie"]);
                            Debug.Log ("login ok");
                        }else{
                            // 
                            Debug.Log ("login error");
                            throw new Exception("login error");
                        }
                    }catch{
                        //                        NowLoadingScreen.SetActive(false);
                        yield break;
                    }

                }
                WWW www=AccessController.Instance.Access("http://"+hostDomain+url,formParams,hostDomain);
                yield return StartCoroutine(ResponceCheckForTimeOutWWW(www,NETWORK_TIMEOUT_COUNT));
                try{
                    //      yield return www;
                    if(String.IsNullOrEmpty(www.error)){
                        Debug.Log(www.text);
                    }else{
                        Debug.LogError(www.error);
                        throw new Exception("login error");
                    }
                }catch{
                    Debug.Log ("networkErrorDialog");
//                    DialogBox.Instance.viewMessage(DialogForm.DIALOG_OK,MenuResources.Instance.dialogComment["networkErrorDialog"].ToString(),RetryAccess);
                    isNetWorkError=true;
                    AccessController.Instance.NowLoadingScreen.SetActive(false);
                    yield break;
                }
//                serverTime=DateTime.Parse(www.responseHeaders["DATE"]);
//                localBaseTime=DateTime.Now;
//                Debug.Log ("-----------------serverTIme:"+serverTime.ToString("F"));
//                Debug.Log ("-----------------localBaseTime:"+localBaseTime.ToString("F"));
                // Todo:Affter timeout Check Set
                bool decodeResult=false;
                //  Session Error Check
                Hashtable jsonDecode=null;
                if(www.text=="1001"){
                    Debug.Log ("session error");
                    sessionTimeOutFlag=true;
                    decodeResult=false;
                }else{
                    sessionTimeOutFlag=false;
//                    jsonDecode=(Hashtable)JSON.JsonDecode(www.text,ref decodeResult);
                }
//                if(decodeResult==false){
//                    if(int.Parse(www.text)==(int)AccessController.ErrorNo.SESSION_ERROR){
//                        AccessController.Instance.ClearSessionCookie();
//                        yield return LoginAndAccess(url,formParams,callback);
//                    }
//                }
//                yield return jsonDecode;
                yield return www.text;
//                if(loadingScreen){
//                    AccessController.Instance.NowLoadingScreen.SetActive(false);
//                }
            }
//            private IEnumerator Login(Action callback){
//                Debug.Log ("login");
//                string guid=FileController.Instance.read (userIdFilename);
//                if(guid==string.Empty){
//                    guid=Guid.NewGuid().ToString();
//                    FileController.Instance.write(userIdFilename,guid);
//                }
//                Debug.Log ("guid:"+guid);
//                Dictionary<string,string> formParams=new Dictionary<string, string>()
//                {
//                    #if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
//                    {"pc","1"},
//                    #endif 
//                    {"gid",guid},
//                    {"ver",ver},
//                    #if UNITY_IPHONE
//                    {"dv","1"},
//                    #elif UNITY_ANDROID
//                    {"dv","2"},
//                    #else 
//                    {"dv","3"},
//                    #endif
//                };
//                WWW www= Access("http://"+hostDomain+"/login2.aspx",formParams);
//                yield return StartCoroutine(ResponceCheckForTimeOutWWW(www,NETWORK_TIMEOUT_COUNT));
//                try{
//                    if (!String.IsNullOrEmpty(www.error)){
//                        throw new Exception("WWWW.Error");
//                    }
//                    if(!String.IsNullOrEmpty(www.error)||String.IsNullOrEmpty(www.text)){
//                        Debug.Log("network error");
//                        return false;
//                    }
//                }catch{
//                    isNetWorkError=true;
//                    Debug.Log ("networkErrorDialog");
//                    DialogBox.Instance.viewMessage(DialogForm.DIALOG_OK,MenuResources.Instance.dialogComment["networkErrorDialog"].ToString(),RetryAccess);
//                    yield break;
//                }
//                Debug.Log (www.text);
//                Hashtable result=(Hashtable)JSON.JsonDecode(www.text);
//                MoneyController.Instance.StationID=Int16.Parse(result["s"].ToString());
//                if(int.Parse((string)result["rc"])==1) MoneyController.Instance.MyHorseRaceFlg=false;   
//                EnableTwitter=(((string)result["tw"])=="1");
//                EnableFacebook=(((string)result["fb"])=="1");
//                FriendCode=(string)result["fc"];
//                PresentMedal=int.Parse((string)result["c"]);
//                PresentTicket=int.Parse((string)result["t"]);
//                PresentItem=int.Parse((string)result["si"]);
//                LoginDays=int.Parse((string)result["login"]);
//                Debug.Log ((string)result ["vip"]);
//                if ((string)result ["vip"] == "0") {
//                    vipLimit=new DateTime(0);
//                } else {
//                    vipLimit = DateTime.Parse ((string)result ["vip"]);
//                }
//                PresentMessage=WWW.UnEscapeURL ((string)result["tl"]);
//                //      PresentMedal=9999;
//                //      PresentTicket=9999;
//                //      PresentMessage="gomen";
//                MaintenanceFlag=(((string)result["m"])=="1");
//                //      if(ver!=(string)result["ver"]) versionCheck=false;
//                versionCheck = true;
//                if (result.ContainsKey ("vc")) {
//                    if ((string)result ["vc"] != "1"){
//                        versionCheck = false;
//                    }
//                }
//                if(!versionCheck) yield break;
//                #if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
//                if((string)result["k"]=="0") EndTrial=true;
//                if(EndTrial) yield break;
//                #endif
//                if(MaintenanceFlag) yield break;
//                //      string errmsg = "Network communication error."; // Not Use
//                if (www.text != string.Empty)
//                {
//                    try
//                    {
//                        // extract the public name of player
//                        AccessController.Instance.ClearSessionCookie();
//
//                        // check if session cookie was send, if not, well, no use to continue then
//                        Debug.Log (www.responseHeaders.ContainsKey("SET-COOKIE"));
//                        foreach(KeyValuePair<string,string> a in www.responseHeaders){
//                            Debug.Log ("Key:"+a.Key+"/Value:"+a.Value);
//                        }
//                        if (www.responseHeaders.ContainsKey("SET-COOKIE"))
//                        {
//                            // extract the session identifier cookie and save it
//                            // the cookie will be named, "auth" (this could be something else in your case)
//                            char[] splitter = { ';' };
//                            string[] v = www.responseHeaders["SET-COOKIE"].Split(splitter);
//                            foreach (string s in v)
//                            {
//                                Debug.Log (s);
//
//                                if (string.IsNullOrEmpty(s)) continue;
//                                if (s.Substring(0, 17).ToLower().Equals("asp.net_sessionid"))
//                                {   // found it
//                                    AccessController.Instance.SetSessionCookie(s);
//                                    Debug.Log ("Set Session");
//                                    break;
//                                }
//                            }
//                        }
//
//                        // Check Login Bonus Check
//                        LoginBonusCheck (www.text);
//                    }
//                    catch {
//                        // this should only possibly happen during development
//                        //                return www;
//                    }
//                }
//                yield return www;
//            }

            private bool LoginCheck(){
                return true;
//                Debug.Log (MoneyController.Instance.LastLoginDate+"<"+DateTime.Now.Year*10000+DateTime.Now.Month*100+DateTime.Now.Day);
//                return (MoneyController.Instance.LastLoginDate<DateTime.Now.Year*10000+DateTime.Now.Month*100+DateTime.Now.Day);
            }

//            private IEnumerator LoginAndAccess(string url, Dictionary<string,string> formParams,Action callback,bool loadingScreen=true){
//                if(loadingScreen){
//                    NowLoadingScreen.SetActive(true);
//                }
//                if(sameCourseRetryFlag) yield return new WaitForSeconds(RETRY_TIME);
//                if(sameCourseRetryFlag) Debug.Log ("course retry");
//                if((!AccessController.Instance.SessionCookie.ContainsKey("Cookie")) && LoginCheck()){
//                    yield return StartCoroutine(Login(callback));
//                    try{
//                        if(!String.IsNullOrEmpty((string)AccessController.Instance.SessionCookie["Cookie"])){
//                            Debug.Log ("session:"+AccessController.Instance.SessionCookie["Cookie"]);
//                            Debug.Log ("login ok");
//                        }else{
//                            // 
//                            Debug.Log ("login error");
//                            throw new Exception("login error");
//                        }
//                    }catch{
//                        NowLoadingScreen.SetActive(false);
//                        yield break;
//                    }
//
//                }
//                WWW www=Access(url,formParams);
//                yield return StartCoroutine(ResponceCheckForTimeOutWWW(www,NETWORK_TIMEOUT_COUNT));
//                try{
//                    //      yield return www;
//                    if(String.IsNullOrEmpty(www.error)){
//                        Debug.Log(www.text);
//                    }else{
//                        Debug.LogError(www.error);
//                        throw new Exception("login error");
//                    }
//                }catch{
//                    Debug.Log ("networkErrorDialog");
//                    DialogBox.Instance.viewMessage(DialogForm.DIALOG_OK,MenuResources.Instance.dialogComment["networkErrorDialog"].ToString(),RetryAccess);
//                    isNetWorkError=true;
//                    NowLoadingScreen.SetActive(false);
//                    yield break;
//                }
//                serverTime=DateTime.Parse(www.responseHeaders["DATE"]);
//                localBaseTime=DateTime.Now;
//                Debug.Log ("-----------------serverTIme:"+serverTime.ToString("F"));
//                Debug.Log ("-----------------localBaseTime:"+localBaseTime.ToString("F"));
//                // Todo:Affter timeout Check Set
//                bool decodeResult=false;
//                //  Session Error Check
//                Hashtable jsonDecode=null;
//                if(www.text=="1001"){
//                    Debug.Log ("session error");
//                    sessionTimeOutFlag=true;
//                    decodeResult=false;
//                }else{
//                    sessionTimeOutFlag=false;
//                    jsonDecode=(Hashtable)JSON.JsonDecode(www.text,ref decodeResult);
//                }
//                if(decodeResult==false){
//                    if(int.Parse(www.text)==(int)ErrorNo.SESSION_ERROR){
//                        AccessController.Instance.ClearSessionCookie();
//                        //              yield return LoginAndAccess(url,formParams,callback);
//                        // if session timeout is 
//                        //              MainController mainController=MainControllerObj.GetComponent<MainController>(); // Not Use
//                    }
//                }
//                yield return jsonDecode;
//                if(loadingScreen){
//                    AccessController.Instance.NowLoadingScreen.SetActive(false);
//                }
//            }
            /// <summary>
            /// タイムアウトチェック
            /// </summary>
            /// <returns>The check for time out WW.</returns>
            /// <param name="www">Www.</param>
            /// <param name="timeout">Timeout.</param>
            private IEnumerator ResponceCheckForTimeOutWWW(WWW www, float timeout)
            {
                float requestTime = Time.time;

                while(!www.isDone)
                {
                    if(Time.time - requestTime < timeout){
                        //              Debug.Log ("Now Request");
                        yield return null;
                    }
                    else
                    {
                        Debug.Log ("networkErrorDialog");
//                       DialogBox.Instance.viewMessage(DialogForm.DIALOG_OK,MenuResources.Instance.dialogComment["networkErrorDialog"].ToString());
                        Debug.LogWarning("TimeOut");
                        break;
                    }
                }

                yield return null;
            }
        }
    }
}