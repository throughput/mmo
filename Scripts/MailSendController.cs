﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public class MailSendController : ThpmmoMonoBehavier {
            public GameObject itemWindow;
            public MailFormat mailformat = null;
            public UILabel toFriendName;
            public string toFriendCode;
            private UIInput inputBody;

            Dictionary<string,GameObject> popupMailItemSlot = new Dictionary<string,GameObject>();

            void Awake(){
                foreach (UISprite sprite in gameObject.GetComponentsInChildren<UISprite>()) {
                    if (sprite.name.StartsWith ("Slot")) {
                        popupMailItemSlot.Add (sprite.gameObject.name, sprite.gameObject);
                    }
                }
                foreach (UIInput input in gameObject.GetComponentsInChildren<UIInput>()) {
                    switch (input.name) {
                    case "InputMessage":
                        inputBody = input;
                        break;
                    }
                }
            }
            void OnEnable () {
                if (mailformat != null) {

                    inputBody.label.text = mailformat.body;
                    // 添付アイテムがあった時はアイテム表示
                    if (mailformat.itemInfo.id > 0) {
                        itemWindow.SetActive (true);
                        ItemTableInfo itemTable = MainController.Instance.itemTableDic [mailformat.itemInfo.id.ToString ()];

                        foreach (UILabel label in gameObject.GetComponentsInChildren<UILabel>()) {
                            switch (label.name) {
                            case "Name":
                                label.text = itemTable.name;
                                break;
                            case "Param1":
                                label.text = ItemUtility.getAttack (itemTable.atk, mailformat.itemInfo.rank).ToString ();
                                break;
                            case "Param2":
                                label.text = itemTable.speed.ToString ();
                                break;
                            case "Param3":
                                label.text = mailformat.itemInfo.rank.ToString ();
                                break;
                            }
                        }
                    } else {
                        itemWindow.SetActive (false);
                    }
                }
        	}
            // メール受け取り既読
            public void sendMail(){
                int itemBoxNo = 0;
                mailformat.body = inputBody.label.text;
                if (mailformat.body == string.Empty) {
                    MenuController.Instance.OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["mailSendEmptyError"]);
                    return;
                }
                mailformat.friendCode = toFriendCode;
                StartCoroutine(HttpController.Instance.SendMail (sendMailCallback,mailformat.friendCode,mailformat.subject,mailformat.body,mailformat.itemInfo.itemBoxNo));
            }
            public void clearBody(){
                inputBody.label.text = "";
            }

            public void sendMailCallback(MailError mailError){
                MenuController.Instance.CloseMsg (gameObject);
                if (mailError != MailError.SUCCESS) {
                    MenuController.Instance.OpenDialog (DialogForm.DIALOG_OK, GameTextTable.CommonDialogtext () ["mailSendError"]);
                } else {
                    MenuController.Instance.OpenDialog (DialogForm.DIALOG_OK, GameTextTable.CommonDialogtext () ["mailSendOk"]);
                    string[] friendCode = mailformat.friendCode.Split ('-');
                    SocketNetwork.Instance.sendSendMailNotice (int.Parse(friendCode [1]));
                }
            }
            public void itemSelectView(){
                MenuController.Instance.visibleSendMail (false);
                MenuController.Instance.changeMailListScreen (MenuController.ChangeMailScreen.ITEM_LIST);
            }
            public void friendSelectView(){
                MenuController.Instance.visibleSendMail (false);
                MenuController.Instance.changeMailListScreen (MenuController.ChangeMailScreen.FRIEND_LIST);
            }
        }
    }
}