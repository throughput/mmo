﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Throughputjp{
    namespace ThpMmo{
        public class ItemUtility {
            public const int itemBoxNone = -1;
            public static void SlotView(Dictionary<string,GameObject> slotObjectList,int slotCount){
                for(int i=0; i < slotObjectList.Count; ++i){
                    slotObjectList["Slot"+i.ToString()].SetActive(false);
                    if(i<slotCount){
                        slotObjectList["Slot"+i.ToString()].SetActive(true);
                    }else{
                        slotObjectList["Slot"+i.ToString()].SetActive(false);
                    }
                }
            }
            public static int getAttack (int rank, int baseAttack){
                float ratio = MainController.Instance.powerUpPointList [rank - 1];
                return Mathf.FloorToInt (baseAttack * ratio);
            }
            public static int getSpaceItemBoxNo(Dictionary<int,ItemInfo> itemList){
                for(int i=0; i < itemList.Count;++i){
                    ItemInfo itemInfo = itemList[i];
                    if (itemInfo.id == 0) {
                        return itemInfo.itemBoxNo;
                    }
                }
                return itemBoxNone;
            }
        }
    }
}