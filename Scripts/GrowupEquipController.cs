﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Throughputjp{
    namespace ThpMmo{
        public class GrowupEquipController : ThpmmoMonoBehavier {
            int useGrowupPoint;
            int myGrowupPoint;
            public GameObject growPointTextObj;
            public GameObject myGrowPointTextObj;
            public GameObject pointGauge;
            UILabel growupPointText;
            UILabel myGrowupPointText;
            string pointButtonName = "B_Point";
            public int itemNo;
            public ItemInfo itemInfo;
            UIButton okButton;
            public UILabel paramName;
            public UILabel paramAttackText;
            public UILabel paramSpeedText;
            public UILabel paramLevelText;
            public UILabel paramAttackAfterText;
            public UILabel paramSpeedAfterText;
            public UILabel paramLevelAfterText;
            public UILabel paramPrecentText;
            Dictionary<string,GameObject> popupEquipSlot = new Dictionary<string,GameObject>();

            // Use this for initialization
            void Awake () {
                foreach(UIButton button in gameObject.transform.GetComponentsInChildren<UIButton>()){
                    if (button.gameObject.name.StartsWith (pointButtonName)) {
                        EventDelegate callback = new EventDelegate (this, "upGropwupPoint");
                        callback.parameters [0].value = button.gameObject.name;
                        button.onClick.Add (callback);
                        button.enabled = true;
                    }
                    if (button.gameObject.name == "B_Ok") {
                        okButton = button;
                        okButton.enabled = false;
                    }
                }
                foreach (UISprite sprite in gameObject.GetComponentsInChildren<UISprite>()) {
                    if (sprite.name.StartsWith ("Slot")) {
                        popupEquipSlot.Add (sprite.gameObject.name, sprite.gameObject);
                    }
                }
                growupPointText = growPointTextObj.GetComponent<UILabel> ();
                myGrowupPointText = myGrowPointTextObj.GetComponent<UILabel> ();
        	}
            void Start(){
                useGrowupPoint = 0;
            }
        	// Update is called once per frame
        	void Update () {
                if (useGrowupPoint > 0) {
                    okButton.enabled = true;
                } else {
                    okButton.enabled = false;
                }
        	}
            void OnEnable() {
                // ポイント表示
                myGrowupPoint = PlayerInfo.Instance.characterInfo.growupPoint;
                useGrowupPoint = 0;
                if (itemInfo != null) {
                    updatePoint ();
                    updateStatus ();
                    ItemUtility.SlotView (popupEquipSlot,itemInfo.slotCount);
                }
            }
            void updateStatus(){
                ItemTableInfo itemTableInfo = MainController.Instance.itemTableDic [itemInfo.id.ToString ()];
                paramName.text = itemTableInfo.name;
                paramLevelText.text = itemInfo.rank.ToString ();
                paramAttackText.text = ItemUtility.getAttack (itemInfo.rank, itemTableInfo.atk).ToString ();
                paramSpeedText.text = itemTableInfo.speed.ToString ();
                paramAttackAfterText.text = ItemUtility.getAttack (itemInfo.rank+1, itemTableInfo.atk).ToString ();
                paramSpeedAfterText.text = (itemTableInfo.speed).ToString ();
                paramLevelAfterText.text = (itemInfo.rank + 1).ToString ();
            }
            // 成長ポイント追加
            public void upGropwupPoint (string buttonName) {
                int addPoint = int.Parse (buttonName.Replace (pointButtonName, ""));
                Debug.Log (addPoint);
                // ボタンメニューからポイントを取得
                if (myGrowupPoint > useGrowupPoint + addPoint) {
                    useGrowupPoint += addPoint;
                }
                updatePoint ();
            }
            // 成長ポイント追加
            public void upGropwupPointReset () {
                useGrowupPoint = 0;
                updatePoint ();
            }
            private void updatePoint(){
                int  taragetPoint = MainController.Instance.successPointList [itemInfo.rank];
                float ratioAtSlot = MainController.Instance.powerUpProbAtSlotList [itemInfo.slotCount];
                float percent = Mathf.Floor(((float)useGrowupPoint/(float)taragetPoint/ratioAtSlot)*100.0f*1000.0f)/1000.0f;
                paramPrecentText.text = Mathf.Min(percent,100.0f).ToString();

                growupPointText.text = useGrowupPoint.ToString();
                myGrowupPointText.text = (myGrowupPoint - useGrowupPoint).ToString ();
            }
            public void onClickGrowup(){
                MenuController.Instance.OpenDialog (DialogForm.DIALOG_YES_NO, "合成しますか", startGrowup);
            }
            private void startGrowup(DialogResult result){
                if (result == DialogResult.RESULT_YES) {
                    StartCoroutine(HttpController.Instance.growupItem (callbackGrowupItem, itemInfo.itemBoxNo, useGrowupPoint));
                }
            }
            private void callbackGrowupItem(GrowupResult result,GrowupCritical critical){
                if (result != GrowupResult.GROW_UP_ERROR) {
                    myGrowupPoint = myGrowupPoint - useGrowupPoint;
                    useGrowupPoint = 0;
                    PlayerInfo.Instance.characterInfo.growupPoint = myGrowupPoint;
                    updatePoint ();
                    if (result != GrowupResult.GROW_UP_FAIL) {
                        itemInfo.rank += (int)result;
                        PlayerInfo.Instance.myItemList [itemInfo.itemBoxNo] = itemInfo;
                        updateStatus ();
                    }
                } else {
                    return;
                }
                switch (critical) {
                case GrowupCritical.GROW_UP_CRITICAL_BLUE:
                    EffectManager.Instance.playCutin (CutinKind.CUTIN_GROWUP_FAIL);
                    break;
                case GrowupCritical.GROW_UP_CRITICAL_YELLOW:
                    EffectManager.Instance.playCutin (CutinKind.CUTIN_GROWUP_NORMAL);
                    break;
                case GrowupCritical.GROW_UP_CRITICAL_GREEN:
                    EffectManager.Instance.playCutin (CutinKind.CUTIN_GROWUP_HOT);
                    break;
                case GrowupCritical.GROW_UP_CRITICAL_RED:
                    EffectManager.Instance.playCutin (CutinKind.CUTIN_GROWUP_VERY_HOT);
                    break;
                case GrowupCritical.GROW_UP_CRITICAL_GOLD:
                    EffectManager.Instance.playCutin (CutinKind.CUTIN_GROWUP_PERFECT);
                    break;
                }
                StartCoroutine(timerPlayEffect(result,2.0f));
            }

            /// <summary>
            /// Timers the play effect.
            /// </summary>
            /// <returns>The play effect.</returns>
            /// <param name="result">Result.</param>
            /// <param name="time">Time.</param>
            private IEnumerator timerPlayEffect(GrowupResult result,float time){
                yield return new WaitForSeconds(time);
                switch (result) {
                case GrowupResult.GROW_UP_ULTRA_SUCCESS:
                    EffectManager.Instance.playEffect (EffectKind.EFFECT_GROWUP_GREAT_SUCCESS,EffectCalback);
                    break;
                case GrowupResult.GROW_UP_BIG_SUCCESS:
                    EffectManager.Instance.playEffect (EffectKind.EFFECT_GROWUP_GREAT_SUCCESS,EffectCalback);
                    break;
                case GrowupResult.GROW_UP_SUCCESS:
                    EffectManager.Instance.playEffect (EffectKind.EFFECT_GROWUP_SUCCESS,EffectCalback);
                    break;
                case GrowupResult.GROW_UP_FAIL:
                    EffectManager.Instance.playEffect (EffectKind.EFFECT_GROWUP_FAIL,EffectCalback);
                    break;
                }
            }

            private void EffectCalback(Animator animator){
                Debug.Log ("EffectCalback");
                // ここでカットインをリセット
                EffectManager.Instance.resetCutin ();
            }
        }
    }
}
