﻿// #define OFFLINE_PLAY

using UnityEngine;
using System.Collections;
using Throughputjp;
using Nostalgia;
namespace Throughputjp{
	namespace ThpMmo{
        public class DungeonPlayer : SingletonMonoBehaviour<DungeonPlayer> {

			private float TimedeltaTime;
			
			float angles;

			float playerWait;

			Vector3 playerPos;
			Vector3 camPos;
			Vector3 camMovePoint;

			GameObject Directional;
            GameObject nameObject;
            GameObject slotObject;
            GameObject levelupObject;
			public GameObject playerObj;
			public GameObject playerObj2;
			public GameObject gameCamera;
            public Camera uiCamera;

			public float areaOutCamMove = 10.0f;
			public float playerSpeed = 0.1f;
			public const float ROOM_CAMERA_SIZE = 10.0f;
			public const float MAX_AREA_SIZE_X=10.0f;
			public const float MAX_AREA_SIZE_Y=10.0f;
			public const float ROOM_SIZE = 80.0f;
            public const int NAME_FONT_SIZE = 16;
            public const float NAME_POSISION = 60.0f;
            public const float MAGIC_SLOT_POSISION = 90.0f;
            public const float DAMAGE_POSISION = 40.0f;
			public GameObject pointerObject=null;
            public GameObject UIName;
            public Mator hpMator;
            public Mator spMator;
            public Mator expMator;
            public UILabel levelLabel;
            public UILabel moneyLabel;
            public UILabel ticketLabel;
            private bool nextFloorTouchFlag = false;
            // レベルアップ演出
            private const float LEVELUP_EFFECT_TIME = 1.0f;

            private const float touch_base_x = 640.0f;
            private const float touch_base_y = 1136.0f;
#if UNITY_EDITOR
            private const float BLOCK_SIZE = 64.0f;
#endif
#if !UNITY_EDITOR
#if UNITY_STANDALONE
            private const float BLOCK_SIZE = 64.0f;
#else
            private const float BLOCK_SIZE = 64.0f;
#endif
#endif
			private string characterID;
			ArrayList mapList;
			private Vector2 touchPos=new Vector2(0.0f,0.0f);
			private Vector2 movePos=new Vector2(0.0f,0.0f);
			bool goalXFlag=true;
			bool goalYFlag=true;
            bool attackFlag = false;

            // 選択中の攻撃方法
            private int selectedAttackID = 0;
//            bool wayMoveFlag = false; // 通路移動中
            MonsterInfo taregetMonsterInfo  = null;
			enum PHASE_ID{
				START,
				LOGIN,
				PLAY,
				BATTLE,
				LOGOUT,
			}
			enum PROC{
				CHARA_MOVE_WAIT,
			}
			PROC proc=PROC.CHARA_MOVE_WAIT;
			private PHASE_ID phaseID=PHASE_ID.START;  

			// Use this for initialization
			void Awake () {
				DungeonCharacterManager.Instance.gameCamera=gameCamera;
                DungeonMonsterManager.Instance.gameCamera=gameCamera;
                UIName = GameObject.Find ("UIName");
			}
			void Start () {
                //		delay = 0;
                //		movedelay = 10;PlayerInfo.Instance
                Directional = GameObject.Find("Directional");
                print(Directional.GetComponent<Light>().color);
                gameObject.transform.localPosition = new Vector3(32000.0f,0.0f,32000.0f);
                gameCamera.transform.localPosition = new Vector3(32000.0f,0.0f,32000.0f);
                gameObject.transform.localRotation = Quaternion.Euler(0,0,0);
                gameCamera.transform.localRotation = Quaternion.Euler(-15,0,0);
                PlayerInfo.Instance.characterInfo.x=(int)transform.position.x;
                PlayerInfo.Instance.characterInfo.y=(int)transform.position.z;

                StartCoroutine(HttpController.Instance.GameStart (StartCallBack));
            }
            /// <summary>
            /// 開始時コールバック
            /// </summary>
            void StartCallBack(){
                string[] friendCode = HttpController.Instance.friendCode.Split ('-');
                PlayerInfo.Instance.characterInfo.characterID = int.Parse(friendCode [1]);
                PlayerInfo.Instance.characterInfo.friendCode = HttpController.Instance.friendCode;
                PlayerInfo.Instance.characterInfo.name = HttpController.Instance.userName;
                PlayerInfo.Instance.characterInfo.swordID = HttpController.Instance.swordID;
                PlayerInfo.Instance.characterInfo.armorID = HttpController.Instance.armorID;
                PlayerInfo.Instance.characterInfo.shieldID = HttpController.Instance.shieldID;
                PlayerInfo.Instance.characterInfo.growupPoint = HttpController.Instance.growupPoint;
                PlayerInfo.Instance.characterInfo.medal = HttpController.Instance.medal;
                PlayerInfo.Instance.characterInfo.ticket = HttpController.Instance.ticket;
                PlayerInfo.Instance.magicSlot = HttpController.Instance.magicalSlot;
                GameInfo.Instance.chargeGachaIncentive = HttpController.Instance.chargeGachaIncentive;
                GameInfo.Instance.chargeGachaIncentiveExpensive = HttpController.Instance.chargeGachaIncentiveExpensive;
                GameInfo.Instance.freeGachaIncentive = HttpController.Instance.freeGachaIncentive;
                updateHeader ();

                // アイテム取得
                while(HttpController.Instance.myItemInfo.Count > 0){
                    ItemInfo itemInfo = HttpController.Instance.myItemInfo.Dequeue ();
                    PlayerInfo.Instance.myItemList[itemInfo.itemBoxNo] = itemInfo;
                }
                foreach(MeshRenderer renderer in gameObject.GetComponentsInChildren<MeshRenderer>()){
                    if (renderer.name == "Shield") {
                        Material material = renderer.materials [0];
                    }
                    if (renderer.name == "Weapon") {
                        Material material = renderer.materials [0];
                    }
                }

                characterID=HttpController.Instance.friendCode;
                phaseID=PHASE_ID.LOGIN;
                MainController.Instance.getMailCount ();
                ItemController.characterEquip (PlayerInfo.Instance.characterInfo, DungeonPlayer.Instance.gameObject);
			}
			private void OnGUI(){
				GUILayout.BeginArea(new Rect (0,0,100,300));
                if(characterID != null) characterID=GUI.TextField(new Rect(0, 10, 100, 30), characterID);
				GUILayout.Space (40);
				if (GUILayout.Button("ChangeID", GUILayout.MinHeight(40))) {
					PlayerInfo.Instance.characterInfo.characterID=int.Parse(characterID);
				}
				GUILayout.TextArea (string.Format ("screen:{0},{1}", Screen.width, Screen.height));
				GUILayout.TextArea (string.Format ("touchPos:{0},{1}", touchPos.x, touchPos.y));
				GUILayout.TextArea (string.Format ("movePos:{0},{1}", movePos.x, movePos.y));
				GUILayout.TextArea (string.Format ("cameraPos:{0},{1}", gameCamera.transform.position.x, gameCamera.transform.position.z));
				GUILayout.TextArea (string.Format ("charaPos:{0},{1}", transform.position.x, transform.position.z));
				GUILayout.EndArea ();
			}
            // Update is called once per frame
			void Update () {
				switch(phaseID){
					case PHASE_ID.START:
                        phaseID=PHASE_ID.PLAY;
					break;

                    case PHASE_ID.LOGIN:
                    #if OFFLINE_PLAY
                    #else
						if(SocketNetwork.Instance.IsConnect()){
                            phaseID=PHASE_ID.PLAY;
                            SocketNetwork.Instance.sendCharaInfo(PlayerInfo.Instance.characterInfo);
                            SocketNetwork.Instance.sendSetEquip(PlayerInfo.Instance.characterInfo);
                            SocketNetwork.Instance.sendLogin(PlayerInfo.Instance.characterInfo);
						}
                    #endif
					    // オフラインでも遊べるように移動をここにもいれておく
						PlayerMove();
					break;

					case PHASE_ID.PLAY:
						PlayerMove();
					break;

					case PHASE_ID.BATTLE:
						PlayerMove();
					break;

					case PHASE_ID.LOGOUT:
					break;
				}
			}
            /// <summary>
            /// ポインタの配置
            /// </summary>
            /// <param name="targetPos">Target position.</param>
			public void setPointer(Vector2 targetPos){
				if(pointerObject==null){
					pointerObject=(GameObject)Instantiate(Resources.Load ("Graphics/UI/TapEffect"));
                    SpriteRenderer renderer = pointerObject.GetComponent<SpriteRenderer> ();
                    Vector3 pointerSize = renderer.bounds.size;
                    pointerObject.transform.position=new Vector3((int)(targetPos.x), 0.0f, (int)(targetPos.y));
                }
			}
            /// <summary>
            /// ポインタの削除
            /// </summary>
			public void deletePointer(){
				if(pointerObject!=null){
					Destroy(pointerObject);
					pointerObject=null;
				}
			}
            /// <summary>
            /// UI表示
            /// </summary>
            private void viewUI(){
                if (slotObject == null) {
                    slotObject = (GameObject)Instantiate (Resources.Load ("Prefab/B_RareSelectSlot"));
                    foreach (UISprite pSprite in slotObject.GetComponentsInChildren<UISprite>()) {
                    }
                }
                // キャラクター名配置
                {
                    nameObject = CharacterUtility.viewCharaName (gameObject, nameObject, PlayerInfo.Instance.characterInfo, UIName.transform);
                }
                // 選択魔法配置
                if (slotObject != null) {
                    slotObject.transform.parent = UIName.transform;
                    slotObject.transform.localScale = new Vector3 (0.25f, 0.25f, 1.0f);
                    var cameraPos = Camera.main.WorldToScreenPoint(gameObject.transform.localPosition);
                    // キャラクターの分上にあげる
                    var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + MAGIC_SLOT_POSISION, -10.0f);
                    // 表示位置
                    slotObject.transform.localPosition = pointPos;
                }
            }
            /// <summary>
            /// キャラクター情報の更新
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            public void updateCharacterParam(CharacterInfo charaInfo){
//                PlayerInfo.Instance.characterInfo.swordID = charaInfo.swordID;
//                PlayerInfo.Instance.characterInfo.armorID = charaInfo.armorID;
//                PlayerInfo.Instance.characterInfo.shieldID = charaInfo.shieldID;
                PlayerInfo.Instance.characterInfo.treasureLevel = charaInfo.treasureLevel;
                PlayerInfo.Instance.characterInfo.battleLevel = charaInfo.battleLevel;
                PlayerInfo.Instance.characterInfo.attack = charaInfo.attack;
                PlayerInfo.Instance.characterInfo.attackSpeed = charaInfo.attackSpeed;
                PlayerInfo.Instance.characterInfo.maxHp = charaInfo.maxHp;
                PlayerInfo.Instance.characterInfo.maxSp = charaInfo.maxSp;
                PlayerInfo.Instance.characterInfo.Hp = charaInfo.Hp;
                PlayerInfo.Instance.characterInfo.Sp = charaInfo.Sp;
                updateHeader ();
            }
            public void startLevelup(){
                CharacterUtility.startLevelup (gameObject, UIName.transform);
            }
            /// <summary>
            /// ヘッダー更新
            /// </summary>
            public void updateHeader(){
                // メータの反映
                hpMator.initParam (PlayerInfo.Instance.characterInfo.Hp, PlayerInfo.Instance.characterInfo.maxHp);
                spMator.initParam (PlayerInfo.Instance.characterInfo.Sp, PlayerInfo.Instance.characterInfo.maxSp);
                levelLabel.text = PlayerInfo.Instance.characterInfo.battleLevel.ToString ();
                moneyLabel.text = PlayerInfo.Instance.characterInfo.medal.ToString();
                ticketLabel.text = PlayerInfo.Instance.characterInfo.ticket.ToString();
                expMator.initParam (0,1000);

                PlayerInfo.Instance.characterInfo.medal = HttpController.Instance.medal;
                PlayerInfo.Instance.characterInfo.ticket = HttpController.Instance.ticket;
            }
            /// <summary>
            /// Players the move.
            /// </summary>
			public void PlayerMove(){
				TimedeltaTime = 55 * Time.deltaTime;
				camPos = gameCamera.transform.position;
				playerPos = transform.position;
                viewUI ();
                // HP反映
                hpMator.param = PlayerInfo.Instance.characterInfo.Hp;

				switch(proc){
                case PROC.CHARA_MOVE_WAIT:
                    bool touchFlag = false;
                    Vector2 mousePos = new Vector2 (0.0f, 0.0f);
#if UNITY_EDITOR || UNITY_STANDALONE
                    Vector3 cursor=new Vector3(0.0f, 0.0f, 0.0f); 
                    if (Input.GetMouseButtonDown (0)) {
                        mousePos = Input.mousePosition;

//                        Map map = gameObject.GetComponent<Map>();
//                        cursor = Input.mousePosition;
//                        cursor.z = Camera.main.transform.position.y;
//                        cursor = Camera.main.ScreenToWorldPoint(cursor) - transform.position;
//                        mousePos = new Vector2(cursor.x, cursor.z);
//                        map.LocalPointToMapPoint(cursor);

                        Ray ray = new Ray ();
                        RaycastHit hit = new RaycastHit ();

                        // マウスの位置が、カメラに写っていて、且つ、カメラから100以内にいるかチェック
                       ray = uiCamera.ScreenPointToRay (Input.mousePosition);
                        if (Physics.Raycast (ray, out hit, 100.0f)) {
                            if (hit.collider.gameObject.name == "TouchLayer") {
                                touchFlag = true;
                            }
                        }
                    }
#else
					if(0< Input.touchCount) {
						Touch touch = Input.GetTouch (0);
						if(touch.phase == TouchPhase.Ended){
                            Ray ray= new Ray();
                            RaycastHit hit=new RaycastHit();
							mousePos=Input.mousePosition;
                            ray = uiCamera.ScreenPointToRay(Input.mousePosition);
                            // マウスの位置が、カメラに写っていて、且つ、カメラから100以内にいるかチェック
                            if (Physics.Raycast(ray, out hit, 100.0f)) {
                                if(hit.collider.gameObject.name == "TouchLayer"){
                                    touchFlag=true;
                                }
                            }
						}
					}
#endif
                    if (touchFlag) {
//                        // 通路移動中
//                        if (wayMoveFlag) {
//                            break;
//                        }
                        float aspect_rect = touch_base_y/Screen.height;
                        touchPos = new Vector2 (((mousePos.x - Screen.width / 2) * aspect_rect) / BLOCK_SIZE, ((mousePos.y - Screen.height / 2) * aspect_rect) / BLOCK_SIZE);
                        touchPos = new Vector2 (Mathf.Round (touchPos.x), Mathf.Round (touchPos.y));
                        bool moveFlag = false;
                        movePos = new Vector2 (Mathf.Round (camPos.x + touchPos.x), Mathf.Round (camPos.z + touchPos.y)); 
                        movePos = new Vector2 (Mathf.Round (movePos.x), Mathf.Round (movePos.y - 1.0f)); // ずれるので補正かけてる 
                        // 謎の処理
//                        if (movePos.x % 2 == 0) {
//                            movePos = new Vector2 (movePos.x + 1, movePos.y);
//                        }
//                        if (movePos.y % 2 != 0) {
//                            movePos = new Vector2 (movePos.x, movePos.y + 1);
//                        }
//                        // 今ターゲットしてるモンスターを記録
//                        MonsterInfo nowMonsterInfo = taregetMonsterInfo;
//                        taregetMonsterInfo = DungeonMonsterManager.Instance.getPositionMonsterID ((int)movePos.x,(int)movePos.y);
//                        // モンスターをターゲットしていて
//                        if (taregetMonsterInfo != null) {
//                            // すでにターゲット済みの場合
//                            if (nowMonsterInfo != null) {
//                                if (nowMonsterInfo.monsterID == taregetMonsterInfo.monsterID && nowMonsterInfo.monsterNum == taregetMonsterInfo.monsterNum) {
//                                    // 移動する
//                                    // 同じターゲットの場合
//                                    attackFlag = true;
//                                    if (PlayerInfo.Instance.characterInfo.x != (int)(movePos.x)) {
//                                        PlayerInfo.Instance.characterInfo.x = (int)(movePos.x);
//                                        goalXFlag = false;
//                                    }
//                                    if (PlayerInfo.Instance.characterInfo.y != (int)(movePos.y)) {
//                                        PlayerInfo.Instance.characterInfo.y = (int)(movePos.y);
//                                        goalYFlag = false;
//                                    }
//                                } else {
//                                    // ターゲットが変わっったら移動しない
//                                }
//                            } else {
//                                // まだターゲットしていなかった場合、ターゲットのみで移動しない
//                            }
//                        } else {
                        // モンスターがターゲットできて、魔法がセットされている場合
                        taregetMonsterInfo = DungeonMonsterManager.Instance.getPositionMonsterID ((int)movePos.x,(int)movePos.y);
                        if(taregetMonsterInfo!=null && selectedAttackID != 0){
                            // 魔法攻撃
                            SocketNetwork.Instance.sendPlayerAttack (PlayerInfo.Instance.characterInfo, selectedAttackID, taregetMonsterInfo);
                        }else{
                            attackFlag = true; // 攻撃フラグ
                            // ターゲットない場合は移動
                            if (PlayerInfo.Instance.characterInfo.x != (int)(movePos.x)) {
                                PlayerInfo.Instance.characterInfo.x = (int)(movePos.x);
                                goalXFlag = false;
                            }
                            if (PlayerInfo.Instance.characterInfo.y != (int)(movePos.y)) {
                                PlayerInfo.Instance.characterInfo.y = (int)(movePos.y);
                                goalYFlag = false;
                            }
                        }
                        if (!goalXFlag || !goalYFlag) {
                            deletePointer ();
                            setPointer (movePos);
                            #if OFFLINE_PLAY
                            #else
                            SocketNetwork.Instance.sendMoveData (PlayerInfo.Instance.characterInfo);
                            #endif
                        }
                    }
                    Vector3 beforePos = transform.position;
                    // Todo:コリジョン用のXYを移動方向で判定して、丸め込むこと
                    float collisionX = transform.position.x;
                    float collisionY = transform.position.z;
                    if (!goalYFlag) {
                        // 上移動
                        if (Mathf.FloorToInt (transform.position.z) < Mathf.FloorToInt (movePos.y)) {
                            transform.position += new Vector3 (0, 0, playerSpeed * TimedeltaTime);
                            if (Mathf.Abs (transform.position.z - Mathf.Floor (movePos.y)) < 0.1f) {
                                transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Floor (movePos.y));
                                goalYFlag = true;
                            }
                            collisionY = Mathf.Floor (transform.position.z);
                        }
						// 下移動
						else if (Mathf.CeilToInt (transform.position.z) > Mathf.FloorToInt (movePos.y)) {
                            transform.position -= new Vector3 (0, 0, playerSpeed * TimedeltaTime);
                            if (Mathf.Abs (transform.position.z - Mathf.Floor (movePos.y)) < 0.1f) {
                                transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Floor (movePos.y));
                                goalYFlag = true;
                            }
                            collisionY = Mathf.Ceil (transform.position.z);
                        } else {
                            goalYFlag = true;
                        }
                    }
                    if (!goalXFlag) {
                        // 右移動
                        if (Mathf.FloorToInt (transform.position.x) < Mathf.FloorToInt (movePos.x)) {
                            transform.position += new Vector3 (playerSpeed * TimedeltaTime, 0, 0);
                            if (Mathf.Abs (transform.position.x - Mathf.Floor (movePos.x)) < 0.1f) {
                                transform.position = new Vector3 (Mathf.Floor (movePos.x), transform.position.y, transform.position.z);
                                goalXFlag = true;
                            }
                            collisionX = Mathf.Floor (transform.position.x);
                        }
						// 左移動
						else if (Mathf.CeilToInt (transform.position.x) > Mathf.FloorToInt (movePos.x)) {
                            transform.position -= new Vector3 (playerSpeed * TimedeltaTime, 0, 0);
                            if (Mathf.Abs (transform.position.x - Mathf.Floor (movePos.x)) < 0.1f) {
                                transform.position = new Vector3 (Mathf.Floor (movePos.x), transform.position.y, transform.position.z);
                                goalXFlag = true;
                            }
                            collisionX = Mathf.Ceil (transform.position.x);
                        } else {
                            transform.position = new Vector3 (movePos.x, transform.position.y, transform.position.z);
                            goalXFlag = true;
                        }
                    }
                    // コリジョンチェック
                    COLLISION_RESULT collisionResult = DungeonMaker.Instance.checkCollision (transform.position.x, transform.position.z);
                    if (collisionResult == COLLISION_RESULT.COLLISION_RESULT_WALL) {
                        goalXFlag = true;
                        goalYFlag = true;
                        transform.position = beforePos;
                    }
//                    if (collisionResult == COLLISION_RESULT.COLLISION_RESULT_WAY) {
//                        wayMoveFlag = true;
//                        goalXFlag = true;
//                        goalYFlag = true;
//                        Vector2 nextPos = new Vector2 (0.0f, 0.0f);
//                        Debug.Log ("L_Direction"+playerObj2.GetComponent<Animator> ().GetInteger ("L_Direction"));
//                        int direction = playerObj2.GetComponent<Animator> ().GetInteger ("L_Direction");
//                        nextPos = DungeonMaker.Instance.wayTargetList [direction - 1];
//
//                        Vector2 resultNextPos = DungeonMaker.Instance.nextWay (new Vector2 (transform.position.x, transform.position.z),nextPos);
//                        // 方向を元に向きを変更
//                        for (int i = 0; i < DungeonMaker.Instance.wayTargetList.Count; ++i) {
//                            if (resultNextPos == DungeonMaker.Instance.wayTargetList [i]) {
//                                direction = i + 1;
//                            }
//                        }
//                        playerObj2.GetComponent<Animator> ().SetInteger ("L_Direction", direction);
//
//                        // 移動
//                        transform.position = new Vector3 (resultNextPos.x, 0.0f, resultNextPos.y);
//                        // 移動してなかったら、通路移動フラグ消す
//                        if (transform.position.x == resultNextPos.x && transform.position.z == resultNextPos.y) {
//                            wayMoveFlag = false;
//                        }
//                    }
                    if (collisionResult == COLLISION_RESULT.COLLISION_RESULT_GOAL) {
                        if (!nextFloorTouchFlag) {
                            Debug.Log ("OpenNextFloor");
                            Debug.Log((TextTable.Instance.Table["table_gameDialogtext"])["nextFloorConfirm"]);
                            MenuController.Instance.OpenDialog (DialogForm.DIALOG_YES_NO,GameTextTable.GameDialogtext()["nextFloorConfirm"], NextFloorConfirmCallBack);
                            nextFloorTouchFlag = true;
                        }
                    } else {
                        nextFloorTouchFlag = false;
                    }

					if(goalXFlag && goalYFlag){
						deletePointer();
                        if (taregetMonsterInfo != null) {
                            if (attackFlag) {
                                // 通常攻撃
                                SocketNetwork.Instance.sendPlayerAttack (PlayerInfo.Instance.characterInfo, 0, taregetMonsterInfo);
                                taregetMonsterInfo = null;
                                attackFlag = false;
                            }
                        }
					}
                    // カメラ追従
                    gameCamera.transform.position=transform.position;
					break;
				}
			}
            public void setMagicSlot(int attackID){
                selectedAttackID = attackID;
            }

            private void NextFloorConfirmCallBack(DialogResult rResult){
                if (rResult == DialogResult.RESULT_YES) {
                    MainController.Instance.GoalFloor ();
                }
            }

            /// <summary>
            /// 画面演出設置
            /// </summary>
			public void ScreenEffectSelect (){
				if(camMovePoint.x == 0){
					if(camMovePoint.z == 0){
						Directional.GetComponent<Light>().color = new Color(0.515f,0.515f,0.515f,1);
						Directional.GetComponent<Light>().intensity =  1f;
					}
				}
				else if(camMovePoint.x == 10){
					if(camMovePoint.z == 0){
						Directional.GetComponent<Light>().color = new Color(0.19f,0.23f,0.32f,1);
						Directional.GetComponent<Light>().intensity =  0.22f;
					}
				}

			}
            /// <summary>
            /// ダメージを受けるアクションを再生
            /// </summary>
            /// <param name="damageInfo">Damage info.</param>
            public void playDamageAction(DamageInfo damageInfo){
                GameObject damageTextObject;
                damageTextObject = (GameObject)Instantiate (Resources.Load ("Prefab/DamageNum"));
                damageTextObject.name = "damageLabel";
                UILabel label = damageTextObject.GetComponent<UILabel> ();
                label.fontSize = DungeonPlayer.NAME_FONT_SIZE;
                damageTextObject.transform.parent = DungeonCharacterManager.Instance.nameParent.transform;
                label.gameObject.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
                label.text = "[ffffff]"+damageInfo.damage.ToString()+"[ffffff]";
                var cameraPos = Camera.main.WorldToScreenPoint(transform.localPosition);
                var pointPos = new Vector3(cameraPos.x - Screen.width / 2, cameraPos.y - Screen.height / 2 + DungeonPlayer.DAMAGE_POSISION, -10.0f);
                // ダメージ数演出スクリプト追加
                damageTextObject.AddComponent<DamageLabel> ();

                // ダメージパーティクル表示
                DamageManager.playDamageParticle (damageInfo.magicKind, damageInfo.magicLevel,gameObject);

                // HP反映
                PlayerInfo.Instance.characterInfo.Hp = damageInfo.characterInfo.Hp;

                // 表示位置
                damageTextObject.transform.localPosition = pointPos;
            }

            /// <summary>
            /// ダンジョン開始位置に配置
            /// </summary>
            /// <param name="mapData">Map data.</param>
            public void setDungeonStartPosion(string mapData){
                float tipSize = DungeonMaker.TIP_SIZE;
                // 開始位置（センター分3ずらす。ダンジョンを-方向に生成するため、65535が開始位置)
                float playerX = 3;
                float playerY = 65532;
                for(int i =0; i<mapData.Length; ++i){
                    if (mapData.Substring (i, 1) == "\r") {
                        playerY += -tipSize;
                        playerX = 3;
                        continue; // 次の行へ
                    }
                    if (mapData.Substring (i, 1) == "2") {
                        break;
                    }
                    playerX += tipSize;
                }
                PlayerInfo.Instance.characterInfo.x = (int)playerX;
                PlayerInfo.Instance.characterInfo.y = (int)playerY;
                transform.position = new Vector3 ( PlayerInfo.Instance.characterInfo.x, transform.position.y, PlayerInfo.Instance.characterInfo.y);
            }
		}
}}
