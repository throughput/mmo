using UnityEngine;
using System;
using System.Collections;

namespace Throughputjp{
	public class SoundManager : SingletonMonoBehaviour<SoundManager> {
	  
	public SoundVolume volume = new SoundVolume();
	private float SoundFade=1f;
	private int FadeFlg=0;
	private int NextSound=-1;
	private AudioSource BGMsource;
	private AudioSource[] SEsources = new AudioSource[16];
	private AudioSource[] VoiceSources = new AudioSource[16];
		
	private int WaitBGMFlg=0;
	private float sePitch=1.0f;
	private float bgmPitch=1.0f;
	  
	 public AudioClip[] BGM;
	 public AudioClip[] SE;
	 public AudioClip[] Voice;
	 
	new  void Awake (){
	  GameObject[] obj = GameObject.FindGameObjectsWithTag("SoundManager");
	  if( obj.Length > 1 ){
	   Destroy(gameObject);
	  }else{
	   DontDestroyOnLoad(gameObject);
	  }
	  BGMsource = gameObject.AddComponent<AudioSource>();
	  BGMsource.loop = true;
	   
	  for(int i = 0 ; i < SEsources.Length ; i++ ){
	   SEsources[i] = gameObject.AddComponent<AudioSource>();
	  }
	   
	  for(int i = 0 ; i < VoiceSources.Length ; i++ ){
	   VoiceSources[i] = gameObject.AddComponent<AudioSource>();
	  }
	 }
	  
	 void Update () {
		BGMsource.mute = volume.Mute;
		foreach(AudioSource source in SEsources ){
			source.mute = volume.Mute;
		}
		foreach(AudioSource source in VoiceSources ){
			source.mute = volume.Mute;
		}
		
		BGMsource.volume = volume.BGM;
		foreach(AudioSource source in SEsources ){
			source.volume = volume.SE;
		}
		foreach(AudioSource source in VoiceSources ){
			source.volume = volume.Voice;
		}
		if (FadeFlg==1){
			if (NextSound==-1){
				BGMsource.pitch=bgmPitch;
				BGMsource.Stop();
				BGMsource.clip = BGM[NextSound];
				BGMsource.Play();
				FadeFlg=0;
				SoundFade=1f;
			}else{
				SoundFade=SoundFade-0.05f;				
				volume.BGM=SoundFade;
				if (SoundFade<0.1){
					BGMsource.pitch=bgmPitch;
					BGMsource.Stop();
					BGMsource.clip = BGM[NextSound];
					BGMsource.Play();
					FadeFlg=2;
				}
			}	
		}else if (FadeFlg==2){
			SoundFade=SoundFade+0.05f;				
			volume.BGM=SoundFade;
			if (SoundFade>0.9){
				FadeFlg=0;
				SoundFade=1f;
			}
				
				
		}
				
	 }
		
		public void BGMPitch(float index){
			bgmPitch=index;	
		}
	 
		public void SEPitch(float index){
			sePitch=index;	
		} 
	 public void PlayBGM(int index){
	  if( 0 > index || BGM.Length <= index ){
	   return;
	  }
		if(WaitBGMFlg==1) {
			if(index!=1){
				Debug.Log("SoundStop");
				return;
			}
			else{
				WaitBGMFlg=0;		
			}
		}
	  if( BGMsource.clip == BGM[index] ){
	   return;
	  }
		if(FadeFlg==0){
		  FadeFlg=1;
		  NextSound=index;
		}else{
	  BGMsource.pitch=bgmPitch;
	  BGMsource.Stop();
	  BGMsource.clip = BGM[index];
	  BGMsource.Play();
				
		}
	//  BGMsource.pitch=bgmPitch;
	//  BGMsource.Stop();
	//  BGMsource.clip = BGM[index];
	//  BGMsource.Play();
	 }
	  
	 public void StopBGM(){
	  BGMsource.Stop();
	  BGMsource.clip = null;
	 }
	 
	  
	 public void PlaySE(int index){
	  if( 0 > index || SE.Length <= index ){
	   return;
	  }
	    
	  foreach(AudioSource source in SEsources){
	   if( false == source.isPlaying ){
	    source.clip = SE[index];
		source.pitch=sePitch;
	    source.Play();
	    return;
	   }
	  } 
	 }
	  
	 public void StopSE(){
	  foreach(AudioSource source in SEsources){
	   source.Stop();
	   source.clip = null;
	  } 
	 }
	  
	  
	 public void PlayVoice(int index){
	  if( 0 > index || Voice.Length <= index ){
	   return;
	  }
	  foreach(AudioSource source in VoiceSources){
	   if( false == source.isPlaying ){
	    source.clip = Voice[index];
	    source.Play();
	    return;
	   }
	  }
	 }
	 public void StopVoice(){
	  foreach(AudioSource source in VoiceSources){
	   source.Stop();
	   source.clip = null;
	  } 
	 }
	}

	// 音量クラス
	[Serializable]
	public class SoundVolume{
	 public float BGM = 1.0f;
	 public float Voice = 1.0f;
	 public float SE = 1.0f;
	 public bool Mute = false;
	  
	 public void Init(){
	  BGM = 1.0f;
	  Voice = 1.0f;
	  SE = 1.0f;
	  Mute = false;
	 }
	}
}
