﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
    public enum MatorDirection{
        DIRECTION_V,
        DIRECTION_H
    }
    public class Mator :  ThpMonoBehaviour {
        public float matorLength = 1.0f;
        public float minLength = 0.0f;
        public int param=0;
        public int maxParam=1;
        public UISprite matorSprite=null;
        public UILabel paramLabel;
        public UILabel maxParamLabel;
        public MatorDirection direction = MatorDirection.DIRECTION_H;
        private int viewParam;
        public int matorSpeed = 10;
        const int baseSpeed = 100;

    	// Use this for initialization
    	void Start () {

    	}
        public void initParam(int rParam,int rMaxParam){
            param = rParam;
            viewParam = rParam;
            maxParam = rMaxParam;
        }
    	// Update is called once per frame
    	void Update () {
            int addParam = (maxParam * matorSpeed) / baseSpeed;
            if (param < 0) {
                param = 0;
            }
            if (param < viewParam) {
                viewParam = viewParam - addParam;
                if (param > viewParam) {
                    viewParam = param;
                }
            } else if (param > viewParam) {
                viewParam = viewParam + addParam;
                if (param < viewParam) {
                    viewParam = param;
                }
            }

            float length = (viewParam * matorLength) / maxParam;
            if(paramLabel != null) paramLabel.text = viewParam.ToString();
            if(maxParamLabel != null) maxParamLabel.text = maxParam.ToString();
            if (direction == MatorDirection.DIRECTION_V) {
                matorSprite.height = Mathf.FloorToInt(length);
            } else {
                matorSprite.width = Mathf.FloorToInt(length);
            }
    	}
    }
}