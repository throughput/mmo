// #define ENABLE_WEBVIEWNEWS
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Procurios.Public;
using System.IO;
using System;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

namespace Throughputjp{
    public class AccessController : SingletonMonoBehaviour<AccessController> {
        public enum ErrorNo{
    	    SESSION_ERROR = 1001,
    	    STATION_NOTHING_ERROR = 1002,
    	    SERVER_TIME_OUT = 1003,
    	    SERVER_ERROR = 1004,
    	};
        //    	private WebViewObject webViewObject;	
    	public GameObject NowLoadingScreen;
    	private string onePassword;
    	private bool isNetWorkError=false;

    	//	public WWWForm form = new WWWForm();
    	private WWW www;
    	private DateTime serverTime=DateTime.Now;
    	private DateTime localBaseTime=DateTime.Now;

    	// will be set to some session id after login
    	private Dictionary<string,string> session_ident = new Dictionary<string,string>();

    	// and some helper functions and properties
    	public void ClearSessionCookie(){
    		session_ident.Clear ();
    	}
    	
    	public void SetSessionCookie(string s){
    	    session_ident["Cookie"] = s;
    	}
    	
    	public Dictionary<string,string> SessionCookie{
    		get { return session_ident; }
    	}

    	public string GetSessionCookie(){
    	    return session_ident["Cookie"];
    	}
    	 
    	public bool SessionCookieIsSet{
    	    get { return session_ident.ContainsKey("Cookie"); }
    	}
    	public enum ReturnCode : int { Failed=-1, Error=0, OK=1 }
    	public delegate void OnNetResult(ReturnCode code, string result);	



//    	public void OpenWebView(string Url,int left,int top,int right,int bottom){
//    		string cb0=((TextAsset)Resources.Load ("cb0",typeof(TextAsset))).text;
//    		string cb1=((TextAsset)Resources.Load ("cb1",typeof(TextAsset))).text;
//    		webViewObject =
//    			(new GameObject("WebViewObject")).AddComponent<WebViewObject>();
//    		webViewObject.Init((msg)=>{
//    			Debug.Log(string.Format("CallFromJS[{0}]", msg));
//    		});
//
//    		webViewObject.LoadURL(Url);
//    		webViewObject.SetMargins(left,top,right,bottom);
//    		webViewObject.SetVisibility(true);
//
//    		switch (Application.platform) {
//    		case RuntimePlatform.OSXEditor:
//    		case RuntimePlatform.OSXPlayer:
//    		case RuntimePlatform.IPhonePlayer:
//    			webViewObject.EvaluateJS(cb0);
//    			break;
//    		}
//    		webViewObject.EvaluateJS(cb1);
//    	}

//    	public void CloseWebView(){
//    		Debug.Log("CloseWebView");
//    #if ENABLE_WEBVIEWNEWS
//    		#if UNITY_EDITOR
//    		// SetVisibility(false) makes Unity Editor crash! :(
//    		webViewObject.enabled = false;
//    		#else 
//    		Destroy(webViewObject);
//    //		webViewObject.SetVisibility(false);
//    		#endif
//    #endif
//    	}

    	public DateTime ServerBaseTime{
    		get{
    			TimeSpan tSpan=DateTime.Now.Subtract(localBaseTime);
    			return serverTime.Add(tSpan);
    		}
    	}
 
        public WWW Access(string url,Dictionary<string,string> formParams,string hostDomain){
    		WWWForm form=new WWWForm(); 
    		form.headers["Host"] = hostDomain;
    		Debug.Log ("Access Url"+url);
    		foreach(KeyValuePair<string,string> formParam in formParams){
    			Debug.Log ("Key:"+formParam.Key+",Value:"+formParam.Value);
    			form.AddField(formParam.Key,formParam.Value);
    		}
    		if (AccessController.Instance.SessionCookie.ContainsKey ("Cookie")) {
    			Debug.Log (AccessController.Instance.SessionCookie ["Cookie"]);
    		}
    		Debug.Log (form.data);
    		Debug.Log (url);
    		WWW www=new WWW(url,form.data,AccessController.Instance.SessionCookie);
    		return www;
    //		string errmsg;
    //		if (!string.IsNullOrEmpty(www.error) || string.IsNullOrEmpty(www.text))
    //    	{
    //       		errmsg = "Network communication error.";
    //    	}
    //
    //        if (www.text != string.Empty && AccessController.Instance.SessionCookieIsSet)
    //        {
    //			yield break;
    //        }
    //        else if (www.text == string.Empty || !AccessController.Instance.SessionCookieIsSet)
    //        {
    //            errmsg = "Invalid login name or password.";
    //            // my server sometimes sends "0some_explenation", therefore this next line
    //            if (www.text.Length > 1) errmsg = www.text.Substring(1);
    //			yield break;
    //        }
    //        else
    //        {
    //            // this should only happen during development since there was an unexpected
    //            // value at [0], not 0 or 1 as expected, so probably some script error
    //            errmsg = "Network communication error.";
    //			yield break;
    //        }
    //			
    //		yield return www;
    	}

    	private string zipEx(byte[] zipdata){
    		MemoryStream zipMemStream=new MemoryStream(zipdata);
    		ZipInputStream zis = new ZipInputStream(zipMemStream);	
    		MemoryStream memStream= new MemoryStream();
    		string ex=string.Empty;
    //		ICSharpCode.SharpZipLib.Zip.ZipEntry ze; // Not Use
    //		while ((ze = zis.GetNextEntry()) != null)
    		while (zis.GetNextEntry()!= null)
    		{
    			byte[] buffer = new byte[2048];
    	        int len;
    	        while ((len = zis.Read(buffer, 0, buffer.Length)) > 0)
    	        {
    	            memStream.Write(buffer, 0, len);
    	        }
    			byte[] test=memStream.GetBuffer();
    			ex=Encoding.UTF8.GetString(test);
    		}
    		return ex;
    	}

    }
}
