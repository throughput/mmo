﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	public enum InputDialogResult
	{
		INPUT_RESULT_YES=1,
		INPUT_RESULT_NO=-1,
	}

	public class InputBox : SingletonMonoBehaviour<InputBox> {
        public string DialogYesButton = "Button_YES";
        public string DialogNoButton = "Button_NO";

		public System.Action<InputDialogResult,string> m_callBack;
		public Dictionary<string,UIButton> m_buttons;
		public UILabel dialogMessage;
		public GameObject dialogMessageObj;
		public UIInput inputText;

		new void Awake(){
			base.Awake ();
			m_buttons=new Dictionary<string,UIButton>();
			foreach(UIButton button in gameObject.transform.GetComponentsInChildren<UIButton>()){
				m_buttons.Add(button.gameObject.name,button);
                EventDelegate callback = new EventDelegate (this, "pushButton");
                callback.parameters [0].value = button.gameObject;
                button.onClick.Add (callback);
                button.enabled = true;
			}
            foreach (UIButtonMessage uiButtonMessage in gameObject.transform.GetComponentsInChildren<UIButtonMessage>()) {
                uiButtonMessage.target = gameObject;
                uiButtonMessage.functionName = "pushButton";
            }

			// 入力文字とテキスト
			inputText = gameObject.transform.GetComponentInChildren<UIInput> ();
			dialogMessage = dialogMessageObj.GetComponent<UILabel> ();
			gameObject.SetActive(false);
		}
		// Use this for initialization
		void Start () {
		}
		
		// Update is called once per frame
		void Update () {
		
		}
        public void viewMessage(string message,string formatText,System.Action<InputDialogResult,string> rCallBack=null){
			m_callBack=rCallBack;
			gameObject.SetActive(true);
			dialogMessage.text=message;
            inputText.label.text = formatText;
		}
		void pushButton(GameObject obj){
			Debug.Log (obj.name);
			gameObject.SetActive(false);
            InputDialogResult dialogResult=InputDialogResult.INPUT_RESULT_YES;
            if(obj.name.Equals(DialogYesButton)) dialogResult=InputDialogResult.INPUT_RESULT_YES;
            if(obj.name.Equals(DialogNoButton)) dialogResult=InputDialogResult.INPUT_RESULT_NO;
            if(m_callBack!=null) m_callBack(dialogResult,inputText.label.text);
		}
	}
}