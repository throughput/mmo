using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System;
namespace Throughputjp{
	public class Crypt {
		public static string EncryptRJ256(string prm_key, string prm_iv, string prm_text_to_encrypt)
		{
			string sToEncrypt = prm_text_to_encrypt;
			Debug.Log ("encrlypt:"+prm_text_to_encrypt);
			Debug.Log ("iv:"+prm_iv);
			RijndaelManaged myRijndael = new RijndaelManaged();
			myRijndael.Padding = PaddingMode.PKCS7;
			myRijndael.Mode = CipherMode.CBC;
			myRijndael.KeySize = 256;
			myRijndael.BlockSize = 256;

			byte[] encrypted;
			byte[] toEncrypt;

			byte[] key = new byte[0];
			byte[] IV = new byte[0];

			key = System.Text.Encoding.UTF8.GetBytes(prm_key);
			IV = Convert.FromBase64String(prm_iv);
				
			ICryptoTransform encryptor = myRijndael.CreateEncryptor(key, IV);

			MemoryStream msEncrypt = new MemoryStream();
			CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
			
			toEncrypt = System.Text.Encoding.UTF8.GetBytes(sToEncrypt);

			csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
			csEncrypt.FlushFinalBlock();
			encrypted = msEncrypt.ToArray();
			
			return (Convert.ToBase64String(encrypted));
		}
		
		public static byte[] DecryptRJ256(string prm_key, string prm_iv, string prm_text_to_decrypt)
		{
			string sEncryptedString = prm_text_to_decrypt;
			RijndaelManaged myRijndael = new RijndaelManaged();

			myRijndael.Mode = CipherMode.CBC;
			myRijndael.Padding = PaddingMode.PKCS7;
			myRijndael.KeySize = 256;
			myRijndael.BlockSize = 256;

			byte[] key = new byte[0];
			byte[] IV = new byte[0];

			key = System.Text.Encoding.UTF8.GetBytes(prm_key);
			IV = System.Text.Encoding.UTF8.GetBytes(prm_iv);

			ICryptoTransform decryptor = myRijndael.CreateDecryptor(key, IV);
			Debug.Log (sEncryptedString);
			byte[] sEncrypted = Convert.FromBase64String(sEncryptedString);
			byte[] fromEncrypt = new byte[sEncrypted.Length];
			Array.Clear(fromEncrypt,0,sEncrypted.Length);
			MemoryStream msDecrypt = new MemoryStream(sEncrypted);
			CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

			csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

			return fromEncrypt;
		}
		public static string Rot13(string value)
		{
		    char[] array = value.ToCharArray();
		    for (int i = 0; i < array.Length; i++)
		    {
			int number = (int)array[i];

			if (number >= 'a' && number <= 'z')
			{
			    if (number > 'm')
			    {
				number -= 13;
			    }
			    else
			    {
				number += 13;
			    }
			}
			else if (number >= 'A' && number <= 'Z')
			{
			    if (number > 'M')
			    {
				number -= 13;
			    }
			    else
			    {
				number += 13;
			    }
			}
			array[i] = (char)number;
		    }
		    return new string(array);
		}	
	}
}