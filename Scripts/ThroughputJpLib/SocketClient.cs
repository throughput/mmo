﻿#define NETWORK_DEBUG

using UnityEngine;
using System;
using System.Threading;
using LostPolygon.System.Net;
using LostPolygon.System.Net.Sockets;

namespace Throughputjp{
	class SocketClient : SingletonMonoBehaviour<SocketClient> {
		public TcpClient objSck;
		public NetworkStream objStm;
		private Thread ThreadRead;
		public byte[] recvData;
		enum PROC{
			PROC_CONNECT,
			PROC_CONNECT_WAIT,
			PROC_WAIT,
		}
		PROC proc;
		void Start(){
			try
			{
				// Creating a new TcpClient instance
				objSck = new TcpClient();
				recvData=new byte[]{};
			}
			catch (Exception e)
			{
				Debug.Log(e.ToString());
				throw;
			}
		}
		void OnDisable(){
			closed();
		}
//	//	private WebSocket ws;
//	//	private string address="ws://127.0.0.1:8080";
//		void OnGUI() {
//	//		address = GUILayout.TextArea (address, 100);
//			if (GUILayout.Button("Connect", GUILayout.MinHeight(80))) {
//				connect();
//			}
//			
//			if (GUILayout.Button("Send", GUILayout.MinHeight(80))) {
////				send();
//			}
//			
//			if (GUILayout.Button("Close", GUILayout.MinHeight(80))) {
//				closed();
//			}
//			GUI.TextField(new Rect(100, 10, 100, 100), RecvText);
//		}

		private void connect()
		{
			Debug.Log ("Connect");
			// ソケット接続
	//		objSck.BeginConnect("dwsrv.com", 10101,OnConnect,objSck);
//			objSck.Connect ("192.168.11.6", 10101);
			try {
				objSck.Connect ("dwsrv.com", 10101);
				// ソケットストリーム取得
				objStm = objSck.GetStream();
			} catch (SocketException ex) {
				Debug.Log(string.Format("<color=red>Error at TCP connection: {0}</color>", ex.Message));
			} catch (ObjectDisposedException) {
				// The listener was Stop()'d, disposing the underlying socket and
				// triggering the completion of the callback. We're already exiting,
				// so just return.
			} catch (Exception ex) {
				// Some other error occured. This should not happen
				Debug.LogException(ex);
				Debug.Log(string.Format("<color=red>An error occured: {0}</color>", ex.Message));
			}
		}
		// Called when a connection to a server is established
		private void OnConnect(IAsyncResult asyncResult) {
			// Retrieving TcpClient from IAsyncResult
			Debug.Log("OnConnect");
			proc=PROC.PROC_WAIT;
			TcpClient tcpClient = (TcpClient) asyncResult.AsyncState;
			if (!tcpClient.Connected) {
				Debug.Log ("Connect Fail");
				return;
			}
			try {
				// Finish the connection procedure
				tcpClient.EndConnect(asyncResult);
#if NETWORK_DEBUG
				Debug.Log(string.Format("Connection to {0} successfull.", tcpClient.Client.RemoteEndPoint));
#endif				
				// Start data read thread
				ThreadRead = new Thread(() => ThreadReadProcedure(tcpClient));
				ThreadRead.Start();
			} catch (SocketException ex) {
				Debug.Log(string.Format("<color=red>Error at TCP connection: {0}</color>", ex.Message));
			} catch (ObjectDisposedException) {
				// The listener was Stop()'d, disposing the underlying socket and
				// triggering the completion of the callback. We're already exiting,
				// so just return.
			} catch (Exception ex) {
				// Some other error occured. This should not happen
				Debug.LogException(ex);
				Debug.Log(string.Format("<color=red>An error occured: {0}</color>", ex.Message));
			}
		}
		// Receives data until connection is interrupted
		private void ThreadReadProcedure(TcpClient tcpClient) {
			// A string that will contain the received data
			string data = string.Empty;
			// A temporary byte[] buffer for receiving data
			byte[] buffer = new byte[256];
			// Number of bytes received last time
			int receivedBytes;
			
			// Receive the data until the end
			while ((receivedBytes = tcpClient.Client.Receive(buffer)) != 0) {
				// Add newly-received data to a string
				data += System.Text.Encoding.UTF8.GetString(buffer, 0, receivedBytes);
			}
			
			// No more data to read, display the quote
			Debug.Log(string.Format("Quote Of The Day:\r\n<b><size=15>{0}</size></b>", data));
			Debug.Log(string.Format("Disconnected from {0}.", tcpClient.Client.RemoteEndPoint));
		}

		private void closed()
		{
			// ソケットクローズ
			if(objStm!=null) objStm.Close();
			if(objSck!=null) objSck.Close();
		}

		public void send(Byte[] dat)
		{
#if NETWORK_DEBUG
			Debug.Log("send data:"+BitConverter.ToString(dat));
#endif
			try {
				objStm.Write(dat, 0, dat.GetLength(0));
			} catch (SocketException ex) {
				Debug.Log(string.Format("<color=red>Error at TCP connection: {0}</color>", ex.Message));
			} catch (ObjectDisposedException) {
				// The listener was Stop()'d, disposing the underlying socket and
				// triggering the completion of the callback. We're already exiting,
				// so just return.
			} catch (Exception ex) {
				// Some other error occured. This should not happen
				Debug.LogException(ex);
				Debug.Log(string.Format("<color=red>An error occured: {0}</color>", ex.Message));
				// ソケットを閉じる
				closed();
			}
		}
		public bool IsConnect(){
            return (proc==PROC.PROC_WAIT);
		}
		private void Update()
		{
			switch (proc) {
			case PROC.PROC_CONNECT:
				connect();
				proc=PROC.PROC_CONNECT_WAIT;
				break;
			case PROC.PROC_CONNECT_WAIT:
				if(objSck.Connected == true){
					proc=PROC.PROC_WAIT;
				}
				break;
			case PROC.PROC_WAIT:
				// ソケット受信
				if (objSck.Available > 0)
				{
					Byte[] dat = new Byte[objSck.Available];
					objStm.Read(dat, 0, dat.GetLength(0));
#if NETWORK_DEBUG
					Debug.Log("recv data:"+BitConverter.ToString(dat));
#endif
					recvData=dat;
				}
				break;
			}
		}
	}
}