using UnityEngine;
using System;
using System.Collections;
using Procurios.Public;
using System.Collections.Generic;

namespace Throughputjp{
	public class TextTable : SingletonMonoBehaviour<TextTable> {
        private Dictionary<string,Dictionary<string,string>> tables;
        private Dictionary<string,List<string>> lists;
		
		new void Awake (){
			base.Awake();
            Debug.Log ("TextTable");
			GameObject[] obj = GameObject.FindGameObjectsWithTag("menuResources");
			if( obj.Length > 1 ){
				Destroy(gameObject);
			}else{
				DontDestroyOnLoad(gameObject);
			}
            tables = new Dictionary<string,Dictionary<string,string>> ();
            lists = new Dictionary<string,List<string>> ();
            foreach (TextAsset textAsset in Resources.LoadAll<TextAsset>("TableText")) {
				if (textAsset.name.StartsWith("list_")){
                    List<string> stringList = new List<string> ();
                    foreach (string data in (ArrayList)JSON.JsonDecode(textAsset.text)) {
                        stringList.Add (data);
                    }
                    lists.Add (textAsset.name,stringList);		
				}
                if (textAsset.name.StartsWith("table_")){
                    Debug.Log (textAsset.text);
                    Dictionary<string,string> stringTable= new Dictionary<string,string> ();
                    Hashtable table = (Hashtable)JSON.JsonDecode (textAsset.text);
                    foreach(string key in table.Keys){
                        stringTable.Add (key,(string)table[key]);		
                    }
                    tables.Add (textAsset.name, stringTable);
	            }
	        }
		}

        public Dictionary<string,Dictionary<string,string>> Table{
			get{return tables;}
		}
        public Dictionary<string,List<string>> List{
			get{return lists;}
		}
	}
}
