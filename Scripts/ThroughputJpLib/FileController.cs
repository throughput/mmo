#define FILE_ENCRYPT
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
namespace Throughputjp{
	public class FileController: SingletonMonoBehaviour<FileController> {
	#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
		const string password="ezLojK9bRVJgWH8&UEPch:.r.EvVWdnY";
	#else
		const string password="Tb&aATw3rXopasMgT>G&RV6P4TmPEmZE";
	#endif

		// 書き込み
	    public void write(string filename,string text){
	 		Debug.Log ("filename"+filename+"/text:"+text);
			string filePath=Application.persistentDataPath+"/";
			string fileFullPath=getFilePath(filename);

			if(!File.Exists(filePath)){
				Directory.CreateDirectory(filePath);
				File.Create (fileFullPath).Close ();
			}
			//write
	    	StreamWriter sw = new StreamWriter(fileFullPath);
	#if FILE_ENCRYPT
			string guid_str=Guid.NewGuid().ToString().Replace("-",String.Empty).Substring(0,32);
			while(guid_str.Length<32){
				guid_str=Guid.NewGuid().ToString().Replace("-",String.Empty).Substring(0,32);
			}
			System.Text.Encoding enc=System.Text.Encoding.GetEncoding("UTF-8");
			string iv=Convert.ToBase64String(enc.GetBytes(guid_str));

			string encryptData=Crypt.EncryptRJ256(Crypt.Rot13(password),iv,text);
			sw.Write(guid_str+encryptData);
	#else
			sw.Write(text);
	#endif
			sw.Flush();
	    	sw.Close(); 
	    }
	 
	    // FIle Exists
		public bool exists (string filename) {
			string filePath=Application.persistentDataPath+"/";
			string fileFullPath=filePath+filename;
			Debug.Log ("exists:"+fileFullPath);
			return File.Exists(fileFullPath);
		}
		private static string getFilePath(string filename){
			string localFilemame=filename;;
	#if FILE_ENCRYPT
			localFilemame="e"+filename;
	#endif
			string filePath=Application.persistentDataPath+"/";
			string fileFullPath=filePath+localFilemame;
			return fileFullPath;
		}
		// 
	    public string read (string filename) {
	//		string filePath=Application.persistentDataPath+"/"; // Not Use
			string fileFullPath=getFilePath(filename);
			Debug.Log (fileFullPath);
	        string result=string.Empty;
			if(File.Exists(fileFullPath)){
				FileInfo fi = new FileInfo(fileFullPath);
				StreamReader sr = new StreamReader(fi.OpenRead());
				while( sr.Peek() != -1 ){
		            result=result+sr.ReadLine();
		        }
		        sr.Close();
			}
			if(result.Length==0){
				return result;
			}
			string decryptResult;
	#if FILE_ENCRYPT
			string iv=result.Substring(0,32);
	//		System.Text.Encoding enc=System.Text.Encoding.GetEncoding("UTF-8"); // Not Use
			string encrypt=result.Substring(32);
			byte[] resultByte=Crypt.DecryptRJ256(Crypt.Rot13(password),iv,encrypt);
			decryptResult=System.Text.Encoding.UTF8.GetString(resultByte);
	#else
			decryptResult=result;
	#endif
			Debug.Log ("loadData:"+decryptResult);
			return decryptResult;
	    }

		public static Dictionary<K,V> HashtableToDictionary<K,V> (Hashtable table)
		{
			Dictionary<K,V> dic =new Dictionary<K, V> ();
			foreach(DictionaryEntry de in table){
				Debug.Log ((double)de.Value);
				dic[(K)de.Key]=(V)de.Value;
			}
			return dic;
		}
		public static bool fileDelete(string src){
			string srcFullPath=getFilePath(src);
			try{
				File.Delete (srcFullPath);
			}catch(Exception exc){
				Debug.Log (exc.Message);
				return false;
			}
			return true;
		}
		public static bool fileCopy(string src,string dst){
			string srcFullPath=getFilePath(src);
			string dstFullPath=getFilePath(dst);
			try{
				File.Copy(srcFullPath,dstFullPath);
			}catch(Exception exc){
				Debug.Log (exc.Message);
				return false;
			}
			return true;
		}
	}
}
