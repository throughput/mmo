﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	public enum DialogForm
	{
		DIALOG_YES_NO,
		DIALOG_OK,
	}

	public enum DialogResult
	{
		RESULT_YES=1,
		RESULT_OK=0,
		RESULT_NO=-1,
	}

	public class DialogBox : SingletonMonoBehaviour<DialogBox> {
        public string DialogYesButton = "Button_YES";
        public string DialogNoButton = "Button_NO";
        public string DialogOkButton = "Button_OK";

        public System.Action<DialogResult> m_callBack;
		public Dictionary<string,UIButton> m_buttons;
		public UILabel dialogMessage;

		new void Awake(){
			base.Awake ();
			m_buttons=new Dictionary<string,UIButton>();
			foreach(UIButton button in gameObject.transform.GetComponentsInChildren<UIButton>()){
				m_buttons.Add(button.gameObject.name,button);
                EventDelegate callback = new EventDelegate (this, "pushButton");
                callback.parameters [0].value = button.gameObject;
                button.onClick.Add (callback);
                button.enabled = true;
			}
            foreach (UIButtonMessage uiButtonMessage in gameObject.transform.GetComponentsInChildren<UIButtonMessage>()) {
                uiButtonMessage.target = gameObject;
                uiButtonMessage.functionName = "pushButton";
            }

			dialogMessage=gameObject.transform.GetComponentInChildren<UILabel>();
            Debug.Log (dialogMessage.gameObject.name);
			gameObject.SetActive(false);
		}
		// Use this for initialization
		void Start () {
		}
		
		// Update is called once per frame
		void Update () {
		
		}
		public void viewMessage(DialogForm rDialogForm,string message,System.Action<DialogResult> rCallBack=null){
			m_callBack=rCallBack;
			gameObject.SetActive(true);
			Debug.Log (message);
			dialogMessage.text=message;
			switch(rDialogForm){
			case DialogForm.DIALOG_YES_NO:
                m_buttons[DialogYesButton].gameObject.SetActive(true);
                m_buttons[DialogNoButton].gameObject.SetActive(true);
                m_buttons[DialogOkButton].gameObject.SetActive(false);
				break;
			case DialogForm.DIALOG_OK:
                m_buttons[DialogYesButton].gameObject.SetActive(false);
                m_buttons[DialogNoButton].gameObject.SetActive(false);
                m_buttons[DialogOkButton].gameObject.SetActive(true);
				break;
			}
		}
		void pushButton(GameObject obj){
			Debug.Log (obj.name);
			gameObject.SetActive(false);
			DialogResult dialogResult=DialogResult.RESULT_OK;
            if(obj.name.Equals(DialogYesButton)) dialogResult=DialogResult.RESULT_YES;
            if(obj.name.Equals(DialogOkButton)) dialogResult=DialogResult.RESULT_OK;
            if(obj.name.Equals(DialogNoButton)) dialogResult=DialogResult.RESULT_NO;
			if(m_callBack!=null) m_callBack(dialogResult);
		}
	}
}