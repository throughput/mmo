﻿using UnityEngine;
using System;
using System.Threading;

namespace Throughputjp{
	class WebSocketClient : SingletonMonoBehaviour<WebSocketClient> {
		private MobileWebSocket webSocket;

		private Thread ThreadRead;
		public byte[] recvData;
		enum PROC{
			PROC_CONNECT,
			PROC_WAIT,
		}
		PROC proc;
		void Start(){
			try
			{
				// 接続開始
				connect();
				recvData=new byte[]{};
			}
			catch (Exception e)
			{
				Debug.Log(e.ToString());
				throw;
			}
		}

		private void connect()
		{
			Debug.Log ("Connect");
			// ソケット接続
			this.webSocket = GetComponent<MobileWebSocket>();
			this.webSocket.WebSocketUri="wss://localhost:8080";
		}

		private void closed()
		{
			// ソケットクローズ
		}

		public void send(Byte[] dat)
		{
			webSocket.SendBinaryMessage (dat);
		}
		
		private void Update()
		{
	        if (webSocket.HasIncomingMessages) {
	            var messages = webSocket.TakeMessages();
	            int length = messages.Length;
	            for (int i = 0; i < length; i++) {
	                var message = messages[i];
	                if (message is byte[]) {
						recvData=message as byte[];
						Debug.Log("New binary message of length " + recvData.Length);
	                }
	                else if (message is string) {
	                    string text = message as string;
						Debug.Log (text);
	                }
	            }
	        }
        }
	}
}