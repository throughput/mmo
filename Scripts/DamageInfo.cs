﻿using UnityEngine;
using System.Collections;
namespace Throughputjp{
	namespace ThpMmo{
        public enum MagicKind{
            MAGIC_FIRE    = 1,
            MAGIC_WATER   = 2,
            MAGIC_WIND    = 3,
            MAGIC_ICE     = 4,
            MAGIC_EARTH   = 5,
            MAGIC_THUNDER = 6,
            MAGIC_LIGHT   = 7,
            MAGIC_DARK    = 8
        };
        public class AttackInfo{
            public MonsterInfo monsterInfo = new MonsterInfo();
            public CharacterInfo characterInfo = new CharacterInfo();
            public MagicKind magicKind = MagicKind.MAGIC_FIRE;
            public int magicLevel;
            public int damage;
        }
		public class DamageInfo{
            public MonsterInfo monsterInfo = new MonsterInfo();
            public CharacterInfo characterInfo = new CharacterInfo();
            public MagicKind magicKind = MagicKind.MAGIC_FIRE;
            public int magicLevel;
            public int damage;
		}
        public class BattleResult{
            public CharacterInfo characterInfo = new CharacterInfo();
            public int getExp;
            public int getGold;
            public int levelUp;
            public TreasureInfo treasureInfo = new TreasureInfo();
            public ItemInfo dropItem;
        }
	}
}