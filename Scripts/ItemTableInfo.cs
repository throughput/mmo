﻿namespace Throughputjp{
	namespace ThpMmo{
		public class ItemTableInfo{
			public string name = "";
            public string file = "";
			public int atk = 0;
			public int speed = 0;
			public int stamina = 0;
            public EquipKind kind = 0;
		}
        public class StoneTableInfo{
            public string name = "";
            public string file = "";
        }
	}
}