﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Throughputjp{
    namespace ThpMmo{
        public class MenuController : SingletonMonoBehaviour<MenuController> {
            public enum ChangeMailScreen{
                MAIL_LIST,
                ITEM_LIST,
                FRIEND_LIST,
            };

        	GameObject controller;
            public GameObject touchLayer;
            public GameObject chatWindow;
            Dictionary<string,GameObject> uiWindow = new Dictionary<string,GameObject>();
            Dictionary<string,GameObject> uiLists = new Dictionary<string,GameObject>();
            Dictionary<string,GameObject> uiMsg = new Dictionary<string,GameObject>();
            Dictionary<string,GameObject> popupEquipSlot = new Dictionary<string,GameObject>();
            Dictionary<string,GameObject> tapBarrier = new Dictionary<string,GameObject>();
            Dictionary<string,UILabel> statusLabel;
            Dictionary<string,GameObject> statusObject = new Dictionary<string, GameObject>();

            string word;
        	UILabel msgTitle;
        	UILabel msgText;
            UILabel pageTitleLabel;
            Stack<GameObject> openObject = new Stack<GameObject> ();

        	int uiLength;
        	int uiListLength;
        	int subMenuLength;
            private int selectedFriendId;
            string m_newName;
            public GameObject newMailIcon;
            const int MAIL_TITLE_LENGTH = 15;
            const int NOT_EQUIPMENT = -1;
            private ItemInfo selectedJewelItemInfo;

            void Awake () {
                Debug.Log (gameObject.name);

                uiLists = getGameObjects("MenuList");
                uiMsg = getGameObjects ("MsgWindow");
                uiWindow = getGameObjects ("MenuWindow");
                tapBarrier = getGameObjects ("MsgTapBarrier");
                foreach (UISprite sprite in uiMsg["MsgItemStatus"].GetComponentsInChildren<UISprite>()) {
                    if (sprite.name.StartsWith ("Slot")) {
                        popupEquipSlot.Add (sprite.gameObject.name, sprite.gameObject);
                    }
                }
                pageTitleLabel = uiWindow ["T_PageTitle"].GetComponentInChildren<UILabel> ();
                foreach(Transform obj in uiWindow["M_Status"].transform)
                {
                    switch (obj.name) {
                    case "Status":
                        statusLabel = getLabels (obj);
                        foreach (Transform objStatus in obj) {
                            statusObject.Add (objStatus.name, objStatus.gameObject); 
                        }
                        break;
                    }
                }
                newMailIcon.SetActive (false);
                // この処理は最後に行うこと
                CloseObjectGroup(uiLists);
                CloseObjectGroup(uiMsg);
                CloseObjectGroup(uiWindow);
                CloseObjectGroup(tapBarrier);
            }
            public void setMailCount(int mailCount){
                if (mailCount > 0) {
                    newMailIcon.SetActive (true);
                    UILabel countLable = newMailIcon.GetComponentInChildren<UILabel> ();
                    countLable.text = mailCount.ToString ();
                } else {
                    newMailIcon.SetActive (false);
                }
            }
            private Dictionary<string,UILabel> getLabels(Transform parentObj){
                Dictionary<string,UILabel>labels = new Dictionary<string,UILabel>();
                foreach (Transform obj in parentObj) {
                    UILabel label = obj.gameObject.GetComponentInChildren<UILabel>();
                    if (label != null) {
                        labels.Add (obj.name, label);
                    }
                }
                return labels;
            }
            private Dictionary<string,GameObject> getGameObjects(string tagName){
                Debug.Log (tagName);
                Dictionary<string,GameObject> dic = new Dictionary<string,GameObject> ();
                foreach (GameObject obj in GameObject.FindGameObjectsWithTag(tagName)) {
                    dic.Add (obj.name, obj);
                }
                return dic;
            }
            private void CloseObjectGroup(Dictionary<string,GameObject> targetDic){
                foreach(KeyValuePair<string,GameObject> obj in targetDic){
                    obj.Value.SetActive(false);
                }
            }
        	void Start () {
        	}


        //----Menu Button Action------------------------------------------------
        	public void MenuButton (string word) {
                Debug.Log ("MenuButton:" + word);
        		switch(word){
        			
        		case "B_MenuOpen":
        			break;
        			
        		case "B_Close":
                    CloseMenu ();
                    SocketNetwork.Instance.sendSetEquip(PlayerInfo.Instance.characterInfo);
        			break;

                case "B_Dungeon":
                    CloseMenu ();
                    openMenu ("M_Dungeon");
        			break;
        			
                case "B_Cave":
                    StartCoroutine (HttpController.Instance.DownLoadDungeon (MoveDungeon, 1));
                    CloseMenu ();
        			break;
        			
                case "B_Premium":
                    CloseMenu ();
                    MainController.Instance.MoveGachaRoom (GachaController.GachaKind.GACHA_GOLD);
        			break;

                case "B_Normal":
                    CloseMenu ();
                    MainController.Instance.MoveGachaRoom (GachaController.GachaKind.GACHA_BRONZE);
                    break;

                case "B_Tower":
                    CloseMenu ();
                    MainController.Instance.MoveGachaRoom (GachaController.GachaKind.GACHA_BRONZE);
                    break;

                case "B_Mail":
                    StartCoroutine (HttpController.Instance.RecvMail (openMailList));
                    break;

                case "B_Inventory":
                    CloseMenu ();
                    openMenu ("M_Status");

                    // ユーザーステータス
                    statusLabel ["Name"].text = PlayerInfo.Instance.characterInfo.name;
                    equipment (PlayerInfo.Instance.characterInfo.swordID);
                    equipment (PlayerInfo.Instance.characterInfo.armorID);
                    equipment (PlayerInfo.Instance.characterInfo.shieldID);
                    // アイテム表示
                    setItemList (uiLists ["InventoryList"],ItemWindowKind.ITEM_WINDOW_EQUIP);
        			break;
        			
                case "B_Shop":
                    CloseMenu ();
                    openMenu ("M_Shop");
                    ShopController.Instance.Open();
//        			subMenus[2].SetActive(true);
        			break;

                case "B_Gatya":
                    CloseMenu ();
                    openMenu ("M_TreasureRoom");
                    break;
        			
        		case "B_Kakin":
        			break;
        			
                case "B_Friend":
                    getFriendList (openFriendList);
        			break;
                case "B_Option":
                    break;
                case "B_NameTitleChange":
                    openChageNameEdit ();
                    break;
        		default:
        			break;
        		}
        		
        	}
            private void getFriendList(Action callback){
                StartCoroutine (HttpController.Instance.GetFriendList (callback));
            }
            private void setMailFriendList(){
                PlayerInfo.Instance.friendList.Clear ();
                while (HttpController.Instance.friendList.Count>0) {
                    PlayerInfo.Instance.friendList.Add (HttpController.Instance.friendList.Dequeue ());
                }
                uiLists ["MultiList"].SetActive (true);
                reloadFriendList (uiLists ["MultiList"]);
                uiLists ["MultiList"].SetActive (false);
            }

            private void openFriendList(){
                CloseMenu ();
                // アイテム選択リスト生成
                uiLists ["ItemSelectList"].SetActive (true);
                setItemList (uiLists ["ItemSelectList"], ItemWindowKind.ITEM_WINDOW_ATTACH);
                uiLists ["ItemSelectList"].SetActive (false);

                PlayerInfo.Instance.friendList.Clear ();
                while (HttpController.Instance.friendList.Count>0) {
                    PlayerInfo.Instance.friendList.Add (HttpController.Instance.friendList.Dequeue ());
                }
                openMenu ("M_Friend");
                foreach (UILabel label in uiWindow["M_Friend"].GetComponentsInChildren<UILabel>()){
                    switch(label.name){
                    case "FriendCode":
                        label.text = PlayerInfo.Instance.characterInfo.friendCode;
                        break;
                    }
                }
                reloadFriendList (uiLists ["FriendList"]);
            }
            private void setItemList(GameObject listObject,ItemWindowKind itemWindowKind){
                UIGrid grid = listObject.GetComponentInChildren<UIGrid> ();
                DestroyList (grid.gameObject);
                int pos = 0;
                for (int i = 0; i<PlayerInfo.Instance.myItemList.Count; ++i) {
                    ItemInfo itemInfo = PlayerInfo.Instance.myItemList[i];
                    if (itemInfo.id == 0) {
                        continue;
                    }
                    GameObject listObj = Instantiate ((GameObject)Resources.Load ("Prefab/List/L_InventoryMini"));
                    UILabel countLabel = listObj.GetComponentInChildren<UILabel> ();
                    Debug.Log (itemInfo.id);
                    ItemTableInfo itemTableInfo = MainController.Instance.itemTableDic[itemInfo.id.ToString()];
                    if (itemTableInfo.kind < EquipKind.EQUIP_MAX) {
                        countLabel.text = "Lv." + itemInfo.rank.ToString ();
                    } else {
                        countLabel.text = "x" + itemInfo.count.ToString ();
                    }
                    listObj.transform.parent = grid.gameObject.transform;
                    UIButton button = listObj.GetComponentInChildren<UIButton> ();
                    EventDelegate callback = new EventDelegate (this, "openMsgItemStatus");
                    callback.parameters [0].value = i;
                    callback.parameters [1].value = itemWindowKind;
                    button.onClick.Add(callback);
                    button.enabled = true;
                    listObj.transform.localScale = Vector3.one;
                    foreach(UISprite sprite in listObj.GetComponentsInChildren<UISprite>()){
                        if (sprite.name == "L_Img") {
                            sprite.spriteName = itemTableInfo.file;
                        }
                    }
                }
                UIScrollView scrollView = listObject.GetComponentInChildren<UIScrollView> ();
                grid.Reposition ();
                scrollView.ResetPosition();
            }
            public void equipment(int itemBoxNo){
                Debug.Log (String.Format("itemBoxNo:{0}",itemBoxNo));
                // 装備がしてない場合は処理しない
                if (itemBoxNo == NOT_EQUIPMENT) {
                    return;
                }
                ItemInfo targetItemInfo = null;
                foreach(ItemInfo itemInfo in PlayerInfo.Instance.myItemList.Values){
                    if (itemBoxNo == itemInfo.itemBoxNo) {
                        targetItemInfo = itemInfo;
                    }
                }
                if (targetItemInfo == null) {
                    return;
                }
                Debug.Log (targetItemInfo.id.ToString());
                ItemTableInfo itemTableInfo = MainController.Instance.itemTableDic[targetItemInfo.id.ToString()];
                GameObject listObj = Instantiate ((GameObject)Resources.Load ("Prefab/List/L_InventoryMini"));
                UILabel countLabel = listObj.GetComponentInChildren<UILabel> ();
                GameObject equipWindow = null;
                switch (itemTableInfo.kind) {
                case EquipKind.EQUIP_SWORD:
                    equipWindow = statusObject ["B_Weapon"];
                    break;
                case EquipKind.EQUIP_SHIELD:
                    equipWindow = statusObject ["B_Shield"];
                    break;
                case EquipKind.EQUIP_ARMOR:
                    equipWindow = statusObject ["B_Armor"];
                    break;
                }
                if (itemTableInfo.kind < EquipKind.EQUIP_MAX) {
                    countLabel.text = "Lv." + targetItemInfo.rank.ToString ();
                } else {
                    countLabel.text = "x" + targetItemInfo.count.ToString ();
                }
                foreach(UISprite sprite in listObj.GetComponentsInChildren<UISprite>()){
                    if (sprite.name == "L_Img") {
                        sprite.spriteName = itemTableInfo.file;
                    }
                }
                if (equipWindow != null) {
                    listObj.transform.parent = equipWindow.transform;
                    listObj.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
                    listObj.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                }
            }
            // 名前変更ボックスオープン
            public void openChageNameEdit(){
                OpenInputDialog(GameTextTable.GameDialogtext()["inputName"], PlayerInfo.Instance.characterInfo.name, nameChangeDialogCallback);
            }
            // 名前変更ダイアログボックス処理後コールバック
            private void nameChangeDialogCallback(InputDialogResult result, string newName){
                if (result == InputDialogResult.INPUT_RESULT_YES) {
                    m_newName = newName;
                    StartCoroutine(HttpController.Instance.changeName(nameChangeCallback,newName));
                }
            }
            // 名前変更コールバック
            private void nameChangeCallback(){
                PlayerInfo.Instance.characterInfo.name = m_newName;
                OpenDialog (DialogForm.DIALOG_OK, GameTextTable.CommonDialogtext()["changedName"]);
                SocketNetwork.Instance.sendName (PlayerInfo.Instance.characterInfo);
            }
            // メール送信コールバック
            private void sendMailCallback(MailError mailError){
                if (mailError == MailError.SUCCESS) {
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["mailSendOk"]);
                }else if(mailError == MailError.SEND_ERROR){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["mailSendError"]);
                }
            }

            // アイテム詳細ボックスオープン
            public void openMsgItemStatus(int itemListNo,ItemWindowKind itewWindowKind){
                ItemController itemController = uiMsg ["MsgItemStatus"].GetComponent<ItemController> ();
                itemController.itewWindowKind = itewWindowKind;
                itemController.itemInfo = PlayerInfo.Instance.myItemList [itemListNo];
                _openMsgItemStatus (itemController.itemInfo);
            }
            // メール送信ウィンドウオープン
            public void openSendMail(int friendlistId){
                MailSendController mailSend = uiMsg ["MsgMailSubmit"].GetComponent<MailSendController> ();
                mailSend.toFriendCode = PlayerInfo.Instance.friendList [friendlistId].friendCode;
                mailSend.toFriendName.text = PlayerInfo.Instance.friendList [friendlistId].name;
                mailSend.mailformat = new MailFormat();
                uiMsg ["MsgMailSubmit"].SetActive(true);
                tapBarrier ["TapBarrier"].SetActive (true);
            }
            // メール送信ウィンドウオープン
            public void visibleSendMail(bool visible){
                tapBarrier ["TapBarrier"].SetActive (visible);
                uiMsg ["MsgMailSubmit"].SetActive (visible);
            }
            // アイテム詳細ボックス共通処理
            private void _openMsgItemStatus(ItemInfo itemInfo,int gachaRank =-1){
                uiMsg ["MsgItemStatus"].SetActive (true);
                tapBarrier ["TapBarrier"].SetActive (true);
                ItemTableInfo itemTableInfo = MainController.Instance.itemTableDic [itemInfo.id.ToString ()];
                foreach (UILabel label in uiMsg["MsgItemStatus"].GetComponentsInChildren<UILabel>()) {
                    switch (label.name) {
                    case "Name":
                        string rankStr = "";
                        if (gachaRank > 0) {
                            rankStr = gachaRank.ToString ();
                            if (gachaRank == 0)
                                rankStr = "特";
                            rankStr += "等:";
                        }
                        label.text = rankStr + MainController.Instance.itemTableDic [itemInfo.id.ToString()].name;
                        break;
                    }
                }
                foreach (UISprite sprite in uiMsg["MsgItemStatus"].GetComponentsInChildren<UISprite>()) {
                    switch (sprite.name) {
                    case "Img":
                        sprite.spriteName = itemTableInfo.file;
                        break;
                    }
                }
                ItemUtility.SlotView (popupEquipSlot,itemInfo.slotCount);
            }
            // メニューを開く
            public void openMenu(string menuName){
                touchLayer.SetActive (false);
                chatWindow.SetActive (false);
                uiWindow ["T_PageTitle"].SetActive(true);
                if(uiWindow.ContainsKey(menuName)){
                    uiWindow [menuName].SetActive(true);
                    openObject.Push(uiWindow[menuName]);
                }
                Debug.Log (menuName);
                if (GameTextTable.PageTitle().ContainsKey (menuName)) {
                    pageTitleLabel.text = GameTextTable.PageTitle () [menuName];
                }
                // メニューと関連するリストを開く
                switch (menuName) {
                case "M_Shop":
                    openList ("ShopList");
                    break;
                case "M_Status":
                    openList ("InventoryList");
                    break;
                case "M_Mail":
                    openList ("MailList");
                    break;
                case "M_Friend":
                    openList ("FriendList");
                    break;
                }
                // 魔石セットの時のアイテムリストのセット
                if(menuName == "M_Rare"){
                    uiLists ["ItemSelectList"].SetActive (true);
                    setItemList (uiLists ["ItemSelectList"], ItemWindowKind.ITEM_WINDOW_SLOT_SET);
                    uiLists ["ItemSelectList"].SetActive (false);
                }
            }
            public GameObject GetMenuWindow(string objectName){
                return uiWindow [objectName];
            }
            // リストを開く
            private void openList(string listName){
                uiLists [listName].SetActive(true);
                openObject.Push(uiLists[listName]);
            }
            public void hideAllWindow(){
                while(openObject.Count>0){
                    openObject.Pop().SetActive (false);
                }
            }

            // メニューをまとめてクローズ
            public void CloseMenu(){
                Debug.Log ("CloseMenu");
                touchLayer.SetActive (true);
                chatWindow.SetActive (true);
                tapBarrier ["TapBarrier"].SetActive (false);
                while(openObject.Count>0){
                    openObject.Pop().SetActive (false);
                }
                uiWindow ["T_PageTitle"].SetActive(false);
                foreach(KeyValuePair<string,GameObject> obj in uiLists){
                    obj.Value.SetActive (false);
                }
            }
            public void changeMailListScreen(ChangeMailScreen mailScreen){
                uiLists ["MultiList"].SetActive (false);
                uiLists ["ItemSelectList"].SetActive (false);
                uiLists ["MailList"].SetActive (false);
                hideAllWindow ();
                switch (mailScreen) {
                case ChangeMailScreen.MAIL_LIST:
                    openMenu("M_Mail");
                    uiLists ["MailList"].SetActive (true);
                    break;
                case ChangeMailScreen.ITEM_LIST:
                    openMenu("M_FlatScroll");
                    uiLists ["ItemSelectList"].SetActive (true);
                    break;
                case ChangeMailScreen.FRIEND_LIST:
                    openMenu("M_Friend");
                    uiLists ["MultiList"].SetActive (true);
                    break;
                }
            }
            // リストの削除
            static public void DestroyList(GameObject listObject){
                while (listObject.transform.childCount>0)
                {
                    Transform n = listObject.transform.GetChild (0);
                    n.transform.parent = null;
                    GameObject.Destroy(n.gameObject);
                }
            }
        //----List Button Action------------------------------------------------
        	public void MsgOpen (string word) {
        		
        		switch(word){
        			
        		case "LB_Inventory":
//        			MsgText("Item","アイテム名","テストテストテストテステストテストトテストテストテストテステストテストト\n1234567890\n1234567890\n1234567890");
        			break;
        			
        		case "LB_Shop":
//        			MsgText("Shop","アイテム名","テストテストテストテステストテストトテストテストテストテステストテストト\n1234567890\n1234567890\n1234567890");
        			break;
        			
        		case "B_MsgClose":
//        			uiMsgNml[0].SetActive(false);
        			break;
        			
        		default:
        			break;
        		}

        	}
            private void MoveDungeon(){
                MainController.Instance.MoveDungeon ();
            }
            private void openMailList(MailError mailError){
                CloseMenu ();
                PlayerInfo.Instance.myMailList.Clear ();
                while (HttpController.Instance.mailBox.Count>0) {
                    PlayerInfo.Instance.myMailList.Add (HttpController.Instance.mailBox.Dequeue ());
                }
                openMenu ("M_Mail");
                reloadMailList ();
                getFriendList (setMailFriendList);
            }
            public void registFriend(string friendCode1,string friendCode2){
                StartCoroutine (HttpController.Instance.RegistFriend (registFriendCallback, friendCode1+"-"+friendCode2));
            }
            private void registFriendCallback(FriendError friendError){
                if(friendError == FriendError.SUCCESS){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["registFriendComplete"]);
                    getFriendList (openFriendList);
                }else if(friendError == FriendError.ERROR){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["registFriendError"]);
                }else if(friendError == FriendError.REGIST_ERROR){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["registedFriendError"]);
                }
            }
            public void reloadFriendList(GameObject listObject){
                UIGrid grid = listObject.GetComponentInChildren<UIGrid> ();
                DestroyList (grid.gameObject);

                int pos = 0;
                for (int i = 0; i<PlayerInfo.Instance.friendList.Count; ++i) {
                    CharacterInfo charaInfo = PlayerInfo.Instance.friendList[i];
                    GameObject listObj = Instantiate ((GameObject)Resources.Load ("Prefab/List/L_Friend"));
                    foreach(UILabel label in listObj.GetComponentsInChildren<UILabel> ()){
                        switch (label.name) {
                        case "Name":
                            label.text = charaInfo.name;
                            break;
                        }
                    }
                    listObj.transform.parent = grid.gameObject.transform;
                    foreach (UIButton button in listObj.GetComponentsInChildren<UIButton> ()) {
                        EventDelegate callback= null;
                        Debug.Log (button.name);
                        switch(button.name){
                        case "B_Delete":
                            callback = new EventDelegate (this, "deleteFriend");
                            callback.parameters [0].value = i;
                            break;
                        case "B_Select":
                            callback = new EventDelegate (this, "openSendMail");
                            callback.parameters [0].value = i;
                            break;
                        }
                        if (callback!=null) {
                            button.onClick.Add (callback);
                            button.enabled = true;
                        }
                    }
                    listObj.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                    listObj.transform.localScale = Vector3.one;
                }
                UIScrollView scrollView = listObject.GetComponentInChildren<UIScrollView> ();
                grid.Reposition ();
                scrollView.ResetPosition();
            }
            public void deleteFriend(int friendlistId){
                selectedFriendId = friendlistId;
                OpenDialog(DialogForm.DIALOG_YES_NO,GameTextTable.CommonDialogtext()["deleteFriendConfirm"],deleteFriendConfirmCallback);
            }
            public void deleteFriendConfirmCallback(DialogResult result){
                if (result == DialogResult.RESULT_YES) {
                    StartCoroutine (HttpController.Instance.DeleteFriend (deleteFriendCallback, PlayerInfo.Instance.friendList [selectedFriendId].friendCode));
                }
            }
            public void deleteFriendCallback(FriendError friendError){
                if(friendError == FriendError.SUCCESS){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["deleteFriendComplete"]);
                    getFriendList (openFriendList);
                }else if(friendError == FriendError.ERROR){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["deleteFriendError"]);
                }else if(friendError == FriendError.REGIST_ERROR){
                    OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["deletedFriendError"]);
                }
            }
            public void reloadMailList(){
                UIGrid grid = uiLists ["MailList"].GetComponentInChildren<UIGrid> ();
                DestroyList (grid.gameObject);

                int pos = 0;
                for (int i = 0; i<PlayerInfo.Instance.myMailList.Count; ++i) {
                    MailFormat mailformat = PlayerInfo.Instance.myMailList[i];
                    GameObject listObj = Instantiate ((GameObject)Resources.Load ("Prefab/List/L_Mail"));
                    foreach(UILabel label in listObj.GetComponentsInChildren<UILabel> ()){
                        switch (label.name) {
                        case "From":
                            label.text = mailformat.from;
                            break;
                        case "Subject":
                            string body = mailformat.body;
                            if (body.Length > MAIL_TITLE_LENGTH) {
                                body = body.Substring (0, MAIL_TITLE_LENGTH)+"...";
                            }
                            label.text = body;
                            break;
                        }
                    }
                    listObj.transform.parent = grid.gameObject.transform;
                    UIButton button = listObj.GetComponentInChildren<UIButton> ();
                    EventDelegate callback = new EventDelegate (this, "openMail");
                    callback.parameters [0].value = i;
                    button.onClick.Add(callback);
                    button.enabled = true;
                    listObj.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                    listObj.transform.localScale = Vector3.one;
                }
                UIScrollView scrollView = uiLists ["MailList"].GetComponentInChildren<UIScrollView> ();
                grid.Reposition ();
                scrollView.ResetPosition();
            }
        //---- ------------------------------------------------
            // メール開く
            void openMail(int listNo){
                MailReceiveController mailReceive = uiMsg ["MsgMailReceive"].GetComponent<MailReceiveController> ();
                Debug.Log (listNo);
                mailReceive.mailformat = PlayerInfo.Instance.myMailList[listNo];
                uiMsg ["MsgMailReceive"].SetActive(true);
                tapBarrier ["TapBarrier"].SetActive (true);
            }
            // メールにアイテム添付
            public void sendMailAttachItem(ItemInfo itemInfo){
                MailSendController mailSend = uiMsg ["MsgMailSubmit"].GetComponent<MailSendController> ();
                mailSend.mailformat.itemInfo = itemInfo;
                uiMsg ["MsgItemStatus"].SetActive (false);
                changeMailListScreen (ChangeMailScreen.FRIEND_LIST);
                visibleSendMail (true);
            }
            /// <summary>
            ///  スロットに魔石セット
            /// </summary>
            /// <param name="jewelItemInfo">Jewel item info.</param>
            public void setSlot(ItemInfo jewelItemInfo){
                Debug.Log ("setSlot");


                selectedJewelItemInfo = jewelItemInfo;
                OpenDialog(DialogForm.DIALOG_YES_NO,GameTextTable.GameDialogtext()["questionSetStone"],setSlotDialogBoxCallBack);
            }
            private void setSlotDialogBoxCallBack(DialogResult result){
                if (result == DialogResult.RESULT_YES) {
                    SlotSetController slotSetController = uiWindow ["M_Rare"].GetComponent<SlotSetController> ();
                    slotSetController.selectSlotItemInfo = selectedJewelItemInfo;
                    StartCoroutine (HttpController.Instance.setSlot (
                        setSlotCallback,
                        slotSetController.itemInfo.itemBoxNo,
                        slotSetController.selectSlotNo,
                        slotSetController.selectSlotItemInfo.itemBoxNo
                    ));
                } else {
                    uiLists ["ItemSelectList"].SetActive (false);
                    uiWindow ["M_FlatScroll"].SetActive (false);
                    openMenu("M_Rare");
                    tapBarrier ["TapBarrier"].SetActive (false);
                }
            }

            /// <summary>
            /// スロットセットの戻り
            /// </summary>
            /// <param name="result">Result.</param>
            public void setSlotCallback(SetSlotResult result){
                if (result == SetSlotResult.SUCCESS) {
                    Debug.Log ("setSocekt SUCCESS");
                    SlotSetController slotSetController = uiWindow ["M_Rare"].GetComponent<SlotSetController> ();
                    ItemInfo itemInfo = PlayerInfo.Instance.getItem (slotSetController.itemInfo.itemBoxNo);
                    itemInfo.slot [slotSetController.selectSlotNo] = slotSetController.selectSlotItemInfo.id;
                    PlayerInfo.Instance.setItem (itemInfo);
                    PlayerInfo.Instance.deleteItem (slotSetController.selectSlotItemInfo.itemBoxNo);
                    slotSetController.itemInfo = itemInfo;
                    slotSetController.selectSlotItemInfo.itemBoxNo = 0;
                    uiLists ["ItemSelectList"].SetActive (false);
                    uiWindow ["M_FlatScroll"].SetActive (false);
                    openMenu("M_Rare");
                    tapBarrier ["TapBarrier"].SetActive (false);
                } else {
                    Debug.Log ("setSocekt Fail");
                }

            }
        	void UIListEventSet(GameObject listObjBtn){
        		UIButton uiBtn = listObjBtn.GetComponent<UIButton>();
        		List<EventDelegate> list = new List<EventDelegate>();
        		EventDelegate eveDel = new EventDelegate ();
        		eveDel.target = this;
        		eveDel.methodName = "MsgOpen";
        		list.Add (eveDel);
        		EventDelegate.Parameter param = eveDel.parameters[0];
        		param.obj = listObjBtn;
        		param.field = "name";
        		uiBtn.onClick = list;
        	}
            public void CloseButton(GameObject target){
                target.SetActive (false);
            }
            public void CloseMsg(GameObject target){
                tapBarrier ["TapBarrier"].SetActive (false);
                target.SetActive (false);
            }

            // 装備ダイアログ呼び出し
            public void MsgEquip(EquipInfo equipInfo, int itemBoxNo){
                ItemInfo itemInfo = new ItemInfo();
                itemInfo.id = equipInfo.id;
                itemInfo.itemBoxNo = itemBoxNo;
                itemInfo.slotCount = equipInfo.socket;
                _openMsgItemStatus (itemInfo, equipInfo.rank);

                //                equipInfo.socket ("Item",uiMsg ["MsgItemStatus"]);
            }

            // ダイアログ呼び出しセット
            private System.Action<DialogResult> dialogCallBack;
            public void OpenDialog(DialogForm rDialogForm,string message,System.Action<DialogResult> rCallBack=null){
                tapBarrier ["DialogTapBarrier"].SetActive (true);
//                touchLayer.SetActive (false);
//                chatWindow.SetActive (false);
                dialogCallBack = rCallBack;
                DialogBox.Instance.viewMessage (rDialogForm, message, DialogCallBack);
            }
            private void DialogCallBack(DialogResult dialogResult){
                tapBarrier ["DialogTapBarrier"].SetActive (false);
//                touchLayer.SetActive (true);
//                chatWindow.SetActive (true);
                if (dialogCallBack != null) {
                    dialogCallBack (dialogResult);
                }
            }

            // 入力ダイアログ呼び出しセット
            private System.Action<InputDialogResult,string> inputDialogCallBack;
            public void OpenInputDialog(string message,string formatText,System.Action<InputDialogResult,string> rCallBack=null){
                tapBarrier ["TapBarrier"].SetActive (true);
                inputDialogCallBack = rCallBack;
                InputBox.Instance.viewMessage (message, formatText, InputDialogCallBack);
            }
            private void InputDialogCallBack(InputDialogResult dialogResult,string inputText){
                Debug.Log ("TapBarrier false");
                tapBarrier ["TapBarrier"].SetActive (false);
                if (inputDialogCallBack != null) {
                    inputDialogCallBack (dialogResult,inputText);
                }
            }

        	void MsgShow(string mode,GameObject window){
        		window.transform.localPosition = new Vector3(20,0,0);
        		iTween.MoveTo(window, iTween.Hash("islocal",true, "x",0, "time", 0.5));
        		StartCoroutine("MsgButtonSet",mode);
        	}

        }
    }
}