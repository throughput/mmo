﻿using UnityEngine;

namespace Throughputjp{
	namespace ThpMmo{
		public class GameInfo : SingletonMonoBehaviour<GameInfo>{
			public int chargeGachaIncentiveExpensive=0;
			public int chargeGachaIncentive=0;
			public int freeGachaIncentive=0;
			public int firstChargeGachaIncentiveExpensive=0;
			public int firstChargeGachaIncentive=0;
			public int firstFreeGachaIncentive=0;
		}
	}
}