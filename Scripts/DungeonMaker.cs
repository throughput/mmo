﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Throughputjp;
using System;

namespace Throughputjp{
    namespace ThpMmo{
        public enum DataParam{
            WALL = 0,
            FLOOR = 1,
            START = 2,
            GOAL =3,
            TREASURE_RANK1 =4,
            TREASURE_RANK2 =5,
            TREASURE_RANK3 =6,
            MONSTER_SET =7,
            WAY = 8,
        }
        public enum COLLISION_RESULT{
            COLLISION_RESULT_FLOOR = 0,
            COLLISION_RESULT_WALL = 1,
            COLLISION_RESULT_WAY = 2,
            COLLISION_RESULT_GOAL = 3,
        }
        public class DungeonMaker : SingletonMonoBehaviour<DungeonMaker> {
            Vector2 mapBasePoint = new Vector2(0.0f,65535.0f);

            private GameObject baseTips;
            public GameObject dungeonObject;
            public const float TIP_SIZE = 2.0f;
            public MapMaker mapMaker = new MapMaker ();
            int mapSizeX=1;
            int mapSizeY=1;
            string gMapData = "";
            public bool makeDungeonFlag = false;
            private Dictionary<int,string> dungeonTips;
            private Dictionary<string,GameObject>tipCache;

            public List<Vector2> wayTargetList = new List<Vector2>(){
                new Vector2 ( 0.0f,-2.0f),
                new Vector2 ( 2.0f, 0.0f),
                new Vector2 ( 0.0f, 2.0f),
                new Vector2 (-2.0f, 0.0f)
            };
            private const string dungeon_folder = "Graphics/Objects/Dungeon/Cave/1/";

            public void Start(){
                baseTips = Instantiate(new GameObject ());
                dungeonTips = new Dictionary<int, string>();
                tipCache = new Dictionary<string,GameObject>();
                // ダンジョンチップの数値化
                TextAsset jsondataAsset = (TextAsset)Resources.Load ("dungeonTipList");
                string jsondata = jsondataAsset.ToString ();
                Hashtable dungeonTipList = (Hashtable)JsonController.JsonDecode (jsondata);
                foreach (string key in dungeonTipList.Keys) {
                    dungeonTips.Add(Convert.ToInt32(key, 2),(string)dungeonTipList[key]);
                }
                foreach(KeyValuePair<int,string> dungeonTip  in dungeonTips){
                    GameObject mapTipObj = (GameObject)Instantiate (Resources.Load (dungeon_folder + dungeonTip.Value));
                    mapTipObj.transform.parent = baseTips.transform;
                    tipCache.Add (dungeonTip.Value, mapTipObj);
                }
                {
                    GameObject mapTipObj = (GameObject)Instantiate (Resources.Load (dungeon_folder + "Down"));
                    mapTipObj.transform.parent = baseTips.transform;
                    tipCache.Add ("Down", mapTipObj);
                }

                dungeonObject.transform.localPosition = new Vector3 (0.0f, 0.0f, mapBasePoint.y);
            }

        	// Use this for initialization
            public void localMapMake () {
                mapMaker.makeRandomSizeDungeon (100, 100);
                mapMaker.printDungeon ();
                Debug.Log (mapMaker.mapStr);
            }
            public string SetFrame(string mapData){
                int mapX = 0;
                for(int i =0; i<mapData.Length; ++i){
                    if (mapData.Substring (i, 1) == "\r") {
                        mapX = i+1;
                        break;
                    }
                }
                // 最初と最後に壁を入れる
                mapData = new string('0',mapX) + "\r" +mapData + new string('0',mapX);
                // 左右の端に壁を入れる
                mapData = mapData.Replace ("\r", "0\r0");
                return mapData;
            }
            public void SetTips(string mapData,bool gachaFlag = false){
                gMapData = mapData;
                makeDungeonFlag = true;
                float posX = 0.0f;
                float posY = 0.0f;
                string common_folder = "Graphics/Objects/Fbx/Dungeon/Object/";
                for(int i =0; i<mapData.Length; ++i){
                    if (mapData.Substring (i, 1) == "\r") {
                        mapSizeX = i+1;
                        break;
                    }
                }
                mapSizeY = mapData.Length / mapSizeX;
                for(int i =0; i<mapData.Length; ++i){
                    GameObject mapTipObj;
                    string mapNoStr = mapData.Substring (i, 1);
                    if (mapNoStr == "\r") {
                        posY += -TIP_SIZE;
                        posX = 0.0f;
                        continue; // 次の行へ
                    }

                    int tipNo = 0x0;
                    // 壁のとき
                    if (!DecideFloor (mapNoStr)) {
                        tipNo = getWallTipNo (mapData, i, mapSizeX, mapSizeY);
                        if (tipNo == 0xF) tipNo = 0;
                    } else {
                        tipNo = 0xF;
                    }
                    string tipString = dungeonTips[tipNo];
                    // ゴールの時
                    if (mapNoStr == "3") {
                        tipString = "Down";
                    } else if (mapNoStr == "4") {
                        mapTipObj = (GameObject)Instantiate (Resources.Load (common_folder + "Chest_Bronze"));
                        TreasureController treasureController = mapTipObj.AddComponent<TreasureController> ();
                        treasureController.treasureKind = TreasureKind.TREASURE_BRONZE;
                        treasureController.gachaFlag = gachaFlag;
                        mapTipObj.transform.parent = dungeonObject.transform;
                        mapTipObj.transform.localPosition = new Vector3 (posX+1.0f, 0.0f, posY-1.0f);
                    } else if (mapNoStr == "5") {
                        mapTipObj = (GameObject)Instantiate (Resources.Load (common_folder + "Chest_Silver"));
                        TreasureController treasureController = mapTipObj.AddComponent<TreasureController> ();
                        treasureController.treasureKind = TreasureKind.TREASURE_SILVER;
                        treasureController.gachaFlag = gachaFlag;
                        mapTipObj.transform.parent = dungeonObject.transform;
                        mapTipObj.transform.localPosition = new Vector3 (posX+1.0f, 0.0f, posY-1.0f);
                    } else if (mapNoStr == "6") {
                        mapTipObj = (GameObject)Instantiate (Resources.Load (common_folder + "Chest_Gold"));
                        TreasureController treasureController = mapTipObj.AddComponent<TreasureController> ();
                        treasureController.treasureKind = TreasureKind.TREASURE_GOLD;
                        treasureController.gachaFlag = gachaFlag;
                        mapTipObj.transform.parent = dungeonObject.transform;
                        mapTipObj.transform.localPosition = new Vector3 (posX+1.0f, 0.0f, posY-1.0f);
                    }
                    mapTipObj = (GameObject)Instantiate (tipCache[tipString]);
                    mapTipObj.transform.parent = dungeonObject.transform;
                    BoxCollider collider = mapTipObj.GetComponentInChildren<BoxCollider> ();
                    if(collider) collider.enabled = false;
                    mapTipObj.transform.localPosition = new Vector3 (posX, 0.0f, posY);
                    mapTipObj.transform.localScale = new Vector3 (TIP_SIZE / 2.0f, 1.0f, TIP_SIZE / 2.0f);
                    posX += TIP_SIZE;
                }
            }

            private int getWallTipNo(string mapData,int strPos,int mapSizeX,int mapSizeY){
                string[] sideTip = {"1","1","1","1"};
                // 上の壁
                if (strPos - mapSizeX > 0) {
                    string tipNoStr = mapData.Substring (strPos - mapSizeX, 1);
                    if (DecideFloor(tipNoStr)){
                        sideTip [0] = "0";
                    }
                }
                // 右の壁
                if (strPos + 1 < mapData.Length) {
                    string tipNoStr = mapData.Substring (strPos + 1, 1);
                    if(DecideFloor(tipNoStr)){
                        sideTip [1] = "0";
                    }
                }
                // 下の壁
                if (strPos + mapSizeX < mapData.Length) {
                    string tipNoStr = mapData.Substring (strPos + mapSizeX, 1);
                    if (DecideFloor(tipNoStr)) {
                        sideTip [2] = "0";
                    }
                }
                // 左の壁
                if (strPos > 1) {
                    string tipNoStr = mapData.Substring (strPos - 1, 1);
                    if(DecideFloor(tipNoStr)){
                        sideTip [3] = "0";
                    }
                }
                string sideTipStr = string.Join ("", sideTip);
                return Convert.ToInt32 (sideTipStr, 2);
            }
            // 床かどうか判定
            bool DecideFloor(string tipNoStr){
                bool isFloor = true;
                int tipNo = 0;
                if (!int.TryParse(tipNoStr,out tipNo)) {
                    tipNo = 0;
                }
                switch ((DataParam)tipNo) {
                case DataParam.FLOOR:
                case DataParam.START:
                case DataParam.GOAL:
                case DataParam.TREASURE_RANK1:
                case DataParam.TREASURE_RANK2:
                case DataParam.TREASURE_RANK3:
                case DataParam.MONSTER_SET:
                case DataParam.WAY:
                    isFloor = true;
                    break;
                case DataParam.WALL:
                default:
                    isFloor = false;
                    break;
                }
                return isFloor;
            }
            public COLLISION_RESULT checkCollision(float x,float y){
                if (!makeDungeonFlag) {
                    return COLLISION_RESULT.COLLISION_RESULT_FLOOR;
                }
                int mapX = (int)(x / TIP_SIZE);
                int mapY = (int)(Mathf.Abs(y - mapBasePoint.y) / TIP_SIZE);
                int pos = mapX + mapY * (mapSizeX);
                // はみ出る時はfalseを返しておく
                if (pos > gMapData.Length) {
                    return COLLISION_RESULT.COLLISION_RESULT_WALL;
                }
                if (pos < 0) {
                    return COLLISION_RESULT.COLLISION_RESULT_WALL;
                }
                string mapTipStr = gMapData.Substring (pos, 1);
                // マップがないところは壁を返す
                if (mapTipStr == "") {
                    return COLLISION_RESULT.COLLISION_RESULT_WALL;
                }
                int intMapTip = 0;
                if (!int.TryParse (mapTipStr,out intMapTip)) {
                    return COLLISION_RESULT.COLLISION_RESULT_WALL; 
                }
                DataParam mapTip = (DataParam)intMapTip;
                COLLISION_RESULT collisionResult = COLLISION_RESULT.COLLISION_RESULT_FLOOR;
                if (!DecideFloor (mapTipStr)) {
                    collisionResult = COLLISION_RESULT.COLLISION_RESULT_WALL;
                } else {
                    collisionResult = COLLISION_RESULT.COLLISION_RESULT_FLOOR;
                    if (mapTip == DataParam.GOAL) {
                        collisionResult = COLLISION_RESULT.COLLISION_RESULT_GOAL;
                    }
                    if (mapTip == DataParam.WAY) {
                        collisionResult = COLLISION_RESULT.COLLISION_RESULT_WAY;
                    }
                }
                return collisionResult;
            }
            public Vector2 nextWay(Vector2 userPos,Vector2 nextPos){
                Debug.Log ("nextWay");
                Debug.Log ("nextPos:" + nextPos.x + "," + nextPos.y);
                if (!makeDungeonFlag) {
                    return new Vector2(0.0f,0.0f);
                }
                string mapTipStr = "";
                DataParam mapTip = DataParam.FLOOR;
                {
                    int mapX = (int)((userPos.x + nextPos.x) / TIP_SIZE);
                    int mapY = (int)(Mathf.Abs ((userPos.y + nextPos.y) - mapBasePoint.y) / TIP_SIZE);
                    int pos = mapX + mapY * (mapSizeX);
                    Debug.Log ("userPos:" + userPos.x + "," + userPos.y);
                    Debug.Log ("userPos+nextPos:" + mapX + "," + mapY);
                    // 基本はみ出ないはず
                    if (pos > gMapData.Length) {
                        return new Vector2 (0.0f,0.0f);
                    }
                    if (pos < 0) {
                        return new Vector2 (0.0f,0.0f);
                    }
                    mapTipStr = gMapData.Substring (pos, 1);
                    int mapTipInt = 0;
                    if (!int.TryParse(mapTipStr,out mapTipInt)) {
                        mapTipInt = 0;
                    }
                    Debug.Log ("mapTipStr:" + mapTipStr);
                    Debug.Log ("mapTipInt:" + mapTipInt);
                    mapTip = (DataParam)mapTipInt;
                }
                Vector2 returnPos = userPos;
                if (mapTip == DataParam.WAY) {
                    Debug.Log ("next Way");
                    returnPos = userPos + nextPos;
                } else {
                    // 移動予定先が通路でなかったら探す
                    for(int i=0;i<wayTargetList.Count;++i){
                        // 同じものはスキップ
                        Debug.Log ("wayTargetList"+wayTargetList [i]);
                        if (nextPos == wayTargetList [i]) {
                            Debug.Log ("Same Way");
                            continue;
                        }
                        // 逆転方向もスキップ
                        Vector2 reverseVector2 = new Vector2(wayTargetList[i].x * -1.0f, wayTargetList[i].y * -1.0f);
                        if (nextPos == reverseVector2) {
                            Debug.Log ("reverse Way");
                            continue;
                        }
                        int mapX = (int)(userPos.x + nextPos.x / TIP_SIZE);
                        int mapY = (int)(Mathf.Abs(userPos.y + nextPos.y - mapBasePoint.y) / TIP_SIZE);
                        int pos = mapX + mapY * (mapSizeX);
                        mapTipStr = gMapData.Substring (pos, 1);
                        int mapTipInt = 0;
                        if (!int.TryParse(mapTipStr,out mapTipInt)) {
                            mapTipInt = 0;
                        }
                        Debug.Log ("roop mapTipInt:" + mapTipInt);
                        mapTip = (DataParam)mapTipInt;
                        if (mapTip == DataParam.WAY) {
                            Debug.Log ("change Way");
                            returnPos = userPos + wayTargetList[i];
                            break;
                        }
                    }
                }
                return returnPos;
            }
        	// Update is called once per frame
        	void Update () {
        	
        	}
        }
    }

}


