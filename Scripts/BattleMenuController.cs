﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	namespace ThpMmo{
		public class BattleMenuController : SingletonMonoBehaviour<BattleMenuController>{
            public GameObject skillList;
            public const int SLOT_NUM = 8;
            public void Awake(){
                int slotCount = 0;
                foreach (UIButton button in skillList.GetComponentsInChildren<UIButton>()) {
                    EventDelegate callback= null;
                    callback = new EventDelegate (this, "selectSlot");
                    callback.parameters [0].value = slotCount;
                    button.onClick.Add (callback);
                    button.enabled = true;
                    ++slotCount;
                }
                skillList.SetActive (false);
			}
            public void selectSlot(int slot){
                // 攻撃方法を選択
                DungeonPlayer.Instance.setMagicSlot (slot);
                skillList.SetActive (false);
            }
            public void changeViewSkillList(){
                skillList.SetActive (!skillList.activeSelf);
            }
		}
	}
}