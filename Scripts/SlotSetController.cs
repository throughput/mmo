﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public class SlotSetController : ThpmmoMonoBehavier {
            public ItemInfo itemInfo;
            public ItemInfo selectSlotItemInfo;
			public List<GameObject> slotObjects = new List<GameObject>();
			public List<GameObject> miniSlotObjects = new List<GameObject>();
			public List<UILabel> passiveLabels = new List<UILabel>();
            public UILabel slotCount;
			public UISprite itemSprite;
            public int selectSlotNo=0;
            // Use this for initialization
            void Awake () {
                int i = 0;
                foreach (GameObject slotObject in slotObjects) {
                    UIButton button = slotObject.GetComponentInChildren<UIButton> ();
                    EventDelegate callback = new EventDelegate (this, "openSelectSlot");
                    callback.parameters [0].value = i;
                    button.onClick.Add (callback);
                    ++i;
                }
        	}
            void Start(){
            }
        	// Update is called once per frame
        	void Update () {
        	}
            void OnEnable() {
                Debug.Log ("OnEnable");
                int i = 0;
                // スロット設定
				foreach (GameObject slotObject in slotObjects) {
                    _setSlot (slotObject,i);
                    ++i;
				}
                // ミニスロット設定
                int slotNo = 0;
                foreach (GameObject miniSlotObject in miniSlotObjects) {
                    UISprite sprite = miniSlotObject.GetComponent<UISprite> ();
                    sprite.spriteName = "nothing";
                    if (itemInfo.slot [slotNo] > 0) {
                        ItemTableInfo slotInfoTable = MainController.Instance.itemTableDic [itemInfo.slot [slotNo].ToString ()];
                        if (itemInfo.slotCount > slotNo) {
                            sprite.spriteName = (string)slotInfoTable.file;
                        }
                    }
                    ++slotNo;
                }
                // スロット数
                slotCount.text = itemInfo.slotCount.ToString();

                // アイテム画像
                ItemTableInfo itemInfoTable = MainController.Instance.itemTableDic [itemInfo.id.ToString()];
                itemSprite.spriteName = (string)itemInfoTable.file;
			}
            void _setSlot(GameObject slotObject,int slotNo){
                UIButton button = slotObject.GetComponentInChildren<UIButton> ();
				// 画像セット
                foreach (UISprite sprite in slotObject.GetComponentsInChildren<UISprite>()) {
                    if (sprite.name == "Background") {
                        if (itemInfo.slotCount > slotNo) {
                            sprite.spriteName = "Slot_Frame";
                            button.enabled = true;
                        } else {
                            sprite.spriteName = "Slot_Frame_Close";
                            button.enabled = false;
                        }
                    }
                }
                foreach(UISprite sprite in slotObject.GetComponentsInChildren<UISprite>()){
                    if (sprite.name == "Stone") {
                        sprite.spriteName = "nothing";
                        if (itemInfo.slotCount > slotNo) {
                            if (itemInfo.slot [slotNo] > 0) {
                                ItemTableInfo itemInfoTable = MainController.Instance.itemTableDic [itemInfo.slot [slotNo].ToString ()];
                                sprite.spriteName = (string)itemInfoTable.file;
                                button.enabled = false;
                            }
                        }
                    }
                }
                // テキストセット
                foreach (UILabel label in slotObject.GetComponentsInChildren<UILabel>()) {
                    if (label.name == "LV") {
                        label.text = "";
                    }
                }
			}
            // スロット選択
            public void openSelectSlot(int buttonNo){
                Debug.Log (buttonNo);
                gameObject.SetActive (false);
                selectSlotNo = buttonNo;
                MenuController.Instance.changeMailListScreen (MenuController.ChangeMailScreen.ITEM_LIST);
            }
       }
    }
}
