﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
    namespace ThpMmo{
        public class GachaController : SingletonMonoBehaviour<GachaController> {

            // ガチャルーム情報
            const int GACHA_ROOM_X = 8;
            const int GACHA_ROOM_Y = 6;
            const int TRESURE_SPACE_X = 2;
            const int TRESURE_SPACE_Y = 2;
            const int GACHA_ROOM_START_X =6;
            const int GACHA_ROOM_START_Y =4;
            public enum GachaKind{
                GACHA_BRONZE,
                GACHA_SILVER,
                GACHA_GOLD,
            };
            // ガチャルーム作成
            public static string makeGachaRoom(GachaKind gachaKind){
                string map = "";
                for (int y = 0; y < GACHA_ROOM_Y; ++y) {
                    for (int x = 0; x < GACHA_ROOM_X; ++x) {
                        if (x == GACHA_ROOM_START_X && y == GACHA_ROOM_START_Y) {
                            map += ((int)MapMaker.DataParam.START).ToString ();
                        }else if (x % TRESURE_SPACE_X == 0 && y % TRESURE_SPACE_Y == 0) {
                            switch (gachaKind) {
                            case GachaKind.GACHA_BRONZE:
                                map += ((int)MapMaker.DataParam.TREASURE_RANK1).ToString();
                                break;
                            case GachaKind.GACHA_SILVER:
                                map += ((int)MapMaker.DataParam.TREASURE_RANK2).ToString();
                                break;
                            case GachaKind.GACHA_GOLD:
                                map += ((int)MapMaker.DataParam.TREASURE_RANK3).ToString();
                                break;
                            }
                        }else {
                            map += ((int)MapMaker.DataParam.FLOOR).ToString ();
                        }
                    }
                    map += "\r";
                }
                Debug.Log (map);
                return map;
            }
            public void openGacha(GachaKind gachaKind,bool isTicket){
                // アイテムBOXの空きを探す
                int itemBoxNo = ItemUtility.getSpaceItemBoxNo (PlayerInfo.Instance.myItemList);
                if (itemBoxNo == ItemUtility.itemBoxNone) {
                    MenuController.Instance.OpenDialog (DialogForm.DIALOG_OK, GameTextTable.GameDialogtext()["notSpaceItemBox"]);
                } else {
                    StartCoroutine (HttpController.Instance.openGacha (openGachaCallback, gachaKind, itemBoxNo,isTicket));
                }
            }
            private void openGachaCallback(GachaKind gachaKind,GachaResult result){
                if (result == GachaResult.SUCCESS) {
                    EffectKind effectKind = EffectKind.EFFECT_GACHA_NORMAL;
                    switch (HttpController.Instance.gachaRank) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        effectKind = EffectKind.EFFECT_GACHA_NORMAL;
                        break;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        effectKind = EffectKind.EFFECT_GACHA_RARE;
                        break;
                    case 8:
                    case 9:
                    case 10:
                        effectKind = EffectKind.EFFECT_GACHA_GEKIRARE;
                        break;
                    }
                    EffectManager.Instance.playEffect (effectKind, openGachaEffectCallback);
                } else if (result == GachaResult.NOT_ENOUGH)  {
                    switch (gachaKind) {
                    case GachaKind.GACHA_BRONZE:
                        MenuController.Instance.OpenDialog (DialogForm.DIALOG_OK, GameTextTable.SystemText () ["commonFreeInsentive"] + GameTextTable.GameDialogtext () ["notEnough"]);
                        break;
                    case GachaKind.GACHA_SILVER:
                        MenuController.Instance.OpenDialog(DialogForm.DIALOG_OK,GameTextTable.SystemText()["commonChargeInsentive"]+GameTextTable.GameDialogtext()["notEnoughOpenShop"]);
                        break;
                    case GachaKind.GACHA_GOLD:
                        MenuController.Instance.OpenDialog(DialogForm.DIALOG_OK,GameTextTable.SystemText()["commonChargeInsentive"]+GameTextTable.GameDialogtext()["notEnoughOpenShop"]);
                        break;
                    }
                }
            }
            private void openGachaEffectCallback(Animator animator){
                EquipInfo equipInfo= HttpController.Instance.treasureResult.Pop ();
                ItemInfo itemInfo = HttpController.Instance.myItemInfo.Dequeue ();
                PlayerInfo.Instance.setItem (itemInfo);
                MenuController.Instance.MsgEquip (equipInfo,itemInfo.itemBoxNo);
            }
        }
    }
}