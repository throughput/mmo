﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
		public class DamageManager : SingletonMonoBehaviour<DamageManager> {
            public UILabel battleLog;
            public GameObject battleLogObject;
            // Use this for initialization
            void Awake () {
                battleLog = battleLogObject.GetComponentInChildren<UILabel> ();
                battleLogObject.SetActive (false);
        	}
            void Start(){
            }
        	// Update is called once per frame
            void Update () {
                while (SocketNetwork.Instance.damageInfoQueue.Count > 0) {
                    DamageInfo damageInfo = SocketNetwork.Instance.damageInfoQueue.Dequeue();
                    if (PlayerInfo.Instance.characterInfo.characterID == damageInfo.characterInfo.characterID) {
                        DungeonPlayer.Instance.playDamageAction (damageInfo);
                    }
                    DungeonMonsterManager.Instance.playAttackAction(damageInfo);
                    DungeonCharacterManager.Instance.playDamageAction(damageInfo);
                }
                while (SocketNetwork.Instance.attackInfoQueue.Count > 0) {
                    AttackInfo attackInfo = SocketNetwork.Instance.attackInfoQueue.Dequeue();
                    DungeonMonsterManager.Instance.playDamageAction(attackInfo);
                }
                while (SocketNetwork.Instance.levelUpStatus != null) {
                    PlayerInfo.Instance.levelup(SocketNetwork.Instance.levelUpStatus);
                    DungeonPlayer.Instance.startLevelup ();
                    SocketNetwork.Instance.levelUpStatus = null;
                }
                while (SocketNetwork.Instance.battleResultQueue.Count > 0) {
                    BattleResult battleResult = SocketNetwork.Instance.battleResultQueue.Dequeue ();
                    if (PlayerInfo.Instance.characterInfo.characterID == battleResult.characterInfo.characterID) {
                        /// 0以上のとき宝箱ドロップ
                        if (battleResult.treasureInfo.rank > 0) {
                            PlayerInfo.Instance.resultInfo.treasures.Add (battleResult.treasureInfo);
                            ++PlayerInfo.Instance.resultInfo.treasureCount [(TreasureKind)battleResult.treasureInfo.rank];
                            switch ((TreasureKind)battleResult.treasureInfo.rank) {
                            case TreasureKind.TREASURE_GOLD:
                                battleLog.text = GameTextTable.SystemText () ["goldTreasure"] + GameTextTable.SystemText () ["get"];
                                break;
                            case TreasureKind.TREASURE_SILVER:
                                battleLog.text = GameTextTable.SystemText () ["silverTreasure"] + GameTextTable.SystemText () ["get"];
                                break;
                            case TreasureKind.TREASURE_BRONZE:
                                battleLog.text = GameTextTable.SystemText () ["bronzeTreasure"] + GameTextTable.SystemText () ["get"];
                                break;
                            }
                            battleLogObject.SetActive (false);
                            battleLogObject.SetActive (true);
                            StartCoroutine (MainController.timerSetActive (battleLogObject, false, 10.0f));
                        } else {
                            if(battleResult.levelUp == 1){
                                DungeonCharacterManager.Instance.startLevelup (battleResult.characterInfo.characterID);
                            }
                        }
                        ItemInfo itemInfo = PlayerInfo.Instance.setItemWithOutBoxNo (battleResult.dropItem);
                        // 使用しているアイテムの記録
                        if (PlayerInfo.Instance.resultInfo.useItems.ContainsKey (itemInfo.itemBoxNo)) {
                            PlayerInfo.Instance.resultInfo.useItems [itemInfo.itemBoxNo] = itemInfo;
                        } else {
                            PlayerInfo.Instance.resultInfo.useItems.Add (itemInfo.itemBoxNo, itemInfo);
                        }
                    }
                }
            }

            static public void playDamageParticle(MagicKind magicKind,int magicLevel,GameObject tragetObject){
                GameObject particleObject = null;
                Debug.Log (magicKind);
                switch (magicKind) {
                case MagicKind.MAGIC_FIRE:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Fire"));
                    break;
                case MagicKind.MAGIC_WATER:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Water"));
                    break;
                case MagicKind.MAGIC_WIND:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Wind"));
                    break;
                case MagicKind.MAGIC_ICE:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Water"));
                    break;
                case MagicKind.MAGIC_EARTH:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Water"));
                    break;
                case MagicKind.MAGIC_THUNDER:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Thunder"));
                    break;
                case MagicKind.MAGIC_LIGHT:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Water"));
                    break;
                case MagicKind.MAGIC_DARK:
                    particleObject = (GameObject)Instantiate (Resources.Load ("Graphics/Particle/BattleEffects/Water"));
                    break;
                }
                if (particleObject != null) {
                    particleObject.AddComponent<DestroyParticle> ();
                    particleObject.transform.parent = tragetObject.transform;
                    particleObject.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                }
            }
        }
    }
}
