﻿using UnityEngine;
using System.Collections;

namespace Throughputjp{
    namespace ThpMmo{
        public class MonsterInfo{
            public int monsterID=Random.Range (0,10000);
            public int monsterNum=Random.Range (0,256);
            public int mapCode=0;
            public int areaCodex=0; 
            public int areaCodey=0; 
            public int x;
            public int y;
            public int Hp;
            public int Sp;
            public int MaxHp;
            public int MaxSp;
            public bool target;
        }
    }
}