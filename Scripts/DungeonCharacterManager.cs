﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Throughputjp{
	namespace ThpMmo{
		public class DungeonCharacterManager : SingletonMonoBehaviour<DungeonCharacterManager> {
			List<GameObject> dungeonCharacterArray;
			public GameObject gameCamera;
            public GameObject nameParent;
			// Use this for initialization
			void Start () {
				dungeonCharacterArray = new List<GameObject> ();
			}
			
			// Update is called once per frame
			void Update () {
				if(SocketNetwork.Instance.charaInfoArray != null){
					if(SocketNetwork.Instance.charaInfoArray.Count>0){
						foreach(CharacterInfo netWorkCharaInfo in SocketNetwork.Instance.charaInfoArray){
							bool newCharaFlag=true;
							foreach(GameObject dungeonCharacterObject in dungeonCharacterArray){
								DungeonCharacter dungeonCharacter=dungeonCharacterObject.GetComponent<DungeonCharacter>();
								CharacterInfo localCharaInfo=dungeonCharacter.charaInfo;
								if(localCharaInfo.characterID==netWorkCharaInfo.characterID){
									localCharaInfo.x=netWorkCharaInfo.x;
									localCharaInfo.y=netWorkCharaInfo.y;
									localCharaInfo.mapCode=netWorkCharaInfo.mapCode;
									localCharaInfo.characterID=netWorkCharaInfo.characterID;
									dungeonCharacter.charaInfo=localCharaInfo;
									newCharaFlag=false;
								}
							}
							if(newCharaFlag)
							{
								GameObject newCharacter=(GameObject)Instantiate(Resources.Load ("DungeonCharacter"));
								DungeonCharacter dungeonCharacter=newCharacter.GetComponent<DungeonCharacter>();
								CharacterInfo localCharaInfo=new CharacterInfo();
								localCharaInfo.x=netWorkCharaInfo.x;
								localCharaInfo.y=netWorkCharaInfo.y;
								localCharaInfo.mapCode=netWorkCharaInfo.mapCode;
								localCharaInfo.characterID=netWorkCharaInfo.characterID;
								dungeonCharacter.charaInfo=localCharaInfo;
								dungeonCharacter.gameCamera=gameCamera;
								dungeonCharacterArray.Add(newCharacter);
								newCharacter.transform.parent=gameObject.transform;
					        }
						}
		                SocketNetwork.Instance.charaInfoArray.Clear();
					}
                    if (SocketNetwork.Instance.nameList != null) {
                        if (SocketNetwork.Instance.nameList.Count > 0) {
                            foreach (KeyValuePair<int,string> data in SocketNetwork.Instance.nameList) {
                                foreach (GameObject dungeonCharacterObject in dungeonCharacterArray) {
                                    DungeonCharacter dungeonCharacter = dungeonCharacterObject.GetComponent<DungeonCharacter> ();
                                    CharacterInfo localCharaInfo = dungeonCharacter.charaInfo;
                                    if (localCharaInfo.characterID == data.Key) {
                                        localCharaInfo.name = data.Value;
                                    }
                                }
                            }
                        }
                    }
				}
				if(SocketNetwork.Instance.deleteCharaIDArray != null){
					if(SocketNetwork.Instance.deleteCharaIDArray.Count>0){
						foreach(int deleteCharaID in SocketNetwork.Instance.deleteCharaIDArray){
							for(int i=0;i<dungeonCharacterArray.Count;++i){
								GameObject dungeonCharacterObject=dungeonCharacterArray[i];
								if(deleteCharaID==dungeonCharacterObject.GetComponent<DungeonCharacter>().charaInfo.characterID){
									Debug.Log ("Delete Chara:"+deleteCharaID);
                                    dungeonCharacterObject.GetComponent<DungeonCharacter> ().beforeDestroy ();
									dungeonCharacterArray.Remove(dungeonCharacterObject);
									Destroy(dungeonCharacterObject);
								}
							}
						}
						SocketNetwork.Instance.deleteCharaIDArray.Clear();
					}
				}
			}
            public void startLevelup(int characterID){
                foreach (GameObject dungeonCharacterObject in dungeonCharacterArray) {
                    DungeonCharacter dungeonCharacter = dungeonCharacterObject.GetComponent<DungeonCharacter> ();
                    if (dungeonCharacter.charaInfo.characterID == characterID) {
                        dungeonCharacter.startLevelup ();
                    }
                }
            }
            public void updateCharacterParam(CharacterInfo charaInfo){
                foreach(GameObject charaObj in dungeonCharacterArray){
                    DungeonCharacter dungeonCharacter=charaObj.GetComponent<DungeonCharacter>();
                    if (dungeonCharacter.charaInfo.characterID == charaInfo.characterID) {
                        dungeonCharacter.charaInfo.swordID = charaInfo.swordID;
                        dungeonCharacter.charaInfo.armorID = charaInfo.armorID;
                        dungeonCharacter.charaInfo.shieldID = charaInfo.shieldID;
                        dungeonCharacter.charaInfo.treasureLevel = charaInfo.treasureLevel;
                        dungeonCharacter.charaInfo.battleLevel = charaInfo.battleLevel;
                        dungeonCharacter.charaInfo.attack = charaInfo.attack;
                        dungeonCharacter.charaInfo.attackSpeed = charaInfo.attackSpeed;
                        dungeonCharacter.charaInfo.maxHp = charaInfo.maxHp;
                        dungeonCharacter.charaInfo.maxSp = charaInfo.maxSp;
                        dungeonCharacter.charaInfo.Hp = charaInfo.Hp;
                        dungeonCharacter.charaInfo.Sp = charaInfo.Sp;
                        ItemController.characterEquip (dungeonCharacter.charaInfo, charaObj);
                    }
                }
            }
            public void playDamageAction(DamageInfo damageInfo){
                Debug.Log ("playDamageAction");
                foreach (GameObject dungeonCharacterObject in dungeonCharacterArray) {
                    DungeonCharacter dungeonCharacter = dungeonCharacterObject.GetComponent<DungeonCharacter> ();
                    if (dungeonCharacter.charaInfo.characterID == damageInfo.characterInfo.characterID) {
                        dungeonCharacter.playDamageAction (damageInfo);
                    }
                }
            }
			public void DestroyAllCharacter(){
				if(dungeonCharacterArray!=null){
					if(dungeonCharacterArray.Count>0){
                        for(int i=0;i<dungeonCharacterArray.Count;++i){
                            GameObject dungeonCharacterObject=dungeonCharacterArray[i];
                            dungeonCharacterObject.GetComponent<DungeonCharacter> ().beforeDestroy ();
							Destroy(dungeonCharacterObject);
						}
						dungeonCharacterArray.Clear();
                        SocketNetwork.Instance.charaInfoArray.Clear();
					}
				}
			}
		}
	}
}