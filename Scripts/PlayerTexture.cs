﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public class PlayerTexture{
        	protected const int PLAYER_TEXTURE_BLOCK_SIZE=128;
        	protected const int PLAYER_TEXTURE_BLOCK_SIZE_MINI64=64;
        	public const int PLAYER_TEXTURE_VSIZE=2048;
        	public const int PLAYER_TEXTURE_HSIZE=1024;
        	enum PlayerTexturePoint{
        		KISYU_TEX_POS=0,
        		PLAYER_TEX_POS,
        		WING_TEX_POS,
        	}
        	public class ItemInfo{
        		int itemForm=0;
        		int itemKind=0;
        		int itemColor=0;
        		int useItemFlag=0;
        		public int ItemForm{get{return itemForm;}}
        		public int ItemKind{get{return itemKind;}}
        		public int ItemColor{get{return itemColor;}}
        		public int UseItemFLag{get{return useItemFlag;}}

        		public ItemInfo(int itemID){
                    useItemFlag= Mathf.FloorToInt( itemID/100000000 );
                    itemForm   = Mathf.FloorToInt((itemID%100000000/1000000));
                    itemKind   = Mathf.FloorToInt((itemID%1000000  /10000));
                    itemColor  = Mathf.FloorToInt( itemID%1000 );
        			Debug.Log ("useItemFlag:"+useItemFlag.ToString());
        			Debug.Log ("itemForm:"+itemForm.ToString());
        			Debug.Log ("itemKind:"+itemKind.ToString());
        			Debug.Log ("itemColor:"+itemColor.ToString());
        		}
        	}
        	public enum enItemForm{
                weapon=1,
                body=2,
                shield=3,
                head=4,
            };
        	public enum enColorID{
        		defaultColor=0,
        		white=1,
        		blue=2,
        		yellow=3,
        		green=4,
        		red=5,
        		orange=6,
        		silver=7,
        		gold=8,
        		purple=10,
        	};
        	public static Dictionary<enColorID,Color> colorTable=new Dictionary<enColorID, Color>(){
        		{enColorID.white,new Color(255,255,255,255)},
        		{enColorID.blue,new Color(0,0,255,255)},
        		{enColorID.yellow,new Color(255,255,0,255)},
        		{enColorID.green,new Color(0,255,0,255)},
        		{enColorID.red,new Color(255,0,0,255)},
        		{enColorID.orange,new Color(255,165,0,255)},
        		{enColorID.silver,new Color(192,192,192,255)},
        		{enColorID.gold,new Color(255,215,0,255)},
        		{enColorID.purple,new Color(186,15,255,255)},
        	};
            public static Vector2 PlayerTextureMiniBasePos=new Vector2(0.0f,1024.0f);
            public static Dictionary<int,Vector2> PlayerTexturePos=new Dictionary<int,Vector2>(){
        		{0, new Vector2(128.0f*0.0f,128.0f* 2.0f)}, // hane01
        	};
        	public static Dictionary<int,Vector2> PlayerTextureMiniPos=new Dictionary<int,Vector2>(){
        		{0, new Vector2(64.0f*0.0f,64.0f* 0.0f)}, // hair01
        		{1, new Vector2(64.0f*1.0f,64.0f* 0.0f)}, // hair02
        		{2, new Vector2(64.0f*0.0f,64.0f* 1.0f)}, // hair03
        		{3, new Vector2(64.0f*1.0f,64.0f* 1.0f)}, // hair04
        		{4, new Vector2(64.0f*0.0f,64.0f* 2.0f)}, // hair05
        		{5, new Vector2(64.0f*1.0f,64.0f* 2.0f)}, // hair06
        		{6, new Vector2(64.0f*0.0f,64.0f* 3.0f)}, // hair07
        		{7, new Vector2(64.0f*1.0f,64.0f* 3.0f)}, // hair08
        		{8, new Vector2(64.0f*0.0f,64.0f* 4.0f)}, // hair09
        		{9, new Vector2(64.0f*1.0f,64.0f* 4.0f)}, // hair10
        		{10,new Vector2(64.0f*0.0f,64.0f* 5.0f)}, // hair11
        		{11,new Vector2(64.0f*1.0f,64.0f* 5.0f)}, // hair12
        		{12,new Vector2(64.0f*0.0f,64.0f* 6.0f)}, // empty
        		{13,new Vector2(64.0f*1.0f,64.0f* 6.0f)}, // empty
        		{14,new Vector2(64.0f*0.0f,64.0f* 7.0f)}, // empty
        		{15,new Vector2(64.0f*1.0f,64.0f* 7.0f)}, // empty
        		{16,new Vector2(64.0f*0.0f,64.0f* 8.0f)}, // eye01
        		{17,new Vector2(64.0f*1.0f,64.0f* 8.0f)}, // eye02
        		{18,new Vector2(64.0f*0.0f,64.0f* 9.0f)}, // eye03
        		{19,new Vector2(64.0f*1.0f,64.0f* 9.0f)}, // eye04
        		{20,new Vector2(64.0f*0.0f,64.0f*10.0f)}, // eye05
        		{21,new Vector2(64.0f*1.0f,64.0f*10.0f)}, // eye06
        		{22,new Vector2(64.0f*0.0f,64.0f*11.0f)}, // eye07
        		{23,new Vector2(64.0f*1.0f,64.0f*11.0f)}, // eye08
        		{24,new Vector2(64.0f*0.0f,64.0f*12.0f)}, // horn
        		{25,new Vector2(64.0f*1.0f,64.0f*12.0f)}, // ribbon
        		{26,new Vector2(64.0f*0.0f,64.0f*13.0f)}, // shadow
        		{27,new Vector2(64.0f*1.0f,64.0f*13.0f)}, // menko
        		{28,new Vector2(64.0f*0.0f,64.0f*14.0f)}, // empty
        		{29,new Vector2(64.0f*1.0f,64.0f*14.0f)}, // empty
        		{30,new Vector2(64.0f*0.0f,64.0f*15.0f)}, // empty
        		{31,new Vector2(64.0f*1.0f,64.0f*15.0f)}, // empty
        	};

            public static void MakePlayerTexture(PlayerInfo playerInfo,Texture2D baseTexture,Texture2D texture){
        		for(int x=0;x<PLAYER_TEXTURE_BLOCK_SIZE;++x){
        			for(int y=0;y<PLAYER_TEXTURE_BLOCK_SIZE;++y){
        				Color pixelColor=new Color(0,0,0,0);
        				// Player ColorSet
//                        pixelColor=baseTexture.GetPixel(((int)playerInfo.PlayerColor)*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-y);
//        				texture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(y+PLAYER_TEXTURE_BLOCK_SIZE*(int)PlayerTexturePoint.KISYU_TEX_POS),pixelColor);
//        				// Kisyu ColorSet
//        				pixelColor=baseTexture.GetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(y+PLAYER_TEXTURE_BLOCK_SIZE));
//        				texture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(y+PLAYER_TEXTURE_BLOCK_SIZE*(int)PlayerTexturePoint.PLAYER_TEX_POS),pixelColor);
//        				// Wing ColorSet
        				pixelColor=new Color(0,0,0,0);
        				// newTexture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(y+PLAYER_TEXTURE_BLOCK_SIZE*(int)PlayerTexturePoint.WING_TEX_POS),wingTexture.GetPixel(x,PLAYER_TEXTURE_BLOCK_SIZE-y));
        				texture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(y+PLAYER_TEXTURE_BLOCK_SIZE*(int)PlayerTexturePoint.WING_TEX_POS),pixelColor);
        				// Hair ColorSet
        				pixelColor=new Color(0,0,0,0);
        				// newTexture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(y+PLAYER_TEXTURE_BLOCK_SIZE*(int)PlayerTexturePoint.WING_TEX_POS),wingTexture.GetPixel(x,PLAYER_TEXTURE_BLOCK_SIZE-y));
        			}
        		}
        		for(int x=0;x<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++x){
        			for(int y=0;y<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++y){
        				Color pixelColor=new Color(0,0,0,0);
        				foreach(KeyValuePair<int,Vector2>pos in PlayerTextureMiniPos){
        					texture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x+(int)pos.Value.x,PLAYER_TEXTURE_VSIZE-(int)(y+PlayerTextureMiniBasePos.y+ pos.Value.y),pixelColor);
        				}
        			}
        		}
        		// set Wing Texture;
                if(playerInfo.characterInfo.swordID>0){
                    ItemInfo itemInfo=new ItemInfo(playerInfo.characterInfo.swordID);
        			int hanaID=0;
        			switch(itemInfo.ItemKind){
        			case 01:hanaID=0;break;
        			}
        			if(itemInfo.ItemKind>0){
        				for(int x=0;x<PLAYER_TEXTURE_BLOCK_SIZE;++x){
        					for(int y=0;y<PLAYER_TEXTURE_BLOCK_SIZE;++y){
        						setPlayerTexturePixel(hanaID,playerInfo,baseTexture,texture,x,y,(enColorID)itemInfo.ItemColor);
        					}
        				}
        			}
        		}

        		// setHair Texture;
                if(playerInfo.characterInfo.armorID>0){
                    ItemInfo itemInfo=new ItemInfo(playerInfo.characterInfo.armorID);
        			int hairID=0;
        			if(itemInfo.ItemKind>0){
        				switch(itemInfo.ItemKind){
        				case 01:hairID=0;break;
        				case 02:hairID=1;break;
        				case 03:hairID=2;break;
        				case 04:hairID=3;break;
        				case 05:hairID=4;break;
        				case 06:hairID=5;break;
        				case 07:hairID=6;break;
        				case 08:hairID=7;break;
        				case 09:hairID=8;break;
        				case 10:hairID=9;break;
        				case 11:hairID=10;break;
        				case 12:hairID=11;break;
        				}
        				for(int x=0;x<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++x){
        					for(int y=0;y<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++y){
        						setPlayerTextureMiniPixel(hairID,playerInfo,baseTexture,texture,x,y,(enColorID)itemInfo.ItemColor);
        					}
        				}
        			}
        		}
        		// setEye Texture;
        		{
        			int eyeID=16;
                    ItemInfo itemInfo=new ItemInfo(playerInfo.characterInfo.shieldID);
                    if(playerInfo.characterInfo.shieldID>0){
        				switch(itemInfo.ItemKind){
        				case 00:eyeID=16;break;
        				case 01:eyeID=17;break;
        				case 02:eyeID=18;break;
        				case 03:eyeID=19;break;
        				case 04:eyeID=20;break;
        				case 05:eyeID=21;break;
        				case 06:eyeID=22;break;
        				case 07:eyeID=23;break;
        				}
        			}
        			for(int x=0;x<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++x){
        				for(int y=0;y<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++y){
        					setPlayerTextureMiniPixel(eyeID,playerInfo,baseTexture,texture,x,y,(enColorID)itemInfo.ItemColor);
        				}
        			}
        		}
        		// set Horn Texture;
                if(playerInfo.characterInfo.headID>0){
                    ItemInfo itemInfo=new ItemInfo(playerInfo.characterInfo.headID);
        			int tunoID=0;
        			switch(itemInfo.ItemKind){
        			case 01:tunoID=24;break;
        			}
        			if(tunoID>0){
        				for(int x=0;x<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++x){
        					for(int y=0;y<PLAYER_TEXTURE_BLOCK_SIZE_MINI64;++y){
        						setPlayerTextureMiniPixel(tunoID,playerInfo,baseTexture,texture,x,y,(enColorID)itemInfo.ItemColor);
        					}
        				}
        			}
        		}
        	}
            public static void setPlayerTextureMiniPixel(int itemPosID,PlayerInfo playerInfo,Texture2D getTexture,Texture2D putTexture,int x,int y,enColorID color=enColorID.defaultColor){
                Color pixelColor=getTexture.GetPixel((int) PlayerTextureMiniPos[itemPosID].x+x,PLAYER_TEXTURE_VSIZE-(int)(y+PlayerTextureMiniBasePos.y+ PlayerTextureMiniPos[itemPosID].y));
        		if(color!=enColorID.defaultColor){
        			Color paintColor=colorTable[color];
        			pixelColor=ColorBlend(paintColor,pixelColor);
        		}
        		putTexture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x+(int)PlayerTextureMiniPos[itemPosID].x,PLAYER_TEXTURE_VSIZE-(int)(y+PlayerTextureMiniBasePos.y+ PlayerTextureMiniPos[itemPosID].y),pixelColor);
        	}
            public static void setPlayerTexturePixel(int itemPosID,PlayerInfo playerInfo,Texture2D getTexture,Texture2D putTexture,int x,int y,enColorID color=enColorID.defaultColor){
        		Color pixelColor=getTexture.GetPixel((int) PlayerTexturePos[itemPosID].x*PLAYER_TEXTURE_BLOCK_SIZE+x,PLAYER_TEXTURE_VSIZE-(int)(y+PlayerTexturePos[itemPosID].y));
        		if(color!=enColorID.defaultColor){
        			Color paintColor=colorTable[color];
        			pixelColor=ColorBlend(paintColor,pixelColor);
        		}
        		putTexture.SetPixel(playerInfo.textureSetPosNo*PLAYER_TEXTURE_BLOCK_SIZE+x+(int) PlayerTexturePos[itemPosID].x,PLAYER_TEXTURE_VSIZE-(int)(y+PlayerTexturePos[itemPosID].y),pixelColor);
        	}
        	public static Color ColorBlend(Color color1, Color color2)
        	{
        		return new Color(
        			color1.r*color2.r/255,
        			color1.g*color2.g/255,
        			color1.b*color2.b/255,
        			color1.a*color2.a/255
        			);
        		uint c1 = unchecked(ToUint(color1));
        		uint c2 = unchecked(ToUint(color2));
        		return ToColor(
        			unchecked((uint)(
        			((c1 & 0xfefefefe) >> 1) + ((c2 & 0xfefefefe) >> 1) + (c1 & c2 & 0x01010101)
        			)));
        	}
        	public static Color ColorAdd(Color color1, Color color2)
        	{
        		return new Color(
        			color1.r+color2.r,
        			color1.g+color2.g,
        			color1.b+color2.b,
        			color1.a
        			);
        		uint c1 = unchecked(ToUint(color1));
        		uint c2 = unchecked(ToUint(color2));
        		return ToColor(
        			unchecked((uint)(
        			((c1 & 0xfefefefe) >> 1) + ((c2 & 0xfefefefe) >> 1) + (c1 & c2 & 0x01010101)
        			)));
        	}
        	private static uint ToUint(Color c)
        	{
        		return (uint)((((uint)c.a << 24) | ((uint)c.r << 16) | ((uint)c.g << 8) | (uint)c.b) & 0xffffffffL);
        	}
        	private static Color ToColor(uint value)
        	{
        		return new Color(
        		    (byte)((value >> 16) & 0xFF),
        		    (byte)((value >> 8) & 0xFF),
        		    (byte) (value & 0xFF),
        			(byte)((value >> 24) & 0xFF)
        		);
        	}
        }
    }
}