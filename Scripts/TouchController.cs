﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour {
    public Camera uiCamera;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        bool touchFlag = false;
        Vector2 mousePos;
        if (Input.GetMouseButtonDown(0)){
            Ray ray= new Ray();
            RaycastHit hit=new RaycastHit();
            mousePos=Input.mousePosition;
            ray = uiCamera.ScreenPointToRay(Input.mousePosition);
            // マウスの位置が、カメラに写っていて、且つ、カメラから100以内にいるかチェック
            if (Physics.Raycast(ray, out hit, 1000.0f)) {
                if(hit.collider.gameObject.name == "Map0"){
                    touchFlag=true;
                }
                Debug.Log (hit.collider.gameObject.name);
            }
        }
	
	}
}
