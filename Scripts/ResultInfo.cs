﻿using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public class TreasureInfo{
            public int rank = 0;
            public int serial = 0;
            public ItemInfo itemInfo = new ItemInfo(); 
        }
        public class ResultInfo{
            public List<TreasureInfo> treasures = new List<TreasureInfo>();
            public Dictionary<TreasureKind,int> treasureCount = new Dictionary<TreasureKind,int>(){
                {TreasureKind.TREASURE_BRONZE,0},
                {TreasureKind.TREASURE_SILVER,0},
                {TreasureKind.TREASURE_GOLD,0},
            };
            // 使ったアイテムのリスト(itemBoxNo,持っている数)
            public Dictionary<int,ItemInfo> useItems = new Dictionary<int,ItemInfo>();
        }
    }
}