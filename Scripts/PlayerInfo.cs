﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	namespace ThpMmo{
		public class PlayerInfo : SingletonMonoBehaviour<PlayerInfo> {
			public CharacterInfo characterInfo;
            public Dictionary<int,ItemInfo> myItemList;
            public List<MailFormat> myMailList;
            public List<CharacterInfo> friendList;
            public List<int> magicSlot = new List<int>(BattleMenuController.SLOT_NUM);
            public ResultInfo resultInfo = new ResultInfo();
            public int textureSetPosNo;

            const int MAX_ITEM_COUNT = 50;
            void Awake(){
				characterInfo = new CharacterInfo ();
                myItemList = new Dictionary<int,ItemInfo> (MAX_ITEM_COUNT);
                myMailList = new List<MailFormat> ();
                friendList = new List<CharacterInfo> ();
			}
            /// <summary>
            /// Gets the item.
            /// </summary>
            /// <returns>The item.</returns>
            /// <param name="itemBoxNo">Item box no.</param>
            public ItemInfo getItem(int itemBoxNo){
                for (int i=0;i<myItemList.Count;++i) {
                    if (myItemList[i].itemBoxNo == itemBoxNo) {
                        return myItemList[i];
                    }
                }
                return null;
            }
            /// <summary>
            /// Sets the item.
            /// </summary>
            /// <returns>The item.</returns>
            /// <param name="setItemInfo">Set item info.</param>
            public void setItem(ItemInfo setItemInfo){
                for (int i=0;i<myItemList.Count;++i) {
                    if (myItemList[i].itemBoxNo == setItemInfo.itemBoxNo) {
                        myItemList [i] = setItemInfo;
                    }
                }
            }
            public ItemInfo setItemWithOutBoxNo(ItemInfo setItemInfo){
                for (int i = 0; i < myItemList.Count; ++i) {
                    // すでに持っていたなら
                    if (myItemList[i].id == setItemInfo.id) {
                        myItemList [i].count += setItemInfo.count;
                        return myItemList[i];
                    }
                }
                // 未取得アイテムのとき
                // アイテムBOXの空きを探す
                int itemBoxNo = ItemUtility.getSpaceItemBoxNo (PlayerInfo.Instance.myItemList);
                if (itemBoxNo == ItemUtility.itemBoxNone) {
                    MenuController.Instance.OpenDialog (DialogForm.DIALOG_OK, GameTextTable.GameDialogtext()["notSpaceItemBox"]);
                } else {
                    setItemInfo.itemBoxNo = itemBoxNo;
                    myItemList [itemBoxNo] = setItemInfo;
                }
                return myItemList [itemBoxNo];
            }
            /// <summary>
            /// Deletes the item.
            /// </summary>
            /// <returns>The item.</returns>
            /// <param name="itemBoxNo">Item box no.</param>
            public void deleteItem(int itemBoxNo){
                for (int i=0;i<myItemList.Count;++i) {
                    if (myItemList[i].itemBoxNo == itemBoxNo) {
                        myItemList[i].itemBoxNo=0;
                    }
                }
            }
            public void levelup(CharacterInfo levelupStatus){
                Debug.Log ("----------LEVELUP--------------------");
                characterInfo.battleLevel = levelupStatus.battleLevel;
                characterInfo.attack = levelupStatus.attack;
                characterInfo.defence = levelupStatus.defence;
                characterInfo.attackSpeed = levelupStatus.attackSpeed;
                characterInfo.maxHp = levelupStatus.maxHp;
                characterInfo.maxSp = levelupStatus.maxSp;
                characterInfo.Hp = levelupStatus.Hp;
                characterInfo.Sp = levelupStatus.Sp;
                // メーター類の更新
                DungeonPlayer.Instance.updateHeader ();
            }

		}
	}
}