﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	namespace ThpMmo{
        public class MailFormat{
            public int mailNo = 0;
            public string from = "";
            public string friendCode = "";
            public string subject = "";
			public string body = "";
            public ItemInfo itemInfo = new ItemInfo();
		}
	}
}