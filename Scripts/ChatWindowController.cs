﻿using UnityEngine;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public class ChatWindowController : ThpmmoMonoBehavier {
            public List<GameObject> chatChangeButtons;
            public UILabel nowModeButtonLabel;
            public GameObject WhisperListWindow;
            public GameObject ModeSelectWindow;
            public GameObject ChatWindow;
            public ChatController chatController;
            public GameObject openButton;
            public GameObject closeButton;
            public GameObject whisperList;
            public GameObject toWhisperWindow;
        	// Use this for initialization
        	void Start () {
                for (int i = 0; i < chatChangeButtons.Count; ++i) {
                    UIButton button = chatChangeButtons [i].GetComponent<UIButton>();
                    EventDelegate callback = new EventDelegate (this, "changeChatMode");
                    callback.parameters [0].value = (ChatMode)i;
                    UILabel label = chatChangeButtons [i].GetComponentInChildren<UILabel> ();
                    callback.parameters [1].value = chatChangeButtons [i].GetComponentInChildren<UILabel> ();
                    button.onClick.Add (callback);
                    button.enabled = true;
                }
                WhisperListWindow.SetActive (false);
                ModeSelectWindow.SetActive (false);
                ChatWindow.SetActive (false);
                openButton.SetActive (true);
                closeButton.SetActive (false);
                toWhisperWindow.SetActive (false);
        	}
        	
        	// Update is called once per frame
        	void Update () {
        	
        	}
            void changeChatMode(ChatMode rChatMode,UILabel label){
                chatController.chatMode = rChatMode;
                nowModeButtonLabel.text = label.text;
                nowModeButtonLabel.gradientTop = label.gradientTop;
                nowModeButtonLabel.gradientBottom = label.gradientBottom;
                nowModeButtonLabel.effectColor = label.effectColor;
                changeOpenSelecetMenu ();
                if (rChatMode == ChatMode.CHAT_MODE_TARGET) {
                    StartCoroutine (HttpController.Instance.GetFriendList (openWhisperList));
                } else {
                    WhisperListWindow.SetActive (false);
                    whisperList.SetActive (false);
                    toWhisperWindow.SetActive (false);
                }
            }
            public void changeOpenSelecetMenu(){
                ModeSelectWindow.SetActive (!ModeSelectWindow.activeSelf);
            }
            public void changeOpenChatWindow(){
                ChatWindow.SetActive (!ChatWindow.activeSelf);
                openButton.SetActive (!ChatWindow.activeSelf);
                closeButton.SetActive (ChatWindow.activeSelf);
            }
            private void openWhisperList(){
                WhisperListWindow.SetActive (true);
                whisperList.SetActive (true);
                toWhisperWindow.SetActive (true);

                // アイテム選択リスト生成
                whisperList.SetActive (true);

                PlayerInfo.Instance.friendList.Clear ();
                while (HttpController.Instance.friendList.Count>0) {
                    PlayerInfo.Instance.friendList.Add (HttpController.Instance.friendList.Dequeue ());
                }
                reloadFriendList (whisperList);
            }
            public void reloadFriendList(GameObject listObject){
                UIGrid grid = listObject.GetComponentInChildren<UIGrid> ();
                MenuController.DestroyList (grid.gameObject);

                int pos = 0;
                for (int i = 0; i<PlayerInfo.Instance.friendList.Count; ++i) {
                    CharacterInfo charaInfo = PlayerInfo.Instance.friendList[i];
                    GameObject listObj = Instantiate ((GameObject)Resources.Load ("Prefab/List/L_Whisper"));
                    foreach(UILabel label in listObj.GetComponentsInChildren<UILabel> ()){
                        switch (label.name) {
                        case "Name":
                            label.text = charaInfo.name;
                            break;
                        }
                    }
                    listObj.transform.parent = grid.gameObject.transform;
                    foreach (UIButton button in listObj.GetComponentsInChildren<UIButton> ()) {
                        EventDelegate callback= null;
                        Debug.Log (button.name);
                        switch(button.name){
                        case "B_Select":
                            callback = new EventDelegate (this, "selectTarget");
                            callback.parameters [0].value = i;
                            break;
                        }
                        if (callback!=null) {
                        button.onClick.Add (callback);
                        button.enabled = true;
                        }
                    }
                    listObj.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
                    listObj.transform.localScale = Vector3.one;
                }
                UIScrollView scrollView = listObject.GetComponentInChildren<UIScrollView> ();
                grid.Reposition ();
                scrollView.ResetPosition();
            }
            public void selectTarget(int target){
                string[] friendCode = PlayerInfo.Instance.friendList[target].friendCode.Split ('-');
                foreach (UILabel label in toWhisperWindow.GetComponentsInChildren<UILabel>()) {
                    if (label.name == "Name") {
                        label.text = PlayerInfo.Instance.friendList [target].name;
                    }
                }
                chatController.whisperTargetID = int.Parse (friendCode [1]);
                WhisperListWindow.SetActive (false);
                whisperList.SetActive (false);
            }
        }
    }
}