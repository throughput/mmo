﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
	namespace ThpMmo{
        public class MainController : SingletonMonoBehaviour<MainController> {
            public GameObject tounObject;
            public GameObject dungeonObject;
            public GameObject userObject;
            public GameObject floorTitle;
            public UILabel floorLabel;
            public UILabel goldTreasureCountLabel;
            public UILabel silverTreasureCountLabel;
            public UILabel bronzeTreasureCountLabel;
            // アイテム情報ここにおくけど、のちのちに最適な場所に移動
            public Dictionary<string,ItemTableInfo> itemTableDic;
            public Dictionary<string,StoneTableInfo> stoneTableDic;
            // モンスター情報
            public Dictionary<string,MonsterTableInfo> monsterTableInfoDic;

            public List<float> powerUpPointList;
            public List<int> successPointList;
            public List<float> powerUpProbAtSlotList;
            public const float FLOOR_TIME = 5.0f;
            enum DungeonID{
                CAME_1F = 1,
                GACHA_ROOM_ID_BRONZE = 201,
                GACHA_ROOM_ID_SILVER = 202,
                GACHA_ROOM_ID_GOLD = 203,
            }

            // Use this for initialization
			void Start () {
                floorTitle.SetActive (false);
                itemTableDic = new Dictionary<string,ItemTableInfo> ();
                {
                    string itemTableStr = ((TextAsset)Resources.Load ("weaponList")).text;
                    Hashtable jsonData = (Hashtable)JsonController.JsonDecode (itemTableStr);
                    {
                        foreach (string key in jsonData.Keys) {
                            Hashtable itemTable = (Hashtable)jsonData [key];
                            ItemTableInfo itemTableInfo = new ItemTableInfo ();
                            itemTableInfo.name = (string)itemTable ["name"];
                            itemTableInfo.file = (string)itemTable ["file"];
                            itemTableInfo.atk = int.Parse ((string)itemTable ["ak"]);
                            itemTableInfo.speed = int.Parse ((string)itemTable ["sp"]);
                            itemTableInfo.stamina = int.Parse ((string)itemTable ["st"]);
                            itemTableInfo.kind = (EquipKind)int.Parse ((string)itemTable ["kd"]);
                            itemTableDic.Add (key, itemTableInfo);
                        }
                    }
                }
                stoneTableDic = new Dictionary<string,StoneTableInfo> ();
                {
                    string itemTableStr = ((TextAsset)Resources.Load ("stoneList")).text;
                    Hashtable jsonData = (Hashtable)JsonController.JsonDecode (itemTableStr);
                    {
                        foreach (string key in jsonData.Keys) {
                            Hashtable itemTable = (Hashtable)jsonData [key];
                            StoneTableInfo stoneTableInfo = new StoneTableInfo ();
                            stoneTableInfo.name = (string)itemTable ["name"];
                            stoneTableInfo.file = (string)itemTable ["file"];
                            stoneTableDic.Add (key, stoneTableInfo);
                        }
                    }
                }
                monsterTableInfoDic = new Dictionary<string,MonsterTableInfo> ();
                {
                    string monsterTableInfoTalbeStr = ((TextAsset)Resources.Load ("monsterList")).text;
                    Hashtable jsonData = (Hashtable)JsonController.JsonDecode (monsterTableInfoTalbeStr);
                    {
                        foreach (string key in jsonData.Keys) {
                            Hashtable monsterTable = (Hashtable)jsonData [key];
                            MonsterTableInfo monsterTableInfo = new MonsterTableInfo ();
                            monsterTableInfo.name = (string)monsterTable ["name"];
                            monsterTableInfo.imageID = int.Parse((string)monsterTable ["imageID"]);
                            monsterTableInfo.isBoss = System.Convert.ToBoolean(int.Parse((string)monsterTable ["isBoss"]));
                            monsterTableInfoDic.Add (key, monsterTableInfo);
                        }
                    }
                }

                {
                    string str = ((TextAsset)Resources.Load ("weaponPower")).text;
                    ArrayList jsonList = (ArrayList)JsonController.JsonDecode (str);
                    foreach (string data in jsonList) {
                        powerUpPointList.Add (float.Parse (data));
                    }
                } 
                {
                    string str = ((TextAsset)Resources.Load ("powerupProbAtSlot")).text;
                    ArrayList jsonList = (ArrayList)JsonController.JsonDecode (str);
                    foreach (string data in jsonList) {
                        powerUpProbAtSlotList.Add (float.Parse (data));
                    }
                } 
                {
                    string str = ((TextAsset)Resources.Load ("weaponPoint")).text;
                    ArrayList jsonList = (ArrayList)JsonController.JsonDecode (str);
                    foreach (string data in jsonList) {
                        successPointList.Add (int.Parse (data));
                    }
                }
                dungeonObject.SetActive (false);
			}
            public void getMailCount(){
                StartCoroutine(HttpController.Instance.GetMailCount (getMailCountCallBack));
            }
            public void getMailCountCallBack(MailError mailError){
                MenuController.Instance.setMailCount(HttpController.Instance.mailCount);
            }
			// Update is called once per frame
			void Update () {
                if (PlayerInfo.Instance.characterInfo.mapCode == 0) {
                    floorLabel.text = "街";
                } else {
                    floorLabel.text = PlayerInfo.Instance.characterInfo.mapCode + "F";
                }
                goldTreasureCountLabel.text = PlayerInfo.Instance.resultInfo.treasureCount[TreasureKind.TREASURE_GOLD].ToString();
                silverTreasureCountLabel.text = PlayerInfo.Instance.resultInfo.treasureCount[TreasureKind.TREASURE_SILVER].ToString();
                bronzeTreasureCountLabel.text = PlayerInfo.Instance.resultInfo.treasureCount[TreasureKind.TREASURE_BRONZE].ToString();
			}
            /// <summary>
            /// Moves the dungeon.
            /// </summary>
            public void MoveDungeon(){
                PlayerInfo.Instance.characterInfo.mapCode = (int)DungeonID.CAME_1F;
                MainController.Instance.playStageBgm ();
                tounObject.SetActive (false);
                dungeonObject.SetActive (true);

                // 表示中のキャラを消す
                DungeonCharacterManager.Instance.DestroyAllCharacter ();
                DungeonMonsterManager.Instance.DestroyAllMonster ();

                MakeDungeon (HttpController.Instance.dungeonData);
                SocketNetwork.Instance.sendLogin(PlayerInfo.Instance.characterInfo);
                SocketNetwork.Instance.sendMoveData(PlayerInfo.Instance.characterInfo);
                // フロア表示
                floorTitleView ("洞窟 地下"+PlayerInfo.Instance.characterInfo.mapCode +"F");
            }
            /// <summary>
            /// Moves the gacha room.
            /// </summary>
            /// <param name="gachaKind">Gacha kind.</param>
            public void MoveGachaRoom(GachaController.GachaKind gachaKind){
                switch (gachaKind) {
                case GachaController.GachaKind.GACHA_BRONZE:
                    PlayerInfo.Instance.characterInfo.mapCode = (int)DungeonID.GACHA_ROOM_ID_BRONZE;
                    break;
                case GachaController.GachaKind.GACHA_SILVER:
                    PlayerInfo.Instance.characterInfo.mapCode = (int)DungeonID.GACHA_ROOM_ID_SILVER;
                    break;
                case GachaController.GachaKind.GACHA_GOLD:
                    PlayerInfo.Instance.characterInfo.mapCode = (int)DungeonID.GACHA_ROOM_ID_GOLD;
                    break;
                }
                tounObject.SetActive (false);
                dungeonObject.SetActive (true);

                // 表示中のキャラを消す
                DungeonCharacterManager.Instance.DestroyAllCharacter ();
                DungeonMonsterManager.Instance.DestroyAllMonster ();

                MakeDungeon (GachaController.makeGachaRoom (gachaKind),true);
                SocketNetwork.Instance.sendLogin(PlayerInfo.Instance.characterInfo);
                SocketNetwork.Instance.sendMoveData(PlayerInfo.Instance.characterInfo);
            }
            /// <summary>
            /// Nexts the floor callback.
            /// </summary>
            public void NextFloorCallback(){
                floorTitleView ("洞窟 地下"+PlayerInfo.Instance.characterInfo.mapCode +"F");
                MakeDungeon (HttpController.Instance.dungeonData);
                SocketNetwork.Instance.sendLogin(PlayerInfo.Instance.characterInfo);
                SocketNetwork.Instance.sendMoveData(PlayerInfo.Instance.characterInfo);
                // 表示中のキャラを消す
                DungeonCharacterManager.Instance.DestroyAllCharacter ();
                DungeonMonsterManager.Instance.DestroyAllMonster ();
            }
            /// <summary>
            /// Floors the title view.
            /// </summary>
            /// <param name="title">Title.</param>
            void floorTitleView(string title){
                floorTitle.SetActive (true);
                UILabel floorTitleLabel = floorTitle.GetComponentInChildren<UILabel> ();
                floorTitleLabel.text = title;
                StartCoroutine (MainController.timerSetActive(floorTitle,false,FLOOR_TIME));
            }
            /// <summary>
            /// Floors the title hide.
            /// </summary>
            /// <returns>The title hide.</returns>
            public static IEnumerator timerSetActive(GameObject gameObject,bool active,float time){
                yield return new WaitForSeconds(time);
                gameObject.SetActive (active);
            }
            /// <summary>
            /// Plaies the stage bgm.
            /// </summary>
            public void playStageBgm(){
                // BGMを選択
                int floor = PlayerInfo.Instance.characterInfo.mapCode;
                if (floor == 0) {
                    SoundManager.Instance.PlayBGM ((int)BgmList.TOWN);
                }else if (floor < 11) {
                    SoundManager.Instance.PlayBGM ((int)BgmList.CAVE_01_10);
                } else if (floor < 21) {
                    SoundManager.Instance.PlayBGM ((int)BgmList.CAVE_11_20);
                } else if (floor < 31) {
                    SoundManager.Instance.PlayBGM ((int)BgmList.CAVE_21_30);
                } else if (floor < 41) {
                    SoundManager.Instance.PlayBGM ((int)BgmList.CAVE_31_40);
                } else if (floor < 51) {
                    SoundManager.Instance.PlayBGM ((int)BgmList.CAVE_41_50);
                }
            }
            /// <summary>
            /// Makes the dungeon.
            /// </summary>
            /// <param name="mapData">Map data.</param>
            /// <param name="gachaFlag">If set to <c>true</c> gacha flag.</param>
            public void MakeDungeon(string mapData,bool gachaFlag = false){
                // ダンジョンクリア
                foreach (Transform child in dungeonObject.transform) {
                    Destroy (child.gameObject);
                }
                mapData = DungeonMaker.Instance.SetFrame (mapData);
                DungeonMaker.Instance.SetTips (mapData,gachaFlag);

                DungeonPlayer dungeonPlayer = userObject.GetComponent<DungeonPlayer> ();
                dungeonPlayer.setDungeonStartPosion (mapData);
            }

            /// <summary>
            /// Moves the town.
            /// </summary>
            public void MoveTown(){
                tounObject.SetActive (true);
                dungeonObject.SetActive (false);
            }

            /// <summary>
            /// Goals the floor.
            /// </summary>
            public void GoalFloor(){
                Debug.Log ("GoalFloor");
                if (PlayerInfo.Instance.resultInfo.useItems.Count > 0) {
                    StartCoroutine (HttpController.Instance.ResultItemChange (ResultItemCallBack, PlayerInfo.Instance.resultInfo.useItems));
                } else {
                    // 消費アイテム増減なしの場合
                    ResultItemCallBack (ChangeItemResult.SUCCESS);
                }
            }

            /// <summary>
            /// Goals the floor call back.
            /// </summary>
            /// <param name="result">Result.</param>
            private void ResultItemCallBack(ChangeItemResult result){
                if (result == ChangeItemResult.SUCCESS) {
                    PlayerInfo.Instance.resultInfo.useItems.Clear ();
                    ResultController.Instance.OpenResult ();
                }
            }

            /// <summary>
            /// Results the send.
            /// </summary>
            /// <param name="growupPOint">Growup Point.</param>
            public void ResultSend(){
                // ここで階数すすめる
                ++PlayerInfo.Instance.characterInfo.mapCode;
                // ここのソケットの戻りで次の階へすすめる。
                SocketNetwork.Instance.sendResultFloor (PlayerInfo.Instance.characterInfo, PlayerInfo.Instance.resultInfo.treasures, ResultController.Instance.growupPoint);
            }

            public void NextFloor(){
                // リザルトをクリアする
                PlayerInfo.Instance.resultInfo.treasures.Clear ();
                PlayerInfo.Instance.resultInfo.treasureCount [TreasureKind.TREASURE_GOLD] = 0;
                PlayerInfo.Instance.resultInfo.treasureCount [TreasureKind.TREASURE_SILVER] = 0;
                PlayerInfo.Instance.resultInfo.treasureCount [TreasureKind.TREASURE_BRONZE] = 0;
                StartCoroutine(HttpController.Instance.DownLoadDungeon(NextFloorCallback,PlayerInfo.Instance.characterInfo.mapCode));
            }
		}
	}
}