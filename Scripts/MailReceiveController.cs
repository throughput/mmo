﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Throughputjp{
    namespace ThpMmo{
        public class MailReceiveController : ThpmmoMonoBehavier {
            public GameObject itemWindow;
            public MailFormat mailformat = null;
            Dictionary<string,GameObject> popupMailItemSlot = new Dictionary<string,GameObject>();

            void Awake(){
                foreach (UISprite sprite in gameObject.GetComponentsInChildren<UISprite>()) {
                    if (sprite.name.StartsWith ("Slot")) {
                        popupMailItemSlot.Add (sprite.gameObject.name, sprite.gameObject);
                    }
                }
            }
            void OnEnable () {
                if (mailformat != null) {
                    foreach (UILabel label in gameObject.GetComponentsInChildren<UILabel>()) {
                        switch (label.name) {
                        case "Message":
                            label.text = mailformat.body;
                            break;
                        }
                    }
                    // 添付アイテムがあった時はアイテム表示
                    if (mailformat.itemInfo.id > 0) {
                        itemWindow.SetActive (true);
                        ItemTableInfo itemTable = MainController.Instance.itemTableDic [mailformat.itemInfo.id.ToString ()];

                        foreach (UILabel label in gameObject.GetComponentsInChildren<UILabel>()) {
                            switch (label.name) {
                            case "Name":
                                label.text = itemTable.name;
                                break;
                            case "Param1":
                                label.text = ItemUtility.getAttack (itemTable.atk, mailformat.itemInfo.rank).ToString ();
                                break;
                            case "Param2":
                                label.text = itemTable.speed.ToString ();
                                break;
                            case "Param3":
                                label.text = mailformat.itemInfo.rank.ToString ();
                                break;
                            }
                        }
                    } else {
                        itemWindow.SetActive (false);
                    }
                }
        	}
            // メール受け取り既読
            public void alreadyMail(){
                int itemBoxNo = 0;
                if (mailformat.itemInfo.id>0) {
                    itemBoxNo = ItemUtility.getSpaceItemBoxNo (PlayerInfo.Instance.myItemList);
                    if (itemBoxNo == ItemUtility.itemBoxNone) {
                        MenuController.Instance.CloseMsg (gameObject);
                        MenuController.Instance.OpenDialog(DialogForm.DIALOG_OK,GameTextTable.GameDialogtext()["notSpaceItemBox"]);
                    }
                    mailformat.itemInfo.itemBoxNo = itemBoxNo;
                }
                StartCoroutine(HttpController.Instance.OpenMail (alreadyMailCallback, mailformat.mailNo, itemBoxNo));
            }
            public void alreadyMailCallback(MailError mailError){
                MenuController.Instance.CloseMsg (gameObject);
                if (mailError != MailError.SUCCESS) {
                    MenuController.Instance.OpenDialog(DialogForm.DIALOG_OK,GameTextTable.CommonDialogtext()["mailRecvError"]);
                    return;
                }
                for (int i = 0; i < PlayerInfo.Instance.myItemList.Count; ++i){
                    ItemInfo itemInfo = PlayerInfo.Instance.myItemList [i];
                    if (itemInfo.itemBoxNo == mailformat.itemInfo.itemBoxNo) {
                        PlayerInfo.Instance.myItemList [i] = mailformat.itemInfo;
                    }
                }
                PlayerInfo.Instance.myMailList.Remove (mailformat);
                MenuController.Instance.reloadMailList ();
                // メールの数を減らす
                if (HttpController.Instance.mailCount > 0) {
                    --HttpController.Instance.mailCount;
                }
                MenuController.Instance.setMailCount (HttpController.Instance.mailCount);
            }
        	
        }
    }
}