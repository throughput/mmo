﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Throughputjp{
	namespace ThpMmo{
        public enum EquipKind{
            EQUIP_SWORD = 0,
            EQUIP_ARMOR = 1,
            EQUIP_SHIELD = 2,
            EQUIP_MAX = 3,
        }
        public enum FloorResultResult{
            SUCCESS =1,
            ERROR = 0,
        }

        public class SocketNetwork : SingletonMonoBehaviour<SocketNetwork> {
			const byte startByte0=0xff;
			const byte startByte1=0xfe;
			const byte liveByte=0xfc;
			const byte endByte0=0xff;
			const byte endByte1=0xfd;
			public List<CharacterInfo> charaInfoArray;
            public List<MonsterInfo> monsterInfoArray;
			public List<ChatInfo> chatInfoArray;
			public List<int> deleteCharaIDArray;
            public List<int> deleteMonsterIDArray;
            public Dictionary<int,string> nameList;
			const float LIVE_SEND_TIMER=20.0f;
			private float liveTimer = LIVE_SEND_TIMER;
            public Queue<DamageInfo> damageInfoQueue = new Queue<DamageInfo>();
            public Queue<AttackInfo> attackInfoQueue = new Queue<AttackInfo>();
            public Queue<BattleResult> battleResultQueue = new Queue<BattleResult>();
            public CharacterInfo levelUpStatus = null;
            enum commandID{
				COM_CHAT=0,
				COM_MOVE=1,
                COM_SET_NAME=2,
				COM_END_CHARA=3,
				COM_LOGIN=5,
                COM_MOSNTER_START=6,
                COM_MOSNTER_MOVE=7,
                COM_EQUIP_SWORD=8,
                COM_EQUIP_ARMOR=9,
                COM_EQUIP_SHIELD=10,
                COM_CHARAINFO_ALL=11,
                COM_SEND_MAIL_NOTICE=12,
                COM_SET_EQUIP=13,
                COM_WISPER_CHAT=14,
                COM_WISPER_CHAT_FAIL=15,
                COM_CHAT_ALL=16,
                COM_RESULT_FLOOR=17,
                COM_BATTLE_MONSTER_ATTACK = 100,
                COM_BATTLE_PLAYER_ATTACK  = 101,
                COM_BATTLE_PLAYER_HEAL    = 102,
                COM_BATTLE_MONSTER_HEAL   = 103,
                COM_BATTLE_U_TO_M_DRAIN   = 104,
                COM_BATTLE_M_TO_U_DRAIN   = 105,
                COM_BATTLE_WIN   = 106,
                COM_LEVEL_UP   = 107,
			}

			// Use this for initialization
			void Start () {
				charaInfoArray = new List<CharacterInfo> ();
				charaInfoArray.Clear ();
                monsterInfoArray = new List<MonsterInfo> ();
                monsterInfoArray.Clear ();
				chatInfoArray = new List<ChatInfo> ();
				chatInfoArray.Clear ();
				deleteCharaIDArray = new List<int>();
				deleteCharaIDArray.Clear ();
                deleteMonsterIDArray = new List<int>();
                deleteMonsterIDArray.Clear ();
                nameList = new Dictionary<int,string> ();
                nameList.Clear ();
			}
			
			// Update is called once per frame
			void Update () {
				if(IsConnect()){
					liveTimer -= Time.deltaTime;
					if( 0.0f >= liveTimer ){
						sendLive();
					}
				}else{
					return;
				}
				byte[] dat = SocketClient.Instance.recvData;
				byte[] tempByte;
				if(dat!=null && dat.Length>0){
                    int i=0;
                    while ( i<dat.Length) {
                        Debug.Log (string.Format ("{0}/{1}", i, dat.Length));
                        if (i + 1 < dat.Length) {
                            Debug.Log (string.Format ("{0}-{1}", dat [i].ToString ("X"), dat [i + 1].ToString ("X")));
                        }
                        if (dat [i] == 255 && dat [i + 1] == 254) {
                            ++i; // startCode
                            ++i; // startCode
                            tempByte = new byte[]{ dat [i++], 0, 0, 0 };
                            commandID command = (commandID)BitConverter.ToInt32 (tempByte, 0);
                            Debug.Log (string.Format ("command:{0}", command));
                            switch (command) {
                            // チャットデータ受信
                            case commandID.COM_CHAT:
                                i = RecvChat (dat, i, command);
                                break;
                            // 移動データ受信
                            case commandID.COM_MOVE:
                                i = RecvMove (dat, i);
                                break;
                            // ログイン時情報取得
                            case commandID.COM_LOGIN:
                                i = RecvLogin (dat, i);
                                break;
                            // 名前データ受信
                            case commandID.COM_SET_NAME:
                                i = RecvSetName (dat, i);
                                break;
                            // 剣装備受信
                            case commandID.COM_EQUIP_SWORD:
                                i = RecvSetEquip (dat, i, EquipKind.EQUIP_SWORD);
                                break;
                            // 鎧装備受信
                            case commandID.COM_EQUIP_ARMOR:
                                i = RecvSetEquip (dat, i, EquipKind.EQUIP_ARMOR);
                                break;
                            // 盾装備受信
                            case commandID.COM_EQUIP_SHIELD:
                                i = RecvSetEquip (dat, i, EquipKind.EQUIP_SHIELD);
                                break;
                            case commandID.COM_END_CHARA:
                                i = RecvEndChara (dat, i);
                                break;
                            // モンスター開始
                            case commandID.COM_MOSNTER_START:
                                i = RecvMonsterMove (dat, i);
                                break;
                            // モンスター移動
                            case commandID.COM_MOSNTER_MOVE:
                                i = RecvMonsterMove (dat, i);
                                break;
                            // メール通知
                            case commandID.COM_SEND_MAIL_NOTICE:
                                i = RecvSendMailNotice (dat, i);
                                break;
                            // 装備結果
                            case commandID.COM_SET_EQUIP:
                                i = RecvSetEquipAll (dat, i);
                                break;
                            // ささやきチャットデータ受信
                            case commandID.COM_WISPER_CHAT:
                                i = RecvChat (dat, i, command);
                                break;
                            // ささやき失敗チャットデータ受信
                            case commandID.COM_WISPER_CHAT_FAIL:
                                i = RecvChat (dat, i, command, true);
                                break;
                            // 全体チャットデータ受信
                            case commandID.COM_CHAT_ALL:
                                i = RecvChat (dat, i, command);
                                break;
                            case commandID.COM_RESULT_FLOOR:
                                i = RecvResultFloor (dat, i);
                                break;
                            // モンスターの攻撃
                            case commandID.COM_BATTLE_MONSTER_ATTACK:
                                i = RecvMonsterAttack (dat, i);
                                break;
                            // ユーザーの攻撃
                            case commandID.COM_BATTLE_PLAYER_ATTACK:
                                i = RecvUserAttack (dat, i);
                                break;
                            case commandID.COM_BATTLE_PLAYER_HEAL:
                            case commandID.COM_BATTLE_MONSTER_HEAL:
                            case commandID.COM_BATTLE_U_TO_M_DRAIN:
                            case commandID.COM_BATTLE_M_TO_U_DRAIN:
                                break;
                            // バトル勝利
                            case commandID.COM_BATTLE_WIN:
                                i = RecvBattleWin (dat, i);
                                break;
                            // レベルアップ
                            case commandID.COM_LEVEL_UP:
                                i = RecvLevelup (dat, i);
                                break;
                            default:
                                Debug.LogError ("Not CommandID" + BitConverter.ToInt32 (tempByte, 0).ToString ());
                                break;
                            }
                        } else {
                            // コマンドを探してすすめる（Todo:きれない対応をしたらいらない）
                            ++i;
                        }
					}
					SocketClient.Instance.recvData=new byte[]{};
				}
			}
			public bool IsConnect(){
				return SocketClient.Instance.IsConnect();
			}
            // ログイン後の受信
            private int RecvLogin(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                CharacterInfo charaInfo=new CharacterInfo();
                // マップコードとキャラクターID
                tempByte=new byte[]{dat[i++],0,0,0};
                charaInfo.mapCode=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                charaInfo.characterID=BitConverter.ToInt32(tempByte,0);
                // 座標
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.x=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.y=BitConverter.ToInt32(tempByte,0);
//                // 装備品
//                tempByte=new byte[]{dat[i++],dat[i++],0,0};
//                charaInfo.swordID=BitConverter.ToInt32(tempByte,0);
//                tempByte=new byte[]{dat[i++],dat[i++],0,0};
//                charaInfo.armorID=BitConverter.ToInt32(tempByte,0);
//                tempByte=new byte[]{dat[i++],dat[i++],0,0};
//                charaInfo.shieldID=BitConverter.ToInt32(tempByte,0);
                // 名前
                byte[] nameByte=new byte[256];
                int byteSize=0;
                while(dat[i+byteSize]!=endByte0 || dat[i+byteSize+1]!=endByte1){
                    nameByte[byteSize]=dat[i+byteSize];
                    byteSize++;
                }
                i=i+byteSize;
                charaInfo.name=System.Text.Encoding.UTF8.GetString(nameByte);
                // マップコードが同じ時のみ生成
                if (charaInfo.mapCode == PlayerInfo.Instance.characterInfo.mapCode) {
                    charaInfoArray.Add (charaInfo);
                    if (!nameList.ContainsKey (charaInfo.characterID)) {
                        nameList.Add (charaInfo.characterID, charaInfo.name);
                    }
                }
                ++i; // endCode
                ++i; // endCode
                return i;
            }
            /// <summary>
            /// チャットの受信
            /// </summary>
            /// <returns>The chat.</returns>
            /// <param name="dat">Dat.</param>
            /// <param name="startPoint">Start point.</param>
            /// <param name="command">Command.</param>
            /// <param name="fail">If set to <c>true</c> fail.</param>
            private int RecvChat(byte[] dat,int startPoint,commandID command,bool fail=false){
				int i=startPoint;
				byte[] tempByte;
				ChatInfo chatInfo=new ChatInfo();
                switch (command) {
                case commandID.COM_CHAT_ALL:
                    chatInfo.chatMode = ChatMode.CHAT_MODE_ALL;
                    break;
                case commandID.COM_CHAT:
                    chatInfo.chatMode = ChatMode.CHAT_MODE_AREA;
                    break;
                case commandID.COM_WISPER_CHAT:
                case commandID.COM_WISPER_CHAT_FAIL:
                    chatInfo.chatMode = ChatMode.CHAT_MODE_TARGET;
                    break;
                }
				tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
				chatInfo.characterID=BitConverter.ToInt32(tempByte,0);
				byte[] chatByte=new byte[256];
                int byteSize = 0;
				while(dat[i+byteSize]!=endByte0 || dat[i+byteSize+1]!=endByte1){
					chatByte[byteSize]=dat[i+byteSize];
					byteSize++;
				}
				i=i+byteSize;
                if (fail) {
                    // ささやき相手がいないときエラー
                    chatInfo.chatText=GameTextTable.SystemText()["whisperFail"];
                } else {
                    chatInfo.chatText=System.Text.Encoding.UTF8.GetString(chatByte);
                }
                chatInfoArray.Add (chatInfo);
				Debug.Log (chatInfo.chatText);
				++i; // endCode
				++i; // endCode
				return i;
			}
            /// <summary>
            ///             /// 階のクリアコールバック

            /// </summary>
            /// <returns>The result floor.</returns>
            /// <param name="dat">Dat.</param>
            /// <param name="startPoint">Start point.</param>
            private int RecvResultFloor(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                // 結果
                tempByte=new byte[]{dat[i++],0,0,0};
                int result = BitConverter.ToInt32(tempByte,0);

                if ((FloorResultResult)result == FloorResultResult.SUCCESS) {
                    MainController.Instance.NextFloor ();
                } else {
                    // エラーだったら、再度リザルト(Todo:タイムアウト処理を入れる)
                    MainController.Instance.NextFloor ();
                }
                ++i; // endCode
                ++i; // endCode
                return i;
            }
            // モンスターの攻撃
            private int RecvMonsterAttack(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                // 魔法種別とレベル
                tempByte=new byte[]{dat[i++],0,0,0};
                int command2 = BitConverter.ToInt32(tempByte,0);
                MagicKind magicKind = (MagicKind)(command2 % (int)MagicKind.MAGIC_DARK);
                int magicLevel = Mathf.FloorToInt (command2 / (int)MagicKind.MAGIC_DARK);
                // モンスター情報
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                int monsterID = BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],0,0,0};
                int monsterNum = BitConverter.ToInt32(tempByte,0);
                while(dat[i]!=endByte0 || dat[i+1]!=endByte1){
                    DamageInfo damageInfo = new DamageInfo ();
                    damageInfo.magicKind = magicKind;
                    damageInfo.magicLevel = magicLevel;
                    // モンスターID
                    damageInfo.monsterInfo.monsterID = monsterID;
                    damageInfo.monsterInfo.monsterNum = monsterNum;
                    // ユーザーID
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    damageInfo.characterInfo.characterID = BitConverter.ToInt32(tempByte,0);
                    // ダメージ
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    damageInfo.damage = BitConverter.ToInt32(tempByte,0);
                    // Hp
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    damageInfo.characterInfo.Hp = BitConverter.ToInt32(tempByte,0);
                    damageInfoQueue.Enqueue (damageInfo);
                }
                return i;
            }

            // ユーザーの攻撃
            private int RecvUserAttack(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                // 魔法種別とレベル
                tempByte=new byte[]{dat[i++],0,0,0};
                int command2 = BitConverter.ToInt32(tempByte,0);
                MagicKind magicKind = (MagicKind)(command2 % (int)MagicKind.MAGIC_DARK);
                int magicLevel = Mathf.FloorToInt (command2 / (int)MagicKind.MAGIC_DARK);
                // ユーザーID
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                int characterID = BitConverter.ToInt32(tempByte,0);
                while(dat[i]!=endByte0 || dat[i+1]!=endByte1){
                    AttackInfo attackInfo = new AttackInfo ();
                    attackInfo.magicKind = magicKind;
                    attackInfo.magicLevel = magicLevel;
                    // モンスターID
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    attackInfo.monsterInfo.monsterID = BitConverter.ToInt32(tempByte,0);
                    tempByte=new byte[]{dat[i++],0,0,0};
                    attackInfo.monsterInfo.monsterNum = BitConverter.ToInt32(tempByte,0);
                    // ユーザーID
                    attackInfo.characterInfo.characterID = characterID;
                    // ダメージ
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    attackInfo.damage = BitConverter.ToInt32(tempByte,0);
                    // Hp
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    attackInfo.monsterInfo.Hp = BitConverter.ToInt32(tempByte,0);
                    attackInfoQueue.Enqueue (attackInfo);
                }
                return i;
            }
            // キャラクター名受信
            private int RecvSetName(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                CharacterInfo charaInfo = new CharacterInfo ();
                tempByte=new byte[]{dat[i++],0,0,0};
                charaInfo.mapCode=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                charaInfo.characterID=BitConverter.ToInt32(tempByte,0);
                byte[] nameByte=new byte[256];
                int byteSize=0;
                while(dat[i+byteSize]!=endByte0 || dat[i+byteSize+1]!=endByte1){
                    nameByte[byteSize]=dat[i+byteSize];
                    byteSize++;
                }
                i=i+byteSize;
                charaInfo.name=System.Text.Encoding.UTF8.GetString(nameByte);
                Debug.Log (charaInfo.characterID);
                Debug.Log (charaInfo.name);
                if (!nameList.ContainsKey (charaInfo.characterID)) {
                    nameList.Add (charaInfo.characterID, charaInfo.name);
                }
                ++i; // endCode
                ++i; // endCode
                return i;
            }
			// 移動の受信
			private int RecvMove(byte[] dat,int startPoint){
				int i=startPoint;
				byte[] tempByte;
				CharacterInfo charaInfo=new CharacterInfo();
				tempByte=new byte[]{dat[i++],0,0,0};
				charaInfo.mapCode=BitConverter.ToInt32(tempByte,0);
				tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
				charaInfo.characterID=BitConverter.ToInt32(tempByte,0);
				Debug.Log("RecvMove:"+charaInfo.characterID);
				tempByte=new byte[]{dat[i++],dat[i++],0,0};
				charaInfo.x=BitConverter.ToInt32(tempByte,0);
				tempByte=new byte[]{dat[i++],dat[i++],0,0};
				charaInfo.y=BitConverter.ToInt32(tempByte,0);
                // マップコードが同じ時のみ生成
                if (charaInfo.mapCode == PlayerInfo.Instance.characterInfo.mapCode) {
                    charaInfoArray.Add (charaInfo);
                }
				++i; // endCode
				++i; // endCode
				return i;
			}
            // 装備品の受信
            private int RecvSetEquip(byte[] dat,int startPoint,EquipKind equip){
                int i=startPoint;
                byte[] tempByte;
                CharacterInfo charaInfo=new CharacterInfo();
                tempByte=new byte[]{dat[i++],0,0,0};
                charaInfo.mapCode=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                charaInfo.characterID=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                switch (equip) {
                case EquipKind.EQUIP_SWORD:
                    charaInfo.swordID = BitConverter.ToInt32 (tempByte, 0);
                    break;
                case EquipKind.EQUIP_ARMOR:
                    charaInfo.armorID = BitConverter.ToInt32 (tempByte, 0);
                    break;
                case EquipKind.EQUIP_SHIELD:
                    charaInfo.shieldID = BitConverter.ToInt32 (tempByte, 0);
                    break;
                }
//                // マップコードが同じ時のみ生成
//                if (charaInfo.mapCode == PlayerInfo.Instance.characterInfo.mapCode) {
//                    charaInfoArray.Add (charaInfo);
//                }
                ++i; // endCode
                ++i; // endCode
                return i;
            }
            // モンスター移動の受信
            private int RecvMonsterMove(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                MonsterInfo monsterInfo = new MonsterInfo ();
                tempByte=new byte[]{dat[i++],0,0,0};
                monsterInfo.mapCode = BitConverter.ToInt32(tempByte,0);
                tempByte = new byte[]{ dat [i++], dat [i++], dat [i++], 0 };
                monsterInfo.monsterID = BitConverter.ToInt32 (tempByte, 0);
                tempByte = new byte[]{ dat [i++], 0, 0, 0 };
                monsterInfo.monsterNum = BitConverter.ToInt32 (tempByte, 0);
                tempByte = new byte[]{ dat [i++], dat [i++], 0, 0 };
                monsterInfo.x = BitConverter.ToInt32 (tempByte, 0);
                tempByte = new byte[]{ dat [i++], dat [i++], 0, 0 };
                monsterInfo.y = BitConverter.ToInt32 (tempByte, 0);
                tempByte = new byte[]{ dat [i++], dat [i++], dat [i++], 0 };
                monsterInfo.MaxHp = BitConverter.ToInt32 (tempByte, 0);
                tempByte = new byte[]{ dat [i++], dat [i++], dat [i++], 0 };
                monsterInfo.Hp = BitConverter.ToInt32 (tempByte, 0);
                // マップコードが同じ時のみ生成
                if (monsterInfo.mapCode == PlayerInfo.Instance.characterInfo.mapCode) {
                    monsterInfoArray.Add (monsterInfo);
                }
                ++i; // endCode
                ++i; // endCode
                return i;
            }
            // メール送信通知
            private int RecvSendMailNotice(byte[] dat,int startPoint){
                int i=startPoint;
                MainController.Instance.getMailCount ();
                ++i; // endCode
                ++i; // endCode
                return i;
            }
            // 装備変更通知
            private int RecvSetEquipAll(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                CharacterInfo charaInfo=new CharacterInfo();
                tempByte=new byte[]{dat[i++],0,0,0};
                charaInfo.mapCode=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                charaInfo.characterID=BitConverter.ToInt32(tempByte,0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.swordID = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.armorID = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.shieldID = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.treasureLevel = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.battleLevel = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.attack = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.defence = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.attackSpeed = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.maxHp = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.maxSp = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.Hp = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],0,0};
                charaInfo.Sp = BitConverter.ToInt32 (tempByte, 0);
                ++i; // endCode
                ++i; // endCode
                if (PlayerInfo.Instance.characterInfo.characterID == charaInfo.characterID) {
                    DungeonPlayer.Instance.updateCharacterParam(charaInfo);
                }
                DungeonCharacterManager.Instance.updateCharacterParam (charaInfo);
                return i;
            }

            // キャラクターログアウト
            private int RecvEndChara(byte[] dat,int startPoint){
				int i=startPoint;
				byte[] tempByte;
				tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
				deleteCharaIDArray.Add(BitConverter.ToInt32(tempByte,0));
				Debug.Log("RecvEndChara:"+BitConverter.ToInt32(tempByte,0));
				++i; // endCode
				++i; // endCode
				return i;
			}
            // バトル勝利
            private int RecvBattleWin(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                while(dat[i]!=endByte0 || dat[i+1]!=endByte1){
                    BattleResult battleResult = new BattleResult();
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    battleResult.characterInfo.characterID = BitConverter.ToInt32 (tempByte, 0);
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    battleResult.getExp = BitConverter.ToInt32 (tempByte, 0);
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    battleResult.getGold = BitConverter.ToInt32 (tempByte, 0);
                    tempByte=new byte[]{dat[i++],0,0,0};
                    battleResult.levelUp = (int)BitConverter.ToInt32 (tempByte, 0);
                    TreasureInfo treasureInfo = new TreasureInfo ();
                    tempByte=new byte[]{dat[i++],0,0,0};
                    treasureInfo.rank = BitConverter.ToInt32 (tempByte, 0);
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    treasureInfo.itemInfo.id = BitConverter.ToInt32 (tempByte, 0);
                    tempByte=new byte[]{dat[i++],0,0,0};
                    treasureInfo.itemInfo.slotCount = BitConverter.ToInt32 (tempByte, 0);
                    tempByte=new byte[]{dat[i++],0,0,0};
                    treasureInfo.serial = BitConverter.ToInt32 (tempByte, 0);
                    battleResult.treasureInfo = treasureInfo;
                    ItemInfo itemInfo = new ItemInfo ();
                    tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                    itemInfo.id = BitConverter.ToInt32 (tempByte, 0);
                    itemInfo.count = 1;
                    battleResult.dropItem = itemInfo;
                    battleResultQueue.Enqueue (battleResult);
                }
                ++i; // endCode
                ++i; // endCode
                return i;
            }
            // レベルアップ
            private int RecvLevelup(byte[] dat,int startPoint){
                int i=startPoint;
                byte[] tempByte;
                levelUpStatus = new CharacterInfo();
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                levelUpStatus.battleLevel = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                levelUpStatus.maxHp = BitConverter.ToInt32 (tempByte, 0);
                levelUpStatus.Hp = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                levelUpStatus.maxSp = BitConverter.ToInt32 (tempByte, 0);
                levelUpStatus.Sp = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                levelUpStatus.attack = BitConverter.ToInt32 (tempByte, 0);
                tempByte=new byte[]{dat[i++],dat[i++],dat[i++],0};
                levelUpStatus.defence = BitConverter.ToInt32 (tempByte, 0);
                ++i; // endCode
                ++i; // endCode
                return i;

            }
            /// <summary>
            /// 生存データの送信
            /// </summary>
			public void sendLive(){
				byte[] dat = new byte[256];
				int size = 0;
				dat [size++] = liveByte;
				SendData(dat,size);
			}
            /// <summary>
            /// チャットデータの送信
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            /// <param name="chatText">Chat text.</param>
            /// <param name="all">If set to <c>true</c> all.</param>
            public void sendChatData(CharacterInfo charaInfo,String chatText,bool all = false){
				byte[] tempByte;
				byte[] dat = new byte[chatText.Length*4+9];
				int size = 0;
				dat [size++] = startByte0;
				dat [size++] = startByte1;
                if (all) {
                    tempByte = BitConverter.GetBytes ((int)commandID.COM_CHAT_ALL);
                } else {
                    tempByte = BitConverter.GetBytes ((int)commandID.COM_CHAT);
                }
				dat [size++] = tempByte[0];
				tempByte = BitConverter.GetBytes(charaInfo.mapCode);
				dat [size++] = tempByte[0];
				tempByte = BitConverter.GetBytes(charaInfo.characterID);
				dat [size++] = tempByte [0];
				dat [size++] = tempByte [1];
				dat [size++] = tempByte [2];
				tempByte = System.Text.Encoding.UTF8.GetBytes (chatText);
				foreach (byte b in tempByte) {
					dat[size++]=b;
				}
				dat [size++] = endByte0;
				dat [size++] = endByte1;
				SendData(dat,size);
			}
            /// <summary>
            /// ささやきチャットデータの送信
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            /// <param name="chatText">Chat text.</param>
            /// <param name="targetCharacterID">Target character I.</param>
            public void sendWhisperChatData(CharacterInfo charaInfo,String chatText,int targetCharacterID){
                byte[] tempByte;
                byte[] dat = new byte[chatText.Length*4+9];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_WISPER_CHAT);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.characterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                Debug.Log ("targetCharacterID:"+targetCharacterID);
                tempByte = BitConverter.GetBytes(targetCharacterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                tempByte = System.Text.Encoding.UTF8.GetBytes (chatText);
                foreach (byte b in tempByte) {
                    dat[size++]=b;
                }
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);
            }
            /// <summary>
            /// 名前の送信
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            public void sendName(CharacterInfo charaInfo){
                // Todo:ログイン処理（今は仮）
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_SET_NAME);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.characterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                tempByte = System.Text.Encoding.UTF8.GetBytes (charaInfo.name);
                foreach (byte b in tempByte) {
                    dat[size++]=b;
                }
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);            
            }
			// ログインの送信
			public void sendLogin(CharacterInfo charaInfo){
                Debug.Log ("sendLogin");
				// Todo:ログイン処理（今は仮）
				byte[] tempByte;
				byte[] dat = new byte[256];
				int size = 0;
				dat [size++] = startByte0;
				dat [size++] = startByte1;
				tempByte = BitConverter.GetBytes((int)commandID.COM_LOGIN);
				dat [size++] = tempByte[0];
				tempByte = BitConverter.GetBytes(charaInfo.mapCode);
				dat [size++] = tempByte[0];
				tempByte = BitConverter.GetBytes(charaInfo.characterID);
				dat [size++] = tempByte [0];
				dat [size++] = tempByte [1];
				dat [size++] = tempByte [2];
                tempByte = BitConverter.GetBytes(charaInfo.x);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                tempByte = BitConverter.GetBytes(charaInfo.y);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
				dat [size++] = endByte0;
				dat [size++] = endByte1;
				SendData(dat,size);
			}

            /// <summary>
            /// 移動データの送信
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
			public void sendMoveData(CharacterInfo charaInfo){
				byte[] tempByte;
				byte[] dat = new byte[256];
				int size = 0;
				dat [size++] = startByte0;
				dat [size++] = startByte1;
				tempByte = BitConverter.GetBytes((int)commandID.COM_MOVE);
				dat [size++] = tempByte[0];
				tempByte = BitConverter.GetBytes(charaInfo.mapCode);
				dat [size++] = tempByte[0];
				tempByte = BitConverter.GetBytes(charaInfo.characterID);
				dat [size++] = tempByte [0];
				dat [size++] = tempByte [1];
				dat [size++] = tempByte [2];
				tempByte = BitConverter.GetBytes(charaInfo.x);
				dat [size++] = tempByte [0];
				dat [size++] = tempByte [1];
				tempByte = BitConverter.GetBytes(charaInfo.y);
				dat [size++] = tempByte [0];
				dat [size++] = tempByte [1];
				dat [size++] = endByte0;
				dat [size++] = endByte1;
				SendData(dat,size);
			}

            /// <summary>
            /// 装備データ送信.
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            /// <param name="equip">Equip.</param>
            public void sendEquip(CharacterInfo charaInfo, EquipKind equip){
                commandID command = commandID.COM_EQUIP_SWORD;
                int equipID = charaInfo.swordID;
                switch (equip) {
                case EquipKind.EQUIP_SWORD:
                    equipID = charaInfo.swordID;
                    command = commandID.COM_EQUIP_SWORD;
                    break;
                case EquipKind.EQUIP_ARMOR:
                    equipID = charaInfo.armorID;
                    command = commandID.COM_EQUIP_ARMOR;
                    break;
                case EquipKind.EQUIP_SHIELD:
                    equipID = charaInfo.shieldID;
                    command = commandID.COM_EQUIP_SHIELD;
                    break;
                }
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)command);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.characterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                tempByte = BitConverter.GetBytes(equipID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);
            }
            /// <summary>
            /// キャラクタ情報の送信
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            public void sendCharaInfo(CharacterInfo charaInfo){
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_CHARAINFO_ALL);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.characterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
//                tempByte = BitConverter.GetBytes(charaInfo.swordID);
//                dat [size++] = tempByte [0];
//                dat [size++] = tempByte [1];
//                tempByte = BitConverter.GetBytes(charaInfo.armorID);
//                dat [size++] = tempByte [0];
//                dat [size++] = tempByte [1];
//                tempByte = BitConverter.GetBytes(charaInfo.shieldID);
//                dat [size++] = tempByte [0];
//                dat [size++] = tempByte [1];
                tempByte = System.Text.Encoding.UTF8.GetBytes (charaInfo.name);
                foreach (byte b in tempByte) {
                    dat[size++]=b;
                }
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);            
            }
            /// <summary>
            /// 装備の変更
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            public void sendSetEquip(CharacterInfo charaInfo){
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_SET_EQUIP);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.characterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);            
            }

            /// <summary>
            /// メール送信の通知
            /// </summary>
            /// <param name="characterID">Character I.</param>
            public void sendSendMailNotice(int characterID){
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_SEND_MAIL_NOTICE);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(characterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                foreach (byte b in tempByte) {
                    dat[size++]=b;
                }
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);            
            }
            /// <summary>
            /// プレイヤーの攻撃
            /// </summary>
            /// <param name="charaInfo">Chara info.</param>
            /// <param name="attackSlot">Attack slot.</param>
            /// <param name="monsterInfo">Monster info.</param>
            public void sendPlayerAttack(CharacterInfo charaInfo,int attackSlot,MonsterInfo monsterInfo){
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_BATTLE_PLAYER_ATTACK);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(attackSlot);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(monsterInfo.monsterID);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                tempByte = BitConverter.GetBytes(monsterInfo.monsterNum);
                dat [size++] = tempByte [0];
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);            
            }
            public void sendResultFloor(CharacterInfo charaInfo,List<TreasureInfo> treasures, int growupPoint){
                byte[] tempByte;
                byte[] dat = new byte[256];
                int size = 0;
                dat [size++] = startByte0;
                dat [size++] = startByte1;
                tempByte = BitConverter.GetBytes((int)commandID.COM_RESULT_FLOOR);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(charaInfo.mapCode);
                dat [size++] = tempByte[0];
                tempByte = BitConverter.GetBytes(growupPoint);
                dat [size++] = tempByte [0];
                dat [size++] = tempByte [1];
                dat [size++] = tempByte [2];
                foreach (TreasureInfo treasure in treasures) {
                    tempByte = BitConverter.GetBytes (treasure.rank);
                    dat [size++] = tempByte [0];
                    tempByte = BitConverter.GetBytes (treasure.serial);
                    dat [size++] = tempByte [0];
                    tempByte = BitConverter.GetBytes (treasure.itemInfo.itemBoxNo);
                    dat [size++] = tempByte [0];
                }
                dat [size++] = endByte0;
                dat [size++] = endByte1;
                SendData(dat,size);            
            }

            // データの送信
			public void SendData(byte[] dat,int size){
				byte[] sendByte = new byte[size];
				for (int i=0; i<size; ++i) {
					sendByte[i]=dat[i];
				}
				SocketClient.Instance.send (sendByte);
				// 生存タイマーのリセット
                liveTimer=LIVE_SEND_TIMER;
            }

		}
}}