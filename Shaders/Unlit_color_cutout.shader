﻿Shader "Unlit/Transparent Color Cutout" {
	Properties {
	    _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	    _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	}

	SubShader {
	    Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
	    
	   CGPROGRAM
       #pragma surface surf Lambert alphatest:_Cutoff
           
       // サーフェスShaderとしての処理.
       sampler2D _MainTex;
           
       struct Input {
           float2 uv_MainTex;
           float4 color : COLOR;   // 頂点カラーを受け取る.
       };

       void surf (Input IN, inout SurfaceOutput o) {
           half4 c = tex2D (_MainTex, IN.uv_MainTex) * 0.8;
           c *= IN.color;      // 頂点カラーの反映.
           o.Albedo = c.rgb;
           o.Alpha  = c.a;
       }
       ENDCG
	}
}
