using UnityEngine;

using System;
using System.Text;
using System.IO;

using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using System.Runtime.Serialization;
using System.Xml;
using System.Linq;

using System.Threading;

/**
	completion control
*/
public class SublimeSocketCompletionController {

	bool test = false;
	readonly string basePath;

	// completion support.
	private CompletionMatcher compMatcher;
	private TypeMapGenerateController typeMapGenCont;
	private CompletionCacheController compCacheCont;


	// completion push method.
	private Action<CompletionParamDict> PushCompletion;

	// box for latest completion information.
	private string currentCompletionIdentity;

	private string currentPath; 
	private string currentBody;
	private Point currentPoint;
	private string currentLineStr;
	private string currentMatchMode;

	/**
		constructor

		load after compiling.
	*/
	public SublimeSocketCompletionController (
		Action<Dictionary<string, string>> PushMessage,
		Action<CompletionParamDict> PushCompletion, 
		string basePath, 
		string assetBasePath) {

		this.PushCompletion = PushCompletion;
		var projectActiveLibAddress = basePath + CompletionDLLInformations.PROJECT_DLL_PATH;
		var otherLibAddresses = LoadProjctStaticLibAddresses(basePath, assetBasePath);
		

		// initialize helpers.
		compMatcher = new CompletionMatcher(
			(string completionIdentity, CompletionCollection compCollection, bool isFinal) => {
				Completed(completionIdentity, compCollection, isFinal);
			},
			(string completionIdentity, string nextMatchMode) => {
				Recomplete(completionIdentity, nextMatchMode);
			}
		);

		
		typeMapGenCont = new TypeMapGenerateController(
			() => {
				// do nothing.
			},
			(string completionIdentity, TypeMap typeMap, string body, Point point) => {
				TypeMapped(completionIdentity, typeMap, body, point);
			}, 
			projectActiveLibAddress, otherLibAddresses
		);
		

		compCacheCont = new CompletionCacheController(
			(string message) => {
				PushMessage(new Dictionary<string, string>{{"message", message}});
			},
			(TreeDict completionLibrary) => {
				UpdateCompletionLibrary(completionLibrary);
			}, 
			basePath, projectActiveLibAddress, otherLibAddresses
		);

		compCacheCont.YamYam();// no mean. erase unused warning.
		this.basePath = basePath;
	}

	public void ApplyFileSetting (string source) {
		var p = source.Split(new char[] {':'});
		if (p.Length != 3) return;

		int tabCount;
		if (!int.TryParse(p[1], out tabCount)) {
			return;
		}

		bool useSpace;
		if (!bool.TryParse(p[2], out useSpace)) {
			return;
		}

		// by default
		compMatcher.tabReplacer = " ";

		if (useSpace) {
			var tabStr = "";
			for (int i = 0; i < tabCount; i++) {
				tabStr = tabStr + " ";
			}

			compMatcher.tabReplacer = tabStr;
		}
	}

	
	public List<string> LoadProjctStaticLibAddresses (string basePath, string assetBasePath) {
		var libAddresses = new List<string>();

		// add platform dependent dll
		switch (Application.platform) {
			case RuntimePlatform.OSXEditor:{
				libAddresses.Add(CompletionDLLInformations.MAC_UNITY_DLL_PATH);
				libAddresses.Add(CompletionDLLInformations.MAC_CSHARP_DLL_PATH);
				break;
			}
			case RuntimePlatform.WindowsEditor:{
				libAddresses.Add(CompletionDLLInformations.WINDOWS_UNITY_DLL_PATH);
				libAddresses.Add(CompletionDLLInformations.WINDOWS_CSHARP_DLL_PATH);
				break;
			}
		}

		// add other /project/Assets/*.dll paths
		// exclude /Editor folder.
		var otherDllsPaths = Directory.GetFiles(assetBasePath, "*.dll", SearchOption.AllDirectories).Where(path => !path.Contains(CompletionDLLInformations.SSA_EXCLUDE_PATH_EDITOR)).ToList();
		libAddresses = libAddresses.Union(otherDllsPaths).ToList();
		
		return libAddresses;
	}


	/**
		start completion from received source
	*/
	public void IgniteCompletion (string resource) {
		
		var splittedResource = resource.Split(new char[] {':'}, 5);
		
		if (splittedResource.Length != 5) {
			Debug.LogError(CompletionMessages.RESULTMESSAGE_ILLEGALFORMAT + ":less or much of parameters");
			return;
		}

		if (!splittedResource[2].Contains(",")) {
			Debug.LogError(CompletionMessages.RESULTMESSAGE_ILLEGALFORMAT + ":less of row and col");
			return;
		}


		var completionIdentity = splittedResource[1];
		

		var rowColStrArray = splittedResource[2].Split(',');
		

		int row;
		if (!int.TryParse(rowColStrArray[0], out row)) {
			return;
		}
		
		int col;
		if (!int.TryParse(rowColStrArray[1], out col)) {
			return;
		}


		var path = splittedResource[3];
		var body = splittedResource[4].Replace("\r", "");// r is for windows.
		
		var point = new Point(row, col);

		// Debug.LogError("row "+row +", col "+col);

		// --------------- data receiving verified! start completing. ---------------



		Complete(path, completionIdentity, body, point);

		// return control as soon as possible.
		return;
	}


	/**
		ignite complete
			-> return controll

			-> generate TypeMap
				-> Match
					-> send completions
	*/
	public void Complete (string path, string completionIdentity, string body, Point point) {
		
		// update latest
		this.currentCompletionIdentity = completionIdentity;

		this.currentPath = path;
		this.currentBody = body;
		this.currentPoint = point;
		this.currentLineStr = "";
		
		this.currentMatchMode = compMatcher.DetectMatchingTrigger(ref body, point, ref currentLineStr);

		// type matching.
		Action completion = () => {
			try {
				typeMapGenCont.TypeMapUpdate(completionIdentity, path, body, point);
			} catch (Exception e) {
				Debug.LogWarning("SSA:Completion error in type mapping "+e);
			}
		};

		
		var thread = new Thread(new ThreadStart(completion));
		thread.Start();

	}

	/**
		retry with same data. 2nd another type matching.
		-> generate TypeMap
			-> Match
				-> send completions
	*/
	public void Recomplete (string completionIdentity, string nextMatchMode) {
		if (this.currentCompletionIdentity != completionIdentity) return;

		var path = this.currentPath;
		var body = this.currentBody;
		var point = this.currentPoint;
		this.currentLineStr = "";


		this.currentMatchMode = compMatcher.ModifyByMatchedTrigger(nextMatchMode, ref body, point, ref currentLineStr);

		if (this.currentMatchMode == CompletionMatches.MATCH_KIND_NONE) return;// abort
		
		// type matching. with sync(already in other thread.)
		typeMapGenCont.ResetIdentity();// same completion identity, other data.
		typeMapGenCont.TypeMapUpdate(completionIdentity, path, body, point);
	}


	/**
		new TypeMap pushed from TypeMapGenerateController -> CompletionMatcher
	*/
	private void TypeMapped (string completionIdentity, TypeMap typeMap, string body, Point point) {
		/*
			if identity not changed (by incoming latest completion), continue to push completion.
				parameters never changed from start. also matchMode too.

			else,
				generate latest completion newly. like "It is put to here JUST NOW."

				this mechanism reduces number of generation of the TypeMap, although the Threads.
		*/

		if (this.currentCompletionIdentity == completionIdentity) {
			compMatcher.MatchModeMatching(currentCompletionIdentity, currentMatchMode, typeMap, currentPoint, currentLineStr);

			if (test) TestController.Test(this, 1, completionIdentity, basePath);

		} else {
			// another new data is set! run with latest data.
			Complete(currentPath, currentCompletionIdentity, currentBody, currentPoint);
		}
	}



	/**
		push completed data via identity gate.
	*/
	private void Completed (string completionIdentity, CompletionCollection compCollection, bool isFinal) {
		

		// if empty, do nothing.
		if (compCollection.count == 0) return;

		if (currentCompletionIdentity == completionIdentity) {
			if (isFinal) {
				var data = new CompletionParamDict(
					currentPath,
					completionIdentity,
					CompletionDataFormats.FORMAT_LARGEHEAD + CompletionDataFormats.FORMAT_PARAMSTYPEFMT + "\t"+CompletionDataFormats.FORMAT_RETURNTYPE,
					CompletionDataFormats.FORMAT_SMALLHEAD + CompletionDataFormats.FORMAT_PARAMSTARGETFMT + "$0",
					compCollection,
					true // this is "show completion window" flag for SublimeSocket.
				);
				PushCompletion(data);
			} else {
				var data = new CompletionParamDict(
					currentPath,
					completionIdentity,
					CompletionDataFormats.FORMAT_LARGEHEAD + CompletionDataFormats.FORMAT_PARAMSTYPEFMT + "\t"+CompletionDataFormats.FORMAT_RETURNTYPE,
					CompletionDataFormats.FORMAT_SMALLHEAD + CompletionDataFormats.FORMAT_PARAMSTARGETFMT + "$0",
					compCollection,
					false
				);
				PushCompletion(data);
			}
		}
	}


	/**
		add library cache to matcher.
		all loading sequence is over.
	*/
	private void UpdateCompletionLibrary (TreeDict completionLibrary) {
		compMatcher.UpdateCompletionLibrary(completionLibrary);
		if (test) TestController.Test(this, 0, "no identity. this is start.", basePath);
	}



	static void Assert(bool condition, string message) {
		if (!condition) throw new Exception(message);
	}

}



/**
	the completion info dict
*/
public class CompletionParamDict {
	public readonly Dictionary<string, object> data;

	/**
		constructor for formatted data of Completion.
	*/
	public CompletionParamDict (
		string path, 
		string identity, 
		string fmtLeft, 
		string fmtRight, 
		CompletionCollection completionCollections,
		bool show = false
	) {
		data = new Dictionary<string, object>();
		// set format and parts 
		data["name"] = path;
		data["identity"] = identity;
		data["pool"] = identity;
		if (show) data["show"] = identity;
		data["formathead"] = fmtLeft;
		data["formattail"] = fmtRight;
		data["completion"] = completionCollections;
	}
}

