
/**
	definitions
*/
public static class CompletionSettings {
	public const int HEADHIT_MIN = 1;
	public const int HEADHIT_LIMIT = 1;
	public const string HEADHIT_EXCLUDE_BRACKET = "}";

	public const string RETURN_SYSTEM_VOID = "System.Void";
	public const string RETURN_VOID = "Void";

	public const string ANONYMOUNS_LINE_DEFINITION = "_";
	public const string TYPE_UNKNOWN = "?";


	public const int COMPLETION_PUSH_LIMIT_SIZE = 1024*20;
}

public static class CompletionMatches {
	public const string MATCH_KIND_NONE = "";
	public const string MATCH_KIND_SPACE = " ";

	public const string MATCH_KIND_DOT = ".";
	public const string MATCH_KIND_DOT_RETRY = ".RETRY";
	
	public const string MATCH_KIND_DOT_EXIT_BRACKET = "}";
	public const string MATCH_KIND_DOTIGNORE = "}.";
	public const string MATCH_KIND_NEWSPACE = "new ";
	public const string MATCH_KIND_EQUAL = "=";
	public const string MATCH_KIND_RETURN = "return";
	
	public const string MATCH_KIND_THIS = "this";
	public const string MATCH_KIND_THIS_RETRY = "thisRETRY";

	public const string MATCH_KIND_HEAD  = "HEAD";
	public const string MATCH_KIND_ARRAY_MARK = "[]";

}

public static class CompletionTypeStrings {
	public const string TYPE_ARRAY = "System.Array";
	public const string TYPE_TYPE_RESOLVERESULT = "USSA.NRefactory.Semantics.TypeResolveResult";
	public const string TYPE_NAMESPACERE_SOLVERESULT = "USSA.NRefactory.Semantics.NamespaceResolveResult";
	public const string TYPE_ALIASNAMESPACE_RESOLVERESULT = "USSA.NRefactory.CSharp.Resolver.AliasNamespaceResolveResult";


}

public static class CompletionDictSettings {
	// type-tree
	public const string KEY_CLASS		= "c";
	public const string KEY_CONSTRUCTOR = "o";
	public const string KEY_METHOD = "m";
	public const string KEY_PROPERTY = "p";
	public const string KEY_FIELD = "f";
	public const string KEY_DEFAULT = "d";

	public const string COMPLETIONKEY_HEAD = "h";
	public const string COMPLETIONKEY_RETURN = "r";
	public const string COMPLETIONKEY_PARAMTYPES = "t";
	public const string COMPLETIONKEY_PARAMNAMES = "n";
}


public static class CompletionDataFormats {
	public const string FORMAT_LARGEHEAD = "HEAD";
	public const string FORMAT_SMALLHEAD = "head";
	public const string FORMAT_RETURNTYPE = "return";
	public const string FORMAT_PARAMSTYPEFMT = "paramsTypeDef";
	public const string FORMAT_PARAMSTARGETFMT = "paramsTargetFmt";
}

public static class CompletionMessages {
	// messages
	public const string RESULTMESSAGE_HIT	= "SSA: completion loaded.";
	public const string RESULTMESSAGE_NOTRIGGER = "SSA: no completion trigger.";
	public const string RESULTMESSAGE_ILLEGALFORMAT	= "SSA: illegal format.";
}

public static class CompletionLimitations {
	// limit completion candidate by return type
	public const int RETURN_TYPE_LIMIT_NOTHING 		= 0x000000;
	public const int RETURN_TYPE_LIMIT_VOID			= 0x000001;
	public const int RETURN_TYPE_LIMIT_PRIMITIVE	= 0x000010;
	public const int RETURN_TYPE_LIMIT_OBJECT		= 0x000100;

	// limit completion candidate by attr type
	public const int RETURN_ATTR_LIMIT_NOTHING		= 0x000000;
	public const int RETURN_ATTR_LIMIT_PROPERTY		= 0x000001;
	public const int RETURN_ATTR_LIMIT_CLASS		= 0x000010;
	public const int RETURN_ATTR_LIMIT_CONSTRUCTOR	= 0x000100;
	public const int RETURN_ATTR_LIMIT_METHOD		= 0x001000;
	public const int RETURN_ATTR_LIMIT_FIELD		= 0x010000;
	public const int RETURN_ATTR_LIMIT_DEFAULT		= 0x100000;
}

public static class CompletionDLLInformations {
	public const char DELIM_GENERIC = '`';
	public const char DELIM_OPENBRACE = '[';
	public const string PROJECT_DLL_PATH = "Library/ScriptAssemblies/Assembly-CSharp.dll";
	public const string SSA_EXCLUDE_PATH_EDITOR = "/Editor/";

	public const string MAC_UNITY_DLL_PATH = "/Applications/Unity/Unity.app/Contents/Frameworks/Managed/UnityEngine.dll";
	public const string MAC_CSHARP_DLL_PATH = "/Applications/Unity/Unity.app/Contents/Frameworks/Mono/lib/mono/unity/mscorlib.dll";

	public const string WINDOWS_UNITY_DLL_PATH = "C:/Program Files (x86)/Unity/Editor/Data/Managed/UnityEngine.dll";
	public const string WINDOWS_CSHARP_DLL_PATH = "C:/Program Files (x86)/Unity/Editor/Data/Mono/lib/mono/unity/mscorlib.dll";

	public const string STATIC_COMPLETION_CACHE_PATH = "Assets/SublimeSocketAsset/StaticCompletionCache.cache";

	public const string MESSAGE_ALREADYCACHEGENERATED = "completion data already cached, load cache from:";
	public const string MESSAGE_CACHING = "completion data generating, then load from:";
	public const string MESSAGE_CACHED = "completion data loaded.";

}

public static class SocketDefinitions {
	public const string MAC_LOGFILE_PATH = "/Library/Logs/Unity/Editor.log";
	public const string WINDOWS_LOGFILE_PATH = "\\Local\\Unity\\Editor\\Editor.log";
}


