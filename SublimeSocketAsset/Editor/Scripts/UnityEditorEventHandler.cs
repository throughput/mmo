using UnityEngine;
using UnityEditor;
using System.Collections;

[InitializeOnLoad]
public class UnityEditorEventHandler {
 
	/**
		run with Launch / Play
	*/
	static UnityEditorEventHandler () {
		SublimeSocketClient.Automate();
	}
}
