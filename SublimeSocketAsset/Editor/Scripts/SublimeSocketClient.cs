using UnityEngine;
using UnityEditor;

using System;

using System.IO;
using System.Collections;
using System.Collections.Generic;

using System.Text;
using System.Threading;
using System.Linq;



public class SublimeSocketClient : MonoBehaviour {
    /*
        constructor
    */
    static SublimeSocketClient () {
        assetBasePath = Application.dataPath;
        unityVersionNumber = Application.unityVersion.Split('.')[0];

        // Deeply depends on Unity's [InitializeOnLoad], WebScoket will be kill before re-run. set notConnected here.
        UpdateConnectionStatus(PREFERENCE_PARAM_STATUS_NOTCONNECTED);
    }
    
    public static readonly string SublimeTextVersion = "3";
    public static readonly string SublimeSocketApiVersion = "1.5.0";

    public static readonly string ASSET_FOLDER_NAME = "/SublimeSocketAsset";
    
    // preferences
    public static readonly string PREFERENCE_FILE_PATH = ASSET_FOLDER_NAME + "/Preferences.txt";

    // compile anyway trigger
    public static readonly string PREFERENCE_COMPILETRIGGER_PATH = ASSET_FOLDER_NAME + "/CompileTriggerFile/trigger.cs";
 
    // filer source
    public static readonly string FILTERSOURCE_PATH = ASSET_FOLDER_NAME + "/Filter/UnityFilterSource.txt";

    // switchApp location
    public static readonly string SWITCHAPP_PATH = "SUBLIMESOCKET_PATH:tool/switch/SwitchApp";
    


    // data key-values
    public const string PREFERENCE_ITEM_VERSION = "version";

    public const string PREFERENCE_PARAM_VERSION = "1.8.1";


    public const string PREFERENCE_ITEM_SERVER = "server";
    public const string PREFERENCE_PARAM_DEFAULT_PROTOCOL = "ws://";
    public const string PREFERENCE_PARAM_DEFAULT_HOST = "127.0.0.1";
    public const int PREFERENCE_PARAM_DEFAULT_WSSERVER_PORT = 8823;
    public const int PREFERENCE_PARAM_DEFAULT_BYTESERVER_PORT = 8824;
 
    public const string PREFERENCE_ITEM_TARGET = "target";
 
    public const string PREFERENCE_ITEM_STATUS = "status"; 
    public const string PREFERENCE_PARAM_STATUS_NOTCONNECTED = "notConnected";
    public const string PREFERENCE_PARAM_STATUS_CONNECTING = "connecting";
    public const string PREFERENCE_PARAM_STATUS_CONNECTED = "connected";
 
    public const string PREFERENCE_ITEM_ERROR = "error";
    public const string PREFERENCE_PARAM_ERROR_NONE = "none";
    public const string PREFERENCE_PARAM_ERROR_CONNECTIONREFUSED = "Connection refused";
    public const string PREFERENCE_PARAM_ERROR_THEWEBSOCKETFRAMECANNOTBEREADFROMTHENETWORKSTREAM = "The WebSocket frame can not be read from the network stream.";
    public const string PREFERENCE_PARAM_ERROR_THREADWASBEINGABORTED = "Thread was being aborted";
    
    public const string PREFERENCE_ITEM_AUTOCONNECT = "autoConnect";
    public const string PREFERENCE_PARAM_AUTO_ON = "on";
    public const string PREFERENCE_PARAM_AUTO_OFF = "off";

    public const string PREFERENCE_ITEM_COMPLETION = "completion";
    public const string PREFERENCE_PARAM_COMPLETION_ON = "on";
    public const string PREFERENCE_PARAM_COMPLETION_OFF = "off";
 
    public const string PREFERENCE_ITEM_PLAYFLAG = "playFlag";
    public const string PREFERENCE_PARAM_PLAY_ON = "on";
    public const string PREFERENCE_PARAM_PLAY_OFF = "off";

    public const string PREFERENCE_ITEM_BREAKFLAG = "breakFlag";
    public const string PREFERENCE_PARAM_BREAK_ON = "on";
    public const string PREFERENCE_PARAM_BREAK_OFF = "off";

    public const string PREFERENCE_ITEM_COMPILE_BY_SAVE = "compileBySave";
    public const string PREFERENCE_PARAM_COMPILE_BY_SAVE_ON = "on";
    public const string PREFERENCE_PARAM_COMPILE_BY_SAVE_OFF = "off";

    public const string PREFERENCE_ITEM_COMPILE_ANYWAY = "compileAnyway";
    public const string PREFERENCE_PARAM_COMPILE_ANYWAY_ON = "on";
    public const string PREFERENCE_PARAM_COMPILE_ANYWAY_OFF = "off";


    // settings
    const string HEADER_SAVED = "saved";
    const string HEADER_COMPLETION = "completion";
    const string HEADER_SETTING = "setting";


	const string HEADER_IDENTIFICATION_VERIFY = "VERIFIED:";
	const string HEADER_IDENTIFICATION_VERIFY_UPDATABLE = "VERIFIED/CLIENT_UPDATE";

	const string HEADER_REFUSED_SSUPDATE = "REFUSED/SUBLIMESOCKET_UPDATE";
	const string HEADER_REFUSED_CLIENTUPDATE = "REFUSED/CLIENT_UPDATE";
	const string HEADER_REFUSED_SSDIFFERENT = "REFUSED/DIFFERENT_SUBLIMESOCKET";

     /*
        local updatable datas
    */
    static string assetBasePath;
    static string unityVersionNumber;


    static SublimeSocketCompletionController completionCont;


    /*
        called when construct
    */
    public static void Automate() {
        assetBasePath = Application.dataPath;
        
        if (System.IO.File.Exists(assetBasePath + PREFERENCE_FILE_PATH)) {
            // load pref as dictionary
            var paramDict = PreferenceDict(assetBasePath + PREFERENCE_FILE_PATH);

            AutoConnect(
                paramDict[PREFERENCE_ITEM_AUTOCONNECT], 
                paramDict[PREFERENCE_ITEM_STATUS]
            );
        }
    }


    /**
        return enviroment-modified path.
    */
    public static string TargetFilePath () {
        var path = "";

        switch (Application.platform) {
            case RuntimePlatform.OSXEditor:{
                path = "/Users/"+System.Environment.UserName+SocketDefinitions.MAC_LOGFILE_PATH;
                break;
            }
            case RuntimePlatform.WindowsEditor:{
                var basePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                DirectoryInfo d = new DirectoryInfo(basePath);
                var result = d.Parent.FullName;
                path = result+SocketDefinitions.WINDOWS_LOGFILE_PATH;
                break;
            }
        }
        
        return path;
    }

    /**
        return IsConnectable or not (actually run StartConnect and CloseConnection.)
    */
    public static bool IsConnectable () {
        StartConnect();
        
        var latestParamDict = PreferenceDict(assetBasePath + PREFERENCE_FILE_PATH);
        if (latestParamDict[PREFERENCE_ITEM_ERROR] == PREFERENCE_PARAM_ERROR_NONE) {
            CloseConnection();
            return true;
        }
        CloseConnection();
        return false;
    }

    /**
        connect automatically
    */
    public static void AutoConnect(string autoConnectStatusStr, string connectionStatusStr) {
        if (autoConnectStatusStr == PREFERENCE_PARAM_AUTO_OFF) return;

        // check connected or not
        if (connectionStatusStr == PREFERENCE_PARAM_STATUS_NOTCONNECTED) {
            StartConnect();
        }
    }

    /**
		initialize completion function
	*/
	public static void InitCompletion(string completionStatusStr,  Action<Dictionary<string, string>> PushMessageToSublimeSocket, Action<CompletionParamDict> PushCompletionToSublimeSocket) {
		if (completionStatusStr == PREFERENCE_PARAM_COMPLETION_OFF) return;

		var projectDLLBasePath = Directory.GetParent(assetBasePath).ToString()+ "/";

		completionCont = new SublimeSocketCompletionController(PushMessageToSublimeSocket, PushCompletionToSublimeSocket, projectDLLBasePath, assetBasePath);
	}
    
    /**
        get paramDict from path
    */
    public static Dictionary<string, string> PreferenceDict(string preferencePath) {
        if (!System.IO.File.Exists(preferencePath)) {
            // generate
            ResetPreference(preferencePath);
        }
        
        // read file
        StreamReader reader = new StreamReader(preferencePath);
        string jsonText = reader.ReadToEnd();
        reader.Close();
        Dictionary<string, string> paramDict = null;
        try {
            paramDict = USSAJson.USSAJsonConvert.DeserializeObject<Dictionary<string, string>>(jsonText);
        } catch (Exception e) {
            Debug.LogWarning("SSA:JSON deserialize error " + e);
        }
        return paramDict;
    }

    /**
        re-generate preference file @ filePath with default parameters.
    */
    public static void ResetPreference (string filePath) {

        var defaultDict = new Dictionary<string, string> () {
            {PREFERENCE_ITEM_VERSION, PREFERENCE_PARAM_VERSION},
            {PREFERENCE_ITEM_SERVER, PREFERENCE_PARAM_DEFAULT_PROTOCOL + PREFERENCE_PARAM_DEFAULT_HOST + ":" + PREFERENCE_PARAM_DEFAULT_WSSERVER_PORT},
            {PREFERENCE_ITEM_TARGET, TargetFilePath()},
            {PREFERENCE_ITEM_STATUS, PREFERENCE_PARAM_STATUS_NOTCONNECTED},
            {PREFERENCE_ITEM_ERROR, PREFERENCE_PARAM_ERROR_NONE},
            {PREFERENCE_ITEM_AUTOCONNECT, PREFERENCE_PARAM_AUTO_ON},
            {PREFERENCE_ITEM_COMPLETION, PREFERENCE_PARAM_COMPLETION_ON},
            {PREFERENCE_ITEM_PLAYFLAG, PREFERENCE_PARAM_PLAY_OFF},
            {PREFERENCE_ITEM_BREAKFLAG, PREFERENCE_PARAM_BREAK_OFF},
            {PREFERENCE_ITEM_COMPILE_BY_SAVE, PREFERENCE_PARAM_COMPILE_BY_SAVE_ON},
            {PREFERENCE_ITEM_COMPILE_ANYWAY, PREFERENCE_PARAM_COMPILE_ANYWAY_ON}
        };

        var defaultJSON = USSAJson.USSAJsonConvert.SerializeObject(defaultDict);
        using (StreamWriter sw = new StreamWriter(filePath)) {
            sw.WriteLine(defaultJSON);
        }
    }

    /**
        update connection status.
    */
    public static void UpdateConnectionStatus (string nextStatus) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_STATUS] = nextStatus;

        WriteOutPreferences(paramDict, path);
    }

    /**
        update connection error.
    */
    public static void UpdateConnectionError (string nextError) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_ERROR] = nextError;

        WriteOutPreferences(paramDict, path);
    }

    /**
        update autoConnect.
    */
    public static void UpdateAutoConnect (string nextFlag) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_AUTOCONNECT] = nextFlag;

        WriteOutPreferences(paramDict, path);
    }

    /**
        update completion status.
    */  
    public static void UpdateCompletion (string nextFlag) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_COMPLETION] = nextFlag;

        if (nextFlag == PREFERENCE_PARAM_COMPLETION_OFF) completionCont = null;

        WriteOutPreferences(paramDict, path);
    }

    
    /**
        update play flag.
    */
    public static void UpdatePlayFlag (string nextPlayFlag) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_PLAYFLAG] = nextPlayFlag;

        WriteOutPreferences(paramDict, path);
    }

    /**
        update break flag.
    */
    public static void UpdateBreakFlag (string nextBreakFlag) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_BREAKFLAG] = nextBreakFlag;

        WriteOutPreferences(paramDict, path);
    }


    public static void UpdateCompileBySave (string compileBySaveFlag) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_COMPILE_BY_SAVE] = compileBySaveFlag;

        WriteOutPreferences(paramDict, path);
    }


    public static void UpdateCompileAnyway (string compileAnywayFlag) {
        var path = assetBasePath + PREFERENCE_FILE_PATH;

        var paramDict = PreferenceDict(path);
        paramDict[PREFERENCE_ITEM_COMPILE_ANYWAY] = compileAnywayFlag;

        WriteOutPreferences(paramDict, path);
    }


    private static void WriteOutPreferences (Dictionary<string, string> paramDict, string path) {
        // rewrite to file
        var changed = USSAJson.USSAJsonConvert.SerializeObject(paramDict).Replace(",", ",\n\t").Replace("{", "{\n\t").Replace("}", "\n}");

        using (StreamWriter sw = new StreamWriter(path)) {
            sw.WriteLine(changed);
        }
    }

    public static bool ShouldCompileBySave () {
        var path = assetBasePath + PREFERENCE_FILE_PATH;
        var paramDict = PreferenceDict(path);

        switch (paramDict[PREFERENCE_ITEM_COMPILE_BY_SAVE]) {
            case PREFERENCE_PARAM_COMPILE_BY_SAVE_ON:
                return true;
            default:
                return false;
        }
    }

    /**
        set compiled time to force-check triggered for compile 
    */
    public static void SetLatestSavedTime () {
        var path = assetBasePath + PREFERENCE_FILE_PATH;
        var paramDict = PreferenceDict(path);

        switch (paramDict[PREFERENCE_ITEM_COMPILE_ANYWAY]) {
            case PREFERENCE_PARAM_COMPILE_ANYWAY_ON:
                var triggetPath = assetBasePath + PREFERENCE_COMPILETRIGGER_PATH;

                // rewrite as file
                var commentedDateDescription = "//"+DateTime.Now.ToString();
                using (StreamWriter sw = new StreamWriter(triggetPath)) {
                    sw.WriteLine(commentedDateDescription);
                }
                break;
            case PREFERENCE_PARAM_COMPILE_ANYWAY_OFF:
                break;
        }
    }


    

    /**
        close connection.
    */
    public static void CloseConnection () {
        Assert(ws != null, "ws is null");

        if (ws.ReadyState == USSAWebSocketSharp.WsState.OPEN) ws.Close();

        UpdateConnectionStatus(PREFERENCE_PARAM_STATUS_NOTCONNECTED);
    }




    //////////////////////////////////////////////////////////////
    // connector
    //////////////////////////////////////////////////////////////

    public static USSAWebSocketSharp.WebSocket ws;
    public static TailLogThenSendToWebSocket tail;

    /**
        start connect to SublimeSocket
    */
    public static void StartConnect () {

        var paramDict = PreferenceDict(assetBasePath + PREFERENCE_FILE_PATH);
        // check if ws is exist and not ready for open.
        if (ws != null) {
            Assert(ws.ReadyState == USSAWebSocketSharp.WsState.CLOSED, "already connecting. ws.ReadyState:"+ws.ReadyState);
        }

        var serverPath = paramDict[PREFERENCE_ITEM_SERVER];
        Assert(0 < serverPath.Length, "serverPath is empty, server: "+serverPath);

        
        // set status to connecting
        UpdateConnectionStatus(PREFERENCE_PARAM_STATUS_CONNECTING);

        ws = new USSAWebSocketSharp.WebSocket(serverPath);

        ws.OnOpen += (sender, e) => {
            // version verify
            var versionVerifyDict = new Hashtable() {
                    {"socketVersion", SublimeTextVersion},
                    {"apiVersion", SublimeSocketApiVersion}
            };

            var verifyStr = USSAJson.USSAJsonConvert.SerializeObject(versionVerifyDict);
            ws.Send("ss@versionVerify:"+verifyStr);

            UpdateConnectionStatus(PREFERENCE_PARAM_STATUS_CONNECTED);
            UpdateConnectionError(PREFERENCE_PARAM_ERROR_NONE);

            var filterSourceStr = "ss@filterSettingLoadError";

            // load filterSourceStr from text file
            if (System.IO.File.Exists(assetBasePath + FILTERSOURCE_PATH)) {
                
                using (StreamReader sr = File.OpenText(assetBasePath + FILTERSOURCE_PATH)) {
                    filterSourceStr = "";
                    
                    var backet = "";
                    while ((backet = sr.ReadLine()) != null) {
                        // remove 4 space
                        backet = backet.Replace("    ", "");

                        // ignore commented line
                        if (backet.StartsWith("//")) {
                            
                        } else {
                            // remove line.
                            filterSourceStr += backet.Replace("\n", "");
                        }
                    }
                }
            }

            
            /*  
                initialize completion with push-method.                
            */
            InitCompletion(
                paramDict[PREFERENCE_ITEM_COMPLETION],
                message => {
                    try {
                        var messageData = USSAJson.USSAJsonConvert.SerializeObject(message);
                        ws.Send("ss@showAtLog:"+messageData);
                    } catch (Exception e1) {
                        Debug.LogError("SSA:failed to send message:"+e1);
                    }
                },
                completionData => {
                    try {
                        var completionCandidates = USSAJson.USSAJsonConvert.SerializeObject(completionData.data);
                        ws.Send("ss@runCompletion:"+completionCandidates);
                    } catch (Exception e2) {
                        Debug.LogError("SSA:failed to send completion:"+e2);
                    }
                }
            );


			Assert(System.IO.File.Exists(paramDict[PREFERENCE_ITEM_TARGET]), "observeTargetFilePath does not exist, target: "+paramDict[PREFERENCE_ITEM_TARGET] + ".\nPlease delete "+ ASSET_FOLDER_NAME +"/Preferences.txt once then choose Window > SublimeSocket > connect.");
			StartTailing(paramDict[PREFERENCE_ITEM_TARGET]);
            var filterSourceArray = filterSourceStr.Split(new [] {"ss@"}, StringSplitOptions.RemoveEmptyEntries);
            filterSourceArray.Select(
                        x => {
                            // input
                            ws.Send("ss@"+x);
                            return x;
                        }
                ).ToList();
        };

        ws.OnMessage += (sender, e) => {
            if (!String.IsNullOrEmpty(e.Data)) {
                

                if (e.Data == HEADER_SAVED) {
                    if (!ShouldCompileBySave()) return;

                	// update saveTrigger.cs
                    SetLatestSavedTime();

                    switch (Application.platform) {
                        case RuntimePlatform.OSXEditor:{
                            // ask "call SwitchApp" to SublimeSocket
                            var shellPath = SWITCHAPP_PATH;

                            var shellDict = new Hashtable() {
                                {"main", ""},
                                {"", shellPath},
                                {"-f", "com.unity3d.UnityEditor" + unityVersionNumber + ".x"},
                                {"-t", "com.sublimetext."+SublimeTextVersion}
                                // ,{"debug", "true"}
                            };
                            
                            var savedEvent = new Hashtable() {
                                {"target", "saved"},
                                {"event", "event_saved"}
                            };

                            var shellStr = USSAJson.USSAJsonConvert.SerializeObject(shellDict);
                            var savedEventStr = USSAJson.USSAJsonConvert.SerializeObject(savedEvent);

                            ws.Send("ss@runShell:"+shellStr+"->eventEmit:"+savedEventStr);
                            break;
                        }
                        case RuntimePlatform.WindowsEditor:{
                            var shellPath = SWITCHAPP_PATH.Replace("/", "\\\\");

                            var shellDict = new Hashtable() {
                                {"main", ""},
                                {"", shellPath+".exe"},
                                {"-f", "Unity"}
                                // ,{"debug", "true"}
                            };
                            
                            var shellStr = USSAJson.USSAJsonConvert.SerializeObject(shellDict);
                            ws.Send("ss@runShell:"+shellStr);

                            var shellDict2 = new Hashtable() {
                                {"main", ""},
                                {"", shellPath+".exe"},
                                {"-f", "sublime_text"},
                                {"delay", "100"}
                                // ,{"debug", "true"}
                            };
                            
                            shellStr = USSAJson.USSAJsonConvert.SerializeObject(shellDict2);
                            ws.Send("ss@runShell:"+shellStr);
                            break;
                        }
                    }

                    return;
                }

                if (e.Data.StartsWith(HEADER_COMPLETION)) {
                    if (completionCont == null) return;
					completionCont.IgniteCompletion(e.Data);
                    return;
				}

                if (e.Data.StartsWith(HEADER_SETTING)) {
                    if (completionCont == null) return;
                    completionCont.ApplyFileSetting(e.Data);
                    return;
                }

                // verification and refuse

				//verified case
				if (e.Data.StartsWith(HEADER_IDENTIFICATION_VERIFY)) {
					Debug.Log("SublimeSocket verified");
					return;
				} else if (e.Data.StartsWith(HEADER_IDENTIFICATION_VERIFY_UPDATABLE)) {
					Debug.Log("SublimeSocket verify result:"+e.Data);
					return;
				}

				//connection refused
				else if (e.Data.StartsWith(HEADER_REFUSED_SSUPDATE))  {
					Debug.LogError("SublimeSocket verify result:"+e.Data); 
					return;
				} else if (e.Data.StartsWith(HEADER_REFUSED_CLIENTUPDATE)) {
					Debug.LogError("SublimeSocket verify result:"+e.Data); 
					return;
				} else if (e.Data.StartsWith(HEADER_REFUSED_SSDIFFERENT)) {
					Debug.LogError("SublimeSocket verify result:"+e.Data); 
					return;
				}
            }
        };

        ws.OnError += (sender, e) => {
            UpdateConnectionStatus(PREFERENCE_PARAM_STATUS_NOTCONNECTED);
            switch (e.Message) {
                case "Connection refused":

                    Debug.LogWarning("SSA:error. reason:SublimeSocket returns no-signal. please check the SublimeText & SublimeSocket is running.");
                    UpdateConnectionError(PREFERENCE_PARAM_ERROR_CONNECTIONREFUSED);
                    break;

                case "The WebSocket frame can not be read from the network stream.":
                    Debug.LogWarning("SSA:error. reason:"+e.Message);
                    UpdateConnectionError(PREFERENCE_PARAM_ERROR_THEWEBSOCKETFRAMECANNOTBEREADFROMTHENETWORKSTREAM);
                    break;

                case "Thread was being aborted":
                    Debug.LogWarning("SSA:WebSocket Thread error. reason:"+e.Message);
                    UpdateConnectionError(PREFERENCE_PARAM_ERROR_THREADWASBEINGABORTED);
                    break;

                default:
                    Debug.LogWarning("SSA:other error. reason:"+e.Message);
                    break;
            }

            if (tail != null) tail.Dispose();
        };

        ws.OnClose += (sender, e) => {
            switch (e.Reason) {
                case "The WebSocket frame can not be read from the network stream.":
                    Debug.LogWarning("SSA:connection closed. reason:"+e.Reason);
                    break;
        		default:
					Debug.LogWarning("SSA:connection closed with unexpected reason: "+e.Reason);
					break;
            }
            
            UpdateConnectionStatus(PREFERENCE_PARAM_STATUS_NOTCONNECTED);

            if (tail != null) tail.Dispose();
        };

        // if WebPlayer, get policy before connect.
        if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.WebPlayer || 
            EditorUserBuildSettings.activeBuildTarget == BuildTarget.WebPlayerStreamed) {

            // try or die.
            var result = Security.PrefetchSocketPolicy(PREFERENCE_PARAM_DEFAULT_HOST, PREFERENCE_PARAM_DEFAULT_BYTESERVER_PORT);
            if (!result) {
                Debug.LogWarning("SSA:connection aborted. reason:cannot detect Socket Policy server for Web Player @"+PREFERENCE_PARAM_DEFAULT_HOST + ":" + PREFERENCE_PARAM_DEFAULT_BYTESERVER_PORT+", please setup SublimeSocket's ByteDataServer on SublimeSocket before running. see README");
                return;
            }
        }
        

        ws.Connect();
        
    }
    static List<string> Split(string str, int chunkSize) {
        return Enumerable.Range(0, (str.Length / chunkSize) + 1).Select(i => {
            var readableSize = str.Length - i * chunkSize;
            if (chunkSize < readableSize) {
                readableSize = chunkSize;
            }

            return str.Substring(i * chunkSize, readableSize);
        }).ToList();
    }

static void StartTailing (string observeTargetFilePath) {
		// start tail-observing of Log
		tail = new TailLogThenSendToWebSocket(
			new FileInfo(observeTargetFilePath),
			Encoding.UTF8,
			message => {
				if (ws == null) return;
				if (ws.ReadyState != USSAWebSocketSharp.WsState.OPEN) return;

				var sendableSize = (int)(USSAWebSocketSharp.WebSocket._fragmentLen * 0.8);
                
                message = message.Replace("->", "'-''>'").Replace("//", "'/''/'");

				if (message.Length < sendableSize) {
                    var filterDict = new Hashtable() {
						{"name", "unity"},
						{"source", message}
						// ,{"debug", true}
					};

					// back \n for multiline matching.
					var filterStr = USSAJson.USSAJsonConvert.SerializeObject(filterDict).Replace("\\n", "\n");
					
					if (!ws.Send("ss@filtering:"+filterStr)) {
						ws.Send("ss@showAtLog:{\"message\":\"failed to send message from SSA to SS.\"}");
					} 
				} else {
                   // too long, should split.
					foreach (string splitted in Split(message, sendableSize)) {
						
						var filterDict = new Hashtable() {
							{"name", "unity"},
							{"source", splitted}
							// ,{"debug", true}
						};

						// back \n for multiline matching.
						var filterStr = USSAJson.USSAJsonConvert.SerializeObject(filterDict).Replace("\\n", "\n");
						
						if (!ws.Send("ss@filtering:"+filterStr)) {
							ws.Send("ss@showAtLog:{\"message\":\"failed to send message from SSA to SS.\"}");
						} 
					}
				}
			}
		);
	}


	static void Assert(bool condition, string message) {
		if (!condition) throw new Exception(message);
	}
}

/**
    tail class
*/
public class TailLogThenSendToWebSocket : IDisposable {
    private readonly Stream _fileStream;
    private readonly Timer _timer;

    public static readonly int TAIL_INTERVAL = 500;
    public static readonly int BUFFER_SIZE = 10240;

    public TailLogThenSendToWebSocket(
        FileInfo file,
        Encoding encoding,
        Action<string> FileChanged) {
            
        _fileStream = new FileStream(
            file.FullName,
            FileMode.Open,
            FileAccess.Read,
            FileShare.ReadWrite
        );

        //set to the end of file.
        _fileStream.Seek(0, SeekOrigin.End);
        
        _timer = new Timer(o => CheckForUpdate(encoding, FileChanged), null, 0, TAIL_INTERVAL);
    }



    private void CheckForUpdate(Encoding encoding, Action<string> FileChanged) {
        var tailedStringBuf = new StringBuilder();

        int read;
        var b = new byte[BUFFER_SIZE];

        while ((read = _fileStream.Read(b, 0, b.Length)) > 0) {
            tailedStringBuf.Append(encoding.GetString(b, 0, read));
        }

        if (tailedStringBuf.Length > 0) {
            FileChanged(tailedStringBuf.ToString());
        } else {
            _fileStream.Seek(0, SeekOrigin.End);
        }
    }

    public void Dispose() {
        _timer.Dispose();
        _fileStream.Dispose();
    }

}


